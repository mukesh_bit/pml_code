package com.pick.mylaundry;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;

import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


public class SupportFragment extends Fragment implements OnClickListener {
	private LinearLayout email,call;
	private TextView cus_title;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState)  {
		super.onCreate(savedInstanceState);
		View rootView = inflater.inflate(R.layout.support, container, false);
		String fontPath = "font/helvetica-normal.ttf";
		Typeface tf = Typeface.createFromAsset(getActivity().getAssets(),fontPath);
		cus_title=(TextView)getActivity().findViewById(R.id.toolbar_title);
		TextView support_email_text=(TextView)rootView.findViewById(R.id.support_email_text);
		support_email_text.setTypeface(tf);
		TextView support_call_text=(TextView)rootView.findViewById(R.id.support_call_text);
		support_call_text.setTypeface(tf);
		cus_title.setText("SUPPORT");
		cus_title.setTypeface(tf);
		email=(LinearLayout) rootView.findViewById(R.id.support_email);
		call=(LinearLayout) rootView.findViewById(R.id.support_call);
		email.setOnClickListener(this);
		call.setOnClickListener(this);
		return rootView;
	}
	@Override
	public void onClick(View arg0) {
		switch (arg0.getId()){
			case R.id.support_email :
				sendmail();
				break;
			case R.id.support_call:
				makecall();
				break;
			default:
				break;
		}
	}

	private void makecall() {
		try {
			Intent intent = new Intent(Intent.ACTION_DIAL);
			intent.setData(Uri.parse("tel:9971522720"));
			startActivity(intent);

		} catch (ActivityNotFoundException activityException) {
			Log.e("Calling a Phone Number", "Call failed", activityException);
		}
	}
	private void sendmail() {
		String[] CC = {""};
	  	Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto","orders@pickmylaundry.in", null));
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "User query");
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT," ");
		startActivity(Intent.createChooser(emailIntent, "Send email..."));
		try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            }
		catch (ActivityNotFoundException ex) {
			Toast.makeText(getActivity(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
		}
 	}
	public void onResume(){
		super.onResume();
		 TextView cus_title=(TextView)getActivity().findViewById(R.id.toolbar_title);
		 cus_title.setText("SUPPORT");
	}
}
