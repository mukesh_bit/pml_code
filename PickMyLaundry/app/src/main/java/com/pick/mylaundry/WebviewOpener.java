package com.pick.mylaundry;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.pick.utill.InternalBrowser;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by 121 on 10/26/2016.
 */
public class WebviewOpener extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq_activity);

        TextView title = (TextView) findViewById(R.id.actionbar_custom_title);
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.pB);
        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_layout);

        ImageView back_iv = (ImageView) findViewById(R.id.back_icon);
        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        WebView webView = (WebView) findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if(progress < 100 && progressBar.getVisibility() == ProgressBar.GONE){
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                }

                progressBar.setProgress(progress);
                if(progress == 100) {
                    progressBar.setVisibility(ProgressBar.GONE);
                    frameLayout.setVisibility(View.GONE);
                }
            }
        });

        Bundle bundle = getIntent().getExtras();
        try {
            JSONObject dataObject = new JSONObject(bundle.getString("uri_data"));
            title.setText(dataObject.getString("title"));
            webView.loadUrl(dataObject.getString("link"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
