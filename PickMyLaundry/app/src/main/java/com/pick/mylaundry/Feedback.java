package com.pick.mylaundry;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 121 on 12/7/2015.
 */
public class Feedback extends Activity{
    private TextView title;
    private View rootView;
    private TextView submit;
    private RatingBar pickup_star, processing_star;
    private float feedback_pickup, feedback_processing;
    private TextView so, amount, pickup_date, pickup_time, delivery_date, delivery_time,comment_tv;
    private String salesorder_no;
    private Spinner spinner;
    private TextInputLayout comment;
    private boolean isSpinnerEnabled = false;
    private LinearLayout rating_reason;
    private ArrayAdapter<String> dataAdapter;

    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.feedback);
        title = (TextView) findViewById(R.id.toolbar_title);
        title.setVisibility(View.VISIBLE);
        title.setText("Feedback");

        //get field view
        submit = (TextView) findViewById(R.id.submit_feedback);
        pickup_star = (RatingBar) findViewById(R.id.ratingBar_pickup);
        processing_star = (RatingBar) findViewById(R.id.ratingBar_processing);
        so = (TextView) findViewById(R.id.so);
        amount = (TextView) findViewById(R.id.amountValue);
        pickup_date = (TextView) findViewById(R.id.pickup_date);
        pickup_time = (TextView) findViewById(R.id.pickup_time);
        delivery_date = (TextView) findViewById(R.id.delivery_date);
        delivery_time = (TextView) findViewById(R.id.delivery_time);
        spinner = (Spinner) findViewById(R.id.spinner_feedback);
        rating_reason = (LinearLayout) findViewById(R.id.rating_reason);
        comment = (TextInputLayout) findViewById(R.id.input_layout_comment);
        comment_tv = (TextView) findViewById(R.id.input_comment);

        //get intent data
        Bundle bundle = getIntent().getExtras();
        salesorder_no = bundle.getString("so");
        so.setText(salesorder_no);
        amount.setText(bundle.getString("amount"));
        pickup_date.setText(bundle.getString("pickup_date"));
        pickup_time.setText(bundle.getString("pickup_time"));
        delivery_date.setText(bundle.getString("delivery_date"));
        delivery_time.setText(bundle.getString("delivery_time"));

        //spinner set
        // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Select a Reason...");
        categories.add("Stains not removed");
        categories.add("Ironing not proper");
        categories.add("Color Bleed");
        categories.add("High Price");
        categories.add("Cloth Torn/Damaged");
        categories.add("Bad Customer Support");
        categories.add("Pickup/Delivery Delayed");

        // Creating adapter for spinner
        dataAdapter =  seting_adpter(categories);

        // Drop down layout style - list view with radio button

        dataAdapter.setDropDownViewResource(R.layout.reason_item);

        // attaching data adapter to spinner
        spinner.setAdapter(dataAdapter);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                feedback_pickup = pickup_star.getRating();
                feedback_processing = processing_star.getRating();
                if (feedback_pickup > 0 && feedback_processing > 0) {
                    if(isSpinnerEnabled && spinner.getSelectedItem().toString().equals("Select a Reason...")){
                        Toast.makeText(getApplicationContext(), "Please select a Reason", Toast.LENGTH_LONG).show();
                    }
                    else {
                        saveFeedBack();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Please share your complete feedback.", Toast.LENGTH_LONG).show();
                }
            }
        });
        pickup_star.setOnRatingBarChangeListener(listener);
        processing_star.setOnRatingBarChangeListener(listener);

    }
    RatingBar.OnRatingBarChangeListener listener = new RatingBar.OnRatingBarChangeListener() {
        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
            float pickup_rating = pickup_star.getRating();
            float processing_rating = processing_star.getRating();
            if(pickup_rating > 0 && processing_rating > 0 && (processing_rating <= 3 || pickup_rating <= 3)){
                spinner.setVisibility(View.VISIBLE);
                rating_reason.setVisibility(View.VISIBLE);
                isSpinnerEnabled = true;
            }
            else{
                spinner.setVisibility(View.GONE);
                rating_reason.setVisibility(View.GONE);
                isSpinnerEnabled = false;
            }
        }
    };
    private void saveFeedBack(){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(Feedback.this)));
        params.add(new BasicNameValuePair("salesorder_no", salesorder_no));
        params.add(new BasicNameValuePair("pickup_feedback", feedback_pickup + ""));
        params.add(new BasicNameValuePair("processing_feedback", feedback_processing + ""));
        if(isSpinnerEnabled){
            params.add(new BasicNameValuePair("reason", spinner.getSelectedItem().toString()));
            params.add(new BasicNameValuePair("comment", comment_tv.getText().toString()));
        }
        saveFeedbackData(params);
    }
    private void saveFeedbackData(List<NameValuePair> params) {
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")) {
                        Toast.makeText(getApplicationContext(), "Thanks for your feedback.", Toast.LENGTH_LONG).show();
                        SharedPreferences pref = getSharedPreferences("rate_app", Context.MODE_PRIVATE);
                        boolean isRated = pref.getBoolean("isRated", false);
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        float avg_rating = (float)((pickup_star.getRating() + processing_star.getRating()) / 2);
                        if(avg_rating>=4&&!isRated){
                            intent.putExtra("isFeedbackDialogVisible", true);
                            intent.putExtra("SESSION_ID", "0");
                            intent.putExtra("id",0);
                        }
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);
                        finish();
                        Map<String, String> articleParams = new HashMap<String, String>();
                        articleParams.put("mobile", AppPrefrence.getMobile(Feedback.this));
                        //up to 10 params can be logged with each event
                        FlurryAgent.logEvent("Feedback Submitted", articleParams);
                    }
                    else{
                        Toast.makeText(getApplicationContext(), "Sorry, There was some problem in submiting your feedback. Please try again.", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e){
                    Toast.makeText(getApplicationContext(), "Sorry, There was some problem in submiting your feedback. Please try again.", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void taskException() {
                //Toast.makeText(getActivity().getApplicationContext(),"Please check Your Internet Connection",Toast.LENGTH_LONG).show();

            }
        },Feedback.this, Constant.SAVEFEEDBACK_URL,params, true);

        task.execute();
    }
    private ArrayAdapter<String> seting_adpter(List<String> categories){
        // Initializing an ArrayAdapter
        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,android.R.layout.simple_spinner_item, categories){
            @Override
            public boolean isEnabled(int position){
                if(position == 0) {
                    return false;
                }
                else {
                    return true;
                }
            }
            @Override
            public View getDropDownView(int position, View convertView,
                                        ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                CheckedTextView tv = (CheckedTextView) view;
                if(tv.getText().toString().equals("Select a Reason...")){
                    // Set the hint text color gray
                    tv.setTextColor(Color.GRAY);
                    tv.setCheckMarkDrawable(null);
                }
                else {
                    tv.setTextColor(Color.BLACK);
                    if(spinner.getSelectedItem().toString().equals(tv.getText().toString())){
                        tv.setCheckMarkDrawable(R.drawable.rb);
                    }
                }
                return view;
            }
        };
        return spinnerArrayAdapter;
    }
    @Override
    protected void onStart() {
        super.onStart();
        //initialize flurry
        FlurryAgent.setLogEnabled(false);
        // init Flurry
        FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
        FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

}
