package in.pickmylaundry.vendorapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.vendorapp.Utils.Constant;
import in.pickmylaundry.vendorapp.Utils.UploadTask;

/**
 * Created by 121 on 2/4/2016.
 */
public class LoginActivity extends Activity {
    private EditText user_name, password;
    private Button submit;
    private UploadTask task;
    private Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        user_name = (EditText) findViewById(R.id.id);
        password = (EditText) findViewById(R.id.pass);
        submit = (Button) findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginRequest();
            }
        });
    }
    public void loginRequest(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("user_name", user_name.getText().toString());
        params.put("password", password.getText().toString());
        sendLoginRequest(params);
    }
    private void sendLoginRequest(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){
                        SharedPreferences prefs = getSharedPreferences("login_status", MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("vendor_name", response.getString("vendor_name"));
                        editor.putBoolean("isLoggedIn", true);
                        editor.commit();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(getApplicationContext(), response.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(getApplicationContext(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, LoginActivity.this, Constant.LOGIN, params);
        task.execute();
    }
}

