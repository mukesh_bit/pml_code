package in.pickmylaundry.riderapp.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by 121 on 5/12/2016.
 */
public class AppPrefrence {
    private static String MobileNUmber = "id";
    private static String Phone = "phone";
    private static String Name = "name";


    public static String getName(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Name, "");
    }

    public static void setName(Context context, String name) {
        SharedPreferences _sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = _sharedPreferences.edit();
        editor.putString(Name, name);
        editor.commit();
    }

    public static String getPhone(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(Phone, "");
    }

    public static  void setPhone(Context context, String phone) {
        SharedPreferences _sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = _sharedPreferences.edit();
        editor.putString(Phone, phone);
        editor.commit();
    }

    public static void setNumber(Context context, String mobile) {
        SharedPreferences _sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = _sharedPreferences.edit();
        editor.putString(MobileNUmber, mobile);
        editor.commit();
    }

    public static String getMobile(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(MobileNUmber, "");
    }

}
