package in.pickmylaundry.vendorapp;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import in.pickmylaundry.vendorapp.Utils.Constant;
import in.pickmylaundry.vendorapp.Utils.EmptyUtility;
import in.pickmylaundry.vendorapp.adapter.ImageAdapter;

/**
 * Created by 121 on 1/31/2016.
 */
public class AddImageActivity extends Activity {
    private LinearLayout container;
    private View[] image_item = new View[15];//max 15 items can be added
    private int last_index = 0;
    private int current_image = -1;
    private ImageAdapter[] grid_adapter = new ImageAdapter[15];
    private ArrayList<ArrayList<String>> image_path = new ArrayList<ArrayList<String>>();
    private Button add_product;
    private static final int REQ_IM_CAP = 1;
    private static final int SELECT_FILE = 2;
    private String[] products;
    private int val;
    private ArrayAdapter<String> adapter_productlist;
    private String so_no;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private String mCurrentPhotoPath;
    private String vendor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_image_activity);

        //get intent data
        Bundle extras = getIntent().getExtras();
        SharedPreferences prefs = getSharedPreferences("login_status", MODE_PRIVATE);
        vendor = prefs.getString("vendor_name", "");

        //set autocomplete
        products = extras.getStringArray("products");
        adapter_productlist = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, products);

        //set name and so
        TextView name = (TextView) findViewById(R.id.name);
        TextView so = (TextView) findViewById(R.id.so);
        name.setText(extras.getString("name"));
        so_no = extras.getString("so");
        so.setText(so_no);

        //add product view listener
        container = (LinearLayout) findViewById(R.id.addImages);
        addView();
        add_product = (Button) findViewById(R.id.add_product);

        //back button listener
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddImageActivity.this.onBackPressed();
            }
        });
        setPermissions();
    }
    private void addView() {
        image_item[last_index] = (RelativeLayout) getLayoutInflater().inflate(R.layout.add_image_item, container, false);
        image_path.add(new ArrayList<String>());
        grid_adapter[last_index] = new ImageAdapter(getApplicationContext(), image_path.get(last_index), "from_bitmap");
        ExpandableHeightGridView gridView = (ExpandableHeightGridView) image_item[last_index].findViewById(R.id.grid);
        gridView.setAdapter(grid_adapter[last_index]);
        gridView.setExpanded(true);
        Button add_image = (Button) image_item[last_index].findViewById(R.id.add_image);
        Button save_image = (Button) image_item[last_index].findViewById(R.id.save);
        setClickListener(last_index, add_image, save_image);
        container.addView(image_item[last_index]);
        AutoCompleteTextView product_actv = (AutoCompleteTextView) image_item[last_index].findViewById(R.id.products);
        product_actv.setAdapter(adapter_productlist);
        last_index++;
    }

    private void setClickListener(final int val, Button add_image, Button save_image) {
        add_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("index", "" + val + "" + last_index);
                current_image = val;
                captureImageCameraOrGallery();
            }
        });
        save_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                current_image = val;
                String product = ((TextView) image_item[val].findViewById(R.id.products)).getText().toString();
                String desc = ((TextView) image_item[val].findViewById(R.id.description)).getText().toString();
                if (EmptyUtility.isEmpty(product))
                    Toast.makeText(getApplicationContext(), "Please select a product", Toast.LENGTH_LONG).show();
                else if (!Arrays.asList(products).contains(product))
                    Toast.makeText(getApplicationContext(), "Please select a product from dropdown", Toast.LENGTH_LONG).show();
                else if (EmptyUtility.isEmpty(desc))
                    Toast.makeText(getApplicationContext(), "Please add a description", Toast.LENGTH_LONG).show();
                else if (image_path.get(val).size() == 0)
                    Toast.makeText(getApplicationContext(), "Please add atleast one image", Toast.LENGTH_LONG).show();
                else
                    new UploadFileToServer(val).execute();
            }
        });
    }



    public void captureImageCameraOrGallery() {

        final CharSequence[] options = { "Take photo", "Choose from library",
                "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(AddImageActivity.this);

        builder.setTitle("Select");

        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                if (options[which].equals("Take photo")) {
                    Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    // Ensure that there's a camera activity to handle the intent
                    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                        // Create the File where the photo should go
                        File photoFile = null;
                        try {
                            photoFile = createImageFile();
                        } catch (IOException ex) {
                            // Error occurred while creating the File
                        }
                        // Continue only if the File was successfully created
                        if (photoFile != null) {
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                    Uri.fromFile(photoFile));
                            startActivityForResult(takePictureIntent, REQ_IM_CAP);
                        }
                    }
                } else if (options[which].equals("Choose from library")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (options[which].equals("Cancel")) {
                    dialog.dismiss();
                }

            }
        });
        builder.show();
    }
    private void setPermissions(){
        if (Build.VERSION.SDK_INT >= 23){
// Here, thisActivity is the current activity
            if (ContextCompat.checkSelfPermission(AddImageActivity.this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(AddImageActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)
                    != PackageManager.PERMISSION_GRANTED){
                if (ActivityCompat.shouldShowRequestPermissionRationale(AddImageActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                } else {

                    ActivityCompat.requestPermissions(AddImageActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},
                            MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
            }
        }
        else{
            addView();
        }
        add_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addView();
            }
        });
    }
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    addView();
                    add_product.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            addView();
                        }
                    });

                } else {
                    AddImageActivity.this.onBackPressed();
                }
                return;
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        File finalFile = null;
        String provider = "com.android.providers.media.MediaProvider";
        if(resultCode == RESULT_OK&&requestCode==REQ_IM_CAP){
            image_path.get(current_image).add(mCurrentPhotoPath);
        }
        else if(resultCode == RESULT_OK&&requestCode==SELECT_FILE){
            String realPath;
            // grant all three uri permissions!
            grantUriPermission(provider, data.getData(), Intent.FLAG_GRANT_READ_URI_PERMISSION);
            Uri uri = data.getData();
            InputStream is = null;
            if (uri.getAuthority() != null) {
                try {
                    is = getContentResolver().openInputStream(uri);
                    Bitmap bmp = BitmapFactory.decodeStream(is);
                    uri =  writeToTempImageAndGetPathUri(getApplicationContext(), bmp);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }finally {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            finalFile = new File(getRealPathFromURI(uri));
            Log.e("file", finalFile.toString());
            image_path.get(current_image).add(finalFile.toString());
        }
        grid_adapter[current_image].notifyDataSetChanged();

    }
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }
    public Uri getImageUri(Context inContext, Bitmap inImage) {
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    public static Uri writeToTempImageAndGetPathUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    public String getRealPathFromURI(Uri uri) {
        Log.e("uri", uri.toString());
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, proj, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        private int index;
        private Button save_button;
        public UploadFileToServer(int index){
            this.index = index;
            save_button = (Button) image_item[index].findViewById(R.id.save);
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            save_button.setEnabled(false);
            save_button.setBackgroundColor(getResources().getColor(R.color.grey));
            save_button.setText("Saving...");
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(Constant.UPLOAD_IMAGE);

            try {
                AndroidMultipartEntity entity = new AndroidMultipartEntity(
                        new AndroidMultipartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                            }
                        });

                ArrayList<String> image_path_list = image_path.get(index);
                for(int i=0; i<image_path_list.size();i++){
                    Bitmap bmp = BitmapFactory.decodeFile(image_path_list.get(i));
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    bmp.compress(Bitmap.CompressFormat.JPEG, 15, bos);
                    InputStream in = new ByteArrayInputStream(bos.toByteArray());
                    ContentBody foto = new InputStreamBody(in, "image/jpeg", "filename");
                    //File sourceFile = new File(image_path_list.get(i));
                    entity.addPart("image"+i, foto);
                }
                // Extra parameters if you want to pass to server
                entity.addPart("vendor", new StringBody(vendor));
                entity.addPart("so", new StringBody(so_no));
                String product = ((TextView) image_item[index].findViewById(R.id.products)).getText().toString();
                entity.addPart("product", new StringBody(product));
                String desc = ((TextView) image_item[index].findViewById(R.id.description)).getText().toString();
                entity.addPart("description", new StringBody(desc));
                entity.addPart("image_count", new StringBody(image_path_list.size()+""));

                long totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("MainActivity", "Response from server: " + result);
            super.onPostExecute(result);
            save_button.setEnabled(true);
            save_button.setBackgroundColor(Color.parseColor("#0087c1"));
            save_button.setText("Saved");
        }

    }

}
