package com.pick.mylaundry;

import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class WhyusFragment extends Fragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		View rootview=inflater.inflate(R.layout.whyus,container,false);
		return rootview;
	}
    
	public void onResume(){
	    super.onResume();
	   	TextView cus_title=(TextView)getActivity().findViewById(R.id.toolbar_title);
		cus_title.setText("ABOUT");
	} 
  

}