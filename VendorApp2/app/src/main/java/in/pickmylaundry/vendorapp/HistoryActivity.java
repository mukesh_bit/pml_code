package in.pickmylaundry.vendorapp;

import android.app.DialogFragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.vendorapp.Utils.Constant;
import in.pickmylaundry.vendorapp.Utils.UploadTask;
import in.pickmylaundry.vendorapp.adapter.HistoryAdapter;
import in.pickmylaundry.vendorapp.model.HistoryModel;

/**
 * Created by 121 on 2/1/2016.
 */
public class HistoryActivity extends DialogFragment {
    private ListView history_list;
    private UploadTask task;
    private ArrayList<HistoryModel> history_list_data;
    private Button pending_btn, closed_btn, response_btn;
    private String so_status = "pending";
    private static Context context;
    private HistoryAdapter.RefreshListData refreshListData;
    private String vendor;
    private View rootView;
    private Bundle bundle;
    private int id=0, scrolling_item;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_history, container, false);

        SharedPreferences prefs = getActivity().getSharedPreferences("login_status", getActivity().MODE_PRIVATE);
        vendor = prefs.getString("vendor_name", "");
        history_list = (ListView) rootView.findViewById(R.id.history_list);
        pending_btn = (Button) rootView.findViewById(R.id.pending);
        response_btn = (Button) rootView.findViewById(R.id.response);
        closed_btn = (Button) rootView.findViewById(R.id.closed);
        pending_btn.setOnClickListener(listener);
        closed_btn.setOnClickListener(listener);
        response_btn.setOnClickListener(listener);

        //get extras
        bundle = getArguments();
        if(bundle!=null){
            so_status = bundle.getString("status");
            id = Integer.parseInt(bundle.getString("item_id"));
            resetHeader();
            Log.i("status", so_status);
            Button tv_header = (Button) rootView.findViewWithTag(so_status);
            tv_header.setTextColor(getResources().getColor(R.color.white));
            tv_header.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }

        setInterfaceMethods();
        requestForHistory();
        context = getActivity().getApplicationContext();
        return rootView;
    }
    public View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            resetHeader();

            //active clicked button
            TextView clicked_btn = (TextView) rootView.findViewById(v.getId());
            so_status = clicked_btn.getText().toString();
            clicked_btn.setTextColor(getResources().getColor(R.color.white));
            clicked_btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            requestForHistory();
        }
    };
    private void resetHeader(){
        pending_btn.setBackgroundColor(getResources().getColor(R.color.grey));
        closed_btn.setBackgroundColor(getResources().getColor(R.color.grey));
        response_btn.setBackgroundColor(getResources().getColor(R.color.grey));
        pending_btn.setTextColor(getResources().getColor(R.color.black));
        closed_btn.setTextColor(getResources().getColor(R.color.black));
        response_btn.setTextColor(getResources().getColor(R.color.black));
    }
    private void setInterfaceMethods(){
        refreshListData = new HistoryAdapter.RefreshListData() {
            @Override
            public void refresh() {
                requestForHistory();
            }
        };
    }
    public void requestForHistory(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("vendor", vendor);
        params.put("status", so_status);
        history_list_data = new ArrayList<HistoryModel>();
        getHistoryList(params);
    }
    private void getHistoryList(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){
                        JSONArray data = response.getJSONArray("data");
                        for(int i=0;i<data.length();i++) {
                            JSONObject obj = data.getJSONObject(i);
                            HistoryModel product = new HistoryModel();
                            product.setName(obj.getString("name"));
                            product.setDescription(obj.getString("description"));
                            product.setSo(obj.getString("so"));
                            product.setCount(obj.getInt("count"));
                            product.setStatus(obj.getString("status"));
                            product.setProduct(obj.getString("product"));
                            product.setStart(obj.getInt("start"));
                            product.setId(obj.getInt("id"));
                            product.setAcceptingStatus(obj.getInt("isAccepted"));

                            //scroll to list item
                            if(obj.getInt("id") == id){
                                scrolling_item = i;
                                product.setActive(true);
                            }
                            else
                                product.setActive(false);

                            history_list_data.add(product);
                        }
                        HistoryAdapter adapter = new HistoryAdapter(getActivity(), history_list_data, refreshListData);
                        history_list.setAdapter(adapter);
                        ((ListView) history_list).deferNotifyDataSetChanged();
                        if(scrolling_item>0){
                            history_list.setSelection(scrolling_item);
                            scrolling_item = 0;
                            id =0;
                        }
                    }
                    else{
                        Toast.makeText(getActivity().getApplicationContext(), response.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, getActivity(), Constant.GET_HISTORY, params);
        task.execute();
    }


}
