package com.pick.mylaundry;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.pick.navAdapter.FeaturesAdapter;
import com.pick.navmodel.FeatureItemModel;
import com.pick.utill.Constant;
import com.pick.volley.ImageLoader;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Mukesh on 11/10/16.
 */

public class ViewFeatures extends AppCompatActivity implements ViewPager.OnPageChangeListener {

    private List<String> titles = new ArrayList<>();
    private LinearLayout pager_indicator;
    private RelativeLayout toolbar_rl;
    private TabLayout tabLayout;
    private static ArrayList<String>  points = new ArrayList<>();
    private static String cCode = "\u25CF";
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private String[] tab_titles = {"Wash & Fold", "Wash & Iron", "Premium Laundry", "Dry Clean"};
    private int[] tab_images = {R.drawable.wf_fe, R.drawable.wi_fe, R.drawable.pl_fe, R.drawable.dc_fe};
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        setContentView(R.layout.view_features_activity);
        TextView toolbar_title = (TextView) findViewById(R.id.actionbar_custom_title);
        toolbar_rl = (RelativeLayout) findViewById(R.id.toolbar_rl);
        toolbar_title.setText("Features");

        ImageView back_icon = (ImageView) findViewById(R.id.back_icon);
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViewFeatures.super.onBackPressed();
            }
        });


        tabLayout = (TabLayout) findViewById(R.id.tabs);
        get_feature_data();

    }

    private void get_feature_data(){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        UploadTask uploadTask = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if(jsonObject.getString("success").equals("true")){
                        JSONObject data = jsonObject.getJSONObject("data");
                        HashMap<String, ArrayList<FeatureItemModel>> feature_data = new HashMap<String, ArrayList<FeatureItemModel>>();
                        HashMap<String, ArrayList<String>> keypoints_data = new HashMap<String, ArrayList<String>>();
                        for(int i=0; i<tab_titles.length;i++){
                            JSONObject category_data = data.getJSONObject(tab_titles[i]);
                            ArrayList<FeatureItemModel> modelArrayList = new ArrayList<FeatureItemModel>();
                            for (int j=0; j<category_data.getJSONArray("features").length();j++){
                                JSONObject feature_item = category_data.getJSONArray("features").getJSONObject(j);
                                FeatureItemModel itemModel = new FeatureItemModel();
                                itemModel.setIcon(feature_item.getString("icon"));
                                itemModel.setDesc(feature_item.getString("subtitle"));
                                itemModel.setHeading(feature_item.getString("title"));
                                modelArrayList.add(itemModel);
                            }
                            feature_data.put(tab_titles[i], modelArrayList);
                            ArrayList<String> list = new ArrayList<String>();
                            for (int j=0; j<category_data.getJSONArray("points").length();j++) {
                                list.add(category_data.getJSONArray("points").getString(j));
                            }
                            keypoints_data.put(tab_titles[i], list);
                        }
                        setup_adapter(feature_data, keypoints_data);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ViewFeatures.this, "Please check your internet connection", Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void taskException() {
                Toast.makeText(ViewFeatures.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
            }
        }, ViewFeatures.this, Constant.FEATURES, params, true);
        uploadTask.execute();
    }
    private void setup_adapter(HashMap<String, ArrayList<FeatureItemModel>> feature_data, HashMap<String, ArrayList<String>> keypoints_data){
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), feature_data, keypoints_data);
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setPageTransformer(false, new ParallaxPageTransformer());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(this);
        tabLayout.setupWithViewPager(mViewPager);
    }


    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        float[] from = new float[3],
                to =   new float[3];
        switch (position){
            case 0:
                Color.colorToHSV(Color.parseColor(getResources().getString(R.string.colorPrimary1)), from);
                Color.colorToHSV(Color.parseColor(getResources().getString(R.string.colorPrimary2)), to);
                break;
            case 1:
                Color.colorToHSV(Color.parseColor(getResources().getString(R.string.colorPrimary2)), from);
                Color.colorToHSV(Color.parseColor(getResources().getString(R.string.colorPrimary3)), to);
                break;
            case 2:
                Color.colorToHSV(Color.parseColor(getResources().getString(R.string.colorPrimary3)), from);
                Color.colorToHSV(Color.parseColor(getResources().getString(R.string.colorPrimary4)), to);
                break;
            case 3:
                Color.colorToHSV(Color.parseColor(getResources().getString(R.string.colorPrimary4)), from);
                Color.colorToHSV(Color.parseColor(getResources().getString(R.string.colorPrimary4)), to);
                break;
        };
        float[] hsv  = new float[3];
        hsv[0] = from[0] + (to[0] - from[0])*positionOffset;
        hsv[1] = from[1] + (to[1] - from[1])*positionOffset;
        hsv[2] = from[2] + (to[2] - from[2])*positionOffset;
        changeAllColors(Color.HSVToColor(hsv));
    }

    private void changeAllColors(int color) {
        toolbar_rl.setBackgroundColor(color);
        tabLayout.setBackgroundColor(color);
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_POS = "section_pos";
        private static final String ARG_SECTION_TITLE = "section_title";
        private static final String ARG_SECTION_IMAGE = "section_image";
        private static HashMap<String, ArrayList<FeatureItemModel>> feature_data_map;
        private static HashMap<String, ArrayList<String>> keypoints_data_map;
        private ImageLoader imageLoader;

        public PlaceholderFragment() {

        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(String title, int image, int position, HashMap<String, ArrayList<FeatureItemModel>> feature_data, HashMap<String, ArrayList<String>> keypoints_data) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_POS, position);
            args.putString(ARG_SECTION_TITLE, title);
            args.putInt(ARG_SECTION_IMAGE, image);
            fragment.setArguments(args);
            feature_data_map = feature_data;
            keypoints_data_map = keypoints_data;
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.view_features_fragment, container, false);
            CollapsingToolbarLayout ctl = (CollapsingToolbarLayout) rootView.findViewById(R.id.ctl);
            TextView tvBenefits = (TextView) rootView.findViewById(R.id.tv_benefits);
            ImageView ivBenefits = (ImageView) rootView.findViewById(R.id.iv_benefits);
            int image = getArguments().getInt(ARG_SECTION_IMAGE);
            int pos = getArguments().getInt(ARG_SECTION_POS);
            String title = getArguments().getString(ARG_SECTION_TITLE);
            int color;
            switch(pos){
                case 0:
                    color = getResources().getColor(R.color.colorPrimary1) ;
                    break;
                case 1:
                    color =  getResources().getColor( R.color.colorPrimary2) ;
                    break;
                case 2:
                    color =  getResources().getColor(R.color.colorPrimary3) ;
                    break;
                case 3:
                    color =  getResources().getColor(R.color.colorPrimary4) ;
                    break;
                default:
                    color =  getResources().getColor(R.color.colorPrimary1) ;
                    break;
            }
            ctl.setContentScrimColor(color);
            tvBenefits.setTextColor(color);
            ivBenefits.setColorFilter(color);
            ImageView scrollableImage = (ImageView) rootView.findViewById(R.id.iv_feature);
            scrollableImage.setImageDrawable(ContextCompat.getDrawable(getActivity(), image));

            //setting recyclerView features
            RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.rv_items);
            GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
            FeaturesAdapter dataAdapter = new FeaturesAdapter(getActivity(),feature_data_map.get(title),null, 0, color);
            recyclerView.setAdapter(dataAdapter);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.setNestedScrollingEnabled(false);
            ViewGroup.LayoutParams layoutParams =  recyclerView.getLayoutParams();
            layoutParams.height = (int)Constant.convertDpToPixel((float) (Math.ceil((keypoints_data_map.get(title).size()+1)/2) * 95), getActivity());
            recyclerView.setLayoutParams(layoutParams);
            recyclerView.requestLayout();
            //recyclerView points
            RecyclerView recyclerViewPoints = (RecyclerView) rootView.findViewById(R.id.rv_items_points);
            LinearLayoutManager llm = new LinearLayoutManager(getActivity());
            recyclerViewPoints.setAdapter(new FeaturesAdapter(getActivity(), null, keypoints_data_map.get(title), 1, color ));
            recyclerViewPoints.setLayoutManager(llm);
            recyclerViewPoints.setNestedScrollingEnabled(false);
            return rootView;
        }
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        private JSONObject jsonObject;
        private HashMap<String, ArrayList<FeatureItemModel>> feature_data;
        private HashMap<String, ArrayList<String>> keypoints_data;

        public SectionsPagerAdapter(FragmentManager fm, HashMap<String, ArrayList<FeatureItemModel>> feature_data, HashMap<String, ArrayList<String>> keypoints_data) {
            super(fm);
            this.feature_data = feature_data;
            this.keypoints_data = keypoints_data;
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(tab_titles[position], tab_images[position], position, feature_data, keypoints_data);
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tab_titles[position];
        }
    }

    public class ParallaxPageTransformer implements ViewPager.PageTransformer {

        public void transformPage(View view, float position) {

            int pageWidth = view.getWidth();
            ImageView v = (ImageView) view.findViewById(R.id.iv_feature);
            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(1);
            } else if (position <= 1) { // [-1,1]
                v.setTranslationX(-position * (pageWidth / 2)); //Half the normal speed

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(1);
            }


        }
    }
}
