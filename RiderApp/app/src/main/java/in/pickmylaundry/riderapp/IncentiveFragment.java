package in.pickmylaundry.riderapp;

import android.graphics.Color;
import android.support.v4.app.Fragment;;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import in.pickmylaundry.riderapp.Adapter.IncentiveListAdapter;
import in.pickmylaundry.riderapp.Model.IncentiveModel;
import in.pickmylaundry.riderapp.utils.AppPrefrence;
import in.pickmylaundry.riderapp.utils.Constant;
import in.pickmylaundry.riderapp.utils.UploadTask;

/**
 * Created by zomato on 18/07/16.
 */
public class IncentiveFragment extends Fragment {

    private View rootView;
    private ListView listView;
    private IncentiveListAdapter incentiveListAdapter;
    private ArrayList<IncentiveModel> incentiveModelArrayList = new ArrayList<IncentiveModel>();
    private TextView this_month_incentive,last_month_incentive;
    private LinearLayout incentive_ll, this_month_ll, last_month_ll;
    private JSONObject incentive_json;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_incentive, null);

        getActivity().setTitle("Incentive");

        listView = (ListView) rootView.findViewById(R.id.incentive_list);
        this_month_incentive = (TextView) rootView.findViewById(R.id.this_month_incentive);
        last_month_incentive = (TextView) rootView.findViewById(R.id.last_month_incentive);
        incentive_ll = (LinearLayout) rootView.findViewById(R.id.incentive_ll);

        this_month_ll = (LinearLayout) rootView.findViewById(R.id.this_month_ll);
        last_month_ll = (LinearLayout) rootView.findViewById(R.id.last_month_ll);

        this_month_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setData("this_month");
            }
        });
        last_month_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setData("previous_month");
            }
        });
        getIncentiveList();
        return rootView;
    }
    private void getIncentiveList(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rider_id", AppPrefrence.getMobile(getActivity()));
        request_incentive_list(params);
    }
    private void request_incentive_list(Map<String, Object> params){
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result)  {
                try {
                    JSONObject object = new JSONObject(result);
                    if(object.getBoolean("success")){
                        incentive_json = object.getJSONObject("data");
                        setData("this_month");

                        //set incentive month data
                        this_month_incentive.setText(object.getInt("total_this_month")+"");
                        last_month_incentive.setText(object.getInt("total_prevoius_month")+"");
                        incentive_ll.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void taskException() {
                Constant.no_internet_connection(getActivity());
            }
        }, getActivity(), Constant.incentive, params);
        task.execute();
    }
    private void setData(String month_val){
        JSONArray jsonArray = null;
        try {
            jsonArray = incentive_json.getJSONArray(month_val);

            incentiveModelArrayList.clear();
            for (int i=0;i<jsonArray.length();i++){
                JSONObject date_ob = jsonArray.getJSONObject(i);
                int pickup = (date_ob.has("Pickup"))?date_ob.getInt("Pickup"):0;
                int delivery = (date_ob.has("Delivery"))?date_ob.getInt("Delivery"):0;
                int pl = (date_ob.has("pl"))?date_ob.getInt("pl"):0;
                int dc = (date_ob.has("dc"))?date_ob.getInt("dc"):0;
                int marketing = (date_ob.has("coupon"))?date_ob.getInt("coupon"):0;

                IncentiveModel incentiveModel = new IncentiveModel();
                incentiveModel.setDate(date_ob.getString("date"));
                incentiveModel.setPickup(pickup+"");
                incentiveModel.setDelivery(delivery+"");
                incentiveModel.setPl(pl+"");
                incentiveModel.setDc(dc+"");
                incentiveModel.setCoupon(marketing+"");
                incentiveModel.setIncentive(date_ob.getInt("incentive")+"");
                incentiveModelArrayList.add(incentiveModel);
            }
            IncentiveListAdapter incentiveListAdapter = new IncentiveListAdapter(getActivity(), incentiveModelArrayList);
            listView.setAdapter(incentiveListAdapter);
            listView.deferNotifyDataSetChanged();

            if(month_val.equals("this_month")){
                this_month_ll.setBackgroundColor(Color.parseColor("#16a085"));
                last_month_ll.setBackgroundColor(Color.parseColor("#cccccc"));
            }
            else{
                this_month_ll.setBackgroundColor(Color.parseColor("#cccccc"));
                last_month_ll.setBackgroundColor(Color.parseColor("#16a085"));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
