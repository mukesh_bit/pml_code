package com.pick.mylaundry;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.UploadTask;
import com.pick.volley.UploadTask.TaskListner;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class VerifyOtp extends Activity implements OnClickListener {
	private TextView verifyotp_screen_text, title_text;
	private EditText verify_mobile, mobile_number_input_disabled;
	private Button verify_otp;
	private String otptext="";
	private TextView verify_otp_resend, verify_otp_resend_disabled, title, otp_autofetch, otp_title, before_verified;
	private ImageView after_verified;
	private	final String SEND_SMS_INTENT = "SMS_INTENT";
	private SMSReceiver smsreceiver;
	int secondsLeft = 0;
	private View rootView;


    @Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.verify_otp);
		rootView = (RelativeLayout) findViewById(R.id.rootView);
		Typeface tf = Typeface.createFromAsset(getAssets(), "font/HelveticaNeue-Regular.ttf");
		title_text = (TextView) findViewById(R.id.upper_bar);
		title_text.setTypeface(tf);
		tf = Typeface.createFromAsset(getAssets(), "font/helvetica-normal.ttf");
		title = (TextView) findViewById(R.id.title);
		title.setTypeface(tf);
		otp_autofetch = (TextView) findViewById(R.id.otp_autofetch);
		otp_autofetch.setTypeface(tf);
		otp_title = (TextView) findViewById(R.id.verifyotp_screen_text);
		otp_title.setTypeface(tf);
		verify_mobile=(EditText) findViewById(R.id.mobile_number_input);
		verify_mobile.requestFocus();
		verify_mobile.setTypeface(tf);
		verify_otp_resend=(TextView) findViewById(R.id.verify_otp_resend);
		verify_otp_resend.setTypeface(tf);
	    verify_otp_resend_disabled=(TextView) findViewById(R.id.verify_otp_resend_disabled);
		verify_otp_resend_disabled.setTypeface(tf);
		verify_otp_resend.setVisibility(View.GONE);
		verify_otp_resend.setOnClickListener(this);
		mobile_number_input_disabled = (EditText) findViewById(R.id.mobile_number_input_disabled);
		mobile_number_input_disabled.setTypeface(tf);
		mobile_number_input_disabled.setText(AppPrefrence.getMobile(VerifyOtp.this));
		before_verified = (TextView) findViewById(R.id.before_verified);
		after_verified = (ImageView) findViewById(R.id.after_verified);

		smsreceiver = new SMSReceiver();
		verify_mobile.addTextChangedListener(new TextWatcher() {
			 @Override
			 public void onTextChanged(CharSequence s, int start, int before, int count) {
				 if(s.length()>3){
					 sendVerifyOtp(s.toString());
				 }
			 }
			 @Override
			 public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			 }
			 @Override
			 public void afterTextChanged(Editable s) {
			 }
		 });

		 new CountDownTimer(60000,100) {
		     public void onTick(long ms) {
				 if (Math.round((float)ms / 1000.0f) != secondsLeft){
					 secondsLeft = Math.round((float)ms / 1000.0f);
					 String text = "00:"+String.valueOf(secondsLeft);
					 verify_otp_resend_disabled.setText(text);
				 }
		     }
		     public void onFinish() {
				 verify_otp_resend_disabled.setVisibility(View.GONE);
		    	 verify_otp_resend.setVisibility(View.VISIBLE);
		     }
		  }.start();
		FlurryAgent.logEvent("otp_verification_screen");
	 }

	@Override
	public void onResume() {
		super.onResume();
		registerReceiver(smsreceiver, new IntentFilter(SEND_SMS_INTENT));
	}

	@Override
	public void onDestroy() {
		try {
			unregisterReceiver(smsreceiver);
		} catch (Exception e) {
		}
		super.onDestroy();
	}


	@Override
	public void onClick(View v) {
		switch (v.getId()) {
	  	case R.id.verify_otp_resend:
			sendNewOtp();
			verify_otp_resend.setVisibility(View.GONE);
			verify_otp_resend_disabled.setVisibility(View.VISIBLE);
			new CountDownTimer(60000,100) {

				public void onTick(long ms) {
					if (Math.round((float)ms / 1000.0f) != secondsLeft){
						secondsLeft = Math.round((float)ms / 1000.0f);
						String text = "00:"+ String.valueOf(secondsLeft);
						verify_otp_resend_disabled.setText(text);
					}
				}
				public void onFinish() {
					verify_otp_resend_disabled.setVisibility(View.GONE);
					verify_otp_resend.setVisibility(View.VISIBLE);
				}
			}.start();
			Map<String, String> articleParams = new HashMap<String, String>();
			articleParams.put("mobile", AppPrefrence.getMobile(VerifyOtp.this));
			//up to 10 params can be logged with each event
			FlurryAgent.logEvent("Resend_Otp", articleParams);
			break;
		 default:
			startActivity(new Intent(VerifyOtp.this,
        		    LoginAcitivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
        			finish();
			break;
		}
	}
	private void sendNewOtp() {
		List<NameValuePair> otpparams=new ArrayList<NameValuePair>();
		otpparams.add(new BasicNameValuePair("mobile", AppPrefrence.getMobile(VerifyOtp.this)));
		sendNewOtpApi(otpparams);
	}

	private void sendNewOtpApi(List<NameValuePair> otpparams) { 

        UploadTask task = new UploadTask(new TaskListner() {
       @Override
			public void taskResult(String result) {
				try {
					JSONObject json = new JSONObject(result);
                   if (json.getString("success").equals("true")) {
                	   // Toast.makeText(VerifyOtp.this,json.getString("msg"),Toast.LENGTH_SHORT).show();
				     } else if(json.getString("success").equals("false")){
					   Toast.makeText(VerifyOtp.this, json.getString("msg"), Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {
					Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
					e.printStackTrace();
				}

			}
 
			@Override
			public void taskException() {
				Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();

			}
		},VerifyOtp.this,Constant.OTP, otpparams, true);

		task.execute();
 }

	private void sendVerifyOtp( String otp) {
		//otptext=verify_mobile.getText().toString();
		otptext=otp;
		if(otptext.length()>2){
		   List<NameValuePair> params=new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("username",AppPrefrence.getMobile(VerifyOtp.this)));
			params.add(new BasicNameValuePair("otp",otptext));
			sendOTPApi(params);
		   }else{
			Snackbar.make(rootView, "Invalid OTP", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
		   }
	 }

	private void sendOTPApi(List<NameValuePair> params) { 


        UploadTask task = new UploadTask(new TaskListner() {
       @Override
			public void taskResult(String result) {
				try {
					JSONObject json = new JSONObject(result);
					Log.i("data", json.toString());
					if (json.getString("success").equals("true")) {
					   AppPrefrence.savePromoCode(getApplicationContext(), json.getString("promoCode"));
					   AppPrefrence.saveUserImagePath(VerifyOtp.this, "default");
					   AppPrefrence.setLoginStatus(VerifyOtp.this);
					   SharedPreferences prefs = getSharedPreferences("gcmregistartion", Context.MODE_PRIVATE);
					   SharedPreferences.Editor editor = prefs.edit();
					   editor.putString("registration_id", "");
					   editor.putInt("appVersion", 10);
					   editor.putBoolean("loggedOut", false);
					   editor.commit();
					   //hold for 1 sec
					   before_verified.setVisibility(View.GONE);
					   after_verified.setVisibility(View.VISIBLE);
					   try {
						   TimeUnit.SECONDS.sleep(1);
					   } catch (InterruptedException e) {
						   e.printStackTrace();
					   }
					   Toast.makeText(VerifyOtp.this, json.getString("msg"), Toast.LENGTH_LONG).show();
					   Intent intent=new Intent();
                       intent.setClass(VerifyOtp.this, MainActivity.class);
                       VerifyOtp.this.startActivity(intent);

					   Map<String, String> articleParams = new HashMap<String, String>();
					   articleParams.put("main_screen", "verify_otp");
					   //up to 10 params can be logged with each event
					   FlurryAgent.logEvent("MainActivity", articleParams);
					   finish();
                   } else if(json.getString("success").equals("false")){
					   Snackbar.make(rootView, json.getString("msg"), Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
					}
				} catch (JSONException e) {
					Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
					e.printStackTrace();
				}
			}
			@Override
			public void taskException() {
				Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();

			}
		},VerifyOtp.this,Constant.VERIFY, params, true);

		task.execute();
 }
	public class SMSReceiver extends BroadcastReceiver
	{
		@Override
		public void onReceive(Context context, Intent intent)
		{
			String action = intent.getAction();
			if(action.equalsIgnoreCase( SEND_SMS_INTENT)) {
				Bundle extra = intent.getExtras();
				if(extra != null){
					if(extra.getString("message") != null){
						String message = extra.getString("message");
						String otptext = message.substring(12, 16);
						Boolean isOtp = otptext.trim().matches("[0-9]+");
						if(!isOtp)
							otptext = message.substring(0, 4);
						sendVerifyOtp(otptext);
						// Capture event data
						Map<String, String> articleParams = new HashMap<String, String>();
						articleParams.put("time", (60 - secondsLeft) + "");
						//up to 10 params can be logged with each event
						FlurryAgent.logEvent("verified", articleParams);
					}
				}

			}
		}
	}
}



