package com.pick.utill;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 121 on 10/25/2016.
 */
public class ShowInfoDialog {

    private Context mContext;

    public ShowInfoDialog(Context context){
        this.mContext = context;
    }
    public void check_for_info_dialog(){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(mContext)));

        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        showDialog(json.getString("msg"));
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }

            }
            @Override
            public void taskException() {
                Toast.makeText(mContext, "Please check your connection", Toast.LENGTH_LONG).show();
            }
        },mContext, Constant.GETDIALOG, params, false);
        task.execute();
    }
    private void showDialog(String msg){
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setMessage(msg);
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alertDialog.show();
    }
}
