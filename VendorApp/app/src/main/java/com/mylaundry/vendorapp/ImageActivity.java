package com.mylaundry.vendorapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Dell on 9/17/2015.
 */
public class ImageActivity extends AppCompatActivity implements View.OnClickListener {

    private String urlimagepost = "http://crm.pickmylaundry.in/web_services/vendor_app/uploadImage.php?";
    private ProgressBar progressBar;
    private EditText  description;
    private static final int PICK_IMAGE = 300;
    private Button  btncamera, btngallery, btnpictures;
    private TextView vendorname,txtPercentage, nameofcustomer, pickup_boy, pickup_date, salesorderid;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private Uri fileUri;
    public static final int MEDIA_TYPE_IMAGE = 1;
    SharedPreferences sharedPref;
    String filepath = null;
    long totalSize = 0;
    private ProgressDialog progress;
    private ContentBody foto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);
        sharedPref = ImageActivity.this.getSharedPreferences(getString(R.string.PREFS_NAME), Context.MODE_PRIVATE);
        vendorname = (TextView) findViewById(R.id.vendorname);
        btncamera = (Button) findViewById(R.id.btncamera);
        btngallery = (Button) findViewById(R.id.btngallery);
        btnpictures = (Button) findViewById(R.id.btnpictures);
        description=(EditText)findViewById(R.id.description);


        nameofcustomer = (TextView) findViewById(R.id.nameofcustomer);
        pickup_boy = (TextView) findViewById(R.id.pickup_boy);
        pickup_date = (TextView) findViewById(R.id.pickup_date);
        salesorderid = (TextView) findViewById(R.id.salesorderid);

        Intent intent = getIntent();
        nameofcustomer.setText(intent.getStringExtra("nameofcustomer"));
        pickup_boy.setText(intent.getStringExtra("pickup_boy"));
        pickup_date.setText(intent.getStringExtra("pickup_date"));
        salesorderid.setText(intent.getStringExtra("salesorderid"));

        progressBar=(ProgressBar)findViewById(R.id.progressBar);
        txtPercentage=(TextView)findViewById(R.id.txtPercentage);

        vendorname.setText(sharedPref.getString(getString(R.string.VendorName), ""));


        btncamera.setOnClickListener(this);
        btngallery.setOnClickListener(this);
        btnpictures.setOnClickListener(this);
        if(savedInstanceState != null){
            fileUri=savedInstanceState.getParcelable("fileUri");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("fileUri", fileUri);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.title_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.button_logout) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.VendorName), null);
            editor.putString(getString(R.string.Password), null);
            editor.putInt(getString(R.string.vendor_id), 0);
            editor.commit();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btncamera:
                captureImage();
                break;
            case R.id.btnpictures:
               Intent i = new Intent(this, PicturesActivity.class);
                i.putExtra("salesorderid", salesorderid.getText().toString());
                startActivity(i);
                break;
            case R.id.btngallery:
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);
                startActivityForResult(Intent.createChooser(intent, "Select Pictures"), PICK_IMAGE);
                break;
        }
    }

    private void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);// create a file to save the image
        //Toast.makeText(this, "Image saved to:\n" +
          //      fileUri, Toast.LENGTH_LONG).show();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name

        // start the image capture Intent
        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    private Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile(int type) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = null;
        String state = Environment.getExternalStorageState();
        //if (Environment.MEDIA_MOUNTED.equals(state)) {
        mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PML VendorApp images");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("PML VendorApp", "failed to create directory");
                return null;
            }
        }
        //}
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = null;
        if (type == MEDIA_TYPE_IMAGE) {
            //if(mediaStorageDir != null) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
            //}
        } else {
            return null;
        }
        return mediaFile;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                // Image captured and saved to fileUri specified in the Intent
               // Toast.makeText(this, "Image saved to:\n" +
                 //       fileUri, Toast.LENGTH_LONG).show();
                filepath= fileUri.getPath();
                BitmapFactory.Options options = new BitmapFactory.Options();

                // down sizing image as it throws OutOfMemory Exception for larger
                // images
                options.inSampleSize = 8;

                final Bitmap bitmap = BitmapFactory.decodeFile(filepath, options);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bos);
                InputStream in = new ByteArrayInputStream(bos.toByteArray());
                foto = new InputStreamBody(in, "image/jpeg");
                new UploadFileToServer().execute();

            } else if (resultCode == RESULT_CANCELED) {
                // User cancelled the image capture
            } else {
                // Image capture faiiled, advise user
            }
        }

        if(requestCode == PICK_IMAGE){
            if(resultCode == RESULT_OK){
                filepath= getPath(data.getData());
                BitmapFactory.Options options = new BitmapFactory.Options();

                // down sizing image as it throws OutOfMemory Exception for larger
                // images
                options.inSampleSize = 8;

                final Bitmap bitmap = BitmapFactory.decodeFile(filepath, options);
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bos);
                InputStream in = new ByteArrayInputStream(bos.toByteArray());
                foto = new InputStreamBody(in, "image/jpeg");
                new UploadFileToServer().execute();


            }
        }

    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void launchUpLoad(Uri fileUri){
        filepath = fileUri.getPath();
        new UploadFileToServer().execute();
    }


    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {
        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
            //progressBar.setVisibility(View.VISIBLE);
            //progressBar.setProgress(0);
            progress = new ProgressDialog(ImageActivity.this);
            progress.setMessage("Uploading Image...");
            progress.show();
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            // Making progress bar visible
           // progressBar.setVisibility(View.VISIBLE);

            // updating progress bar value
            //progressBar.setProgress(progress[0]);

            // updating percentage value
            //txtPercentage.setText(String.valueOf(progress[0]) + "%");
            super.onProgressUpdate(progress);
        }

        @Override
        protected String doInBackground(Void... params) {
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httppost = new HttpPost(urlimagepost);

            try {
                AndroidMultipartEntity entity = new AndroidMultipartEntity(
                        new AndroidMultipartEntity.ProgressListener() {

                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * 100));
                            }
                        });

                //File sourceFile = new File(filepath);
                //Bitmap bmp = BitmapFactory.decodeFile(filepath);
                //ByteArrayOutputStream bos = new ByteArrayOutputStream();
                //bmp.compress(Bitmap.CompressFormat.JPEG, 70, bos);
                //InputStream in = new ByteArrayInputStream(bos.toByteArray());
                //ContentBody foto = new InputStreamBody(in, "image/jpeg", "filename");

                // Adding file data to http body


                //entity.addPart("image", new FileBody(sourceFile));
                entity.addPart("image", foto);
                // Extra parameters if you want to pass to server
                Log.d("ImageActivity", String.valueOf(sharedPref.getInt(getString(R.string.vendor_id), 0)));
                entity.addPart("vendor_id", new StringBody(String.valueOf(sharedPref.getInt(getString(R.string.vendor_id), 0))));
                entity.addPart("so", new StringBody(salesorderid.getText().toString()));
                entity.addPart("description", new StringBody(description.getText().toString()));

                totalSize = entity.getContentLength();
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (ClientProtocolException e) {
                responseString = e.toString();
            } catch (IOException e) {
                responseString = e.toString();
            }

            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            Log.e("MainActivity", "Response from server: " + result);

            // showing the server response in an alert dialog
           // if (progress != null && progress.isShowing()) {
             //   progress.dismiss();
            //}
            try{
                progress.dismiss();
                progress=null;
            }
            catch(Exception e){
                e.printStackTrace();
            }
            //progress.dismiss();
           // progressBar.setProgress(100);
            showAlert(result);

            super.onPostExecute(result);
        }

    }

    private void showAlert(String message) {
        try {
            JSONObject obj = new JSONObject(message);
            String success = obj.getString("success");
            if (success.contains("true")) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(ImageActivity.this);
                builder.setMessage("File Uploaded Successfully").setTitle("Response from Servers")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // do nothing
                                progressBar.setVisibility(View.INVISIBLE);
                                txtPercentage.setVisibility(View.INVISIBLE);
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
            else{
                final AlertDialog.Builder builder = new AlertDialog.Builder(ImageActivity.this);
                builder.setMessage("Error while Uploading").setTitle("Response from Servers")
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // do nothing
                                progressBar.setVisibility(View.INVISIBLE);
                                txtPercentage.setVisibility(View.INVISIBLE);
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed()
    {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
        //super.onBackPressed();  // optional depending on your needs
    }
}
