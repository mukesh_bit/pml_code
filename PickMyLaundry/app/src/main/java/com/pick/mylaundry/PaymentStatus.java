package com.pick.mylaundry;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 121 on 11/21/2015.
 */
public class PaymentStatus extends Activity {

    private LinearLayout back;
    private TextView status_tv, title,status_caption, so_tv, amount_tv;
    private ImageView sign;
    private String status, order_id, total, contactid, salesorder_no, sostatus;
    private Bundle extras;
    private View rootView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paymentstatus);
        String fontPath = "font/helvetica-normal.ttf";
        Typeface tf = Typeface.createFromAsset(this.getAssets(), fontPath);
        //get parameters
        extras = getIntent().getExtras();
        status = extras.getString("status");
        sostatus = extras.getString("sostatus");
        salesorder_no = extras.getString("salesorder");
        total = extras.getString("total");
        order_id = extras.getString("order_id");
        rootView = (RelativeLayout) findViewById(R.id.rootView);
        back=(LinearLayout)findViewById(R.id.action_bar_custom_slide);
        title=(TextView)findViewById(R.id.actionbar_custom_title);
        title.setText("Payment");
        title.setTypeface(tf);
        status_caption = (TextView) findViewById(R.id.textView4);
        status_caption.setTypeface(tf);
        status_tv = (TextView) findViewById(R.id.textView5);
        status_tv.setTypeface(tf);
        sign = (ImageView) findViewById(R.id.imageView2);

        so_tv = (TextView) findViewById(R.id.textView6);
        so_tv.setTypeface(tf);
        amount_tv = (TextView) findViewById(R.id.textView7);
        amount_tv.setTypeface(tf);
        Button myOrder = (Button) findViewById(R.id.button);
        Button tryAgain = (Button) findViewById(R.id.tryAgain);
        myOrder.setTypeface(tf);
        so_tv.setText("Order ID: "+salesorder_no);
        amount_tv.setText("Amount: " + total);
        if(status.equals("TXN_SUCCESS")){
            myOrder.setText("Back to My Order");
            status_tv.setText("Payment Success");
            status_caption.setText("Congratulations, Your payment with \n Pick My Laundry is successful.");
            sign.setImageResource(R.drawable.confirm);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.weight = 2;
            myOrder.setLayoutParams(params);
            params.weight = 0;
            tryAgain.setLayoutParams(params);
            tryAgain.setVisibility(View.GONE);
        }
        else{
            status_tv.setText("Payment Failed");
            status_tv.setTextColor(Color.parseColor("#ff0000"));
            status_caption.setText("Sorry, There was some problem in\nprocessing your payment. Please try again.");
            sign.setImageResource(R.drawable.error);
        }
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMyOrderActivity();
            }
        });
        myOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMyOrderActivity();
            }
        });
        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactid = extras.getString("contact_id");
                getPaytmOrderId();
            }
        });

        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("mobile", AppPrefrence.getMobile(PaymentStatus.this));
        articleParams.put("status", status);
        //up to 10 params can be logged with each event
        FlurryAgent.logEvent("Payment Status", articleParams);

    }
    private void startMyOrderActivity(){
        Intent intent = new Intent(PaymentStatus.this, Invoice.class);
        intent.putExtra(Constant.ORDERID, order_id);
        intent.putExtra(Constant.ORDERSTATUS, sostatus);
        intent.putExtra("from", "payment");
        intent.putExtra("SESSION_ID", "10");
        startActivity(intent);
    }
    private void getPaytmOrderId(){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("orderid", order_id));
        requestForPaytmId(params);
    }

    private void requestForPaytmId(List<NameValuePair> params) {
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        paytmTransaction paytmInstance = new paytmTransaction(json.getString("paytm_id"), total, extras.getString("contactid"), sostatus, order_id, salesorder_no, PaymentStatus.this);
                        paytmInstance.startPayment();

                    } else if(json.getString("success").equals("false")){
                        Snackbar.make(rootView, "Sorry, Unable to process. Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    }
                } catch (JSONException e){
                    Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    e.printStackTrace();
                }
            }
            @Override
            public void taskException() {
                Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();


            }
        },PaymentStatus.this, Constant.GETPAYTMORDERID,params, true);

        task.execute();
    }
    @Override
    protected void onStart() {
        super.onStart();
        //initialize flurry
        FlurryAgent.setLogEnabled(false);
        // init Flurry
        FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
        FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
