package com.pick.mylaundry;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 121 on 12/22/2015.
 */
public class ConfirmationActivity extends Activity {
    private TextView so;
    private Button order_status, rate_us;
    private String salesorder_no, orderid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirmation_order_activity);
        salesorder_no = getIntent().getExtras().getString("so");
        orderid = getIntent().getExtras().getString("order_id");
        so = (TextView) findViewById(R.id.so);
        so.setText(salesorder_no);
        order_status = (Button) findViewById(R.id.order_status);
        rate_us = (Button) findViewById(R.id.rate_us);
        order_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConfirmationActivity.this, Invoice.class);
                intent.putExtra(Constant.ORDERID, orderid);
                startActivity(intent);
                finish();

                Map<String, String> articleParams = new HashMap<String, String>();
                articleParams.put("mobile", AppPrefrence.getMobile(ConfirmationActivity.this));
                articleParams.put("from", "confirmation");
                //up to 10 params can be logged with each event
                FlurryAgent.logEvent("Invoice", articleParams);
            }
        });
        rate_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, String> articleParams = new HashMap<String, String>();
                articleParams.put("mobile", AppPrefrence.getMobile(ConfirmationActivity.this));
                //up to 10 params can be logged with each event
                FlurryAgent.logEvent("rate_us_review", articleParams);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.pick.mylaundry&hl=en")));
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        intent.putExtra("SESSION_ID", "1");
        intent.putExtra("id",1);
        intent.putExtra("Context","Review");
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //initialize flurry
        FlurryAgent.setLogEnabled(false);
        // init Flurry
        FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
        FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("mobile", AppPrefrence.getMobile(ConfirmationActivity.this));
        //up to 10 params can be logged with each event
        FlurryAgent.logEvent("Confirmation_screen", articleParams);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
