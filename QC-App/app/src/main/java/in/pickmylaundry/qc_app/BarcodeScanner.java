package in.pickmylaundry.qc_app;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.Utils.AppPrefer;
import in.pickmylaundry.Utils.Constant;
import in.pickmylaundry.Utils.UploadTask;

/**
 * Created by 121 on 3/10/2016.
 */
public class BarcodeScanner extends Activity{

    private ArrayList<Map<String, String>> barcode_list;
    private EditText barcode;
    private ArrayList<Map<String, String>> scanned_list = new ArrayList<Map<String, String>>();
    HashMap<String, Integer> total_list = new HashMap<String, Integer>();
    HashMap<String, Integer> done_list = new HashMap<String, Integer>();
    private ArrayList<String> checkedQuality = new ArrayList<String>();
    private TextView remaining_tv, done_tv,remaining_count, done_count;
    private boolean isAllScanned = false, is_qc = false;
    private Button submit;
    private AlertDialog dialog, dialog_exit;
    private UploadTask task;
    private int salesorderid;
    private String vendor;
    private RelativeLayout rootView;
    private LinearLayout bp,fg,wq, qc_report,fold;
    private Button submit_qc, skip_qc;
    private Boolean is_qc_report = false;
    private String order_type, processing_instruction;
    private CheckBox instruction_cb;
    private LinearLayout instruction_ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.barcode_scanner);
        rootView = (RelativeLayout) findViewById(R.id.root_view);

        //get extras
        Bundle bundle = getIntent().getExtras();
        barcode_list = (ArrayList<Map<String, String>>) bundle.getSerializable("barcode");
        salesorderid = bundle.getInt("salesorderid");
        vendor = bundle.getString("vendor");
        is_qc = bundle.getBoolean("is_qc");
        order_type = bundle.getString("order_type");
        processing_instruction = bundle.getString("processing_instruction");

        //set data
        TextView so = (TextView) findViewById(R.id.so);
        TextView name = (TextView) findViewById(R.id.name);
        TextView desc = (TextView) findViewById(R.id.desc);
        Button clear = (Button) findViewById(R.id.clear);
        submit = (Button) findViewById(R.id.submit);
        remaining_tv = (TextView) findViewById(R.id.remaining_tv);
        done_tv = (TextView) findViewById(R.id.done_tv);
        remaining_count = (TextView) findViewById(R.id.remaining_count);
        done_count = (TextView) findViewById(R.id.done_count);
        so.setText(bundle.getString("so"));
        name.setText(bundle.getString("name"));
        desc.setText(bundle.getString("desc"));
        qc_report = (LinearLayout) findViewById(R.id.qc_report);
        submit_qc = (Button) findViewById(R.id.submit_qc);
        skip_qc = (Button) findViewById(R.id.skip_qc);
        instruction_cb = (CheckBox) findViewById(R.id.instruction_check_cb);
        instruction_ll = (LinearLayout) findViewById(R.id.instruction_ll);


        barcode = (EditText) findViewById(R.id.barcode);
        if(savedInstanceState == null){
            setRemaining();
        }
        else{
            total_list = (HashMap<String, Integer>) savedInstanceState.getSerializable("total_list");
            done_list = (HashMap<String, Integer>) savedInstanceState.getSerializable("done_list");
            scanned_list = (ArrayList<Map<String, String>>) savedInstanceState.getSerializable("scanned_list");
        }
        setData();


        barcode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int length = s.toString().trim().length();
                if ( length== 6)
                    checkForCode();
                else if(length % 8 ==0&&length>6)
                    startSound();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                barcode.setText("");
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAllScanned) {
                    if(!is_qc) {
                        qc_report.setVisibility(View.VISIBLE);
                        rootView.setBackgroundColor(Color.parseColor("#87000000"));
                        instruction_ll.setBackgroundColor(Color.parseColor("#87000000"));
                        submit.setVisibility(View.GONE);
                        skip_qc.setVisibility(View.GONE);
                        is_qc_report = false;
                    }
                    else{
                        submitData();
                    }
                } else {
                    showConfirmationDialog();
                }
            }
        });
        ImageView back =(ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkForData();
            }
        });

        //set click listener
        bp = (LinearLayout) rootView.findViewById(R.id.bp);
        wq = (LinearLayout) rootView.findViewById(R.id.wq);
        fg = (LinearLayout) rootView.findViewById(R.id.fg);
        fold = (LinearLayout) rootView.findViewById(R.id.fold);
        bp.setOnClickListener(serviceClickListener);
        wq.setOnClickListener(serviceClickListener);
        fg.setOnClickListener(serviceClickListener);
        fold.setOnClickListener(serviceClickListener);

        //submit data
        submit_qc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                is_qc_report = true;
                qc_report.setVisibility(View.GONE);
                submitData();
            }
        });
        skip_qc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                is_qc_report = false;
                qc_report.setVisibility(View.GONE);
                submitData();
            }
        });
        if(order_type.equals("Rework")){
            LinearLayout rework_strip = (LinearLayout) findViewById(R.id.rework);
            rework_strip.setVisibility(View.VISIBLE);
        }
        if(processing_instruction!=null && ! processing_instruction.equals("")){
            TextView instruction = (TextView) findViewById(R.id.instruction);
            instruction_cb.setVisibility(View.VISIBLE);
            instruction.setText(processing_instruction);
            instruction_ll.setVisibility(View.VISIBLE);

        }

    }

    private void showConfirmationDialog() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(BarcodeScanner.this);
        String msg = "Remaining Items : \n\n";
        for (Map.Entry<String, Integer> entry : total_list.entrySet()) {
            if(entry.getValue()>0)
                msg += entry.getKey()+"                 "+ entry.getValue() + "\n";
        }
        msg += "\n\n Are you sure you want to submit?\n\n";
        builder.setMessage(msg)
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if (!is_qc) {
                            qc_report.setVisibility(View.VISIBLE);
                            rootView.setBackgroundColor(Color.parseColor("#87000000"));
                            instruction_ll.setBackgroundColor(Color.parseColor("#87000000"));
                            is_qc_report = false;
                            submit.setVisibility(View.GONE);
                        }
                        else{
                            submitData();
                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        // Create the AlertDialog object and return it
        dialog = builder.create();
        dialog.show();
    }

    private void checkForCode(){
        String code = barcode.getText().toString().trim();
        Log.i("code", code);
        for(int i=0;i<barcode_list.size();i++){
            Map<String, String> barcodes = barcode_list.get(i);
            if(barcodes.get("barcode").equals(code)){
                boolean scanned = false;
                for (int j=0;j<scanned_list.size();j++){
                    Map<String, String> scanned_barcodes = scanned_list.get(j);
                    if(scanned_barcodes.get("barcode").equals(code)){
                        Toast.makeText(BarcodeScanner.this, "Already Done", Toast.LENGTH_LONG).show();
                        scanned = true;
                    }
                }
                if(scanned_list.size()==0||!scanned){
                    scanned_list.add(barcodes);
                    removeItem(barcodes.get("type"));
                    setData();
                    Toast.makeText(BarcodeScanner.this, "Submitted Succesfully", Toast.LENGTH_LONG).show();
                }
                if(scanned_list.size()==barcode_list.size()){
                    Toast.makeText(BarcodeScanner.this, "All Items Scanned Succesfully", Toast.LENGTH_LONG).show();
                }
                barcode.setText("");
                return;
            }
        }
        Toast.makeText(BarcodeScanner.this, "Wrong Item", Toast.LENGTH_LONG).show();
        startSound();
    }
    private void setRemaining(){
        for(int i=0;i<barcode_list.size();i++){
            Map<String, String> barcodes = barcode_list.get(i);
            String type = barcodes.get("type");
            if(total_list.containsKey(type)){
                int val = total_list.get(type);
                val++;
                total_list.put(type, val);
            }
            else{
                total_list.put(type, 1);
            }
        }
    }
    private void removeItem(String type){
        int val = total_list.get(type);
        val--;
        total_list.put(type, val);
        if(done_list.containsKey(type)){
            val = done_list.get(type);
            val++;
            done_list.put(type, val);
        }
        else{
            done_list.put(type, 1);
        }
    }
    private void setData(){
        int count_ = 0;
        String msg = "\n \n";
        isAllScanned = true;
        for (Map.Entry<String, Integer> entry : total_list.entrySet()) {
            msg += entry.getKey()+"         "+ entry.getValue() + "\n";
            count_ += entry.getValue().intValue();
            if(entry.getValue()>0)
                isAllScanned = false;
        }
        remaining_tv.setText(msg);
        remaining_count.setText(count_ + "");
        //Toast.makeText(BarcodeScanner.this, count_+"", Toast.LENGTH_SHORT).show();

        //done list
        msg = "\n \n";
        count_ = 0;
        for (Map.Entry<String, Integer> entry : done_list.entrySet()) {
            msg += entry.getKey()+"           "+ entry.getValue() + "\n";
            count_ += entry.getValue();
        }
        done_tv.setText(msg);
        done_count.setText(count_ + "");


        if(isAllScanned)
            submit.setBackgroundColor(getResources().getColor(R.color.dark_green));

    }
    private void submitData(){
        submit.setVisibility(View.VISIBLE);
        rootView.setBackgroundColor(Color.parseColor("#eeeeee"));
        instruction_ll.setBackgroundColor(Color.parseColor("#ffffff"));
        Map<String, Integer> data = new HashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : total_list.entrySet()) {
            if(entry.getValue()>0){
                data.put(entry.getKey(), entry.getValue());
            }
        }
        Map<String, Object> params = new HashMap<String, Object>();
        JSONObject json = new JSONObject(data);
        ArrayList<String> barcode_list = new ArrayList<String>();
        for (int i=0;i<scanned_list.size();i++){
            Map<String, String> barcodes = scanned_list.get(i);
            barcode_list.add(barcodes.get("barcode"));
        }
        JSONArray json_barcodes = new JSONArray(barcode_list);
        params.put("pending_cloth", json.toString());
        params.put("done_barcodes", json_barcodes.toString());
        params.put("salesorderid", salesorderid);
        params.put("user", AppPrefer.get_user_name(BarcodeScanner.this));
        if(is_qc_report) {
            if(processing_instruction!=null && ! processing_instruction.equals("")) {
                if(instruction_cb.isChecked())
                    checkedQuality.add("instruction_checked");
                else
                    checkedQuality.add("instruction_not_checked");
            }
            JSONArray json_qc = new JSONArray(checkedQuality);
            params.put("qc_report", json_qc.toString());
            Log.i("checked_quality", json_qc.toString());
        }
        submit.setEnabled(false);
        submit.setText("Submitting...");
        request_submit_data(params);
    }
    private void request_submit_data(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                Log.i("log", result);
                submit.setText("Submitted");
                submit.setEnabled(true);
                if(is_qc_report) {
                    is_qc = true;
                    is_qc_report = false;
                }
            }

            @Override
            public void taskException() {
                submit.setText("Submit Again");
                submit.setEnabled(true);
                Toast.makeText(getApplicationContext(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, BarcodeScanner.this, Constant.submit_data, params);
        task.execute();
    }

    @Override
    public void onBackPressed() {
        checkForData();
    }
    private void startSound(){
        // Load the sound
        SoundPool soundPool = new SoundPool(10, AudioManager.STREAM_MUSIC, 0);
        final int soundID = soundPool.load(this, R.raw.sound, 1);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId,
                                       int status) {
                AudioManager audioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
                float actualVolume = (float) audioManager
                        .getStreamVolume(AudioManager.STREAM_MUSIC);
                float maxVolume = (float) audioManager
                        .getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                float volume = actualVolume / maxVolume;
                soundPool.play(soundID, maxVolume, maxVolume, 1, 0, 1f);
            }
        });
    }
    private void checkForData(){
        if(done_list.size()>0&&!isAllScanned) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(BarcodeScanner.this);
            builder.setMessage("All scanned data will be lost. Are you sure, you want to exit?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Intent intent = new Intent(BarcodeScanner.this, VendorDetail.class);
                            intent.putExtra("vendor", vendor);
                            startActivity(intent);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog_exit.dismiss();
                        }
                    });
            dialog_exit =  builder.create();
            dialog_exit.show();
        }
        else {
            Intent intent = new Intent(BarcodeScanner.this, VendorDetail.class);
            intent.putExtra("vendor", vendor);
            startActivity(intent);
        }
    }
    private View.OnClickListener serviceClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LinearLayout clickedLayout = (LinearLayout) rootView.findViewById(v.getId());
            ImageView clickedImage = (ImageView) clickedLayout.getChildAt(0);
            String serviceClicked = (String) clickedLayout.getTag();
            String imageDrawable = (String) clickedImage.getTag();
            if(checkedQuality.contains(serviceClicked)){
                checkedQuality.remove(serviceClicked);
                clickedLayout.setBackgroundResource(R.drawable.blue_ring);
                int resId = getResources().getIdentifier(imageDrawable, "drawable", BarcodeScanner.this.getPackageName());
                clickedImage.setImageResource(resId);
            }
            else{
                checkedQuality.add(serviceClicked);
                clickedLayout.setBackgroundResource(R.drawable.blue_fill);
                int resId = getResources().getIdentifier(imageDrawable+"_g", "drawable", BarcodeScanner.this.getPackageName());
                clickedImage.setImageResource(resId);
            }
        }
    };

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("total_list", total_list);
        outState.putSerializable("done_list", done_list);
        outState.putSerializable("scanned_list", scanned_list);
        super.onSaveInstanceState(outState);
    }
}
