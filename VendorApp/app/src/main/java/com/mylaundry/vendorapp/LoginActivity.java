package com.mylaundry.vendorapp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class LoginActivity extends Activity {
	TextView login;
	EditText user, pass;
	String url="http://crm.pickmylaundry.in/web_services/vendor_app/login.php?";
	JSONArray contacts = null;
	Boolean internet;
    private final String TAG = "LoginActiviy";


    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
			setContentView(R.layout.activity_login);
			login = (TextView) findViewById(R.id.login);

			user = (EditText) findViewById(R.id.username);
			pass = (EditText) findViewById(R.id.password);


			login.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String vendor = user.getText().toString();
					String ps = pass.getText().toString();
					internet = isOnline();
					if (internet == true) {

						if (vendor.length() == 0) {
							Toast.makeText(LoginActivity.this,
									"Please Fill the Name",
									Toast.LENGTH_LONG).show();
						} else {
							makeStringPost(vendor, ps);

						}
					} else {
						Toast.makeText(LoginActivity.this,
								"You have no internet connection..",
								Toast.LENGTH_LONG).show();
					}
				}
			});
		

	}



    public void makeStringPost(final String vendor, final String ps){
		String tag_json_obj = "post";

		StringRequest post = new StringRequest(Request.Method.POST, url,
				new Response.Listener<String>() {

					@Override
					public void onResponse(String response) {
						try {
							if (response != null) {
								JSONObject obj = new JSONObject(response);
								String res = obj.getString("success");
								if (res.contains("true")) {
									Toast.makeText(LoginActivity.this, "Login successfully..",
											Toast.LENGTH_LONG).show();
									int vendor_id = obj.getInt("vendor_id");
									SharedPreferences sharedPref = LoginActivity.this.getSharedPreferences(getString(R.string.PREFS_NAME), Context.MODE_PRIVATE);
									SharedPreferences.Editor editor = sharedPref.edit();
                                    editor.putString(getString(R.string.VendorName), vendor);
                                    editor.putString(getString(R.string.Password), ps);
                                    editor.putInt(getString(R.string.vendor_id), vendor_id);
									editor.commit();

									Intent i = new Intent(LoginActivity.this, MainActivity.class);
									startActivity(i);

									finish();
									//Toast.makeText(getApplicationContext(), "Thank you for your post", Toast.LENGTH_LONG).show();
							} else {
									Toast.makeText(LoginActivity.this, "Incorrect password..",
											Toast.LENGTH_LONG).show();
								}
							}
							else{
								Log.e("ServiceHandler", "Couldn't get any data from the url");
							}
						}
						catch (JSONException e) {
							e.printStackTrace();
							Toast.makeText(LoginActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
						}
					}
					},new Response.ErrorListener() {

			@Override
			public void onErrorResponse(VolleyError error) {
				VolleyLog.d(TAG, "Error: " + error.getMessage());
				Toast.makeText(getApplicationContext(),
						"Yash"+error.getMessage(), Toast.LENGTH_SHORT).show();
			}
		}){
			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				HashMap<String, String> postParam= new HashMap<String, String>();
				postParam.put("vendorName", vendor);
				postParam.put("password", ps);
				return postParam;
			}
		};
		AppController.getInstance().addToRequestQueue(post, tag_json_obj);
	}


	protected boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnected()) {
			return true;
		} else {
			return false;
		}
	}


}
