package com.pick.mylaundry;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.pick.navAdapter.OfferListAdapter;
import com.pick.navmodel.OfferListModel;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mukesh on 10/21/2015.
 */
public class Offers extends Fragment {

    private Offers customView = null;
    private TextView title;
    private View offerListView;
    private List<OfferListModel> offerList;
    private View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.offer_layout, container, false);
        title = (TextView) getActivity().findViewById(R.id.toolbar_title);
        title.setText("Offers");
        offerListView = (View) rootView.findViewById(R.id.offer_list);
        setListData();

        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("mobile", AppPrefrence.getMobile(getActivity()));
        //up to 10 params can be logged with each event
        FlurryAgent.logEvent("offer", articleParams);

        return rootView;
    }

    private void setListData() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(getActivity())));
        getOffers(params);
    }

    private void getOffers(List<NameValuePair> params) {

        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                offerList = new ArrayList<OfferListModel>();
                try {
                    JSONObject json = new JSONObject(result);
                    if (json.getString("success").equals("true")) {
                        JSONArray orderlist1 = json.getJSONArray("data");
                        int i = 0;
                        for (i = orderlist1.length() - 1; i >= 0; i--) {
                            JSONObject data = orderlist1.getJSONObject(i);
                            final OfferListModel bean = new OfferListModel();
                            bean.setCouponCode(data.getString("couponCode"));
                            bean.setCouponDescription(data.getString("coupon_description"));
                            bean.setTitle(data.getString("title"));
                            bean.setExpiryDate(data.getString("expiry_date"));
                            bean.setImageUrl(data.getString("imageUrl"));
                            bean.setSuccessMessage(data.getString("successMessage"));
                            offerList.add(bean);
                        }
                    } else if (json.getString("success").equals("false")) {
                        Snackbar.make(rootView, json.getString("msg"), Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    }
                } catch (JSONException e) {
                    Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    e.printStackTrace();
                }
                ((ListView) offerListView).setAdapter(new OfferListAdapter(getActivity(),
                        offerList));
                ((ListView) offerListView).deferNotifyDataSetChanged();

            }

            @Override
            public void taskException() {
                Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
            }
        }, getActivity(), Constant.OFFERLIST, params, true);

        task.execute();
    }
}
