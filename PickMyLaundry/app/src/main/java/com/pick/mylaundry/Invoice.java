package com.pick.mylaundry;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.pick.navAdapter.InvoiceAdapter;
import com.pick.navmodel.InvoiceListModel;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.EmptyUtility;
import com.pick.volley.UploadTask;
import com.pick.volley.UploadTask.TaskListner;
import com.rey.material.widget.RadioButton;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Invoice extends Activity implements OnClickListener {
	private String Orderid;
	private List<InvoiceListModel> invoicelist;
	private ListView productlist;
	private ImageView status_icon;
	private Double total=0.0;
	private int disocuntvalue=0;
	private String orderstatus="";
	private Double totalpaid=0.0;
	private int percentcomplete=0;
	private ProgressBar pro;
    private  int lv_height;

	private TextView cus_title, refresh;
    private RelativeLayout adjustment_rl;
    private View adjustment_border;
    private ListView invoice_lv;
	private int randomInt = 0;
	private String processing_status, total_payable;
	private TextView taxText, order_invoice_tax, order_invoice_mycash_used, order_invoice_paid_online, order_invoice_total_payable;
	private String tot, contactid, paytm_id, salesorder_no;
	private RadioButton rb1,rb2,rb3,rb4;
	private Dialog dialog;
	private NetworkInfo activeNetwork;
	private TextView orderid,pickupdate,pickuptime,deliverydate,deliverytime,statustext,payNow,discount,totalbill,subtotalbill, paidInfo;
	private TextView adjustment_tv,discount_text, cancellation_text, order_invoice_text,total_payable_Text,paid_online_Text,mycashText;
	private LinearLayout invoice_detail, invoice_not_found, cancelled;
	private RelativeLayout rootView, paid_online_rl;
	private LinearLayout call_rider;
	private Bundle extras;

	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.order_invoice);
        String fontPath_bold= "font/Helvetica-Bold.otf";
        Typeface tf_bold = Typeface.createFromAsset(getAssets(), fontPath_bold);
        String fontPath = "font/HelveticaNeue-Regular.ttf";
		Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        cus_title=(TextView)findViewById(R.id.actionbar_custom_title);
       	cus_title.setText("ORDER DETAILS");
		cus_title.setTypeface(tf);
		fontPath = "font/HelveticaNeue-Regular.ttf";
		tf = Typeface.createFromAsset(getAssets(), fontPath);
		extras = getIntent().getExtras();
		if(EmptyUtility.isNotEmpty(extras.getString("message"))){
			showDialogBox();
		}
		//refresh=(TextView)getActivity().findViewById(R.id.refresh);
		rootView = (RelativeLayout) findViewById(R.id.rootView);
        TextView orderinvoice_id_text=(TextView) findViewById(R.id.orderinvoice_id_text);
        orderinvoice_id_text.setTypeface(tf);
        TextView pickHeader=(TextView) findViewById(R.id.pickHeader);
        pickHeader.setTypeface(tf);
        TextView delivery=(TextView)findViewById(R.id.delivery);
        delivery.setTypeface(tf);
        order_invoice_text=(TextView)findViewById(R.id.order_invoice_text);
        order_invoice_text.setTypeface(tf);
        TextView totalText=(TextView)findViewById(R.id.totalText);
        totalText.setTypeface(tf);
        adjustment_rl = (RelativeLayout) findViewById(R.id.adjustment_layout);
        adjustment_border = (View) findViewById(R.id.viewLine5);
        invoice_lv = (ListView) findViewById(R.id.order_invoice_productlist);
		productlist=(ListView)findViewById(R.id.order_invoice_productlist);
		status_icon=(ImageView)findViewById(R.id.order_invoice_statuscenterimage);
		invoice_detail = (LinearLayout) findViewById(R.id.invoice_detail);
		invoice_not_found = (LinearLayout) findViewById(R.id.invoice_not_found);
		cancelled = (LinearLayout) findViewById(R.id.cancelation_reason);
		cancellation_text = (TextView) findViewById(R.id.cancellation_text);
        Orderid=getIntent().getExtras().getString(Constant.ORDERID);

        orderid=(TextView)findViewById(R.id.idnum);
        orderid.setTypeface(tf);
        pickupdate=(TextView)findViewById(R.id.order_invoice_pickupdate);
        pickupdate.setTypeface(tf);
        pickuptime=(TextView)findViewById(R.id.order_invoice_pickuptime);
        pickuptime.setTypeface(tf);
        deliverydate=(TextView) findViewById(R.id.order_invoice_deliverydate);
        deliverydate.setTypeface(tf);
        deliverytime=(TextView) findViewById(R.id.order_invoice_deliverytime);
        deliverytime.setTypeface(tf);
        discount=(TextView) findViewById(R.id.order_invoice_discount);
        discount.setTypeface(tf);
        totalbill=(TextView) findViewById(R.id.order_invoice_total);
        totalbill.setTypeface(tf_bold);
		subtotalbill=(TextView) findViewById(R.id.order_invoice_subtotal);
		subtotalbill.setTypeface(tf);
        adjustment_tv=(TextView) findViewById(R.id.order_invoice_adjustment);
        adjustment_tv.setTypeface(tf);
        discount_text = (TextView) findViewById(R.id.discountText);
        TextView total_text = (TextView) findViewById(R.id.totalText);
        total_text.setTypeface(tf_bold);
		payNow = (TextView) findViewById(R.id.payurl);
		payNow.setTypeface(tf);
        discount_text.setTypeface(tf);
		taxText = (TextView) findViewById(R.id.taxText);
		taxText.setTypeface(tf);
		order_invoice_tax = (TextView) findViewById(R.id.order_invoice_tax);
		order_invoice_tax.setTypeface(tf);
		order_invoice_mycash_used = (TextView) findViewById(R.id.order_invoice_mycash_used);
		order_invoice_mycash_used.setTypeface(tf);
		order_invoice_paid_online = (TextView) findViewById(R.id.order_invoice_paid_online);
		order_invoice_paid_online.setTypeface(tf);
		order_invoice_total_payable = (TextView) findViewById(R.id.order_invoice_total_payable);
		order_invoice_total_payable.setTypeface(tf_bold);
		total_payable_Text = (TextView) findViewById(R.id.total_payable_Text);
		total_payable_Text.setTypeface(tf_bold);
		mycashText = (TextView) findViewById(R.id.mycashText);
		mycashText.setTypeface(tf);
		paid_online_Text = (TextView) findViewById(R.id.paid_online_Text);
		paid_online_Text.setTypeface(tf);
		paidInfo = (TextView) findViewById(R.id.paidInfo);
		paidInfo.setTypeface(tf);
		call_rider = (LinearLayout) findViewById(R.id.call_rider);
		paid_online_rl = (RelativeLayout) findViewById(R.id.paid_online_rl);

        statustext=(TextView)findViewById(R.id.order_invoice_statuscentertext);
        statustext.setTypeface(tf);
		getOrderInvoice();
		//check internet connection
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		activeNetwork = cm.getActiveNetworkInfo();
		boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
		if(!isConnected)
			rootView.setVisibility(View.INVISIBLE);

		ImageView backIcon = (ImageView) findViewById(R.id.back_icon);
		backIcon.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if(extras.containsKey("from") && extras.getString("from").equals("My Orders")) {
					Invoice.super.onBackPressed();
				}
				else{
					Intent intent = new Intent(Invoice.this, MainActivity.class);
					intent.putExtra("SESSION_ID", "1");
					startActivity(intent);
				}
			}
		});
	}



	private void getOrderInvoice() {
		List<NameValuePair> params=new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("orderId", Orderid));
		params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(getApplicationContext())));
	    sendOrderId(params);
	}
	private void sendOrderId(List<NameValuePair> params) { 
		   UploadTask task = new UploadTask(new TaskListner() {
	      @Override
				public void taskResult(String result) {
	    	  invoicelist= new ArrayList<InvoiceListModel>();
					try {
						final JSONObject json = new JSONObject(result);
						if(json.getString("success").equals("true")) {
							 orderid.setText(json.getString("salesorderno"));
							 orderstatus = json.getString("status");
							 pickupdate.setText(json.getString("pickupdate"));
							 pickuptime.setText(json.getString("pickuptime"));
							 deliverydate.setText(json.getString("deliverydate"));
							 deliverytime.setText(json.getString("deliverytime"));
							 String url = json.getString("url");
							 processing_status = json.getString("processing_status");
							 if(url.equals("Paid Online")) {
								 payNow.setText("Paid Online");
								 payNow.setBackgroundColor(Color.parseColor("#00c200"));
							 }
							 else if(!url.equals("")) {
								 payNow.setText("Pay Now");
								 payNow.setBackgroundColor(Color.parseColor("#f84337"));
							 }
							 else if(orderstatus.equals("Created")||orderstatus.equals("Vendor Assigned")){
								 payNow.setText("Cancel order");
								 payNow.setTextColor(Color.parseColor("#ff0000"));
							 }
							else
							 	payNow.setVisibility(View.GONE);
							final String status = json.getString("status");
							if(status.equals("Created")||status.equals("Vendor Assigned")||status.equals("Pickup Rider Assigned")
									||status.equals("Pickup Done")){
								invoice_detail.setVisibility(View.GONE);
								invoice_not_found.setVisibility(View.VISIBLE);
							}
							else if(status.equals("Cancelled")){
								invoice_detail.setVisibility(View.GONE);
								invoice_not_found.setVisibility(View.GONE);
								cancelled.setVisibility(View.VISIBLE);
								cancellation_text.setText(json.getString("cancellation_reason"));
								order_invoice_text.setVisibility(View.GONE);
							}
							else{
								invoice_detail.setVisibility(View.VISIBLE);
							}
							String discount1=json.getString("discount_amount").toString();
							Integer dis=Integer.valueOf(discount1);

							tot =json.getString("total");
							total_payable = json.getString("total_payable");
							totalpaid=Double.valueOf(tot);
							contactid = json.getString("contact_id");
							paytm_id = json.getString("paytm_id");
							salesorder_no = json.getString("salesorderno");
							String paidInfoValue = json.getString("paidInfo");

                            discount.setText(dis+"");
                            String adjustment = json.getString("adjustment");
                            Integer adj = Integer.valueOf(adjustment);
                            if(adj!=0){
                                adjustment_tv.setText(adjustment);
                            }
                            else{
                                adjustment_rl.setVisibility(View.GONE);
                                adjustment_border.setVisibility(View.GONE);
                            }
							order_invoice_tax.setText(json.getInt("order_invoice_tax")+"");
							taxText.setText(json.getString("tax_text"));
                            subtotalbill.setText(json.getString("subtotal"));
							order_invoice_mycash_used.setText(json.getString("mycash_used"));
							order_invoice_paid_online.setText(json.getString("paid_online"));
							order_invoice_total_payable.setText(json.getString("total_payable"));

							if(json.getString("paid_online").equals("0"))
								paid_online_rl.setVisibility(View.GONE);

                            discount_text.setText(json.getString("discount_text"));
							  JSONArray	orderlist1= json.getJSONArray("productdes");
							final float scale = getResources().getDisplayMetrics().density;
							int dpHeightInPx  = (int) (20 * scale);
							lv_height = dpHeightInPx * (orderlist1.length());

                            int i=0;
							 for(i=orderlist1.length()-1;i>=0;i--){
							 JSONObject data = orderlist1.getJSONObject(i);	
						     final InvoiceListModel bean = new InvoiceListModel();
							 bean.setProductname(data.getString("productname"));
							 String weight1=data.getString("quantity").toString();
							 Double actualweight=Double.valueOf(weight1);
							 bean.setUnit(data.getString("unit").toString());
							 bean.setQuantity(actualweight);
							 String price1=data.getString("price").toString();
							 Double actualPrice=Double.valueOf(price1);
							   bean.setPrice(actualPrice);
							   invoicelist.add(bean);
							 }
							setAnimation();
							setPaymentOption();
							if(!json.getString("rider_number").isEmpty()){
								final String rider_number = json.getString("rider_number");
								call_rider.setVisibility(View.VISIBLE);
								TextView call_text = (TextView) findViewById(R.id.call_rider_tv);
								Typeface tf = Typeface.createFromAsset(getAssets(), "font/helvetica-normal.ttf");
								call_text.setTypeface(tf);
								call_rider.setOnClickListener(new OnClickListener() {
									@Override
									public void onClick(View v) {
										Intent intent = new Intent("android.intent.action.CALL");
										intent.setData(Uri.parse("tel:"+rider_number));
										startActivity(intent);
										Map<String, String> articleParams = new HashMap<String, String>();
										articleParams.put("mobile", AppPrefrence.getMobile(Invoice.this));
										articleParams.put("status", status);
										//up to 10 params can be logged with each event
										FlurryAgent.logEvent("call_rider", articleParams);
									}
								});
							}
							else {
								if(json.getBoolean("isRescheduable"))
									checkForReschedule(status, json.getString("instruction"), json.getString("category"), json.getBoolean("isPreponable"), json.getInt("turnaround"));
							}
						  } else if(json.getString("success").equals("false")){
							Snackbar.make(rootView, json.getString("msg"), Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
						}

					} catch (JSONException e) {
						Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
						e.printStackTrace();
					}catch (NullPointerException e) {
						Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
					}catch (IllegalArgumentException el) {
						Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
					}

					((ListView) productlist).setAdapter(new InvoiceAdapter(getApplicationContext(), invoicelist));
			  		((ListView) productlist).deferNotifyDataSetChanged();


			  totalbill.setText("" + totalpaid);
			  ListAdapter listAdapter = productlist.getAdapter();
			  if (listAdapter == null) {
				  return;
			  }
			  ViewGroup.LayoutParams params = productlist.getLayoutParams();
			  params.height = lv_height;
			  productlist.setLayoutParams(params);
			  productlist.requestLayout();
		  }
				@Override
				public void taskException() {
					Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
				}
			},Invoice.this,Constant.INVOICELIST, params, true);

			task.execute();
		 
			
		}

	public void checkForReschedule(final String status, final String instruction, final String category, final Boolean isPreponable, final int turnaround){
			TextView call_text = (TextView) findViewById(R.id.call_rider_tv);
			ImageView icon = (ImageView) findViewById(R.id.icon);
			icon.setImageResource(R.drawable.schedule_icon);
			Typeface tf = Typeface.createFromAsset(getAssets(), "font/helvetica-normal.ttf");
			call_text.setTypeface(tf);
			if(status.equals("Created")||status.equals("Vendor Assigned"))
				call_text.setText("Reschedule Order");
			else
				call_text.setText("Reschedule Delivery");
			call_rider.setVisibility(View.VISIBLE);
			call_rider.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(Invoice.this, ScheduleActivity.class);
					ArrayList<String> washType = new ArrayList<String>();
					if(EmptyUtility.isNotEmpty(instruction)){
						String[] instruction_arr = instruction.split(",");
						for(int i=0;i<instruction_arr.length;i++){
							washType.add(instruction_arr[i].trim());
						}
					}
					else if(EmptyUtility.isNotEmpty(category)){
						if(category.equals("Laundry"))
							washType.add("Wash & Fold");
						else{
							washType.add("Dry Clean");
						}
					}
					if(washType.size()==0)
						washType.add("Wash & Fold");

					Bundle data_bundle = new Bundle();
					data_bundle.putStringArrayList("washType", washType);
					data_bundle.putInt("turnaround", turnaround);

					intent.putExtra("data_bundle", data_bundle);
					intent.putExtra("pickup_date", pickupdate.getText().toString());
					intent.putExtra("pickup_time", pickuptime.getText().toString());
					intent.putExtra("delivery_date", deliverydate.getText().toString());
					intent.putExtra("delivery_time", deliverytime.getText().toString());
					intent.putExtra("status", status);
					intent.putExtra("salesorderid", Orderid);
					intent.putExtra("isPreponable", isPreponable);
					startActivity(intent);
					Map<String, String> articleParams = new HashMap<String, String>();
					articleParams.put("mobile", AppPrefrence.getMobile(Invoice.this));
					//up to 10 params can be logged with each event
					FlurryAgent.logEvent("Reschedule Clicked", articleParams);
				}
			});
	}

	@Override
	public void onClick(View v) {

	}

	@Override
	public void onBackPressed() {
		if(extras.containsKey("from") && extras.getString("from").equals("My Orders")) {
			super.onBackPressed();
		}
		else{
			Intent intent = new Intent(Invoice.this, MainActivity.class);
			intent.putExtra("SESSION_ID", "1");
			startActivity(intent);
		}
	}

	public void onResume(){
		super.onResume();
	}
	public void setAnimation(){
		if(orderstatus.equals("Created")){
			percentcomplete=17;
			status_icon.setImageResource(R.drawable.status_1);
			statustext.setText("Order Placed");
		}else if(orderstatus.equals("Vendor Assigned")){
			percentcomplete=17;
			status_icon.setImageResource(R.drawable.status_1);
			statustext.setText("Order Confirmed");
		}else if(orderstatus.equals("Pickup Rider Assigned")){
			percentcomplete=33;
			status_icon.setImageResource(R.drawable.status_2_active);
			statustext.setText(orderstatus);
		}else if(orderstatus.equals("Pickup Done")){
			percentcomplete=53;
			status_icon.setImageResource(R.drawable.status_3_active);
			statustext.setText("In Processing");
		}else if(orderstatus.equals("Invoice Created")&&!processing_status.equals("Ready for Delivery")){
			percentcomplete=53;
			status_icon.setImageResource(R.drawable.status_3_active);
			statustext.setText("In Processing");
		}else if(orderstatus.equals("Out for Delivery")){
			percentcomplete=87;
			status_icon.setImageResource(R.drawable.status_4_active);
			statustext.setText(orderstatus);
		}else if(orderstatus.equals("Delivered")){
			percentcomplete=100;
			status_icon.setImageResource(R.drawable.status_5_active);
			statustext.setText(orderstatus);
		}else if(processing_status.equals("Ready for Delivery")){
			percentcomplete=70;
			status_icon.setImageResource(R.drawable.status6);
			statustext.setText("Ready for Delivery");
		}else if (orderstatus.equals("Cancelled")) {
			percentcomplete=0;
			status_icon.setImageResource(R.drawable.cancelled);
			statustext.setText(orderstatus);
		}
		ProgressBar pro=(ProgressBar) findViewById(R.id.performance_progress_bar);
		pro.getProgressDrawable().setColorFilter(0xFF00FF00, android.graphics.PorterDuff.Mode.DST);
		pro.setMax(100);
		ObjectAnimator animation = ObjectAnimator.ofInt(pro, "progress",percentcomplete);
		animation.setDuration(4000);
		animation.setInterpolator(new AccelerateInterpolator());
		animation.start();
	}
	private void setPaymentOption(){
		payNow.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String payText = payNow.getText().toString();
				if (payText.equals("Pay Now")) {
					paytmTransaction paytmInstance = new paytmTransaction(paytm_id, total_payable, contactid, orderstatus, Orderid, salesorder_no, Invoice.this);
					paytmInstance.startPayment();
					//paytmTransaction();
				} else if (payText.equals("Cancel order")) {
					cancelOrder();
				}
			}
		});
	}
	private void cancelOrder(){
		dialog = new Dialog(Invoice.this, R.style.FullHeightDialog);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.cancel_order);
		dialog.show();
		rb1 = (RadioButton) dialog.findViewById(R.id.rb1);
		rb2 = (RadioButton) dialog.findViewById(R.id.rb2);
		rb3 = (RadioButton) dialog.findViewById(R.id.rb3);
		rb4 = (RadioButton) dialog.findViewById(R.id.rb4);
		CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					rb1.setChecked(rb1 == buttonView);
					rb2.setChecked(rb2 == buttonView);
					rb3.setChecked(rb3 == buttonView);
					rb4.setChecked(rb4 == buttonView);
				}

			}

		};

		rb1.setOnCheckedChangeListener(listener);
		rb2.setOnCheckedChangeListener(listener);
		rb3.setOnCheckedChangeListener(listener);
		rb4.setOnCheckedChangeListener(listener);
		TextView close = (TextView) dialog.findViewById(R.id.closeDialog);
		final TextView cancel_order = (TextView) dialog.findViewById(R.id.cancelNow);
		close.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		cancel_order.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				String reason = "";
				if (rb1.isChecked())
					reason = "Ordered By mistake";
				else if (rb2.isChecked())
					reason = "Changed my minds";
				else if (rb3.isChecked())
					reason = "Duplicate order";
				else if (rb4.isChecked())
					reason = "Other";
				if (reason.equals(""))
					Toast.makeText(getApplicationContext(), "Please select a reason for cancellation", Toast.LENGTH_LONG).show();
				else
					cancel_now(reason);
			}
		});

	}
	private void cancel_now(String reason){
		List<NameValuePair> params=new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(getApplicationContext())));
		params.add(new BasicNameValuePair("so", Orderid));
		params.add(new BasicNameValuePair("reason", reason));
		dialog.dismiss();
		makeCancelRequest(params);
	}

	private void makeCancelRequest(List<NameValuePair> params) {
		 new UploadTask(new TaskListner() {
			@Override
			public void taskResult(String result) {
				try {
					JSONObject json = new JSONObject(result);
					if(json.getString("success").equals("true")){
						Snackbar.make(rootView, "Order Cancelled Successfully", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
						getOrderInvoice();
						call_rider.setVisibility(View.GONE);
					}
					else{
						Snackbar.make(rootView, json.getString("msg"), Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
					}

				} catch (JSONException e){
					Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
					e.printStackTrace();
				}

			}

			@Override
			public void taskException() {
				Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();

			}
		},Invoice.this,Constant.CANCELORDER,params, true).execute();

	}
	private void showDialogBox(){
		String dialog_title = extras.getString("dialog_title");
		final RelativeLayout dialog = (RelativeLayout) findViewById(R.id.dialog);
		ImageView close = (ImageView) findViewById(R.id.close);
		final RelativeLayout shade = (RelativeLayout) findViewById(R.id.shade);
		TextView ok = (TextView) findViewById(R.id.ok);
		TextView dialog_title_tv = (TextView) findViewById(R.id.dialog_title);
		TextView message_tv = (TextView) findViewById(R.id.message);
		message_tv.setText(extras.getString("message"));
		dialog_title_tv.setText(dialog_title);
		shade.setVisibility(View.VISIBLE);
		dialog.setVisibility(View.VISIBLE);
		shade.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				shade.setVisibility(View.GONE);
				dialog.setVisibility(View.GONE);
			}
		});
	}
	@Override
	protected void onStart() {
		super.onStart();
		//initialize flurry
		FlurryAgent.setLogEnabled(false);
		// init Flurry
		FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
		FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}

}
