package com.pick.navmodel;

/**
 * Created by 121 on 12/9/2015.
 */
public class PriceModel {
    private String label;
    private String price;
    private String imageUrl;
    private String category;
    private int count;

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    private String itemType;

    public int getCount() {   return count;  }

    public void setCount(int count) {  this.count = count;  }

    public String getCategory() {      return category;   }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
