package in.pickmylaundry.Utils;


        import java.io.BufferedReader;
        import java.io.DataOutputStream;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.net.HttpURLConnection;
        import java.net.URL;
        import java.net.URLEncoder;
        import java.util.Map;

/**
 * Created by 121 on 1/31/2016.
 */
public class ServiceHandler {
    private URL url;
    private HttpURLConnection conn;
    private String encodedParameter ="";
    private String line;
    private String request_url;
    private String request_type;
    private Map<String, Object> params;
    private StringBuffer response = new StringBuffer();

    public ServiceHandler(String request_url, String request_type, Map<String, Object> params)  {
        this.request_url = request_url;
        this.request_type = request_type;
        this.params = params;
    }

    public String makeCall(){
        try {
            url = new URL(request_url);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod(request_type);

            //set input and output
            conn.setDoInput(true);
            conn.setDoOutput(true);

            for(Map.Entry<String, Object> entry: params.entrySet()){
                if(!encodedParameter.isEmpty())
                    encodedParameter += "&";
                encodedParameter += URLEncoder.encode(entry.getKey(), "UTF-8")+"="+URLEncoder.encode(entry.getValue().toString(), "UTF-8");
            }
            //write data
            DataOutputStream ds = new DataOutputStream(conn.getOutputStream());
            ds.writeBytes(encodedParameter);
            ds.flush();
            ds.close();

            //get response
            InputStream is = conn.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine())!=null){
                response.append(line);
                response.append('\r');
            }
            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return response.toString();
    }
}
