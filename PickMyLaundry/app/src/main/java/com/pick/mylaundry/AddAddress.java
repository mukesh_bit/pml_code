package com.pick.mylaundry;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.utill.Pickhelper;
import com.pick.volley.EmptyUtility;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 121 on 12/15/2015.
 */
public class AddAddress extends AppCompatActivity
    {
    private TextView home_label, office_label, other_label;

    private Pickhelper dbhelper;
    private SQLiteDatabase db;
    private EditText flat_et, society_et;
    private String addressType = "Home", addMode;
    private int keyToBeEdited;
    private ImageView back;
    private View rootView;
    private TextView change_tv, locality;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_address);
        rootView = (RelativeLayout) findViewById(R.id.rootView);

        //save address
        TextView saveAddress = (TextView) findViewById(R.id.save);
        flat_et = (EditText) findViewById(R.id.input_flat);
        society_et = (EditText) findViewById(R.id.input_society);
        change_tv = (TextView) findViewById(R.id.change_locality);
        locality = (TextView) findViewById(R.id.locality);
        home_label = (TextView) findViewById(R.id.home_label);
        office_label = (TextView) findViewById(R.id.office_label);
        other_label = (TextView) findViewById(R.id.other_label);


        saveAddress.setOnClickListener(saveAddressClickListener);
        locality.setText(Constant.getLocation(AddAddress.this));
        //get intent data
        Bundle bundle = getIntent().getExtras();
        addMode = bundle.getString("mode");
        if(addMode.equals("Edit")){
            keyToBeEdited = bundle.getInt("primary_key");
            fillAddressDetails(bundle.getInt("primary_key"));
        }
        change_tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddAddress.this, ChooseLocality.class);
                startActivityForResult(intent, 2);
            }
        });
        back = (ImageView) findViewById(R.id.back_icon);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addMode.equals("review")){
                    Intent intent = new Intent(AddAddress.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    AddAddress.this.onBackPressed();
                }

            }
        });

        home_label.setOnClickListener(labelClickListener);
        other_label.setOnClickListener(labelClickListener);
        office_label.setOnClickListener(labelClickListener);

    }
        private View.OnClickListener labelClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                home_label.setBackgroundResource(R.drawable.address_hint);
                other_label.setBackgroundResource(R.drawable.address_hint);
                office_label.setBackgroundResource(R.drawable.address_hint);
                home_label.setTextColor(getResources().getColor(R.color.textColorBlack));
                other_label.setTextColor(getResources().getColor(R.color.textColorBlack));
                office_label.setTextColor(getResources().getColor(R.color.textColorBlack));
                v.setBackgroundResource(R.drawable.address_bg);
                ((TextView) v).setTextColor(getResources().getColor(R.color.white));
                addressType = ((TextView) v).getText().toString();
            }
        };

        private View.OnClickListener saveAddressClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(EmptyUtility.isEmpty(locality.getText().toString()) || locality.getText().toString().equals(Constant.DEFAULTLOCATION)){
                    Snackbar.make(rootView, "Please choose a locality", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                }
                else if(EmptyUtility.isNotEmpty(flat_et.getText().toString())&&EmptyUtility.isNotEmpty(society_et.getText().toString())
                        &&EmptyUtility.isNotEmpty(locality.getText().toString())&&EmptyUtility.isNotEmpty(addressType)) {
                    dbhelper = new Pickhelper(AddAddress.this);
                    db = dbhelper.getWritableDatabase();
                    String selectQuery = "SELECT * FROM " + Pickhelper.TABLE_ADDRESS;
                    Cursor cursor = db.rawQuery(selectQuery, null);
                    if (cursor.moveToFirst()) {
                        ContentValues values = new ContentValues();
                        do {
                            values.put(Pickhelper.KEY_FLATNO, cursor.getString(1));
                            values.put(Pickhelper.KEY_AREA, cursor.getString(3));
                            values.put(Pickhelper.KEY_LANDMARK, cursor.getString(2));
                            values.put(Pickhelper.KEY_TYPE, cursor.getString(4));
                            values.put(Pickhelper.KEY_ACTIVE, "no");
                            db.update(Pickhelper.TABLE_ADDRESS, values, Pickhelper.KEY_PRIMARY + " = ? ", new String[]{String.valueOf(cursor.getInt(0))});
                        } while (cursor.moveToNext());
                    }
                    ContentValues cv = new ContentValues();
                    cv.put(Pickhelper.KEY_FLATNO, flat_et.getText().toString());
                    cv.put(Pickhelper.KEY_LANDMARK, society_et.getText().toString());
                    cv.put(Pickhelper.KEY_AREA, locality.getText().toString());
                    cv.put(Pickhelper.KEY_TYPE, addressType);
                    cv.put(Pickhelper.KEY_ACTIVE, "yes");
                    if (addMode.equals("Edit"))
                        db.update(Pickhelper.TABLE_ADDRESS, cv, Pickhelper.KEY_PRIMARY + "=" + keyToBeEdited, null);
                    else
                        db.insert(Pickhelper.TABLE_ADDRESS, null, cv);
                    db.close();
                    Intent intent = new Intent();
                    intent.putExtra("message", "msg");
                    setResult(1, intent);
                    finish();
                }
                else if(EmptyUtility.isEmpty(addressType)){
                    Snackbar.make(rootView, "Please add a label for address", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                }
                else if(EmptyUtility.isEmpty(flat_et.getText().toString())){
                    Snackbar.make(rootView, "Please Enter Flat no", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                }
                else if(EmptyUtility.isEmpty(society_et.getText().toString())){
                    Snackbar.make(rootView, "Please enter nearest landmark", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                }

            }
        };
        private void fillAddressDetails(int key){
            dbhelper=new Pickhelper(AddAddress.this);
            db=dbhelper.getWritableDatabase();
            String selectQuery = "SELECT * FROM "+Pickhelper.TABLE_ADDRESS+" WHERE "+Pickhelper.KEY_PRIMARY+" = "+key;
            Cursor cursor = db.rawQuery(selectQuery, null);
            if(cursor.moveToFirst()){
                do{
                    flat_et.setText(cursor.getString(1));
                    society_et.setText(cursor.getString(2));
                    locality.setText(cursor.getString(3));
                    String type = cursor.getString(4);
                    addressType = type;
                    rootView.findViewWithTag(addressType).setBackgroundResource(R.drawable.address_hint_selected);
                }while(cursor.moveToNext());
            }
            db.close();
        }

        @Override
        protected void onResume() {
            super.onResume();

        }
        @Override
        protected void onStart() {
            super.onStart();

            FlurryAgent.setLogEnabled(false);
            FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
            FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
            FlurryAgent.logEvent("Splash Screen");
        }

        @Override
        protected void onStop() {
            super.onStop();
            FlurryAgent.onEndSession(this);
        }
        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode, resultCode, data);
            if (requestCode == 2){
                locality.setText(Constant.getLocation(this));
            }
        }

        @Override
        public void onBackPressed() {
            if(addMode.equals("review")){
                Intent intent = new Intent(AddAddress.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
            else {
                super.onBackPressed();
            }
        }
    }