package com.pick.mylaundry;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by 121 on 12/13/2015.
 */
public class SchedulePopup extends AppCompatActivity {
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private String slotType;
    private ArrayList<String> washType;
    private String[] tabTitles = new String[7];
    private String[] tabDates = new String[7];
    private HashMap<Integer, ArrayList<String>> inactive_slots_date = new HashMap<Integer, ArrayList<String>>();
    private TimeslotFragment timeslotFragment;
    private Bundle bundle;
    private TextView schedule_title;
    private int min_index;
    private ImageView back;
    private JSONObject timeslot_data;
    private JSONObject inactive_slots;
    private JSONObject slots;
    private String keys;
    private int turnaround;
    private String selection_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_popup);

        //get extras
        bundle = getIntent().getExtras();
        slotType = bundle.getString("type");
        washType = bundle.getStringArrayList("washType");
        min_index = bundle.getInt("min_index");
        turnaround = bundle.getInt("turnaround");
        selection_type = bundle.getString("selection_type");

        try {
            timeslot_data = new JSONObject(bundle.getString("timeslot_data"));
            inactive_slots = timeslot_data.getJSONObject("inactive_slots");
            slots = timeslot_data.getJSONObject("slots");
            keys = bundle.getString("keys");

        } catch (JSONException e) {
            e.printStackTrace();
        }


        schedule_title = (TextView) findViewById(R.id.popup_title);
        if(slotType.equals("Pickup"))
            schedule_title.setText("Choose Pickup Slot");
        else
            schedule_title.setText("Choose Delivery Slot");
        back = (ImageView) findViewById(R.id.closeImage);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SchedulePopup.this.onBackPressed();
            }
        });

        //setup viewpager
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupTabTitles();
        setupViewPager(viewPager);
        //setup tab
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (int i=0; i<7;i++){
            Bundle tab_bundle = new Bundle();
            tab_bundle.putString("type", slotType);
            tab_bundle.putStringArrayList("washType", washType);
            tab_bundle.putString("date", tabDates[i]);
            tab_bundle.putString("slots", slots.toString());
            tab_bundle.putStringArrayList("inactive_slots", inactive_slots_date.get(i));
            tab_bundle.putString("keys", keys);
            if(slotType.equals("Delivery")&&i==0)
                tab_bundle.putString("pickup_slot", bundle.getString("pickup_slot"));
            if(bundle.containsKey("isPreponable"))
                tab_bundle.putBoolean("isPreponable", bundle.getBoolean("isPreponable"));

            timeslotFragment = new TimeslotFragment();
            timeslotFragment.setArguments(tab_bundle);
            adapter.addFragment(timeslotFragment, tabTitles[i]);
        }
        viewPager.setAdapter(adapter);
    }
    private void setupTabTitles(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat monthFormat = new SimpleDateFormat("E, d MMM");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        Boolean isDayActive = bundle.getBoolean("day_active");
        if(slotType.equals("Pickup")){
            if(isDayActive) {
                tabTitles[0] = "Today";
                tabTitles[1] = "Tomorrow";
                tabDates[0] = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.DATE, 1);
                tabDates[1] = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.DATE, 1);
            }
            else{
                calendar.add(Calendar.DATE, 1);
                tabTitles[0] = "Tomorrow";
                tabDates[0] = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.DATE, 1);
                tabTitles[1] = monthFormat.format(calendar.getTime());
                tabDates[1] = dateFormat.format(calendar.getTime());
                calendar.add(Calendar.DATE, 1);
            }
        }
        else{
            String[] date = bundle.getString("pickup_date").split("-");
            calendar.set(Integer.parseInt(date[2]), Integer.parseInt(date[1]) - 1, Integer.parseInt(date[0]));
            Date today = Calendar.getInstance().getTime();
            calendar.add(Calendar.DATE, turnaround);
            if(today.compareTo(calendar.getTime()) > 0)
                calendar.setTime(today);
            tabTitles[0] = monthFormat.format(calendar.getTime());
            tabDates[0] = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DATE, 1);
            tabTitles[1] = monthFormat.format(calendar.getTime());
            tabDates[1] = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DATE, 1);
        }
        for (int i=2; i<7;i++){
            tabTitles[i] = monthFormat.format(calendar.getTime());
            tabDates[i] = dateFormat.format(calendar.getTime());
            calendar.add(Calendar.DATE, 1);
        }
        addInactiveSlots();
    }
    private void addInactiveSlots() {
        for(int j=0;j<7;j++) {
            String date = tabDates[j];
            ArrayList<String> slots_data = new ArrayList<String>();
            if (inactive_slots.has(date)) {
                JSONArray data = null;
                try {
                    data = inactive_slots.getJSONArray(date);
                    for (int i = 0; i < data.length(); i++) {
                        slots_data.add(data.getString(i));
                    }
                    inactive_slots_date.put(j, slots_data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else {
                inactive_slots_date.put(j, slots_data);
            }
        }
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
