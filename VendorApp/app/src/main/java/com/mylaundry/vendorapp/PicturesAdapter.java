package com.mylaundry.vendorapp;

/**
 * Created by qwew on 08-08-2015.
 */
import java.util.List;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;



public class PicturesAdapter extends ArrayAdapter<Image> {
    private final Context context;
    private final List<Image> values;

    public PicturesAdapter(Context context, List<Image> values) {
        super(context, R.layout.image_row, values);

        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.image_row, parent, false);
        final Image image = values.get(position);

        TextView description = (TextView) rowView.findViewById(R.id.description);
        ImageView imageresource = (ImageView) rowView.findViewById(R.id.imageresource);

        imageresource.setImageBitmap(image.getImage());
        description.setText(image.getDescription());

        return rowView;
    }

}
