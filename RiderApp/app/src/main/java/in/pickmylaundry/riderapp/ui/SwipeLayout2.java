package in.pickmylaundry.riderapp.ui;

import android.content.Context;
import android.content.res.Resources;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import in.pickmylaundry.riderapp.R;
import in.pickmylaundry.riderapp.utils.Constant;

/**
 * Created by 121 on 8/5/2016.
 */
public class SwipeLayout2 extends RelativeLayout {

    private ImageView swipable_drawable;
    private TextView description_tv;
    private SwipeLayoutCustomItems swipeLayoutCustomItems;
    private int xDown;
    private boolean swipeCompleted = false;
    private int swipeType;
    private int screen_width;
    private int[] swipe_resource = {R.drawable.right, R.drawable.left};
    private RelativeLayout drawable_rl;
    private LinearLayout left_drawable_ll, right_drawable_ll;
    private int arrow_width;
    private Context context;


    public SwipeLayout2(Context context) {
        super(context);
        this.context = context;
    }

    public SwipeLayout2(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
    }

    public SwipeLayout2(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
    }
    public void setSwipeLayoutCustomItems(SwipeLayoutCustomItems swipeLayoutCustomItems){
        this.swipeLayoutCustomItems = swipeLayoutCustomItems;
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        super.onLayout(changed, l, t, r, b);
        drawable_rl = (RelativeLayout) getChildAt(1);
        left_drawable_ll = (LinearLayout) drawable_rl.getChildAt(0);
        right_drawable_ll = (LinearLayout) drawable_rl.getChildAt(1);
        description_tv = (TextView) getChildAt(0);
        screen_width = this.getWidth();
        arrow_width = convertDpToPixel(100);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()){
            case MotionEvent.ACTION_DOWN:{
                xDown = (int) event.getX();
                LayoutParams layoutParams=new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.FILL_PARENT);
                if((xDown >=  screen_width/6 && xDown <= 5 *screen_width /6) || swipeCompleted)
                    return false;
                else if (xDown < screen_width / 6){
                    swipeType = Constant.POSITIVE_SWIPE;
                    left_drawable_ll.setVisibility(VISIBLE);
                    right_drawable_ll.setVisibility(GONE);
                    int margin = ((xDown - arrow_width));
                    layoutParams.leftMargin = margin;
                    left_drawable_ll.setLayoutParams(layoutParams);
                    LayoutParams layoutParams_rl = new LayoutParams(xDown, ViewGroup.LayoutParams.FILL_PARENT);
                    drawable_rl.setLayoutParams(layoutParams_rl);
                    drawable_rl.setBackgroundColor(getResources().getColor(R.color.greenLight));
                }
                else {
                    swipeType = Constant.NEGATIVE_SWIPE;
                    left_drawable_ll.setVisibility(GONE);
                    right_drawable_ll.setVisibility(VISIBLE);
                    int margin = ((screen_width - xDown - arrow_width));
                    layoutParams.rightMargin = margin;
                    right_drawable_ll.setLayoutParams(layoutParams);
                    LayoutParams layoutParams_rl = new LayoutParams(screen_width - xDown, ViewGroup.LayoutParams.FILL_PARENT);
                    drawable_rl.setLayoutParams(layoutParams_rl);
                    drawable_rl.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                }
                description_tv.setText(swipeLayoutCustomItems.getButtonPressText(swipeType));
                return true;
            }
            case MotionEvent.ACTION_MOVE:{
                if((xDown >=  screen_width/6 && xDown <= 5 *screen_width /6) || swipeCompleted)
                    return false;
                int x1 = (int) event.getX();
                LayoutParams layoutParams=new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.FILL_PARENT);

                if(x1 > (this.getWidth() * swipeLayoutCustomItems.getActionConfirmDistanceFraction(swipeType)) && swipeType == Constant.POSITIVE_SWIPE){
                    this.setBackgroundColor(0xFF5cb860);
                    swipeCompleted = true;
                    swipeLayoutCustomItems.onSwipeConfirm(swipeType);
                    description_tv.setText(swipeLayoutCustomItems.getActionConfirmText());
                    this.setVisibility(GONE);
                }
                else if(x1 < (this.getWidth() * swipeLayoutCustomItems.getActionConfirmDistanceFraction(swipeType)) && swipeType == Constant.NEGATIVE_SWIPE){
                    this.setBackgroundColor(0xFFf57a1d);
                    swipeCompleted = true;
                    swipeLayoutCustomItems.onSwipeConfirm(swipeType);
                    description_tv.setText(swipeLayoutCustomItems.getActionCancelText());
                    this.setVisibility(GONE);
                }
                else {
                    if(swipeType == Constant.POSITIVE_SWIPE){
                        int margin = ((x1 - arrow_width));
                        layoutParams.leftMargin = margin;
                        left_drawable_ll.setLayoutParams(layoutParams);
                        LayoutParams layoutParams_rl = new LayoutParams(x1, ViewGroup.LayoutParams.FILL_PARENT);
                        drawable_rl.setLayoutParams(layoutParams_rl);
                        drawable_rl.setBackgroundColor(getResources().getColor(R.color.greenLight));
                    }
                    else if(swipeType == Constant.NEGATIVE_SWIPE){
                        int margin = ((screen_width - x1 - arrow_width));
                        layoutParams.rightMargin = margin;
                        right_drawable_ll.setLayoutParams(layoutParams);
                        LayoutParams layoutParams_rl = new LayoutParams(screen_width - x1, ViewGroup.LayoutParams.FILL_PARENT);
                        layoutParams_rl.leftMargin = x1;
                        drawable_rl.setLayoutParams(layoutParams_rl);
                        drawable_rl.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                    }
                    description_tv.setText(swipeLayoutCustomItems.getButtonPressText(swipeType));
                }
                return true;
            }

            case MotionEvent.ACTION_UP:{
                if(!swipeCompleted) {
                    right_drawable_ll.setVisibility(VISIBLE);
                    left_drawable_ll.setVisibility(VISIBLE);
                    LayoutParams layoutParams_rl = new LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.FILL_PARENT);
                    layoutParams_rl.rightMargin = 0;
                    layoutParams_rl.leftMargin = 0;
                    drawable_rl.setLayoutParams(layoutParams_rl);
                    drawable_rl.setBackgroundDrawable(null);
                    LayoutParams layoutParams=new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.FILL_PARENT);
                    layoutParams.leftMargin = -1 * (convertDpToPixel(40));
                    left_drawable_ll.setLayoutParams(layoutParams);
                    left_drawable_ll.requestLayout();
                    LayoutParams layoutParams_ll = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.FILL_PARENT);
                    layoutParams_ll.rightMargin = -1 * (convertDpToPixel(50));
                    layoutParams_ll.leftMargin = screen_width - (convertDpToPixel(50));
                    right_drawable_ll.setLayoutParams(layoutParams_ll);
                }
                description_tv.setText(swipeLayoutCustomItems.getButtonText());
            }
        }
        return super.onInterceptTouchEvent(event);
    }

    public int convertDpToPixel(int dp){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int)px;
    }
}
