package com.pick.mylaundry;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.utill.CurrentLocation;
import com.pick.volley.UploadTask;
import com.rey.material.widget.Spinner;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {

    private static String TAG = MainActivity.class.getSimpleName();
    private TextView toolbartitle;
    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    private int sid=0,id=0;
    private Tracker mTracker;
    Context context;
    AlertDialog.Builder alertDialog;

    private boolean isToastShown = false, isStatusDialogShown = false, isFeedbackDialogVisible = false;
    private boolean isRegIDSent = true;
    private boolean isUnRegistered = false;
    GoogleCloudMessaging gcm;
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    String regid;
    private Handler mHandler = new Handler();
    private String message="";
    private String isAlertDialogVisible = "false", dialog_title;
    private RelativeLayout shade, dialog,spinner_ll;
    private TextView ok, dialog_title_tv, message_tv;
    private ImageView close;
    private FrameLayout fragment_drawer;
    private LinearLayout layout_container;
    private Bundle extras;
    private Dialog dialog_city;
    private TextView location;

    String SENDER_ID = "600458637781";

    static final String TAG2 = "GCMDemo";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        extras = getIntent().getExtras();
        setContentView(R.layout.activity_main);
        layout_container = (LinearLayout) findViewById(R.id.layout_container);
        spinner_ll = (RelativeLayout) findViewById(R.id.spinner_ll);
        location = (TextView) findViewById(R.id.location);

        // configure Flurry
        FlurryAgent.setLogEnabled(false); // init Flurry
        FlurryAgent.init(this,Constant.MY_FLURRY_APIKEY);

        if (extras != null) {
            String ids = extras.getString("SESSION_ID");
            sid=Integer.parseInt(ids);
            message = extras.getString("message");
            isAlertDialogVisible = extras.getString("isAlertDialogVisible");
            if(isAlertDialogVisible!=null) {
                if (isAlertDialogVisible.equals("true")){
                    dialog_title = extras.getString("dialog_title");
                    dialog = (RelativeLayout) findViewById(R.id.dialog);
                    close = (ImageView) findViewById(R.id.close);
                    shade = (RelativeLayout) findViewById(R.id.shade);
                    ok = (TextView) findViewById(R.id.ok);
                    dialog_title_tv = (TextView) findViewById(R.id.dialog_title);
                    message_tv = (TextView) findViewById(R.id.message);
                    showDialogBox();
                }
            }
            else
                isAlertDialogVisible = "false";
            if(message==null)
                message = "";
            if(sid==2){
                id=extras.getInt("id");
            }
        }
        String fontPath = "font/helvetica-normal.ttf";
        Typeface tf = Typeface.createFromAsset(getAssets(),fontPath);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbartitle=(TextView)findViewById(R.id.toolbar_title);
        toolbartitle.setTypeface(tf);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setHomeButtonEnabled(false);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
        if(sid==10){
            Intent intent = new Intent(MainActivity.this, Invoice.class);
            intent.putExtra(Constant.ORDERID, extras.getString(Constant.ORDERID));
            if(extras.containsKey("isAlertDialogVisible"))
                intent.putExtra("from", "notification");
            startActivity(intent);
        }else{
            displayView(sid);
        }

        pickmylaundry application = (pickmylaundry) getApplication();
        mTracker = application.getDefaultTracker();

        context = getApplicationContext();
        regid = getRegistrationId(context);
        mHandler.postDelayed(mRunnable, 1000);


    }

    @Override
    protected void onResume() {
        super.onResume();
        mTracker.setScreenName("Image~MainActivity");
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());
        FragmentManager fragmentManager = getFragmentManager();
        HomeFragment2 fragment = (HomeFragment2) fragmentManager.findFragmentByTag("SCHEDULE A PICKUP");
        if(fragment!=null) {
            if (fragment.isVisible() || fragmentManager.getBackStackEntryCount() == 1) {
                spinner_ll.setVisibility(View.VISIBLE);
                toolbartitle.setVisibility(View.GONE);
            } else {
                spinner_ll.setVisibility(View.GONE);
                toolbartitle.setVisibility(View.VISIBLE);
            }
        }
        if(sid == 0){
            set_spinner();
        }
    }

        @Override
    public void onDrawerItemSelected(View view, int position) {
        if(position==2){
            startActivity(new Intent(MainActivity.this,
                    PriceEstimator.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
            finish();
        }
        else if(position==5) {
            startActivity(new Intent(MainActivity.this,
                    ViewFeatures.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
        }
        else if(position==7) {
            startActivity(new Intent(MainActivity.this,
                    FAQActivity.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP));
        }
        else {
            sid = position;
            displayView(position);
        }

    }
    private void showDialogBox(){
        message_tv.setText(message);
        dialog_title_tv.setText(dialog_title);
        shade.setVisibility(View.VISIBLE);
        dialog.setVisibility(View.VISIBLE);
        close.setOnClickListener(listener);
        ok.setOnClickListener(listener);
        shade.setOnClickListener(listener);
    }

    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            shade.setVisibility(View.GONE);
            dialog.setVisibility(View.GONE);
        }
    };

    private void displayView(int position) {
        Fragment fragment = null;
        String title = "PICK MY LAUNDRY";
        switch (position) {
            case 0:
                fragment = new HomeFragment2();
                title = "SCHEDULE A PICKUP";
                break;
            case 1:
                fragment = new MyBookingFragment();
                title ="MY ORDERS";
                break;
            case 3:
                fragment = new ReferralFragment();
                title = "INVITE AND EARN";
                break;
            case 4:
                fragment = new MyCashFragment();
                title = "MYCASH WALLET";
                break;
            case 5:
                fragment = new Offers();
                title = "Services";
                break;
            case 6:
                fragment = new SupportFragment();
                title = "SUPPORT";
                break;
            case 8:
                fragment = new ProfileFragment();
                title = "ME";
                break;
            default:
                break;
        }
        // Capture event data
        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("mobile", AppPrefrence.getMobile(MainActivity.this));
        articleParams.put("title", title);
        FlurryAgent.logEvent("Drawer_clicked", articleParams);

        Typeface tf = Typeface.createFromAsset(getAssets(), "font/HelveticaNeue-Regular.ttf");
        String name = AppPrefrence.getName(MainActivity.this);
        String promocode = AppPrefrence.getPromoCode(MainActivity.this);
        if((position == 8 && name.equals("")) || (position == 3 && promocode.equals("")) || (position == 4 && name.equals("")))
            get_personal_info(title);
        else if (fragment != null) {
            //remove extra
            if(extras != null && extras.containsKey("payment_status")) {
                if(isStatusDialogShown)
                    extras.remove("payment_status");
                else
                    isStatusDialogShown = true;
            }
            if(extras != null && extras.containsKey("isFeedbackDialogVisible")) {
                if(isFeedbackDialogVisible)
                    extras.remove("isFeedbackDialogVisible");
                else
                    isFeedbackDialogVisible = true;
            }
            FragmentManager fragmentManager =(FragmentManager)getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragment.setArguments(extras);
            fragmentTransaction.replace(R.id.container_body, fragment, title).addToBackStack(null);
            fragmentTransaction.commit();
            toolbartitle.setText(title);
            toolbartitle.setTypeface(tf);
            set_backstack(fragmentManager);
            if(position==0)
                set_spinner();
            else{
                spinner_ll.setVisibility(View.GONE);
                toolbartitle.setVisibility(View.VISIBLE);
            }

        }
    }
    private void get_personal_info(String title){
        Intent intent = new Intent(MainActivity.this, LoginAcitivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.putExtra("from", title);
        startActivity(intent);
    }

    @Override
    public void onBackPressed(){
         FragmentManager fm = getFragmentManager();

        if (sid > 0 ) {
            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            intent.putExtra("SESSION_ID", "0");
            startActivity(intent);
        }
        else{
            new AlertDialog.Builder(this)
                    .setTitle("Really Exit?")
                    .setMessage("Are you sure you want to exit?")
                    .setNegativeButton(android.R.string.no, null)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface arg0, int arg1) {
                            moveTaskToBack(true);
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);
                        }
                    }).create().show();
        }
    }
    private final Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            // Check device for Play Services APK. If check succeeds, proceed with
            //  GCM registration.
            SharedPreferences prefs = getGCMPreferences(getApplicationContext());
            Boolean loggedOut = prefs.getBoolean("loggedOut", false);
            if (checkPlayServices()&&!loggedOut) {
                gcm = GoogleCloudMessaging.getInstance(MainActivity.this);
                regid = getRegistrationId(context);
                //Log.i("register", "inside regid"+regid);
                if (regid.isEmpty() && !isUnRegistered) {
                    registerInBackground();
                    isRegIDSent = false;
                }
                else if(!isRegIDSent){
                    isRegIDSent = true;
                    postRegidRequest(regid);
                }

            } else {
                if(!isUnRegistered){
                    isUnRegistered = true;
                    try {
                        gcm.unregister();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            //findLocation();
            mHandler.postDelayed(mRunnable, 1000);
        }
    };
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            return "";
        }
        return registrationId;
    }

    private boolean checkPlayServices() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
                //showErrorDialog(status);
            } else {
                Toast.makeText(this, "This device is not supported for notification service", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }


    private void registerInBackground() {
        final AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                String msg = "";
                SharedPreferences prefs = getGCMPreferences(getApplicationContext());
                Boolean loggedOut = prefs.getBoolean("loggedOut", false);
                if (loggedOut){
                    //task.cancel(true);
                    return null;
                }
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    if (!loggedOut)
                        regid = gcm.register(SENDER_ID);
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                }
                return null;
            }

        }.execute();

    }

    private void unregisterInBackground() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    gcm.unregister();
                    regid = "";
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                }
                return null;
            }

        }.execute();

    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences("gcmregistartion",
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    private void postRegidRequest(String regid){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(MainActivity.this)));
        params.add(new BasicNameValuePair("reg_id", regid));
        sendRegid(params);
    }

    private void sendRegid(List<NameValuePair> params) {
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                    }

                } catch (JSONException e){
                    //Toast.makeText(getActivity().getApplicationContext(), "Oops Something Went Wrong", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            @Override
            public void taskException() {
                //Toast.makeText(getActivity().getApplicationContext(),"Please check Your Internet Connection",Toast.LENGTH_LONG).show();
            }
        },MainActivity.this, Constant.GCM_URL,params, true);

        task.execute();
    }
    @Override
    protected void onStart() {
        super.onStart();
        //initialize flurry
        FlurryAgent.setLogEnabled(false);
        // init Flurry
        FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
        FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
    private void set_spinner(){
        spinner_ll.setVisibility(View.VISIBLE);
        toolbartitle.setVisibility(View.GONE);
        location.setText(Constant.getLocation(this));
        if(Constant.getLocation(this).equals(Constant.DEFAULTLOCATION)) {
            final CurrentLocation location_cl = new CurrentLocation(this);
            location_cl.getCurrentLocation(new CurrentLocation.LocationInterface() {
                @Override
                public void onResult(String address) {

                    location.setText(address);
                }

                @Override
                public void onError() {

                }
            }, false, false, false);
        }
        spinner_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ChooseLocality.class);
                startActivityForResult(intent, 1);
            }
        });

    }
    private void set_backstack(final FragmentManager fragmentManager){
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                HomeFragment2 fragment = (HomeFragment2) fragmentManager.findFragmentByTag("SCHEDULE A PICKUP");
                if(fragment!=null) {
                    if (fragment.isVisible() || fragmentManager.getBackStackEntryCount() == 1) {
                        spinner_ll.setVisibility(View.VISIBLE);
                        toolbartitle.setVisibility(View.GONE);
                    } else {
                        spinner_ll.setVisibility(View.GONE);
                        toolbartitle.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            location.setText(Constant.getLocation(this));
        }
    }

}