package in.pickmylaundry.riderapp;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

import in.pickmylaundry.riderapp.Adapter.AssignedListAdapter;
import in.pickmylaundry.riderapp.Model.AssignedListModel;
import in.pickmylaundry.riderapp.utils.PickHelper;
import in.pickmylaundry.riderapp.utils.UploadTask;

/**
 * Created by 121 on 5/12/2016.
 */
public class PageFragment extends Fragment {
    private ListView listView;
    private NetworkInfo activeNetwork;
    private boolean isConnected;
    private Bundle bundle;
    private String type, status;
    private UploadTask task;
    private ArrayList<AssignedListModel> list = new ArrayList<AssignedListModel>();
    private PickHelper dbHelper;
    private SQLiteDatabase db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.assigned_list, container, false);

        //get bundle
        bundle = getArguments();
        type = bundle.getString("type");
        status = bundle.getString("status");

        listView = (ListView) rootView.findViewById(R.id.order_list);
        get_list();
        Log.i("frgmnt", type);

        return rootView;
    }
    public void get_list(){
        //check for internet connection
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        getDataFromDB();
    }
    private void getDataFromDB(){
        dbHelper = new PickHelper(getActivity());
        db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+PickHelper.TABLE_ADDRESS+" WHERE "+PickHelper.KEY_TYPE +" = '"+type+"' and "+PickHelper.KEY_STATUS+" = '"+status+"'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        list.clear();
        if(cursor.moveToFirst()){
            do {
                AssignedListModel model = new AssignedListModel();
                model.setSo(cursor.getString(1));
                model.setName(cursor.getString(2));
                model.setTime(cursor.getString(3));
                model.setAddress(cursor.getString(4));
                model.setMobile(cursor.getString(5));
                model.setSalesorderid(cursor.getString(6));
                model.setType(cursor.getString(7));
                Log.i("status", cursor.getString(12));
                model.setSostatus(cursor.getString(9));
                model.setColor(cursor.getString(10));
                model.setIsNext(cursor.getString(11));
                model.setStatus(cursor.getString(12));
                model.setHub(cursor.getString(13));
                model.setProcessing_status(cursor.getString(14));
                model.setLaundry(cursor.getString(15));
                list.add(model);
            }while (cursor.moveToNext());
            listView.setAdapter(new AssignedListAdapter(getActivity(),list, status));
            ((ListView)listView).deferNotifyDataSetChanged();
        }
        db.close();
    }

    @Override
    public void onResume() {
        super.onResume();
        getDataFromDB();
    }
}
