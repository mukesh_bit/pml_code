package in.pickmylaundry.riderapp;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.riderapp.Adapter.TabsPagerAdapter;
import in.pickmylaundry.riderapp.utils.AppPrefrence;
import in.pickmylaundry.riderapp.utils.Constant;
import in.pickmylaundry.riderapp.utils.PickHelper;
import in.pickmylaundry.riderapp.utils.UploadTask;

/**
 * Created by 121 on 5/11/2016.
 */
public class AssignedFragment extends Fragment {
    private UploadTask task;
    private View rootView;
    private ActionBar actionBar;
    private TabsPagerAdapter mAdapter;
    private String[] tabs = { "Pickp", "Delivery" };
    private PickHelper dbHelper;
    private SQLiteDatabase db;
    private ViewPager pager;
    private Bundle bundle;
    private JSONObject count_data = new JSONObject();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, null);

        getActivity().setTitle("Assigned Orders");
        bundle = getArguments();

        mAdapter = new TabsPagerAdapter(getActivity().getSupportFragmentManager(), "pending");
        pager = (ViewPager) rootView.findViewById(R.id.pager);
        pager.setAdapter(mAdapter);

        PagerTabStrip pagerTabStrip = (PagerTabStrip) rootView.findViewById(R.id.pager_tab_strip);
        pagerTabStrip.setDrawFullUnderline(true);
        pagerTabStrip.setTabIndicatorColor(Color.parseColor("#f57a1d"));

        if(bundle != null){
            String type = bundle.getString("type");
            if(type.equals("Pickup"))
                pager.setCurrentItem(0);
            else
                pager.setCurrentItem(1);
        }

        getListDetail();
        load_not_done_reason();
        get_laundry_list();
        return rootView;
    }
    private void getListDetail(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rider_id", AppPrefrence.getMobile(AssignedFragment.this.getActivity()));
        get_vendor_detail_list(params);
    }
    private void get_vendor_detail_list(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                dbHelper = new PickHelper(getActivity());
                db = dbHelper.getWritableDatabase();
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    count_data = response.getJSONObject("count");
                    JSONArray data = response.getJSONArray("data");
                    String next_customer_id = response.getString("next_customer_id");

                    db.execSQL("DELETE FROM " + PickHelper.TABLE_ADDRESS + " WHERE " + PickHelper.KEY_STATUS + " = 'pending'");
                    db.execSQL("DELETE FROM " + PickHelper.TABLE_ADDRESS_DETAIL + " WHERE " + PickHelper.KEY_STATUS + " = 'pending'");
                    for (int i=0; i<data.length();i++){
                        JSONObject obj = data.getJSONObject(i);
                        ContentValues cv = new ContentValues();
                        cv.put(PickHelper.KEY_SO, obj.getString("salesorder_no"));
                        cv.put(PickHelper.KEY_NAME, obj.getString("name"));
                        cv.put(PickHelper.KEY_TIME, obj.getString("slot"));
                        cv.put(PickHelper.KEY_ADDRESS, obj.getString("address"));
                        cv.put(PickHelper.KEY_MOBILE, obj.getString("mobile"));
                        cv.put(PickHelper.KEY_SALESORDERID, obj.getString("salesorderid"));
                        cv.put(PickHelper.KEY_TYPE, obj.getString("type"));
                        cv.put(PickHelper.KEY_HUB, obj.getString("hub"));
                        cv.put(PickHelper.KEY_SOSTATUS, obj.getString("sostatus"));
                        cv.put(PickHelper.KEY_STATUS, "pending");
                        cv.put(PickHelper.KEY_PREFERENCE_COLOR, obj.getString("color"));
                        cv.put(PickHelper.KEY_DELIVERY_STATUS, obj.getString("status"));
                        cv.put(PickHelper.KEY_LAUNDRY, obj.getString("laundry"));
                        cv.put(PickHelper.KEY_PROC_STATUS, obj.getString("processing_status"));
                        if(obj.getString("salesorderid").equals(next_customer_id))
                            cv.put(PickHelper.KEY_NEXT_CUSTOMER, "true");
                        else
                            cv.put(PickHelper.KEY_NEXT_CUSTOMER, "false");


                        db.insert(PickHelper.TABLE_ADDRESS, null, cv);
                        ContentValues cv_detail = new ContentValues();
                        cv_detail.put(PickHelper.KEY_WASH_TYPE, obj.getString("wash_type"));
                        cv_detail.put(PickHelper.KEY_INSTRUCTION, obj.getString("instruction"));
                        cv_detail.put(PickHelper.KEY_ORDER_TYPE, obj.getString("order_type"));
                        cv_detail.put(PickHelper.KEY_BILL, obj.getString("total"));
                        cv_detail.put(PickHelper.KEY_PAYMENT_STATUS, obj.getString("payment_status"));
                        cv_detail.put(PickHelper.KEY_SALESORDERID_DETAIL, obj.getString("salesorderid"));
                        cv_detail.put(PickHelper.KEY_TYPE_DETAIL, obj.getString("type"));
                        cv_detail.put(PickHelper.KEY_STATUS, "pending");
                        if(obj.getString("salesorderid").equals(next_customer_id))
                            cv_detail.put(PickHelper.KEY_NEXT_CUSTOMER, "true");
                        else
                            cv_detail.put(PickHelper.KEY_NEXT_CUSTOMER, "false");
                        cv_detail.put(PickHelper.KEY_RATING, obj.getInt("rating"));
                        Log.i("rating", obj.getInt("rating")+"");

                        db.insert(PickHelper.TABLE_ADDRESS_DETAIL, null, cv_detail);
                    }
                    db.close();
                    mAdapter.setCount_data(count_data);
                    mAdapter.notifyDataSetChanged();

                    //update last updated time
                    SharedPreferences prefs = getActivity().getSharedPreferences("data_refresh", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("list_last_updated", Constant.get_current_time("yyyy-MM-dd HH:mm:ss"));
                    editor.commit();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "There was some error, please try again", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void taskException() {
                Toast.makeText(getActivity(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, AssignedFragment.this.getActivity(), Constant.get_assigned_list2, params);
        task.execute();
    }
    private void load_not_done_reason(){
        SharedPreferences prefs = getActivity().getSharedPreferences("not_done_reason", Context.MODE_PRIVATE);
        String last_updated = prefs.getString("last_updated", "1970-01-01");
        String curr_time = Constant.get_current_time("yyyy-MM-dd");

        long diff_in_sec = Constant.compare_dates(curr_time, last_updated, "yyyy-MM-dd");
        Log.i("not_done", Constant.get_current_time("yyyy-MM-dd")+last_updated);
        //if(diff_in_sec > (60 * 60 * 24)){
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("rider_id", AppPrefrence.getMobile(AssignedFragment.this.getActivity()));
            get_not_done_reason_list(params, prefs);
       // }
    }

    private void get_not_done_reason_list(Map<String, Object> param, final SharedPreferences prefs){

        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("reason_list", result);
                editor.putString("last_updated", Constant.get_current_time("yyyy-MM-dd"));
                editor.commit();
                Log.i("not_done", result);
            }

            @Override
            public void taskException() {
                Toast.makeText(getActivity(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, AssignedFragment.this.getActivity(), Constant.not_done_reason, param);
        task.execute();

    }
    private void get_laundry_list(){

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rider_id", AppPrefrence.getMobile(AssignedFragment.this.getActivity()));
        final SharedPreferences prefs = getActivity().getSharedPreferences("laundry_list", Context.MODE_PRIVATE);

        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("laundry_list", result);
                editor.commit();
            }

            @Override
            public void taskException() {
                Toast.makeText(getActivity(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, AssignedFragment.this.getActivity(), Constant.laundry_list, params);
        task.execute();

    }

    @Override
    public void onResume() {
        super.onResume();
        SharedPreferences prefs = getActivity().getSharedPreferences("data_refresh", Context.MODE_PRIVATE);
        String list_last_updated = prefs.getString("list_last_updated", "1970-01-01 00:00:00");
        String curr_time = Constant.get_current_time("yyyy-MM-dd HH:mm:ss");

        long diff_in_sec = Constant.compare_dates(curr_time, list_last_updated, "yyyy-MM-dd HH:mm:ss");
        Log.i("diff", diff_in_sec + "");
        if(diff_in_sec > (60 * 10)){
            getListDetail();
        }
    }
}
