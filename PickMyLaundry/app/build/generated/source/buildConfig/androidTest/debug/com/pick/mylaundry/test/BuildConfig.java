/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.pick.mylaundry.test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.pick.mylaundry.test";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 35;
  public static final String VERSION_NAME = "2.1.8";
}
