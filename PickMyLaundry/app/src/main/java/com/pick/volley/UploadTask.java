package com.pick.volley;
 

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import org.apache.http.NameValuePair;

import java.util.List;

public class UploadTask extends AsyncTask<String, String, String> {

	private Context context;
	private String url;
	private List<NameValuePair> nameValue;
	private  ProgressDialog pDialog = null;
	private Boolean isDialogVisible;
	 
	
	public interface TaskListner
	{
		public void taskResult(String result);
		public void taskException();
	}
	
	private TaskListner listner;

	public void dismissDialog(){
		if(pDialog!=null)
			pDialog.dismiss();
	}
 
	public UploadTask(TaskListner listner, Context context, String url, List<NameValuePair> nameValue, Boolean isDialogVisible) {
		super();
		this.context = context;
		this.url = url;
		this.nameValue = nameValue;
		this.listner=listner;
		this.isDialogVisible = isDialogVisible;
	}
	 
	@Override
	protected String doInBackground(String... params) {
	
		String json = null;
		try {
			ServiceHandler sh=new ServiceHandler();
			 json = sh.makeServiceCall(url, 2, nameValue);
		     } catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return json;
	}

	@Override
	protected void onPostExecute(String result) {
		
		super.onPostExecute(result);
		if(isDialogVisible)
		pDialog.dismiss();

		if (!EmptyUtility.isEmpty(result)) {
			listner.taskResult(result);
		} else {
			listner.taskException();
		}
	}

	@Override
	protected void onPreExecute() {
		
		super.onPreExecute();
		if(isDialogVisible){
			pDialog = new ProgressDialog(context);
			pDialog.setMessage("Loading...");
			pDialog.show();
			pDialog.setCancelable(false);
			pDialog.setCanceledOnTouchOutside(false);
		}
	}
}
