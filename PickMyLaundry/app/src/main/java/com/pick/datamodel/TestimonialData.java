package com.pick.datamodel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.pick.navAdapter.TestimonialAdapter;
import com.pick.navmodel.TestimonialModel;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.utill.Pickhelper;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 121 on 10/24/2016.
 */
public class TestimonialData {
    private ArrayList<TestimonialModel> modelArrayList = new ArrayList<TestimonialModel>();
    private Context context;
    private TestimonialAdapter testimonialAdapter;
    private Pickhelper dbhelper;
    private SQLiteDatabase db;

    public TestimonialData(Context context, TestimonialAdapter testimonialAdapter){
        this.context = context;
        this.testimonialAdapter = testimonialAdapter;
    }

    public ArrayList<TestimonialModel> getTestimonialData(){
        dbhelper=new Pickhelper(context);
        db=dbhelper.getWritableDatabase();
        db.execSQL(Pickhelper.CREATE_TABLE_TESTIMONIAL);
        String selectQuery = "SELECT * FROM "+Pickhelper.TABLE_TESTIMONIAL;
        Cursor cursor = db.rawQuery(selectQuery, null);
        modelArrayList.clear();
        if (cursor.moveToFirst()) {
            do {
                final TestimonialModel bean=new TestimonialModel();
                bean.setName(cursor.getString(1));
                bean.setProfession(cursor.getString(3));
                bean.setImage_url(cursor.getString(2));
                bean.setTestimonial(cursor.getString(4));
                modelArrayList.add(bean);
            } while (cursor.moveToNext());
        }
        db.close();
        testimonialAdapter.set_data(modelArrayList);
        get_data();
        return modelArrayList;
    }
    private void get_data(){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(context)));

        UploadTask uploadTask = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject object = new JSONObject(result);
                    if(object.getString("success").equals("true")){
                        JSONArray testimonial_arr = object.getJSONArray("data");
                        dbhelper = new Pickhelper(context);
                        db = dbhelper.getWritableDatabase();
                        String selectQuery = "DELETE FROM "+Pickhelper.TABLE_TESTIMONIAL;
                        db.execSQL(selectQuery);
                        modelArrayList.clear();
                        for (int i=0; i<testimonial_arr.length();i++) {
                            JSONObject testimonial_cust = testimonial_arr.getJSONObject(i);
                            ContentValues cv = new ContentValues();
                            cv.put(Pickhelper.KEY_NAME, testimonial_cust.getString("name"));
                            cv.put(Pickhelper.KEY_IMAGE_URL, testimonial_cust.getString("image_url"));
                            cv.put(Pickhelper.KEY_PROFEFSSION, testimonial_cust.getString("profession"));
                            cv.put(Pickhelper.KEY_TESTIMONIAL, testimonial_cust.getString("testimonial"));
                            db.insert(Pickhelper.TABLE_TESTIMONIAL, null, cv);
                            TestimonialModel bean=new TestimonialModel();
                            bean.setName(testimonial_cust.getString("name"));
                            bean.setProfession(testimonial_cust.getString("profession"));
                            bean.setImage_url(testimonial_cust.getString("image_url"));
                            bean.setTestimonial(testimonial_cust.getString("testimonial"));
                            modelArrayList.add(bean);
                        }
                        testimonialAdapter.set_data(modelArrayList);
                        db.close();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void taskException() {

            }
        },context, Constant.TESTIMONIAL,params, false);
        uploadTask.execute();
    }
}
