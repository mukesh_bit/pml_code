package com.pick.mylaundry;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.pick.navAdapter.ListAdapter;
import com.pick.navmodel.OrderListModel;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.EmptyUtility;
import com.pick.volley.UploadTask;
import com.pick.volley.UploadTask.TaskListner;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyBookingFragment  extends Fragment implements OnItemClickListener  {
	public ListAdapter adapter;
	private List<OrderListModel> orderList;
	public  MyBookingFragment CustomListView = null;
	private  ListView pickupListView;
	private TextView title;
    private ListView bookingList;
	private Button schedule;
	private View rootView, bc_view;
    private LinearLayout orderNotFound;
	public  ArrayList<OrderListModel> CustomListViewValuesArr = new ArrayList<OrderListModel>();
	private int page_count = 1;
	private ProgressBar progressBar;
	private Button btnLoadMore;
	private RelativeLayout booking_rl;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
							 Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		rootView = inflater.inflate(R.layout.mybooking, container, false);
		CustomListView = this;
		title=(TextView)getActivity().findViewById(R.id.toolbar_title);
		title.setText("MY ORDERS");

		//SET list data
		pickupListView = (ListView) rootView.findViewById(R.id.mybooking_list);
		booking_rl = (RelativeLayout) rootView.findViewById(R.id.booking_rl);
		((AdapterView<android.widget.ListAdapter>) pickupListView).setOnItemClickListener(this);
		add_more_button();
		setListData();

		//No order found
		orderNotFound = (LinearLayout) rootView.findViewById(R.id.ordernotfound);
		orderNotFound.setVisibility(View.INVISIBLE);
		schedule = (Button) rootView.findViewById(R.id.schedule);
		schedule.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), MainActivity.class);
				intent.putExtra("SESSION_ID", "0");
				intent.putExtra("id", 1);
				intent.putExtra("Context", "Review");
				getActivity().startActivity(intent);
			}
		});

		Map<String, String> articleParams = new HashMap<String, String>();
		articleParams.put("mobile", AppPrefrence.getMobile(getActivity()));
		FlurryAgent.logEvent("my_bookings", articleParams);

		return rootView;
	}

	/****** Function to set data in ArrayList *************/
	public void setListData()
	{
		List<NameValuePair> params=new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("primary_number",AppPrefrence.getMobile(getActivity())));
		params.add(new BasicNameValuePair("page_count", page_count+""));
		boolean isDialogVisible = true;
		if(page_count > 1)
			isDialogVisible = false;
		sendOrderstatusApi(params, isDialogVisible);
	}

	private void sendOrderstatusApi(List<NameValuePair> params, boolean isDialogVisible) {
		final int positionToSave = pickupListView.getFirstVisiblePosition();
		UploadTask task = new UploadTask(new TaskListner() {
			@Override
			public void taskResult(String result) {
				int index = pickupListView.getFirstVisiblePosition();
				View v = pickupListView.getChildAt(0);
				int top = (v == null) ? 0 : v.getTop();
				if(page_count == 1)
					orderList = new ArrayList<OrderListModel>();
				try {
					JSONObject json = new JSONObject(result);
					if(json.getString("success").equals("true")) {
						JSONArray orderlist1= json.getJSONArray("data");
						for(int i=0; i<orderlist1.length(); i++){
							JSONObject data = orderlist1.getJSONObject(i);
							final OrderListModel bean = new OrderListModel();
							bean.setOrderid(data.getString("OrderId"));
							bean.setSalesorder_id(data.getString("salesorder_no"));
							bean.setOrderdate(data.getString("OrderDate"));
							bean.setPickupdate(data.getString("PickupDate"));
							bean.setPickuptime(data.getString("PickupTime_Slot"));
							bean.setDeliveryDate(data.getString("DeliveryDate"));
							bean.setDeliveryTime(data.getString("DeliveryTime_Slot"));
							bean.setTotalbill(data.getString("TotalBill"));
							bean.setOrdderStatus(data.getString("OrderStatus"));
							bean.setProcessing_status(data.getString("processing_status"));
							bean.setInstruction(data.getString("instruction"));
							bean.setCategory(data.getString("category"));
							bean.setIsPreponable(data.getBoolean("isPreponable"));
							bean.setRider(data.getString("rider"));
							bean.setPayment_status(data.getString("payment_status"));
							bean.setContactid(data.getString("contactid"));
							bean.setTurnaround(data.getString("turnaround"));
							bean.setTotal_payable(data.getString("total_payable"));
							orderList.add(bean);
						}
                        orderNotFound.setVisibility(View.GONE);
						progressBar.setVisibility(View.GONE);
						if(!json.getBoolean("isMore"))
							btnLoadMore.setVisibility(View.GONE);
						else
							btnLoadMore.setVisibility(View.VISIBLE);
					}
					else if(json.getString("success").equals("false")) {
						pickupListView.setVisibility(View.GONE);
						orderNotFound.setVisibility(View.VISIBLE);
						booking_rl.setVisibility(View.GONE);
					}
					else if(page_count > 1){
						if(json.getString("msg").equals("Order Not Found"))
							Toast.makeText(getActivity(), "No more Orders", Toast.LENGTH_LONG).show();
						else
							Toast.makeText(getActivity(), "Some error Occurred", Toast.LENGTH_LONG).show();
					}

				} catch (JSONException e) {
					Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
					e.printStackTrace();
				}
				((ListView) pickupListView).setAdapter(new ListAdapter(getActivity(), orderList, pickupListView));
				((ListView) pickupListView).deferNotifyDataSetChanged();
				pickupListView.setSelectionFromTop(index, top);

			}

			@Override
			public void taskException() {
				Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
			}
		},getActivity(),Constant.OREDERLIST, params, isDialogVisible);

		task.execute();
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		String check=orderList.get(position).getOrderid().toString();
		if(!EmptyUtility.isEmpty(check)){
			Intent intent = new Intent(getActivity(), Invoice.class);
			intent.putExtra(Constant.ORDERID, orderList.get(position).getOrderid().toString());
			intent.putExtra("from", "My Orders");
			startActivity(intent);

			Map<String, String> articleParams = new HashMap<String, String>();
			articleParams.put("mobile", AppPrefrence.getMobile(getActivity()));
			articleParams.put("from", "myBookings");
			FlurryAgent.logEvent("Invoice", articleParams);
		}
	}
	private void add_more_button(){
		AbsListView.LayoutParams params = new AbsListView.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

		btnLoadMore = new Button(getActivity());
		btnLoadMore.setText("Load More Orders..");
		btnLoadMore.setLayoutParams(params);
		btnLoadMore.setTransformationMethod(null);
		btnLoadMore.setVisibility(View.GONE);
		btnLoadMore.setBackgroundResource(R.drawable.rectangle_border);
		pickupListView.addFooterView(btnLoadMore);

		progressBar = new ProgressBar(getActivity(), null, android.R.attr.progressBarStyleSmall);
		progressBar.setIndeterminate(true);
		progressBar.setVisibility(View.GONE);
		pickupListView.addFooterView(progressBar);

		btnLoadMore.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				btnLoadMore.setVisibility(View.GONE);
				progressBar.setVisibility(View.VISIBLE);
				page_count++;
				setListData();
			}
		});
	}
}
