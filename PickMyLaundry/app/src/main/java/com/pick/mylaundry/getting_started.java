package com.pick.mylaundry;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.pick.navAdapter.GetStartedAdapter;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 121 on 1/3/2016.
 */
public class getting_started extends FragmentActivity {
    private ViewPager startedPager;
    private GetStartedAdapter getStartedAdapter;
    private RelativeLayout mRelativeLayout;
    private ImageView[] circleList = new ImageView[4];
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.getting_started);
        mRelativeLayout = (RelativeLayout) findViewById(R.id.rootView);
        startedPager = (ViewPager) findViewById(R.id.started_pager);
        getStartedAdapter = new GetStartedAdapter(getSupportFragmentManager());
        startedPager.setAdapter(getStartedAdapter);
        circleList[0] = (ImageView) findViewById(R.id.imageindicator0);
        circleList[1] = (ImageView) findViewById(R.id.imageindicator1);
        circleList[2] = (ImageView) findViewById(R.id.imageindicator2);
        circleList[3] = (ImageView) findViewById(R.id.imageindicator3);

        Typeface tf = Typeface.createFromAsset(getAssets(), "font/helvetica-normal.ttf");
        final TextView get_started = (TextView) findViewById(R.id.get_started);
        get_started.setTypeface(tf);
        get_started.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getSharedPreferences("get_started", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("get_started_status", "done");
                editor.commit();
                if (AppPrefrence.getLoginStatus(getting_started.this)) {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                } else {
                    startActivity(new Intent(getApplicationContext(), MobileActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
                }
                FlurryAgent.logEvent("Getting_started_clicked");
                finish();
            }
        });

        startedPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < 4; i++)
                    circleList[i].setImageResource(R.drawable.g_circle);
                circleList[position].setImageResource(R.drawable.blackcircle);
                // Capture event data
                Map<String, String> articleParams = new HashMap<String, String>();
                articleParams.put("slide_no", position + "");
                //up to 10 params can be logged with each event
                FlurryAgent.logEvent("Getting_started_scroll", articleParams);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });


    }
    @Override
    protected void onStart() {
        super.onStart();
        //initialize flurry
        FlurryAgent.setLogEnabled(false);
        // init Flurry
        FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
        FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
        FlurryAgent.logEvent("Getting_started");
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
}
