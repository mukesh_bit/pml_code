package com.pick.utill;

import android.content.Context;
import android.content.Intent;

import com.pick.mylaundry.MainActivity;
import com.pick.mylaundry.WebviewOpener;

/**
 * Created by 121 on 10/26/2016.
 */
public class UriOpener {

    private Context mContext;

    public UriOpener(Context context){
        this.mContext = context;
    }
    public void open_link(String uri, String data){
        if(uri.startsWith("http")){
            Intent intent = new Intent(mContext, WebviewOpener.class);
            intent.putExtra("uri_data", data);
            mContext.startActivity(intent);
        }
        else if(uri.startsWith("app")){
            String screen_type = uri.split(":")[1];
            Intent intent = get_intent(screen_type);
            intent.putExtra("uri_data", data);
            mContext.startActivity(intent);
        }
    }
    public Intent get_intent(String screen_type){
        Intent intent = new Intent(mContext, MainActivity.class);
        if(screen_type.equals("refer")){
            intent.putExtra("SESSION_ID", "3");
        }
        else if(screen_type.equals("myCash")){
            intent.putExtra("SESSION_ID", "4");
        }
        else if(screen_type.equals("offers")){
            intent.putExtra("SESSION_ID", "5");
        }
        return intent;
    }
}
