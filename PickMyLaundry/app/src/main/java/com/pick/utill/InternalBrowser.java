package com.pick.utill;

import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by 121 on 10/26/2016.
 */
public class InternalBrowser extends WebViewClient {
    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }
}
