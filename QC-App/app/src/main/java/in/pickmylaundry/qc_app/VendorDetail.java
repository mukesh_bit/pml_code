package in.pickmylaundry.qc_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.Utils.Constant;
import in.pickmylaundry.Utils.UploadTask;
import in.pickmylaundry.adapter.VendorDetailAdapter;
import in.pickmylaundry.model.VendorDetailModel;

/**
 * Created by 121 on 3/10/2016.
 */
public class VendorDetail extends Activity {
    private ListView vendor_list;
    private UploadTask task;
    ArrayList<VendorDetailModel> list = new ArrayList<VendorDetailModel>();
    private Bundle bundle;
    private Button submit;
    private EditText so_et;
    private TextView pending, partial, scanned;
    private String status = "Pending";
    private String vendor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vendor_detail);

        vendor_list = (ListView) findViewById(R.id.vendor_list);
        TextView title = (TextView) findViewById(R.id.title);
        submit = (Button) findViewById(R.id.search);
        so_et = (EditText) findViewById(R.id.so);
        pending = (TextView) findViewById(R.id.pending);
        partial = (TextView) findViewById(R.id.partially);
        scanned = (TextView) findViewById(R.id.scanned);

        //get extras
        bundle = getIntent().getExtras();
        vendor = bundle.getString("vendor");
        title.setText(bundle.getString("vendor"));
        getListDetail();
        ImageView back =(ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VendorDetail.this, MainActivity.class);
                startActivity(intent);
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String so = so_et.getText().toString();
                checkSort(so);
            }
        });
        pending.setOnClickListener(listener);
        partial.setOnClickListener(listener);
        scanned.setOnClickListener(listener);



    }
    public View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            resetHeader();

            TextView clicked_btn = (TextView) findViewById(v.getId());
            status = clicked_btn.getText().toString();
            clicked_btn.setTextColor(getResources().getColor(R.color.white));
            clicked_btn.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            getListDetail();
        }
    };
    private void resetHeader(){
        pending.setBackgroundColor(getResources().getColor(R.color.dark_grey));
        partial.setBackgroundColor(getResources().getColor(R.color.dark_grey));
        scanned.setBackgroundColor(getResources().getColor(R.color.dark_grey));
        pending.setTextColor(getResources().getColor(R.color.black));
        partial.setTextColor(getResources().getColor(R.color.black));
        scanned.setTextColor(getResources().getColor(R.color.black));

    }
    private void getListDetail(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("vendor", vendor);
        params.put("status", status);
        get_vendor_detail_list(params);
    }
    private void get_vendor_detail_list(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(list.size()>0)
                        list.clear();
                    for(int i=0;i<response.length();i++){
                        JSONObject obj = response.getJSONObject(i+"");
                        VendorDetailModel model = new VendorDetailModel();
                        model.setSo(obj.getString("salesorder_no"));
                        model.setSalesorderid(obj.getInt("salesorderid"));
                        model.setCloth_desc(obj.getString("cloth_desc"));
                        model.setName(obj.getString("name"));
                        model.setTotal_cloth(obj.getInt("cf_839"));
                        model.setIs_qc(obj.getBoolean("is_qc"));
                        model.setOrder_type(obj.getString("cf_791"));
                        model.setProcessing_instruction(obj.getString("cf_875"));
                        JSONObject barcode_obj_list = obj.getJSONObject("barcode_list");
                        ArrayList<Map<String, String>> barcode_map_list = new ArrayList<Map<String, String>>();
                        for (int j=0;j<barcode_obj_list.length();j++){
                            Map<String, String> barcode = new HashMap<String, String>();
                            JSONObject barcode_obj = barcode_obj_list.getJSONObject(j+"");
                            barcode.put("barcode", barcode_obj.getString("barcode"));
                            barcode.put("type", barcode_obj.getString("type"));
                            barcode_map_list.add(barcode);
                        }
                        model.setBarcode_list(barcode_map_list);
                        list.add(model);
                    }
                    setVendor_list_click_listener();
                    vendor_list.setAdapter(new VendorDetailAdapter(VendorDetail.this, list));
                    ((ListView)vendor_list).deferNotifyDataSetChanged();


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(getApplicationContext(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, VendorDetail.this, Constant.get_vendor_detail, params);
        task.execute();
    }
    private void setVendor_list_click_listener(){
        //list item listener
        vendor_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.i("clicked", "clicked");
                Intent intent = new Intent(VendorDetail.this, BarcodeScanner.class);
                intent.putExtra("so", list.get(position).getSo());
                intent.putExtra("name", list.get(position).getName());
                intent.putExtra("desc", list.get(position).getCloth_desc());
                intent.putExtra("barcode", list.get(position).getBarcode_list());
                intent.putExtra("salesorderid", list.get(position).getSalesorderid());
                intent.putExtra("vendor", bundle.getString("vendor"));
                intent.putExtra("is_qc", list.get(position).getIs_qc());
                intent.putExtra("order_type", list.get(position).getOrder_type());
                intent.putExtra("processing_instruction", list.get(position).getProcessing_instruction());
                if(!status.equals("Scanned"))
                    startActivity(intent);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(VendorDetail.this, MainActivity.class);
        startActivity(intent);
    }
    private void checkSort(String so){
        if(so.equals("")){
            vendor_list.setAdapter(new VendorDetailAdapter(VendorDetail.this, list));
            ((ListView)vendor_list).deferNotifyDataSetChanged();
        }
        else{
            ArrayList<VendorDetailModel> sortedList = new ArrayList<VendorDetailModel>();
            for (int i=0;i<list.size();i++){
                VendorDetailModel model = list.get(i);
                if(model.getSo().toLowerCase().equals(so.toLowerCase())){
                    sortedList.add(model);
                }
            }
            vendor_list.setAdapter(new VendorDetailAdapter(VendorDetail.this, sortedList));
            ((ListView)vendor_list).deferNotifyDataSetChanged();
        }
    }
}
