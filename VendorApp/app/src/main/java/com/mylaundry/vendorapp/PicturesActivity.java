package com.mylaundry.vendorapp;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Dell on 9/17/2015.
 */
public class PicturesActivity extends AppCompatActivity {
    private String urlImageGet;
    private TextView salesorderid;
    private ListView listview;
    private PicturesAdapter picturesAdapter;
    private List<Image> pictures_list;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pictures);

        salesorderid = (TextView) findViewById(R.id.salesorderid);
        listview = (ListView) findViewById(R.id.pictures_list);

        Intent intent = getIntent();
        salesorderid.setText(intent.getStringExtra("salesorderid"));
        urlImageGet = "http://crm.pickmylaundry.in/web_services/vendor_app/getImages.php?salesorderid=" + salesorderid.getText().toString();
        pictures_list = new ArrayList<Image>();
        getImages();
    }
    private void getImages(){

        JsonArrayRequest imageListReq = new JsonArrayRequest(urlImageGet,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d("PicturesActivity", response.toString());
                        pictures_list.clear();
                        // Parsing json
                        for (int i = 0; i < response.length(); i++) {
                            try {

                                JSONObject obj = response.getJSONObject(i);
                                Image image = new Image();
                                image.setDescription(obj.getString("description"));
                                image.setImageUrl(obj.getString("imageUrl"));
                               // image.setPa(picturesAdapter);
                                //image.loadImage(picturesAdapter);

                                pictures_list.add(image);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                Toast.makeText(PicturesActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
                            }

                        }
                        //Toast.makeText(PicturesActivity.this,response.toString(),Toast.LENGTH_LONG).show();
                        // notifying list adapter about data changes
                        // so that it renders the list view with updated data
                        picturesAdapter = new PicturesAdapter(PicturesActivity.this, pictures_list);
                        //picturesAdapter.notifyDataSetChanged();
                        listview.setAdapter(picturesAdapter);
                        for (Image image : pictures_list) {
                            image.loadImage(picturesAdapter);

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("PicturesActivity", "Error: " + error.getMessage());
                Toast.makeText(PicturesActivity.this,error.getMessage(),Toast.LENGTH_LONG).show();
                error.printStackTrace();

            }
        });
        AppController.getInstance().addToRequestQueue(imageListReq);
    }
    }

