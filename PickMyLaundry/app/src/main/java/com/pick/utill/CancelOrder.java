package com.pick.utill;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.Window;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.pick.mylaundry.R;
import com.pick.volley.UploadTask;
import com.rey.material.widget.RadioButton;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 121 on 10/19/2016.
 */
public class CancelOrder {
    private Context context;
    private Dialog dialog;
    private RadioButton rb1,rb2,rb3,rb4;
    private String so;
    private CancelOrderInterface cancelOrderInterface;

    public interface CancelOrderInterface{
        public void onResult();
        public void onError();
    }

    public CancelOrder(CancelOrderInterface cancelOrderInterface, Context context, String so) {
        this.context = context;
        this.so = so;
        this.cancelOrderInterface = cancelOrderInterface;
    }
    public void cancel_order(){
        dialog = new Dialog(context, R.style.FullHeightDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.cancel_order);
        dialog.show();
        rb1 = (RadioButton) dialog.findViewById(R.id.rb1);
        rb2 = (RadioButton) dialog.findViewById(R.id.rb2);
        rb3 = (RadioButton) dialog.findViewById(R.id.rb3);
        rb4 = (RadioButton) dialog.findViewById(R.id.rb4);
        CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    rb1.setChecked(rb1 == buttonView);
                    rb2.setChecked(rb2 == buttonView);
                    rb3.setChecked(rb3 == buttonView);
                    rb4.setChecked(rb4 == buttonView);
                }

            }

        };

        rb1.setOnCheckedChangeListener(listener);
        rb2.setOnCheckedChangeListener(listener);
        rb3.setOnCheckedChangeListener(listener);
        rb4.setOnCheckedChangeListener(listener);
        TextView close = (TextView) dialog.findViewById(R.id.closeDialog);
        final TextView cancel_order = (TextView) dialog.findViewById(R.id.cancelNow);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        cancel_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String reason = "";
                if (rb1.isChecked())
                    reason = "Ordered By mistake";
                else if (rb2.isChecked())
                    reason = "Changed my minds";
                else if (rb3.isChecked())
                    reason = "Duplicate order";
                else if (rb4.isChecked())
                    reason = "Other";
                if (reason.equals(""))
                    Toast.makeText(context, "Please select a reason for cancellation", Toast.LENGTH_LONG).show();
                else
                    cancel_now(reason);
            }
        });
    }

    private void cancel_now(String reason){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(context)));
        params.add(new BasicNameValuePair("so", so));
        params.add(new BasicNameValuePair("reason", reason));
        dialog.dismiss();
        makeCancelRequest(params);
    }

    private void makeCancelRequest(List<NameValuePair> params) {
        new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        cancelOrderInterface.onResult();
                    }
                    else{
                        cancelOrderInterface.onError();
                    }

                } catch (JSONException e){
                    cancelOrderInterface.onError();
                    e.printStackTrace();
                }

            }

            @Override
            public void taskException() {
                cancelOrderInterface.onError();
            }
        },context, Constant.CANCELORDER,params, true).execute();

    }
}
