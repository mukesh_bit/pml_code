package com.pick.mylaundry;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.EmptyUtility;
import com.pick.volley.UploadTask;
import com.rey.material.widget.CheckBox;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 121 on 12/12/2015.
 */
public class  ScheduleActivity extends Activity{
    private TextView toolbar_title;
    private LinearLayout pickup_ll, delivery_ll;
    private ArrayList<String> washType = new ArrayList<String>();
    private String pickup_date, pickup_time, delivery_date, delivery_time;
    private TextView pickup_slot_tv, delivery_slot_tv;
    private CheckBox cb_pickup, cb_delivery;
    private RelativeLayout selectAddress, rootView;
    private ImageView back, next;
    private JSONObject timeslot_data = null;
    private Bundle bundle, data_bundle;
    private boolean isPickupEditable = true;
    private String salesorderid;
    private int turnaround;
    private String type = "regular";
    private TextView tip_tv;
    private RelativeLayout tip_ll;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_activity);
        rootView = (RelativeLayout) findViewById(R.id.rootView);
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/HelveticaNeue-Regular.ttf");

        //get extras
        bundle = getIntent().getExtras();
        data_bundle = bundle.getBundle("data_bundle");
        washType = data_bundle.getStringArrayList("washType");
        turnaround = data_bundle.getInt("turnaround");

        toolbar_title = (TextView) findViewById(R.id.actionbar_custom_title);
        toolbar_title.setText("SELECT TIMESLOT");
        toolbar_title.setTypeface(tf);
        pickup_ll = (LinearLayout) findViewById(R.id.pickup);
        delivery_ll = (LinearLayout) findViewById(R.id.delivery);
        pickup_slot_tv = (TextView) findViewById(R.id.pickup_slot);
        delivery_slot_tv = (TextView) findViewById(R.id.delivery_slot);
        cb_pickup = (CheckBox) findViewById(R.id.checkBox_pickup);
        cb_delivery = (CheckBox) findViewById(R.id.checkBox_delivery);
        selectAddress = (RelativeLayout) findViewById(R.id.selectAddress);
        back = (ImageView) findViewById(R.id.back_icon);
        next = (ImageView) findViewById(R.id.nextImage);
        tip_tv = (TextView) findViewById(R.id.tip_val);
        tip_ll = (RelativeLayout) findViewById(R.id.tip);


        //check if it is for rescheduling order
        if(bundle.containsKey("pickup_date")){
            type = "reschedule";
            reschedulingOrder();
        }
        else {
            //get timeslots data
            selectAddress.setOnClickListener(selectAddressListener);
            next.setOnClickListener(selectAddressListener);
        }

        //get timeslots data
        get_timeslot_data();

        //set touch listener
        selectAddress.setOnTouchListener(touchListener);

        //pickup slot listener
        pickup_ll.setOnClickListener(pickupListener);
        cb_pickup.setOnClickListener(pickupListener);

        //delivery slot listener
        delivery_ll.setOnClickListener(deliveryListener);
        cb_delivery.setOnClickListener(deliveryListener);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ScheduleActivity.this.onBackPressed();
            }
        });
    }
    private View.OnClickListener pickupListener =  new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(timeslot_data != null && isPickupEditable) {
                Intent intent = new Intent(ScheduleActivity.this, SchedulePopup.class);
                intent.putExtra("type", "Pickup");
                intent.putExtra("selection_type", type);
                intent.putStringArrayListExtra("washType", washType);
                intent.putExtra("turnaround", turnaround);
                try {
                    intent.putExtra("timeslot_data", timeslot_data.getJSONObject("Pickup").toString());
                    intent.putExtra("keys", timeslot_data.getJSONArray("keys").toString());
                    intent.putExtra("day_active", timeslot_data.getBoolean("day_active"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(bundle.containsKey("isPreponable"))
                    intent.putExtra("isPreponable", bundle.getBoolean("isPreponable"));

                startActivityForResult(intent, 1);
            }
            else if(!isPickupEditable){
                Snackbar.make(rootView, "You can not change pickup time now", Snackbar.LENGTH_SHORT).setActionTextColor(Color.WHITE).show();
            }
            else{
                get_timeslot_data();
                Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_SHORT).setActionTextColor(Color.WHITE).show();
            }
        }
    };
    private View.OnClickListener deliveryListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!EmptyUtility.isEmpty(pickup_date)){
                Intent intent = new Intent(ScheduleActivity.this, SchedulePopup.class);
                intent.putExtra("type", "Delivery");
                intent.putExtra("selection_type", type);
                intent.putExtra("turnaround", turnaround);
                intent.putExtra("pickup_slot", pickup_time);
                intent.putStringArrayListExtra("washType", washType);
                intent.putExtra("pickup_date", pickup_date);
                try {
                    intent.putExtra("timeslot_data", timeslot_data.getJSONObject("Delivery").toString());
                    intent.putExtra("keys", timeslot_data.getJSONArray("keys").toString());
                    intent.putExtra("day_active", timeslot_data.getBoolean("day_active"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(bundle.containsKey("isPreponable"))
                    intent.putExtra("isPreponable", bundle.getBoolean("isPreponable"));
                startActivityForResult(intent, 2);
            }
            else{
                Snackbar.make(rootView, "Please Select a Pickup Date", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
            }
        }
    };

    private View.OnClickListener selectAddressListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(EmptyUtility.isNotEmpty(pickup_date)&&EmptyUtility.isNotEmpty(delivery_date) && !delivery_date.equals("")) {
                Intent intent;
                if(AppPrefrence.getName(ScheduleActivity.this).equals("")) {
                    intent = new Intent(ScheduleActivity.this, LoginAcitivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    intent.putExtra("from", "address");
                }
                else
                    intent = new Intent(ScheduleActivity.this, ReviewActivity.class);
                intent.putStringArrayListExtra("washType", washType);
                intent.putExtra("pickup_date", pickup_date);
                intent.putExtra("pickup_time", pickup_time);
                intent.putExtra("delivery_date", delivery_date);
                intent.putExtra("delivery_time", delivery_time);
                intent.putExtra("data_bundle", data_bundle);
                startActivity(intent);
            }
            else if(EmptyUtility.isEmpty(pickup_date)){
                Snackbar.make(rootView, "Please Select a Pickup Date", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
            }
            else if(EmptyUtility.isEmpty(delivery_date)){
                Snackbar.make(rootView, "Please Select a Delivery Date", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
            }
        }
    };
    private void get_timeslot_data(){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(ScheduleActivity.this)));
        params.add(new BasicNameValuePair("type", type));
        params.add(new BasicNameValuePair("city", Constant.getLocation(this)));
        params.add(new BasicNameValuePair("wash_type", washType.toString()));
        send_timeslot_request(params);
    }
    private void send_timeslot_request(List<NameValuePair> params){
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        timeslot_data = json.getJSONObject("data");
                        String tip = json.getString("tip");
                        tip_tv.setText(tip);
                        tip_ll.setVisibility(View.VISIBLE);
                    } else if(json.getString("success").equals("false")){
                        Toast.makeText(getApplicationContext(), json.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e){
                    Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_SHORT).setActionTextColor(Color.WHITE).show();
                    e.printStackTrace();
                }
            }
            @Override
            public void taskException() {
                Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_SHORT).setActionTextColor(Color.WHITE).show();
            }
        },ScheduleActivity.this, Constant.GETTIMESLOT, params, true);

        task.execute();
    }
    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            next.setPressed(false);
            next.invalidate();
            next.setPressed(true);
            next.invalidate();
            return false;
        }
    };
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode != RESULT_CANCELED){
            pickup_time = data.getStringExtra("slot");
            pickup_date = data.getStringExtra("date");

            //set date and time
            setDateTime(pickup_slot_tv, pickup_date, pickup_time);

            if(!EmptyUtility.isEmpty(delivery_date)){
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date format_dd = null, format_pd = null;
                try {
                    format_dd = dateFormat.parse(delivery_date);
                    format_pd = dateFormat.parse(pickup_date);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(format_pd);
                calendar.add(Calendar.DATE, turnaround);
                format_pd = dateFormat.parse(dateFormat.format(calendar.getTime()));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(format_dd.compareTo(format_pd)<0||(format_dd.compareTo(format_pd)==0&&checkTimeslot(pickup_time, delivery_time))){
                    delivery_date = "";
                    delivery_time = "";
                    delivery_slot_tv.setText("Choose delivery slot");
                    cb_delivery.setChecked(false);
                }
            }
            cb_pickup.setChecked(true);
        }
        else if(requestCode == 1){
            if(EmptyUtility.isEmpty(pickup_date))
                cb_pickup.setChecked(false);
        }

        if(requestCode == 2 && resultCode != RESULT_CANCELED){
            delivery_time = data.getStringExtra("slot");
            delivery_date = data.getStringExtra("date");

            //set date and time
            setDateTime(delivery_slot_tv, delivery_date, delivery_time);
            cb_delivery.setChecked(true);
        }
        else if(requestCode == 2){
            if(EmptyUtility.isEmpty(delivery_date))
                cb_delivery.setChecked(false);
        }
    }
    private boolean checkTimeslot(String pickup_slot, String delivery_slot){
        String[] pickup_slot_arr = pickup_slot.split(" ");
        int pickup_slot_time = Integer.parseInt(pickup_slot_arr[0]);
        if(pickup_slot_arr[1].equals("PM"))
            pickup_slot_time += 12;
        String[] curr_slot_arr = delivery_slot.split(" ");
        int curr_slot_time = Integer.parseInt(curr_slot_arr[0]);
        if(curr_slot_arr[1].equals("PM"))
            curr_slot_time += 12;
        if(pickup_slot_time>curr_slot_time)
            return true;
        else
            return false;
    }
    private void setDateTime(TextView tv, String date_selected, String time){
        SimpleDateFormat parseFormat = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date date = parseFormat.parse(date_selected);
            SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM");
            tv.setText(dateFormat.format(date)+", "+time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(EmptyUtility.isNotEmpty(pickup_date)){
            cb_pickup.setChecked(true);
        }
        if(EmptyUtility.isNotEmpty(delivery_date)){
            cb_delivery.setChecked(true);
        }
    }
    private void reschedulingOrder(){
        pickup_date = bundle.getString("pickup_date");
        pickup_time = bundle.getString("pickup_time");
        delivery_date = bundle.getString("delivery_date");
        delivery_time = bundle.getString("delivery_time");
        String status = bundle.getString("status");
        salesorderid = bundle.getString("salesorderid");


        if(status.equals("Invoice Created") || status.equals("Ready for Delivery") || status.equals("Pickup Rider Assigned") || status.equals("Pickup Done")) {
            cb_pickup.setEnabled(false);
            isPickupEditable = false;
        }


        //set date and time
        setDateTime(pickup_slot_tv, pickup_date, pickup_time);
        if(EmptyUtility.isNotEmpty(delivery_date) && EmptyUtility.isNotEmpty(delivery_time) && !delivery_date.equals("1970-01-01") && !delivery_date.equals("0000-00-00"))
            setDateTime(delivery_slot_tv, delivery_date, delivery_time);

        TextView bottom_tv = (TextView) rootView.findViewById(R.id.bottom_tv);
        bottom_tv.setText("Reschedule");
        selectAddress.setOnClickListener(rescheduleListener);
        next.setOnClickListener(rescheduleListener);
    }
    View.OnClickListener rescheduleListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(EmptyUtility.isEmpty(pickup_date)){
                Snackbar.make(rootView, "Please Select a Pickup Date", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
            }
            else if(EmptyUtility.isEmpty(delivery_date)){
                Snackbar.make(rootView, "Please Select a Delivery Date", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
            }
            else {
                List<NameValuePair> params = new ArrayList<NameValuePair>();
                params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(ScheduleActivity.this)));
                params.add(new BasicNameValuePair("id", salesorderid));
                params.add(new BasicNameValuePair("pickup_date", pickup_date));
                params.add(new BasicNameValuePair("pickup_time", pickup_time));
                params.add(new BasicNameValuePair("delivery_date", delivery_date));
                params.add(new BasicNameValuePair("delivery_time", delivery_time));
                send_rescedule_request(params);
            }
        }
    };

    private void send_rescedule_request(List<NameValuePair> params){
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        Intent intent = new Intent(ScheduleActivity.this, Invoice.class);
                        intent.putExtra(Constant.ORDERID, salesorderid);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(intent);

                        Map<String, String> articleParams = new HashMap<String, String>();
                        articleParams.put("mobile", AppPrefrence.getMobile(ScheduleActivity.this));
                        articleParams.put("activity", "done");
                        FlurryAgent.logEvent("Reschedule", articleParams);

                    } else if(json.getString("success").equals("false")){
                        Toast.makeText(getApplicationContext(), json.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e){
                    Snackbar.make(rootView, "Please check your internet connection.", Snackbar.LENGTH_SHORT).setActionTextColor(Color.WHITE).show();
                    e.printStackTrace();
                }
            }
            @Override
            public void taskException() {
                Snackbar.make(rootView, "Please check your internet", Snackbar.LENGTH_SHORT).setActionTextColor(Color.WHITE).show();
            }
        },ScheduleActivity.this, Constant.RESCHEDULE, params, true);

        task.execute();
    }
    @Override
    protected void onStart() {
        super.onStart();

        FlurryAgent.setLogEnabled(false);
        FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
        FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

}
