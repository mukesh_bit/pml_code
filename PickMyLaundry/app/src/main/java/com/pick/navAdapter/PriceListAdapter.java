package com.pick.navAdapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.pick.mylaundry.PriceEstimator;
import com.pick.mylaundry.R;
import com.pick.navmodel.PriceModel;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.ImageLoader;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 121 on 12/9/2015.
 */
public class PriceListAdapter extends BaseAdapter {
    private ImageLoader imageLoader;
    private List<PriceModel> priceList;
    private Context context;
    private Map<String, Map<String, Integer >> cartItems;
    private Map<String, Integer> item;
    private int item_count;
    private ListView listView;
    private PriceListAdapter adapter;
    private View cartView;
    private TextView cartCount_tv,cartTotal_tv,tax, plus_sign, tax_text;
    private int cartCount, cartTotal;
    private RelativeLayout cart;
    private View rootView;
    private LayoutInflater mInflater;
    private ViewPager viewPager;
    private final int VIEW_HEADER = 0;
    private final int VIEW_ROW = 1;
    private RowViewHolder rowViewHolder;
    private HeaderViewHolder headerViewHolder;

    public PriceListAdapter(Context context, List<PriceModel> priceList, ListView listView, View rootView){
        this.imageLoader = new ImageLoader(context, 50, 500);
        this.priceList = priceList;
        this.context = context;
        this.listView = listView;
        this.rootView = rootView;
        cartView = PriceEstimator.getCartView();
        cart = (RelativeLayout) cartView.findViewById(R.id.cartView);
        cartCount_tv = (TextView) cartView.findViewById(R.id.cart_items);
        cartTotal_tv = (TextView) cartView.findViewById(R.id.cartTotal);
        tax = (TextView) cartView.findViewById(R.id.tax);
        plus_sign = (TextView) cartView.findViewById(R.id.plus_sign);
        tax_text = (TextView) cartView.findViewById(R.id.taxText);
        viewPager = (ViewPager) cartView.findViewById(R.id.viewpager);
        mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return priceList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        int viewType = this.getItemViewType(position);
        switch (viewType) {
            case 1:
                if(convertView == null) {
                    convertView = mInflater.inflate(R.layout.price_row, null);
                    adapter = this;
                    rowViewHolder = new RowViewHolder();
                    rowViewHolder.label_tv = (TextView) convertView.findViewById(R.id.label);
                    rowViewHolder.price_tv = (TextView) convertView.findViewById(R.id.price);
                    rowViewHolder.count_tv = (TextView) convertView.findViewById(R.id.count);
                    rowViewHolder.minus_tv = (TextView) convertView.findViewById(R.id.minus);
                    rowViewHolder.plus_tv = (LinearLayout) convertView.findViewById(R.id.plus);
                    rowViewHolder.image_iv = (ImageView) convertView.findViewById(R.id.cloth_thumbnail);
                    convertView.setTag(rowViewHolder);
                }
                else{
                    rowViewHolder = (RowViewHolder) convertView.getTag();
                }

                item_count = priceList.get(position).getCount();
                rowViewHolder.image_iv.setImageResource(R.drawable.logo_grey);
                imageLoader.DisplayImage(priceList.get(position).getImageUrl(), rowViewHolder.image_iv, "price", convertView);
                rowViewHolder.label_tv.setText(priceList.get(position).getLabel());
                rowViewHolder.price_tv.setText(priceList.get(position).getPrice());
                rowViewHolder.count_tv.setText(item_count + "");
                cartItems = PriceEstimator.getCart();
                rowViewHolder.plus_tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int item_count = priceList.get(position).getCount();
                        String category = priceList.get(position).getCategory();
                        int count = Integer.parseInt(cartCount_tv.getText().toString());
                        int price = Integer.parseInt(cartTotal_tv.getText().toString());
                        count++;
                        price += Integer.parseInt(priceList.get(position).getPrice());
                        int tax_value = (int) (price * 15) / 100;
                        cartCount_tv.setText(count + "");
                        cartTotal_tv.setText(price + "");
                        item_count++;
                        rowViewHolder.count_tv.setText(item_count + "");
                        priceList.get(position).setCount(item_count);
                        tax.setText(tax_value + "");
                        tax.setVisibility(View.VISIBLE);
                        tax_text.setVisibility(View.VISIBLE);
                        plus_sign.setVisibility(View.VISIBLE);
                        modifyCart(priceList.get(position).getLabel(), item_count, category);
                        adapter.notifyDataSetChanged();
                        if (count == 1) {
                            cart.setVisibility(View.VISIBLE);
                            viewPager.setPadding(0, 0, 0, (int) Constant.convertDpToPixel(40, context));
                        }
                        rowViewHolder.count_tv.setText(item_count + "");

                        Map<String, String> articleParams = new HashMap<String, String>();
                        articleParams.put("mobile", AppPrefrence.getMobile(context));
                        articleParams.put("product", priceList.get(position).getLabel());
                        articleParams.put("type", "increment");
                        FlurryAgent.logEvent("price_estimator_click", articleParams);
                    }
                });
                rowViewHolder.minus_tv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int item_count = priceList.get(position).getCount();
                        String category = priceList.get(position).getCategory();
                        if (item_count > 0) {
                            int count = Integer.parseInt(cartCount_tv.getText().toString());
                            int price = Integer.parseInt(cartTotal_tv.getText().toString());
                            count--;
                            price -= Integer.parseInt(priceList.get(position).getPrice());
                            int tax_value = (int) (price * 15) / 100;
                            tax.setText(tax_value + "");
                            cartCount_tv.setText(count + "");
                            cartTotal_tv.setText(price + "");
                            item_count--;
                            if (count == 0) {
                                cartTotal_tv.setText("0");
                                cartCount_tv.setText("0");
                                tax.setVisibility(View.INVISIBLE);
                                tax_text.setVisibility(View.INVISIBLE);
                                plus_sign.setVisibility(View.INVISIBLE);
                                cart.setVisibility(View.GONE);
                                viewPager.setPadding(0, 0, 0, (int) Constant.convertDpToPixel(0, context));
                            }
                        }
                        rowViewHolder.count_tv.setText(item_count + "");
                        priceList.get(position).setCount(item_count);
                        modifyCart(priceList.get(position).getLabel(), item_count, category);
                        adapter.notifyDataSetChanged();
                        Map<String, String> articleParams = new HashMap<String, String>();
                        articleParams.put("mobile", AppPrefrence.getMobile(context));
                        articleParams.put("product", priceList.get(position).getLabel());
                        articleParams.put("type", "decrement");
                        //up to 10 params can be logged with each event
                        FlurryAgent.logEvent("price_estimator_click", articleParams);
                    }
                });
                break;
            case 0:
                if(convertView == null) {
                    convertView = mInflater.inflate(R.layout.price_row_header, null);
                    headerViewHolder = new HeaderViewHolder();
                    headerViewHolder.header_tv = (TextView) convertView.findViewById(R.id.header);
                    headerViewHolder.help_view = (ImageView) convertView.findViewById(R.id.help);
                    convertView.setTag(headerViewHolder);
                }
                else{
                    headerViewHolder = (HeaderViewHolder) convertView.getTag();
                }
                headerViewHolder.header_tv.setText(priceList.get(position).getLabel());
                headerViewHolder.help_view.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
                        int width = displayMetrics.widthPixels;
                        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams((width * 3) / 4, (width * 3) / 4);
                        LinearLayout info_ll = (LinearLayout) rootView.findViewById(R.id.info_ll);
                        lp.addRule(RelativeLayout.CENTER_IN_PARENT);
                        info_ll.setLayoutParams(lp);
                        RelativeLayout info_rl = (RelativeLayout) rootView.findViewById(R.id.info_rl);
                        info_rl.setVisibility(View.VISIBLE);
                        ImageView info = (ImageView) rootView.findViewById(R.id.info);
                        info.setImageResource(R.drawable.logo_grey);
                        imageLoader.DisplayImage(priceList.get(position).getImageUrl(), info, "price", rootView);

                        Map<String, String> articleParams = new HashMap<String, String>();
                        articleParams.put("mobile", AppPrefrence.getMobile(context));
                        articleParams.put("product", "help");
                        articleParams.put("type", "help");
                        FlurryAgent.logEvent("price_estimator_click", articleParams);
                    }
                });
                break;

            }
        return convertView;
    }
    private void modifyCart(String label, int item_count, String category){
        if(cartItems.containsKey(category))
            item = cartItems.get(category);
        else
            item = new HashMap<String, Integer>();
        item.put(label, item_count);
        cartItems.put(category, item);
    }

    @Override
    public int getItemViewType(int position) {
        String itemType = priceList.get(position).getItemType();
        if(!itemType.equals("header")) {
            return VIEW_ROW;
        }
        else{
            return VIEW_HEADER;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }
    static class RowViewHolder{
        private TextView label_tv, price_tv, minus_tv, count_tv;
        private LinearLayout plus_tv;
        private ImageView image_iv;
    }
    static class HeaderViewHolder{
        private TextView header_tv;
        private ImageView help_view;
    }
}
