package in.pickmylaundry.riderapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.riderapp.Adapter.RecyclerViewAdapter;
import in.pickmylaundry.riderapp.utils.AppPrefrence;
import in.pickmylaundry.riderapp.utils.Constant;
import in.pickmylaundry.riderapp.utils.UploadTask;

public class MainActivity extends AppCompatActivity {
    private Toolbar toolbar;
    public DrawerLayout drawerLayout;
    RecyclerView recyclerView;
    String navTitles[];
    TypedArray navIcons;
    RecyclerView.Adapter recyclerViewAdapter;

    private ActionBarDrawerToggle drawerToggle;
    Context context;

    private boolean isRegIDSent = true;
    private boolean isUnRegistered = false;
    GoogleCloudMessaging gcm;
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    String regid;
    private Handler mHandler = new Handler();
    String SENDER_ID = "600458637781";

    private UploadTask task;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bundle = getIntent().getExtras();

        //Let's first set up toolbar
        setupToolbar();

        //Initialize Views
        recyclerView  = (RecyclerView) findViewById(R.id.recyclerView);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        //Setup Titles and Icons of Navigation Drawer
        navTitles = getResources().getStringArray(R.array.navDrawerItems);
        navIcons = getResources().obtainTypedArray(R.array.navDrawerIcons);

        recyclerViewAdapter = new RecyclerViewAdapter(navTitles,navIcons,this);
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Finally setup ActionBarDrawerToggle
        setupDrawerToggle();
        show_default_fragment();

        context = getApplicationContext();
        regid = getRegistrationId(context);
        mHandler.postDelayed(mRunnable, 1000);
    }
    private void setupToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }
    private void setupDrawerToggle(){
        drawerToggle = new ActionBarDrawerToggle(this,drawerLayout,toolbar,R.string.app_name,R.string.app_name);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        drawerToggle.syncState();
    }
    private void show_default_fragment(){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        Fragment squadFragment = new AssignedFragment();
        if(bundle != null)
            squadFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.containerView,squadFragment).addToBackStack(null);
        fragmentTransaction.commit();
    }
    private final Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            // Check device for Play Services APK. If check succeeds, proceed with
            //  GCM registration.
            SharedPreferences prefs = getGCMPreferences(getApplicationContext());
            Boolean loggedOut = prefs.getBoolean("loggedOut", false);
            if (checkPlayServices()&&!loggedOut) {
                gcm = GoogleCloudMessaging.getInstance(MainActivity.this);
                regid = getRegistrationId(context);
                //Log.i("register", "inside regid"+regid);
                if (regid.isEmpty() && !isUnRegistered) {
                    registerInBackground();
                    isRegIDSent = false;
                }
                else if(!isRegIDSent){
                    isRegIDSent = true;
                    postRegidRequest(regid);
                }

            } else {
                if(!isUnRegistered){
                    isUnRegistered = true;
                }
            }
            //findLocation();
            mHandler.postDelayed(mRunnable, 1000);
        }
    };
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            return "";
        }
        return registrationId;
    }

    private boolean checkPlayServices() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
                //showErrorDialog(status);
            } else {
                Toast.makeText(this, "This device is not supported for notification service", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }


    private void registerInBackground() {
        final AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                String msg = "";
                SharedPreferences prefs = getGCMPreferences(getApplicationContext());
                Boolean loggedOut = prefs.getBoolean("loggedOut", false);
                if (loggedOut){
                    //task.cancel(true);
                    return null;
                }
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    if (!loggedOut)
                        regid = gcm.register(SENDER_ID);
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                }
                return null;
            }

        }.execute();

    }

    private void unregisterInBackground() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    gcm.unregister();
                    regid = "";
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                }
                return null;
            }

        }.execute();

    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences("gcmregistartion",
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    private void postRegidRequest(String regid){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rider_id", AppPrefrence.getMobile(MainActivity.this));
        params.put("regid", regid);
        sendRegid(params);
    }

    private void sendRegid(Map<String, Object> params) {
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){

                    }
                    else{
                        Toast.makeText(MainActivity.this, "There was some error, please try again", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(MainActivity.this, "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, MainActivity.this, Constant.GCM_URL, params);
        task.execute();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);

    }
}
