package com.pick.mylaundry;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.EmptyUtility;
import com.pick.volley.UploadTask;
import com.pick.volley.UploadTask.TaskListner;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MobileActivity extends Activity implements OnClickListener {
	private EditText mobile;
 	private Button process;
	private String number;
 	private String user_id;
	private CheckBox checkbox_term;
    private TextView textTerms, title_text, mobile_title;
	private View rootView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		setContentView(R.layout.mobile_number);
		rootView = (RelativeLayout) findViewById(R.id.activityrootview);
		Typeface tf = Typeface.createFromAsset(getAssets(), "font/HelveticaNeue-Regular.ttf");
		title_text = (TextView) findViewById(R.id.upper_bar);
		title_text.setTypeface(tf);
		tf = Typeface.createFromAsset(getAssets(), "font/helvetica-normal.ttf");

		mobile=(EditText) findViewById(R.id.mobile_number_input);
        process=(Button) findViewById(R.id.mobile_number_send);
		mobile.setTypeface(tf);
		process.setTypeface(tf);
		mobile_title = (TextView) findViewById(R.id.title);
		mobile_title.setTypeface(tf);
        process.setOnClickListener(this);
		mobile.requestFocus();
        checkbox_term=(CheckBox) findViewById(R.id.checkbox_term);
        textTerms = (TextView)findViewById(R.id.textTerms);
		textTerms.setTypeface(tf);
		textTerms.setPaintFlags(textTerms.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        textTerms.setOnClickListener(this);
        //checkbox_term.setOnClickListener(this);

		FlurryAgent.logEvent("Mobile_no_Activity");

	  } 
   @SuppressLint("ShowToast")
	@SuppressWarnings("null")
	@Override
	public void onClick(View v) {
	   switch (v.getId()) {
	case R.id.mobile_number_send:
		Boolean check1 =checkbox_term.isChecked();
		if(check1==true){
			sendMobileApi(); 
		}else{
			Snackbar.make(rootView, "Please accept terms and conditions", Snackbar.LENGTH_SHORT).setActionTextColor(Color.WHITE).show();
		} 
		
		break;
	case R.id.textTerms:
			termtDialog();
		break;
	default:
		break;
	}
		 
		
	}

	private void sendMobileApi() {
		 number=mobile.getText().toString();
			if(EmptyUtility.isNotEmpty(number)){
			     	String s1=number.substring(0,3);
			      	if(mobile.length()==10){
			     		registerUser();
					}else if(mobile.length()==13 && s1.equals("+91")){
			         		registerUser();
					}else{
							Snackbar.make(rootView, "Please Provide a Valid Mobile Number", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
					}
			}else{
				Snackbar.make(rootView, "Please Enter Your Valid Number First", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
			}
			 
	
}

	private void registerUser() {
		AppPrefrence.setNumber(MobileActivity.this, number);
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.clear();
		params.add(new BasicNameValuePair("mobile", number));
		FlurryAgent.logEvent("send_otp_btn_clicked");
		sendCreateUserApi(params);
		Log.i("mobile", number);
	}
	@SuppressWarnings("deprecation")
	private void  sendCreateUserApi(List<NameValuePair> params	) { 
		 UploadTask task = new UploadTask(new TaskListner() {
		       @Override
					public void taskResult(String result) {
						try {
							JSONObject json = new JSONObject(result);

							if (json.getString("success").equals("true")) {
		                   // user_id=json.getString("contactid");
		                     saveStatus();
		                    startActivity(new Intent(getApplicationContext(), VerifyOtp.class));
		                   } else if(json.getString("success").equals("false")){
								FlurryAgent.logEvent("send_otp_btn_error");
								Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
							}
						} catch (JSONException e) {
							Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
							e.printStackTrace();
						}

					}

					private void saveStatus() {
					 // AppPrefrence.saveUserid(getApplicationContext(),user_id);
						AppPrefrence.saveUserInfo1(getApplicationContext(),number);  
				
					}

					@Override
					public void taskException() {
						Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
					}
				},MobileActivity.this,Constant.OTP,params, true);

				task.execute();

			
				 
			
	}

	protected void termtDialog() { 
		LayoutInflater inflater=LayoutInflater.from(MobileActivity.this);
		View contentView=inflater.inflate(R.layout.term_condition, null);
		final Dialog dialog=new Dialog(MobileActivity.this);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		 dialog.setContentView(contentView);
		 WebView terms=(WebView)contentView.findViewById(R.id.termdetail);
	     terms.loadUrl("file:///android_asset/term.html");  
		 Button close=(Button) contentView.findViewById(R.id.readterm);
		 close.setOnClickListener(new OnClickListener() {
			 @Override
			 public void onClick(View v) {
				 dialog.dismiss();

			 }
		 });
		 
		 dialog.show();
		  
		 
	}
	private class LoadProfileImage extends AsyncTask<String, Void,Bitmap> {

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			Bitmap output = Bitmap.createBitmap(result.getWidth(), result
					.getHeight(), Bitmap.Config.ARGB_8888);
			Canvas canvas = new Canvas(output);

			final int color = 0xff424242;
			final Paint paint = new Paint();
			final Rect rect = new Rect(0, 0,result.getWidth(), result.getHeight());
			final RectF rectF = new RectF(rect);
			final float roundPx = 10;

			paint.setAntiAlias(true);
			canvas.drawARGB(0, 0, 0, 0);
			paint.setColor(color);
			canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

			paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
			canvas.drawBitmap(result, rect, rect, paint);

			ContextWrapper cw = new ContextWrapper(getApplicationContext());
			File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
			File mypath=new File(directory,"profile.jpg");
			FileOutputStream fos = null;
			try {
				fos = new FileOutputStream(mypath);
				result.compress(Bitmap.CompressFormat.JPEG, 100, fos);
				fos.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
			AppPrefrence.saveUserImagePath(getApplicationContext(),directory.getAbsolutePath());
		 }
	}

	@Override
	protected void onStart() {
		super.onStart();
		//initialize flurry
		FlurryAgent.setLogEnabled(false);
		// init Flurry
		FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
		FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}
}
