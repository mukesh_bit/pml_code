package com.pick.mylaundry;

import android.animation.Animator;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.internal.view.ContextThemeWrapper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.utill.PaytmRecharge;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by qwew on 10-09-2015.
 */
public class MyCashFragment extends Fragment {
    private TextView credits, title, creditsmessage;
    private View rootView;
    private TextView cash_500,cash_1000,cash_2000;
    private EditText add_money_et;
    private Button add_money_btn;
    private Bundle bundle;
    private TextView history;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.mycash_layout, container, false);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "font/helvetica-normal.ttf");
        Typeface tf_regular = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaUltraComp.otf");
        title=(TextView)getActivity().findViewById(R.id.toolbar_title);
        title.setText("MY CASH");

        bundle = getArguments();

        credits = (TextView) rootView.findViewById(R.id.credits);
        cash_500 = (TextView) rootView.findViewById(R.id.cash1);
        cash_1000 = (TextView) rootView.findViewById(R.id.cash2);
        cash_2000 = (TextView) rootView.findViewById(R.id.cash3);
        history = (TextView) rootView.findViewById(R.id.history);

        add_money_et = (EditText) rootView.findViewById(R.id.add_cash_et);
        add_money_btn = (Button) rootView.findViewById(R.id.add_money_btn);

        makeCreditsRequest();

        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("mobile", AppPrefrence.getMobile(getActivity()));
        FlurryAgent.logEvent("myCash", articleParams);

        cash_500.setOnClickListener(cash_click_listener);
        cash_1000.setOnClickListener(cash_click_listener);
        cash_2000.setOnClickListener(cash_click_listener);

        add_money_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(add_money_et.getText().toString().length() > 0 && Integer.parseInt(add_money_et.getText().toString()) > 0){
                    getRechargeId();
                }
                else{
                    Toast.makeText(getActivity(), "Please enter some amount to add money", Toast.LENGTH_LONG).show();
                }
            }
        });

        history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), MyCashTransaction.class);
                startActivity(intent);

            }
        });

        add_money_et.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()>0) {
                    add_money_btn.setVisibility(View.VISIBLE);
                    add_money_et.setCursorVisible(true);
                }
                else
                    add_money_btn.setVisibility(View.GONE);
                add_money_et.setSelection(s.toString().length());

                if(s.toString().equals("500")){
                    reset_cash();
                    activate_cash_btn(cash_500);
                }
                else if(s.toString().equals("1000")){
                    reset_cash();
                    activate_cash_btn(cash_1000);
                }
                else if(s.toString().equals("2000")){
                    reset_cash();
                    activate_cash_btn(cash_2000);
                }
                else{
                    reset_cash();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        if(bundle !=null && bundle.containsKey("payment_status")){
            show_dialog(bundle.getString("payment_status"), bundle.getString("payment_amount"));
            add_money_et.setCursorVisible(false);
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }


        return rootView;
    }

    View.OnClickListener cash_click_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            add_money_et.setText(((TextView)v).getTag().toString());

        }
    };
    private void reset_cash(){
        cash_500.setTextColor(getResources().getColor(R.color.colorPrimary));
        cash_500.setBackgroundResource(R.drawable.customborder);
        cash_1000.setTextColor(getResources().getColor(R.color.colorPrimary));
        cash_1000.setBackgroundResource(R.drawable.customborder);
        cash_2000.setTextColor(getResources().getColor(R.color.colorPrimary));
        cash_2000.setBackgroundResource(R.drawable.customborder);
    }
    private void activate_cash_btn(TextView view){
        view.setTextColor(getResources().getColor(R.color.white));
        view.setBackgroundResource(R.drawable.button_bg_blue);
    }
    private void makeCreditsRequest(){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(getActivity())));
        sendPromoMessage(params);
    }

    private void sendPromoMessage(List<NameValuePair> params) {
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        credits.setText(getResources().getString(R.string.Rs)+" "+json.getString("credits"));
                        //creditsmessage.setText(json.getString("creditsMessage"));

                    } else if(json.getString("success").equals("false")){
                        credits.setText(0);
                    }

                } catch (JSONException e){
                    Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void taskException() {
                credits.setText("");

            }
        },MyCashFragment.this.getActivity(), Constant.GETCREDITS,params, true);

        task.execute();
    }
    private void getRechargeId(){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("mobile", AppPrefrence.getMobile(getActivity())));
        params.add(new BasicNameValuePair("amount", add_money_et.getText().toString()));

        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        String paytm_id = json.getString("id");
                        Log.i("paytm_id", paytm_id);
                        PaytmRecharge paytmInstance = null;
                        try {
                            paytmInstance = new PaytmRecharge(paytm_id, add_money_et.getText().toString(), json.getString("contactid"), getActivity());
                            paytmInstance.startPayment();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else if(json.getString("success").equals("false")){
                        if(json.getString("msg").equals("Login Problem")){
                            Intent intent = new Intent(getActivity(), LoginAcitivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            intent.putExtra("from", "My Cash");
                            startActivity(intent);
                            Toast.makeText(getActivity(), "Please login again before adding money", Toast.LENGTH_LONG).show();
                        }
                        else{
                            Toast.makeText(getActivity(), json.getString("msg"), Toast.LENGTH_LONG).show();
                        }

                    }

                } catch (JSONException e){
                    Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(getActivity(), "There was some error, please try again", Toast.LENGTH_LONG).show();
            }
        },MyCashFragment.this.getActivity(), Constant.GETRECHARGEID,params, true);

        task.execute();
    }
    private void show_dialog(String status, String amount) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.payment_success);
        dialog.getWindow().getAttributes().windowAnimations = R.style.up_down;

        //layout
        LinearLayout status_bar_ll = (LinearLayout) dialog.findViewById(R.id.status_bar_ll);
        ImageView status_bar_image = (ImageView) dialog.findViewById(R.id.status_bar_image);
        TextView status_tv = (TextView) dialog.findViewById(R.id.status);
        TextView status_messgae = (TextView) dialog.findViewById(R.id.status_messgae);
        Button status_btn = (Button) dialog.findViewById(R.id.status_btn);

        status_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        if(status.equals("Success")){
            status_bar_ll.setBackgroundResource(R.color.greenLight);
            status_bar_image.setImageResource(R.drawable.confirmed);
            status_tv.setText(status);
            status_messgae.setText("Your recharge of Rs "+amount+" was successful");
            status_btn.setBackgroundResource(R.drawable.button_bg_green);
        }
        else{
            status_bar_ll.setBackgroundResource(R.color.color_secondary);
            status_bar_image.setImageResource(R.drawable.close_full);
            status_tv.setText(status);
            status_messgae.setText("Your recharge of Rs "+amount+" was not successful");
            status_btn.setBackgroundResource(R.drawable.button_bg_orange);
        }

        dialog.show();
    }


}
