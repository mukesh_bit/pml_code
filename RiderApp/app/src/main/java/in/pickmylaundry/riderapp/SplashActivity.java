package in.pickmylaundry.riderapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import in.pickmylaundry.riderapp.utils.AppPrefrence;

/**
 * Created by 121 on 5/15/2016.
 */
public class SplashActivity extends Activity {

    private Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);


        if(AppPrefrence.getMobile(SplashActivity.this).equals("")){
            intent = new Intent(this, LoginActivity.class);
        }
        else{
            intent = new Intent(this, MainActivity.class);
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(intent);
                finish();
            }
        }, 1000);
    }
}
