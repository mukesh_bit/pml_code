package in.pickmylaundry.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.pickmylaundry.model.VendorDetailModel;
import in.pickmylaundry.qc_app.R;

/**
 * Created by 121 on 3/10/2016.
 */
public class VendorDetailAdapter extends BaseAdapter{
    private Context context;
    private ArrayList<VendorDetailModel> list;

    public VendorDetailAdapter(Context context, ArrayList<VendorDetailModel> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.list_item, null);
        TextView so = (TextView) convertView.findViewById(R.id.so);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView desc = (TextView) convertView.findViewById(R.id.desc);
        TextView total_cloth = (TextView) convertView.findViewById(R.id.total_cloth);
        so.setText(list.get(position).getSo());
        name.setText(list.get(position).getName());
        desc.setText(list.get(position).getCloth_desc());
        total_cloth.setText(list.get(position).getTotal_cloth() + "");
        if(list.get(position).getOrder_type().equals("Rework")){
            so.setTextColor(Color.parseColor("#ff0000"));
        }
        Log.i("list_", list.get(position).getSo());
        return convertView;
    }

}
