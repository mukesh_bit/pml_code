package in.pickmylaundry.riderapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.riderapp.Adapter.TabsPagerAdapter;
import in.pickmylaundry.riderapp.utils.AppPrefrence;
import in.pickmylaundry.riderapp.utils.Constant;
import in.pickmylaundry.riderapp.utils.PickHelper;
import in.pickmylaundry.riderapp.utils.UploadTask;

/**
 * Created by 121 on 5/11/2016.
 */
public class CompletedFragment extends Fragment implements ActionBar.TabListener {
    private UploadTask task;
    private View rootView;
    private ActionBar actionBar;
    private TabsPagerAdapter mAdapter;
    private String[] tabs = { "Pickp", "Delivery" };
    private Context context;
    private NetworkInfo activeNetwork;
    private boolean isConnected;
    private PickHelper dbHelper;
    private SQLiteDatabase db;
    private ViewPager pager;
    private JSONObject count_data = new JSONObject();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main, null);

        getActivity().setTitle("Completed Orders");

        mAdapter = new TabsPagerAdapter(getActivity().getSupportFragmentManager(), "completed");
        pager = (ViewPager) rootView.findViewById(R.id.pager);
        pager.setAdapter(mAdapter);

        PagerTabStrip pagerTabStrip = (PagerTabStrip) rootView.findViewById(R.id.pager_tab_strip);
        pagerTabStrip.setDrawFullUnderline(true);
        pagerTabStrip.setTabIndicatorColor(Color.parseColor("#f57a1d"));

        Log.i("mobie", AppPrefrence.getMobile(CompletedFragment.this.getActivity()));
        getListDetail();
        return rootView;
    }
    private void getListDetail(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rider_id", AppPrefrence.getMobile(CompletedFragment.this.getActivity()));
        get_vendor_detail_list(params);
    }
    private void get_vendor_detail_list(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                dbHelper = new PickHelper(getActivity());
                db = dbHelper.getWritableDatabase();
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    count_data = response.getJSONObject("count");
                    JSONArray data = response.getJSONArray("data");
                    String next_customer_id = response.getString("next_customer_id");

                    db.execSQL("DELETE FROM " + PickHelper.TABLE_ADDRESS + " WHERE "+ PickHelper.KEY_STATUS +" = 'completed'");
                    db.execSQL("DELETE FROM " + PickHelper.TABLE_ADDRESS_DETAIL + " WHERE "+ PickHelper.KEY_STATUS +" = 'completed'");
                    for (int i=0; i<data.length();i++){
                        JSONObject obj = data.getJSONObject(i);
                        ContentValues cv = new ContentValues();
                        cv.put(PickHelper.KEY_SO, obj.getString("salesorder_no"));
                        cv.put(PickHelper.KEY_NAME, obj.getString("name"));
                        cv.put(PickHelper.KEY_TIME, obj.getString("slot"));
                        cv.put(PickHelper.KEY_ADDRESS, obj.getString("address"));
                        cv.put(PickHelper.KEY_MOBILE, obj.getString("mobile"));
                        cv.put(PickHelper.KEY_SALESORDERID, obj.getString("salesorderid"));
                        cv.put(PickHelper.KEY_TYPE, obj.getString("type"));
                        cv.put(PickHelper.KEY_HUB, obj.getString("hub"));
                        cv.put(PickHelper.KEY_PROC_STATUS, obj.getString("processing_status"));
                        cv.put(PickHelper.KEY_STATUS, "completed");
                        cv.put(PickHelper.KEY_SOSTATUS, obj.getString("sostatus"));
                        cv.put(PickHelper.KEY_PREFERENCE_COLOR, obj.getString("color"));
                        cv.put(PickHelper.KEY_DELIVERY_STATUS, obj.getString("status"));
                        cv.put(PickHelper.KEY_LAUNDRY, obj.getString("laundry"));
                        if(obj.getString("salesorderid").equals(next_customer_id))
                            cv.put(PickHelper.KEY_NEXT_CUSTOMER, "true");
                        else
                            cv.put(PickHelper.KEY_NEXT_CUSTOMER, "false");
                        db.insert(PickHelper.TABLE_ADDRESS, null, cv);
                        ContentValues cv_detail = new ContentValues();
                        cv_detail.put(PickHelper.KEY_WASH_TYPE, obj.getString("wash_type"));
                        cv_detail.put(PickHelper.KEY_INSTRUCTION, obj.getString("instruction"));
                        cv_detail.put(PickHelper.KEY_ORDER_TYPE, obj.getString("order_type"));
                        cv_detail.put(PickHelper.KEY_BILL, obj.getString("total"));
                        cv_detail.put(PickHelper.KEY_PAYMENT_STATUS, obj.getString("payment_status"));
                        cv_detail.put(PickHelper.KEY_SALESORDERID_DETAIL, obj.getString("salesorderid"));
                        cv_detail.put(PickHelper.KEY_TYPE_DETAIL, obj.getString("type"));
                        cv_detail.put(PickHelper.KEY_STATUS, "completed");
                        if(obj.getString("salesorderid").equals(next_customer_id))
                            cv_detail.put(PickHelper.KEY_NEXT_CUSTOMER, "true");
                        else
                            cv_detail.put(PickHelper.KEY_NEXT_CUSTOMER, "false");
                        cv_detail.put(PickHelper.KEY_RATING, obj.getInt("rating"));
                        db.insert(PickHelper.TABLE_ADDRESS_DETAIL, null, cv_detail);
                    }
                    db.close();
                    mAdapter.setCount_data(count_data);
                    pager.getAdapter().notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(getActivity(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, CompletedFragment.this.getActivity(), Constant.get_completed_list2, params);
        task.execute();
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }
}
