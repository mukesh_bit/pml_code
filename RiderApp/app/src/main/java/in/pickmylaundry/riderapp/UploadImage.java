package in.pickmylaundry.riderapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

import in.pickmylaundry.riderapp.utils.AppPrefrence;
import in.pickmylaundry.riderapp.utils.Constant;

/**
 * Created by 121 on 9/21/2016.
 */
public class UploadImage extends AsyncTask<Void, Integer, String> {

    private ProgressDialog progress;
    private Context context;
    private String uri;
    private long totalSize = 0;
    private ContentBody foto;
    private String image_type, image_shift, reading, time;

    public interface AsyncResponse {
        void imageResponse(String output);
    }
    public AsyncResponse delegate = null;

    public UploadImage(Context context, String uri, ContentBody foto, String image_type, String image_shift, String reading, String time, AsyncResponse delegate){
        this.context = context;
        this.uri = uri;
        this.foto = foto;
        this.image_shift = image_shift;
        this.image_type = image_type;
        this.reading = reading;
        this.time = time;
        this.delegate = delegate;
    }


    @Override
    protected void onPreExecute() {
        progress = new ProgressDialog(context);
        progress.setMessage("Uploading Image...");
        progress.show();
        super.onPreExecute();
    }

    @Override
    protected void onProgressUpdate(Integer... progress) {
        super.onProgressUpdate(progress);
    }

    @Override
    protected String doInBackground(Void... params) {
        return uploadFile();
    }

    @SuppressWarnings("deprecation")
    private String uploadFile() {
        String responseString = null;

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost httppost = new HttpPost(Constant.image_post);

        try {
            AndroidMultipartEntity entity = new AndroidMultipartEntity(
                    new AndroidMultipartEntity.ProgressListener() {

                        @Override
                        public void transferred(long num) {
                            publishProgress((int) ((num / (float) totalSize) * 100));
                        }
                    });

            entity.addPart("image", foto);
            entity.addPart("rider_id", new StringBody(AppPrefrence.getMobile(context)));
            entity.addPart("type", new StringBody(image_type));
            entity.addPart("reading", new StringBody(reading));
            entity.addPart("shift", new StringBody(image_shift));
            entity.addPart("time", new StringBody(time));

            totalSize = entity.getContentLength();
            httppost.setEntity(entity);

            // Making server call
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity r_entity = response.getEntity();

            int statusCode = response.getStatusLine().getStatusCode();

            if (statusCode == 200) {
                // Server response
                responseString = EntityUtils.toString(r_entity);
            } else {
                Toast.makeText(context, "Some error occured", Toast.LENGTH_SHORT).show();
            }
            Log.i("upload", "status"+ statusCode+") res-"+ responseString);

        } catch (ClientProtocolException e) {
            responseString = e.toString();
        } catch (IOException e) {
            responseString = e.toString();
        }

        return responseString;

    }

    @Override
    protected void onPostExecute(String result) {
        try{
            progress.dismiss();
            progress=null;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        delegate.imageResponse(result);
    }

}
