package com.pick.volley;

 
import java.util.List;
 
public class EmptyUtility extends Object {
 
	public static <E> boolean isEmpty(List<E> value) {
		return value == null || value.size() == 0 ? true : false;
	}
 
	public static boolean isEmpty(String value) {
		return value == null || value.trim().length() == 0 ? true : false;
	}

	 
	public static <E> boolean isNotEmpty(List<E> value) {
		return !isEmpty(value);
	}

	 
	public static boolean isNotEmpty(String value) {
		return !isEmpty(value);
	}

}
