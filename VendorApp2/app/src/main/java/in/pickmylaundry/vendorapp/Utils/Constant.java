package in.pickmylaundry.vendorapp.Utils;

/**
 * Created by 121 on 1/31/2016.
 */
public class Constant {
    public final static String GET_REQUEST = "GET";
    public final static String POST_REQUEST = "POST";
    public final static String get_so_detail = "http://crm.pickmylaundry.in/web_services/vendor_app/get_so_info.php";

    public final static String UPLOAD_IMAGE = "http://crm.pickmylaundry.in/web_services/vendor_app/test.php";
    public final static String GET_HISTORY = "http://crm.pickmylaundry.in/web_services/vendor_app/get_history.php";
    public final static String SET_REMINDER = "http://crm.pickmylaundry.in/web_services/vendor_app/set_reminder.php";
    public static final String ACCEPT = "http://crm.pickmylaundry.in/web_services/vendor_app/accept.php";
    public static final String LOGIN = "http://crm.pickmylaundry.in/web_services/vendor_app/login.php";
    public static final String ACCEPT_URL = "http://crm.pickmylaundry.in/web_services/vendor_app/get_accept_list.php";
    public static final String ACCEPT_ORDER = "http://crm.pickmylaundry.in/web_services/vendor_app/accept_order.php";
    public static final String READY_URL = "http://crm.pickmylaundry.in/web_services/vendor_app/get_ready_list.php";
    public static final String READY_ORDER = "http://crm.pickmylaundry.in/web_services/vendor_app/ready_order.php";


    public static final String GCM_URL = "http://crm.pickmylaundry.in/web_services/vendor_app/register_gcm.php";
}
