package in.pickmylaundry.riderapp.utils;


        import android.graphics.Bitmap;

        import java.io.BufferedReader;
        import java.io.DataOutputStream;
        import java.io.IOException;
        import java.io.InputStream;
        import java.io.InputStreamReader;
        import java.net.HttpURLConnection;
        import java.net.URL;
        import java.net.URLEncoder;
        import java.util.Map;

        import okhttp3.Call;
        import okhttp3.Callback;
        import okhttp3.FormBody;
        import okhttp3.OkHttpClient;
        import okhttp3.Request;
        import okhttp3.RequestBody;
        import okhttp3.Response;

/**
 * Created by 121 on 1/31/2016.
 */
public class ServiceHandler {
    private URL url;
    private HttpURLConnection conn;
    private String encodedParameter ="";
    private String line;
    private String request_url;
    private String request_type;
    private Map<String, Object> params;
    private StringBuffer response = new StringBuffer();

    public ServiceHandler(String request_url, String request_type, Map<String, Object> params)  {
        this.request_url = request_url;
        this.request_type = request_type;
        this.params = params;
    }

    public String makeCall(){
        OkHttpClient client = new OkHttpClient();

        FormBody.Builder formBody = new FormBody.Builder();
        for (Map.Entry<String, Object> entry : params.entrySet())
        {
            formBody.add(entry.getKey(), entry.getValue().toString());
        }
        RequestBody requestBody = formBody.build();

        Request request = new Request.Builder()
                .url(request_url)
                .post(requestBody)
                .build();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";

    }
}
