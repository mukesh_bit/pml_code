package in.pickmylaundry.riderapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.pickmylaundry.riderapp.Model.IncentiveModel;
import in.pickmylaundry.riderapp.R;

/**
 * Created by zomato on 18/07/16.
 */
public class IncentiveListAdapter extends BaseAdapter {

    private ArrayList<IncentiveModel> incentiveModelArrayList = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private Context context;

    public IncentiveListAdapter(Context context, ArrayList<IncentiveModel> incentiveModelArrayList){
        this.incentiveModelArrayList = incentiveModelArrayList;
        this.context = context;
        layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return incentiveModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = layoutInflater.inflate(R.layout.incentive_item, parent, false);
        }

        TextView date = (TextView) convertView.findViewById(R.id.date);
        TextView pd = (TextView) convertView.findViewById(R.id.pd);
        TextView pl_dc = (TextView) convertView.findViewById(R.id.pl_dc);
        TextView marketing = (TextView) convertView.findViewById(R.id.marketing);
        TextView incentive = (TextView) convertView.findViewById(R.id.incentive);

        date.setText(incentiveModelArrayList.get(position).getDate());
        pd.setText(incentiveModelArrayList.get(position).getPickup()+","+incentiveModelArrayList.get(position).getDelivery());
        pl_dc.setText(incentiveModelArrayList.get(position).getPl()+","+incentiveModelArrayList.get(position).getDc());
        marketing.setText(incentiveModelArrayList.get(position).getCoupon()+"");
        incentive.setText(incentiveModelArrayList.get(position).getIncentive()+"");

        return convertView;
    }
}
