package com.pick.mylaundry;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.flurry.android.FlurryAgent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.EmptyUtility;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoginAcitivity extends Activity implements OnClickListener,
        ConnectionCallbacks, OnConnectionFailedListener {


    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private static final int PROFILE_PIC_SIZE = 100;
    private ConnectionResult mConnectionResult;
    private Button btnSignIn;
    private int i=1;
    private Button loginButton, manual_button;
    private RelativeLayout splash_image;
    private  ProgressDialog pDialog;
    private Context con;
    private View rootView;
    private CallbackManager callbackManager;
    private Bundle bundle;
    private EditText name_et, email_et;
    private String requested_screen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //get bundle data
        bundle = getIntent().getExtras();
        requested_screen = bundle.getString("from");

        con=LoginAcitivity.this;
        FacebookSdk.sdkInitialize(LoginAcitivity.this);

        setContentView(R.layout.login_layout);
        rootView = (LinearLayout) findViewById(R.id.root);
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/helvetica-normal.ttf");
        name_et = (EditText) rootView.findViewById(R.id.name);
        email_et = (EditText) rootView.findViewById(R.id.email);

        btnSignIn = (Button) findViewById(R.id.btn_sign_in);
        btnSignIn.setOnClickListener(this);
        loginButton = ( Button) findViewById(R.id.loginfacebook);
        loginButton.setOnClickListener(this);
        manual_button = ( Button) findViewById(R.id.login_button);
        manual_button.setOnClickListener(this);
        callbackManager = CallbackManager.Factory.create();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .build();
      }
    
  protected void onStart() {
       super.onStart();
       mGoogleApiClient.connect();
      //initialize flurry
      FlurryAgent.setLogEnabled(false);
      // init Flurry
      FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
      FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
      FlurryAgent.logEvent("Login_screen");
    }
 
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        FlurryAgent.onEndSession(this);
    }

    /**
     * Method to resolve any signin errors
     * */
    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }
 
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    0).show();
            
            return;
        }
 
        if (!mIntentInProgress) {
            // Store the Connectieult for later usage
            mConnectionResult = result;
 
            if (mSignInClicked) {
                 resolveSignInError();
            }
        }
 
    }
 
    @Override
    protected void onActivityResult(int requestCode, int responseCode,
            Intent intent) {
    	
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }
             mIntentInProgress = false;
             if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
        super.onActivityResult(requestCode, responseCode, intent);
        callbackManager.onActivityResult(requestCode, responseCode, intent);
    }
 
    @Override
    public void onConnected(Bundle arg0) {
    	  mSignInClicked = false;
       // Toast.makeText(this, "User is connected!", Toast.LENGTH_LONG).show();
          getProfileInformation();
    	 
    }

    private void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String url=currentPerson.getImage().getUrl();
                String personPhotoUrl = url.substring(0,
                		url.length() - 2)+PROFILE_PIC_SIZE;

                AppPrefrence.setProfile(getApplicationContext(),personPhotoUrl);
                currentPerson.getUrl();
                String email=Plus.AccountApi.getAccountName(mGoogleApiClient);
                currentPerson.getBirthday();
                currentPerson.getGender();
                currentPerson.getAboutMe();
                currentPerson.getRelationshipStatus();
                AppPrefrence.saveUserInfo(getApplicationContext(), personName, email);
                signOutFromGplus();
                if(pDialog!=null)
                    pDialog.dismiss();

               new LoadProfileImage().execute(AppPrefrence.getProfile(LoginAcitivity.this));
               request_registeration(personName, email, "Via Google", personPhotoUrl);
               } else {
                pDialog.dismiss();
                Toast.makeText(LoginAcitivity.this, "Profile access permission denied by user", Toast.LENGTH_LONG).show();
                signOutFromGplus();
            }
        } catch (Exception e) {
            e.printStackTrace();
            pDialog.dismiss();
        }
    }
    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }
    @Override
    public void onClick(View v) {
        if(Constant.isConnectingToInternet(LoginAcitivity.this)){
            switch (v.getId()) {
                case R.id.btn_sign_in:
                    pDialog = new ProgressDialog(LoginAcitivity.this);
                    pDialog.setMessage("Logging Please wait..");
                    pDialog.show();
                    signInWithGplus();
                    FlurryAgent.logEvent("Google_button_clicked");
                    break;
                case R.id.loginfacebook:
                     Log.i("fb", "yes");
                     registerCallback();
                     LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile"));
                     FlurryAgent.logEvent("Facebook_button_clicked");
                     break;
                case R.id.login_button:
                    register_manually();
                    break;
            }
        }
        else{
         Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
        }
    }

    private void registerCallback(){
        Log.i("fb", "callback");
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // App code
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                Log.i("fb", "completed");
                                try {
                                    String name = object.getString("name");
                                    String email;
                                    if (!object.has("email")) {
                                        email = "";
                                    } else {
                                        email = object.getString("email");
                                    }
                                    String id = object.getString("id");
                                    JSONObject pic = object.getJSONObject("picture");
                                    JSONObject pic1 = pic.getJSONObject("data");
                                    String Profileurl = pic1.getString("url");
                                    AppPrefrence.setProfile(LoginAcitivity.this, Profileurl);
                                    AppPrefrence.saveUserInfo(LoginAcitivity.this, name, email);
                                    AppPrefrence.saveLoginVia(LoginAcitivity.this, "Via Facebook");
                                    LoginManager.getInstance().logOut();
                                    new LoadProfileImage().execute(AppPrefrence.getProfile(LoginAcitivity.this));
                                    request_registeration(name, email, "Via Facebook", Profileurl);
                                    FlurryAgent.logEvent("Facebook_logged_in");
                                } catch (JSONException e) {
                                    // TODO Auto-generated catch block
                                    e.printStackTrace();
                                    Log.v("LoginActivity", e.toString());
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,picture.width(100).height(100)");
                request.setParameters(parameters);
                request.executeAsync();
                Log.i("fb", "pending");
            }

            @Override
            public void onCancel() {
                Log.i("fb", "canceled");
            }


            @Override
            public void onError(FacebookException error) {
                Log.v("LoginActivity", error.toString());

            }
        } );
    }
 

    private void signInWithGplus() {
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }
    private void signOutFromGplus() {
        if (mGoogleApiClient.isConnected()) {
        	 Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
             mGoogleApiClient.disconnect();
            
             
        }
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }
    private class LoadProfileImage extends AsyncTask<String, Void,Bitmap> {

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            Bitmap output = Bitmap.createBitmap(result.getWidth(), result
                    .getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0,result.getWidth(), result.getHeight());
            final RectF rectF = new RectF(rect);
            final float roundPx = 10;

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(result, rect, rect, paint);

            ContextWrapper cw = new ContextWrapper(LoginAcitivity.this);
            File directory = cw.getDir("imageDir", Context.MODE_PRIVATE);
            File mypath=new File(directory,"profile.jpg");
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(mypath);
                result.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            AppPrefrence.saveUserImagePath(LoginAcitivity.this,directory.getAbsolutePath());
        }
    }

    private void request_registeration(String name, String email, String medium, String url){
        if(EmptyUtility.isEmpty(email)){
            name_et.setText(name);
            Toast.makeText(LoginAcitivity.this, "There was some error while fetching email", Toast.LENGTH_LONG).show();
        }
        else if(EmptyUtility.isEmpty(name)){
            Toast.makeText(LoginAcitivity.this, "Please enter your name", Toast.LENGTH_LONG).show();
        }
        else{
            List<NameValuePair> params=new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("mobile",AppPrefrence.getMobile(LoginAcitivity.this)));
            params.add(new BasicNameValuePair("name",name));
            params.add(new BasicNameValuePair("email",email));
            params.add(new BasicNameValuePair("medium",medium));
            //params.add(new BasicNameValuePair("url",url));
            params.add(new BasicNameValuePair("source","android_app"));
            register(params);
        }
    }
    private void register(List<NameValuePair> params){
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if (json.getString("success").equals("true")) {
                        //save promo code
                        AppPrefrence.savePromoCode(LoginAcitivity.this, json.getString("code"));

                        Intent intent = null;
                        if(requested_screen.equals("address")) {
                            intent = new Intent(LoginAcitivity.this, ReviewActivity.class);
                            intent.putExtra("bundle_login", bundle);
                        }
                        else if(requested_screen.equals("INVITE AND EARN")) {
                            intent = new Intent(LoginAcitivity.this,MainActivity.class);
                            intent.putExtra("SESSION_ID", "3");
                            intent.putExtra("id",3);
                            intent.putExtra("Context", "Login");
                            Log.i("invite","yes");
                        }
                        else if(requested_screen.equals("MYCASH WALLET")) {
                            intent = new Intent(LoginAcitivity.this,MainActivity.class);
                            intent.putExtra("SESSION_ID", "4");
                            intent.putExtra("id",4);
                            intent.putExtra("Context", "Login");
                        }
                        if(requested_screen.equals("ME")) {
                            intent = new Intent(LoginAcitivity.this,MainActivity.class);
                            intent.putExtra("SESSION_ID", "8");
                            intent.putExtra("id",8);
                            intent.putExtra("Context","Login");
                            Log.i("me", "yes");
                        }
                        if(intent != null)
                            startActivity(intent);
                        finish();
                    } else if(json.getString("success").equals("false")){
                        FlurryAgent.logEvent("send_otp_btn_error");
                        Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    }
                } catch (JSONException e) {
                    Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    e.printStackTrace();
                }

            }
            @Override
            public void taskException() {
                Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
            }
        },LoginAcitivity.this,Constant.CONTACT,params, true);

        task.execute();
    }
    private void register_manually(){
        String name = name_et.getText().toString().trim();
        String email = email_et.getText().toString().trim();
        Log.i("register", "manual");
        if(name.equals(""))
            Toast.makeText(LoginAcitivity.this, "Please enter your full name", Toast.LENGTH_LONG).show();
        else if(email.equals("") || !isValidEmailAddress(email))
            Toast.makeText(LoginAcitivity.this, "Please enter a valid email address", Toast.LENGTH_LONG).show();
        else{
            AppPrefrence.saveUserInfo(LoginAcitivity.this, name, email);
            AppPrefrence.saveLoginVia(LoginAcitivity.this, "Default");
            request_registeration(name,email,"manual", "default");
        }
    }

    public boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
        java.util.regex.Matcher m = p.matcher(email);
        return m.matches();
    }

}

  
 
