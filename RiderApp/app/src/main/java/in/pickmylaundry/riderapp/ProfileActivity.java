package in.pickmylaundry.riderapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.riderapp.utils.AppPrefrence;
import in.pickmylaundry.riderapp.utils.Constant;
import in.pickmylaundry.riderapp.utils.UploadTask;

/**
 * Created by 121 on 5/15/2016.
 */
public class ProfileActivity extends Fragment {
    private View rootView;
    private TextView name,mobile;
    private Button logout;
    private UploadTask task;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.profile, null);

        getActivity().setTitle("Profile");

        name = (TextView) rootView.findViewById(R.id.name);
        mobile = (TextView) rootView.findViewById(R.id.mobile);
        logout = (Button) rootView.findViewById(R.id.logout);

        name.setText(AppPrefrence.getName(ProfileActivity.this.getActivity()));
        mobile.setText(AppPrefrence.getPhone(ProfileActivity.this.getActivity()));

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getActivity().getSharedPreferences("gcmregistartion", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("registration_id", "");
                editor.putInt("appVersion", 10);
                editor.putBoolean("loggedOut", true);
                editor.commit();
                postRegidRequest("");
            }
        });
        return rootView;
    }
    private void postRegidRequest(String regid){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rider_id", AppPrefrence.getMobile(getActivity()));
        params.put("regid", regid);
        post_gcm_id(params);
    }
    private void post_gcm_id(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){
                        AppPrefrence.setNumber(ProfileActivity.this.getActivity(), "");
                        Intent intent = new Intent(ProfileActivity.this.getActivity(), LoginActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        getActivity().startActivity(intent);
                    }
                    else{
                        Toast.makeText(getActivity(), "There was some error, please try again", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity(), "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(getActivity(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, getActivity(), Constant.GCM_URL, params);
        task.execute();
    }
}
