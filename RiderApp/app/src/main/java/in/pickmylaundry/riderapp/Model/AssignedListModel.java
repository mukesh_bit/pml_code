package in.pickmylaundry.riderapp.Model;

/**
 * Created by 121 on 5/12/2016.
 */
public class AssignedListModel {
    private String so,name;
    private String mobile;
    private String hub;

    public String getLaundry() {
        return laundry;
    }

    public void setLaundry(String laundry) {
        this.laundry = laundry;
    }

    private String laundry;

    public String getProcessing_status() {
        return processing_status;
    }

    public void setProcessing_status(String processing_status) {
        this.processing_status = processing_status;
    }

    private String processing_status;

    public String getStatus() {
        return status;
    }

    public String getHub() {
        return hub;
    }

    public void setHub(String hub) {
        this.hub = hub;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String status;

    public String getIsNext() {
        return isNext;
    }

    public void setIsNext(String isNext) {
        this.isNext = isNext;
    }

    private String salesorderid;
    private String isNext;

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    private String color;

    public String getSostatus() {
        return sostatus;
    }

    public void setSostatus(String sostatus) {
        this.sostatus = sostatus;
    }

    private String sostatus;
    private String time,type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSalesorderid() {
        return salesorderid;
    }

    public void setSalesorderid(String salesorderid) {
        this.salesorderid = salesorderid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    private String address;

}
