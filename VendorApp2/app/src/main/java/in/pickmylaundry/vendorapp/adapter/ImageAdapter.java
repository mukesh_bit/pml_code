package in.pickmylaundry.vendorapp.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;

import in.pickmylaundry.vendorapp.Utils.ImageLoader;

/**
 * Created by 121 on 1/31/2016.
 */
public class ImageAdapter extends BaseAdapter {
    private ArrayList<String> imagePath;
    private Context context;
    private int width, height;
    private String imageLoadingMethod;
    public ImageAdapter(Context context, ArrayList<String> imagePath, String imageLoadingMethod){
        this.imagePath = imagePath;
        this.context = context;
        this.imageLoadingMethod = imageLoadingMethod;
    }
    @Override
    public int getCount() {
        return imagePath.size();
    }

    @Override
    public Object getItem(int position) {
        return imagePath.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            // if it's not recycled, initialize some attributes
        if(convertView == null) {
            imageView = new ImageView(context);
            width = displayMetrics.widthPixels;
            height = displayMetrics.heightPixels;
            imageView.setLayoutParams(new GridView.LayoutParams(width / 3, height / 4));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setPadding(8, 8, 8, 8);
        }
        else{
            imageView = (ImageView) convertView;
        }

        if(imageLoadingMethod.equals("from_url"))
            loadFromUrl(imagePath.get(position), imageView, convertView);
        else
            setPic(imagePath.get(position), imageView);
        return imageView;
    }
    public void setPic(String path,ImageView imgView) {

        // Get the dimensions of the View
        int targetW = width/3;
        int targetH = height/4;

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, bmOptions);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
        imgView.setImageBitmap(bitmap);

    }
    private void loadFromUrl(String url, ImageView imageView, View convertView){
        ImageLoader loader = new ImageLoader(context);
        loader.DisplayImage(url, imageView, "price", convertView);
    }
}
