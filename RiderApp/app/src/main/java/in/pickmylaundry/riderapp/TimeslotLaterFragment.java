package in.pickmylaundry.riderapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * Created by zomato on 19/07/16.
 */
public class TimeslotLaterFragment extends Fragment {

    private View rootView;
    private String type;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (View) inflater.inflate(R.layout.timeslot_later, container, false);

        Bundle bundle = getArguments();
        type = bundle.getString("type");

        final Button reschedule_btn = (Button) rootView.findViewById(R.id.reschedule_btn);
        if(type.equals("Pickup"))
            reschedule_btn.setText("Call & Schedule");
        else
            reschedule_btn.setText("Call & Deliver");
        reschedule_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("date", "later");
                intent.putExtra("slot", reschedule_btn.getText().toString());
                getActivity().setResult(1, intent);
                getActivity().finish();
            }
        });


        return rootView;
    }
}
