package com.pick.mylaundry;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pick.navAdapter.TransactionAdapter;
import com.pick.navmodel.TransactionModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Mukesh on 12/7/16.
 */

public class TransactionFragment extends Fragment {

    private View rootview, divider;
    private ListView listView;

    public TransactionFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.transaction_frgmnt, container, false);
        Bundle bundle = getArguments();
        listView = (ListView) rootview.findViewById(R.id.transaction_list);
        TextView no_transaction = (TextView) rootview.findViewById(R.id.no_transaction);
        divider = (View) rootview.findViewById(R.id.divider);

        String category = bundle.getString("category");
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(bundle.getString("data"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<TransactionModel> transactionModelArrayList = new ArrayList<TransactionModel>();
        for (int i=0; i<jsonArray.length(); i++){
            JSONObject jsonObject = null;
            try {
                jsonObject = jsonArray.getJSONObject(i);
                TransactionModel transactionModel = new TransactionModel();
                transactionModel.setType(jsonObject.getString("type"));
                transactionModel.setAmount(jsonObject.getString("amount"));
                transactionModel.setTitle(jsonObject.getString("title"));
                transactionModel.setDate(jsonObject.getString("date"));
                if(jsonObject.getString("type").equals("in") && (category.equals("ALL") || category.equals("MONEY IN"))){
                    transactionModelArrayList.add(transactionModel);
                }
                else if(jsonObject.getString("type").equals("out") && (category.equals("ALL") || category.equals("MONEY OUT"))){
                    transactionModelArrayList.add(transactionModel);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
        if(transactionModelArrayList.size() == 0){
            no_transaction.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
            divider.setVisibility(View.GONE);
            Log.i("empty", "empty");
        }
        else{
            no_transaction.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            divider.setVisibility(View.VISIBLE);
        }
        TransactionAdapter transactionAdapter = new TransactionAdapter(transactionModelArrayList, getActivity());
        listView.setAdapter(transactionAdapter);
        listView.deferNotifyDataSetChanged();



        return rootview;
    }

}
