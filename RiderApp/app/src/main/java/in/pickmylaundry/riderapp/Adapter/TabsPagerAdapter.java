package in.pickmylaundry.riderapp.Adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import in.pickmylaundry.riderapp.PageFragment;

/**
 * Created by 121 on 5/12/2016.
 */
public class TabsPagerAdapter extends FragmentStatePagerAdapter {
    private String[] tabs = {"Pickup", "Delivery"};
    private String status;
    private JSONObject count_data = new JSONObject();

    public TabsPagerAdapter(FragmentManager fm, String status) {
        super(fm);
        this.status = status;
    }

    @Override
    public Fragment getItem(int position) {
        PageFragment fragment = new PageFragment();
        Bundle args = new Bundle();
        args.putString("type", tabs[position]);
        args.putString("status", status);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    public void setCount_data(JSONObject count_data){
        this.count_data = count_data;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(count_data.has(tabs[position])) {
            try {
                return tabs[position] + " - " + count_data.getString(tabs[position]);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return tabs[position];
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}
