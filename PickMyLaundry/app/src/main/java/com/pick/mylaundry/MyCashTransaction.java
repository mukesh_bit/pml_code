package com.pick.mylaundry;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.pick.navmodel.TransactionModel;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Mukesh on 12/7/16.
 */

public class MyCashTransaction extends AppCompatActivity {

    private String[] tabTitles = {"ALL", "MONEY IN", "MONEY OUT"};
    private View rootView;
    private TextView title;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TransactionFragment transactionFragment;
    private JSONArray transactionList;
    private HashMap<String, ArrayList<TransactionModel>> transactionModelList = new HashMap<String, ArrayList<TransactionModel>>();
    private ImageView back_icon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mycash_transaction_activity);

        rootView = findViewById(R.id.rootView);

        title = (TextView) findViewById(R.id.actionbar_custom_title);
        title.setText("MyCash Transaction");

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        get_list();

        back_icon = (ImageView) findViewById(R.id.back_icon);
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });


    }
    private void setupTabIcons() {
        for (int i=0; i<3;i++){
            TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.transaction_tab, null);
            tabOne.setText(tabTitles[i]);
            tabLayout.getTabAt(i).setCustomView(tabOne);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        MyCashTransaction.ViewPagerAdapter adapter = new MyCashTransaction.ViewPagerAdapter(getSupportFragmentManager());
        for (int i=0;i<3;i++) {
            transactionFragment = new TransactionFragment();
            Bundle args = new Bundle();
            args.putString("data", transactionList.toString());
            args.putString("category", tabTitles[i]);
            transactionFragment.setArguments(args);
            adapter.addFrag(transactionFragment, tabTitles[i]);
        }
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    private void get_list(){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(MyCashTransaction.this)));
        getListData(params);
    }

    private void getListData(List<NameValuePair> params) {
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {

                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        transactionList = json.getJSONArray("data");
                        setupViewPager(viewPager);
                        tabLayout.setupWithViewPager(viewPager);
                        setupTabIcons();

                    } else if(json.getString("success").equals("false")){

                    }

                } catch (JSONException e){
                    Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void taskException() {


            }
        },MyCashTransaction.this, Constant.TRANSACTIONHISTORY,params, true);

        task.execute();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
