package com.mylaundry.vendorapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;

/**
 * Created by Dell on 9/18/2015.
 */
public class Image {

    private Bitmap image;
    private String description;
    private String imageUrl;
    private PicturesAdapter pa;

    public Image() {

    }

    public Image(Bitmap image, String description, String imageUrl, PicturesAdapter pa) {
        this.image = image;
        this.description = description;
        this.imageUrl = imageUrl;
        this.pa = pa;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public PicturesAdapter getPa() {
        return pa;
    }

    public void setPa(PicturesAdapter pa) {
        this.pa = pa;
    }

    public void loadImage(PicturesAdapter pa) {
        // HOLD A REFERENCE TO THE ADAPTER
        this.pa = pa;
        if (imageUrl != null && !imageUrl.equals("")) {
            new ImageLoadTask().execute(imageUrl);
        }
    }

    // ASYNC TASK TO AVOID CHOKING UP UI THREAD
    private class ImageLoadTask extends AsyncTask<String, String, Bitmap> {

        @Override
        protected void onPreExecute() {
            Log.i("ImageLoadTask", "Loading image...");
        }

        // PARAM[0] IS IMG URL
        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];

            try {
                //BitmapFactory.Options options = new BitmapFactory.Options();
                //options.inSampleSize = 8;
                Bitmap mIcon11;
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
                //mIcon11 = BitmapFactory.decodeFile(urldisplay, options);
                Bitmap resized = Bitmap.createScaledBitmap(mIcon11,(int)(mIcon11.getWidth()*0.4), (int)(mIcon11.getHeight()*0.4), true);
                return resized;
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
                return null;
            }

        }

        protected void onProgressUpdate(String... progress) {
            // NO OP
        }

        protected void onPostExecute(Bitmap ret) {
            if (ret != null) {
                Log.i("ImageLoadTask", "Successfully loaded image");
                image = ret;
                if (pa != null) {
                    // WHEN IMAGE IS LOADED NOTIFY THE ADAPTER
                    pa.notifyDataSetChanged();
                }
            } else {
                Log.e("ImageLoadTask", "Failed to load image");
            }
        }
    }

}

