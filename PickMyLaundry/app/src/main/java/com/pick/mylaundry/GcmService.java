/*
Copyright 2015 Google Inc. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.pick.mylaundry;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;

import com.flurry.android.FlurryAgent;
import com.google.android.gms.gcm.GcmListenerService;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Service used for receiving GCM messages. When a message is received this service will log it.
 */
public class GcmService extends GcmListenerService {

    public static final String SEND_DATA_INTENT = "OrderBroadCast";
    private String orderid="",orderStatus="";
    String id,message,title,text, ticker, dialog_title, type="";
    String isAlertDialogVisible, notification_type;
    private boolean isFeedbackGiven;


    public GcmService() {
        //logger = new LoggingService.Logger(this);
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        sendNotification("Received: " + data.toString());
        id = data.getString("id");
        if(data.containsKey("type"))
            type = data.getString("type");
        title = data.getString("title");
        text = data.getString("text");
        ticker = data.getString("ticker");
        message = data.getString("notification_message");
        dialog_title = data.getString("dialog_title");
        isAlertDialogVisible = data.getString("isAlertDialogVisible");
        if(id.equals("9")){
            orderid = data.getString("orderid");
            orderStatus = data.getString("orderStatus");
        }

        SharedPreferences sharedPref = GcmService.this.getSharedPreferences(getString(R.string.PREFS_NAME), Context.MODE_PRIVATE);
        generateNotification(message);
    }

    @Override
    public void onDeletedMessages() {
        sendNotification("Deleted messages on server");
    }

    @Override
    public void onMessageSent(String msgId) {
        sendNotification("Upstream message sent. Id=" + msgId);
    }

    @Override
    public void onSendError(String msgId, String error) {
        sendNotification("Upstream message send error. Id=" + msgId + ", error" + error);
    }

    private void sendNotification(String msg) {
        //logger.log(Log.INFO, msg);
    }

    private  void generateNotification(String message) {

        if(type.equals("feedback")) {
            checkForFeedback();
        }
        else{
            fillExtraData();
        }
    }
    private void fillExtraData(){
        Intent notificationIntent = new Intent(GcmService.this, MainActivity.class);
        if(orderid!="")
            notificationIntent = new Intent(GcmService.this, Invoice.class);
            notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
            notificationIntent.setAction(Intent.ACTION_MAIN);
        if(orderid!="") {
            notificationIntent.putExtra(Constant.ORDERID, orderid);
            notificationIntent.putExtra(Constant.ORDERSTATUS, orderStatus);
            notificationIntent.putExtra("SESSION_ID", "10");
        }
        else{
            notificationIntent.putExtra("SESSION_ID", id);
        }
        notificationIntent.putExtra("message", message);
        notificationIntent.putExtra("dialog_title", dialog_title);
        notificationIntent.putExtra("isAlertDialogVisible", isAlertDialogVisible);
        generateNotificationInBar(notificationIntent);
    }
    private void generateNotificationInBar(Intent notificationIntent){
        int icon = R.drawable.logo;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // set intent so it does not start a new activity
        // notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(GcmService.this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        // notification.setLatestEventInfo(GcmService.this, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        // Play default notification sound
        notification.defaults |= Notification.DEFAULT_SOUND;

        // Vibrate if vibrate is enabled
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification = new NotificationCompat.Builder(GcmService.this)
                .setContentTitle(title)
                .setContentText(text)
                .setTicker(ticker)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(intent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setSmallIcon(getNotificationIcon())
                .build();

        notificationManager = (NotificationManager)getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("message", text);
        //up to 10 params can be logged with each event
        FlurryAgent.logEvent("Notification", articleParams);
    }
    private void checkForFeedback(){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(GcmService.this)));
        getFeedbackData(params);
    }

    private void getFeedbackData(List<NameValuePair> params) {
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        JSONObject data = json.getJSONObject("order_data");
                        String so = data.getString("so");
                        int amount = data.getInt("amount");
                        String pickup_date = data.getString("pickup_date");
                        String pickup_time = data.getString("pickup_time");
                        String delivery_date = data.getString("delivery_date");
                        String delivery_time = data.getString("delivery_time");
                        Intent intent = new Intent(getApplicationContext(), Feedback.class);
                        intent.putExtra("so", so);
                        intent.putExtra("amount", amount+"");
                        intent.putExtra("pickup_date", pickup_date);
                        intent.putExtra("pickup_time", pickup_time);
                        intent.putExtra("delivery_date", delivery_date);
                        intent.putExtra("delivery_time", delivery_time);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        generateNotificationInBar(intent);
                    }
                    else {
                        fillExtraData();
                    }

                } catch (JSONException e){
                    fillExtraData();
                }

            }

            @Override
            public void taskException() {
                fillExtraData();

            }
        },GcmService.this, Constant.FEEDBACK_URL,params, false);

        task.execute();
    }
    private int getNotificationIcon() {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_launcher2 : R.drawable.ic_launcher;
    }

}
