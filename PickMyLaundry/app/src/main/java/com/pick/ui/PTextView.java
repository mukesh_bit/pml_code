package com.pick.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Mukesh on 11/11/16.
 */

public class PTextView extends TextView {
    public PTextView(Context context) {
        super(context);
        setFont(context);
    }

    public PTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public PTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }
    private void setFont(Context context){
        Typeface face=Typeface.createFromAsset(context.getAssets(), "font/helvetica-normal.ttf");
        setTypeface(face);
    }
}
