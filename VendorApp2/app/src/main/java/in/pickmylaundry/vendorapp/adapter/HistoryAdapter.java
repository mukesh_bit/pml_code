package in.pickmylaundry.vendorapp.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.vendorapp.ExpandableHeightGridView;
import in.pickmylaundry.vendorapp.R;
import in.pickmylaundry.vendorapp.Utils.Constant;
import in.pickmylaundry.vendorapp.Utils.UploadTask;
import in.pickmylaundry.vendorapp.model.HistoryModel;

/**
 * Created by 121 on 2/1/2016.
 */
public class HistoryAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<HistoryModel> history_list_data;
    private ArrayList<String> gridAdapterList;
    private HistoryAdapter adapter;
    private RefreshListData refreshListData;
    private String vendor;

    public interface RefreshListData{
        public void refresh();
    }

    public HistoryAdapter(Context context, ArrayList<HistoryModel> history_list_data, RefreshListData refreshListData){
        this.context = context;
        this.history_list_data = history_list_data;
        this.refreshListData = refreshListData;
        SharedPreferences prefs = context.getSharedPreferences("login_status", context.MODE_PRIVATE);
        vendor = prefs.getString("vendor_name", "");
        adapter = this;
    }
    @Override
    public int getCount() {
        return history_list_data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.history_product_item, null);
        RelativeLayout item_rl = (RelativeLayout) convertView.findViewById(R.id.item_rl);
        if(history_list_data.get(position).isActive())
            item_rl.setBackgroundResource(R.drawable.add_image_item_bg_active);
        TextView so = (TextView) convertView.findViewById(R.id.so);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        final TextView product = (TextView) convertView.findViewById(R.id.product);
        TextView description = (TextView) convertView.findViewById(R.id.description);
        ImageView status = (ImageView) convertView.findViewById(R.id.status_image);
        so.setText(history_list_data.get(position).getSo());
        name.setText(history_list_data.get(position).getName());
        product.setText(history_list_data.get(position).getProduct());
        description.setText(Html.fromHtml(history_list_data.get(position).getDescription()));
        if(history_list_data.get(position).getStatus().equals("Pending")){
            status.setImageResource(R.drawable.error);
        }
        else if(history_list_data.get(position).getStatus().equals("Process")){
            status.setImageResource(R.drawable.confirm);
        }
        else if(history_list_data.get(position).getStatus().equals("Unprocess")){
            status.setImageResource(R.drawable.close2);
        }
        int image_count = history_list_data.get(position).getCount();
        gridAdapterList = new ArrayList<String>();
        int start = history_list_data.get(position).getStart();
        for (int i=start;i<start+image_count;i++){
            String product_name = history_list_data.get(position).getProduct().replace(' ', '_');
            gridAdapterList.add("http://crm.pickmylaundry.in/dashboard/complaint_box/images/"+history_list_data.get(position).getSo()+"_"+product_name+i+".jpg");
        }

        ExpandableHeightGridView gridView = (ExpandableHeightGridView) convertView.findViewById(R.id.grid_history);
        ImageAdapter adapter = new ImageAdapter(context, gridAdapterList, "from_url");
        gridView.setAdapter(adapter);
        gridView.setExpanded(true);
        LinearLayout remind = (LinearLayout) convertView.findViewById(R.id.reminder);
        LinearLayout accept = (LinearLayout) convertView.findViewById(R.id.accept);
        TextView accept_tv = (TextView) convertView.findViewById(R.id.accept_tv);
        if(history_list_data.get(position).getAcceptingStatus()==1){
            accept.setEnabled(false);
            accept.setBackgroundColor(context.getResources().getColor(R.color.grey));
            accept_tv.setTextColor(context.getResources().getColor(R.color.black));
            accept_tv.setText("Accepted");
        }
        remind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogBox(history_list_data.get(position).getId());
            }
        });
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                acceptRequest(history_list_data.get(position).getId(), position);
            }
        });
        return convertView;
    }
    public void showDialogBox(final int product_id){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_popup);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final Button send_reminder = (Button) dialog.findViewById(R.id.send_reminder);
        final TextView comment = (TextView) dialog.findViewById(R.id.comment);
        send_reminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_reminder(product_id, comment.getText().toString(), dialog);
            }
        });
        dialog.show();

    }
    public void submit_reminder(int reminder_popup_id, String comment, Dialog dialog){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("vendor", vendor);
        params.put("id", reminder_popup_id);
        params.put("comment", comment);
        send_reminder(params, dialog);
    }
    private void send_reminder(Map<String, Object> params, final Dialog dialog){
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){
                        Toast.makeText(context, "Reminder sent succesfully", Toast.LENGTH_LONG).show();
                        refreshListData.refresh();
                        dialog.dismiss();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                // Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, context, Constant.SET_REMINDER, params);
        task.execute();
    }
    private void acceptRequest(int id, int position){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("vendor", vendor);
        params.put("id", id);
        send_accept_request(params, position);
    }
    private void send_accept_request(Map<String, Object> params, final int position){
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){
                        Toast.makeText(context, "Accepted succesfully", Toast.LENGTH_LONG).show();
                        refreshListData.refresh();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                // Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, context, Constant.ACCEPT, params);
        task.execute();
    }

}
