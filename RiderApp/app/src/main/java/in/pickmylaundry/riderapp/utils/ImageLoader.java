package in.pickmylaundry.riderapp.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;

public class ImageLoader extends AsyncTask<String, String, Bitmap> {

    Bitmap bitmap;
    String url;
    ImageView imageView;

    public ImageLoader(String url, ImageView imageView){
        this.url = url;
        this.imageView = imageView;
    }

    @Override
    protected void onPreExecute() {

    }
    protected Bitmap doInBackground(String... args) {
        try {
            bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bitmap;
    }

    protected void onPostExecute(Bitmap image) {
            imageView.setImageBitmap(image);

    }
}