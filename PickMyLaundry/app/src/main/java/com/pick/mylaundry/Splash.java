package com.pick.mylaundry;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.utill.CurrentLocation;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class Splash extends Activity {
	private SharedPreferences sharedPref;
	private SharedPreferences.Editor editor;
	private Handler handler;
	private Runnable Update;
	private Timer swipeTimer = new Timer();
	private ImageView logo, title;
	private TextView  description;
	private int width = 0, height = 0, textSize = 5, margin=-75;
	private boolean isConnected, isRequestCompleted = false;
	private LinearLayout splash_image;
	private Intent intent;
   @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.splash);

	   		splash_image = (LinearLayout) findViewById(R.id.splash_image);
	   		logo = (ImageView) findViewById(R.id.logo);
	   		title = (ImageView) findViewById(R.id.title);
	   		Typeface tf = Typeface.createFromAsset(getAssets(), "font/helvetica-normal.ttf");
	   		description = (TextView) findViewById(R.id.description);
	   		description.setTypeface(tf);
	   		ConnectivityManager cm = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
	   		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
	   		isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
	   		if(AppPrefrence.getLoginStatus(Splash.this)&&isConnected) {
		   		checkForFeedback();
	   		}
	   		get_current_location();

		    handler = new Handler();
		    Update = new Runnable() {
			   public void run() {
				   width+=2;
				   height +=2;
				   if(width>=300) {
					   if(width>=400){
						   swipeTimer.cancel();
						   startApp();
					   }
					   else{
						   Float alpha = Float.valueOf((float) ((float) (width - 300)) / 100);
						   title.setAlpha(alpha);
						   description.setAlpha(alpha);
					   }
				   }
				   else {
					   Float alpha = Float.valueOf((float) ((float) (width) / 3) / 100);
					   logo.setAlpha(alpha);
					   if(width%4==0&&margin<0)
						   margin++;
					   FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) splash_image.getLayoutParams();
					   layoutParams.setMargins(margin, margin, margin, margin);
					   splash_image.setLayoutParams(layoutParams);

				   }
			   }
		   };
	   swipeTimer.schedule(new TimerTask() {

		   @Override
		   public void run() {
			   handler.post(Update);
		   }
	   }, 0, 10);

	   //initialize flurry
	   FlurryAgent.setLogEnabled(false);
	   // init Flurry
	   FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
   }

	private void startApp(){
		SharedPreferences prefs = getSharedPreferences("get_started", Context.MODE_PRIVATE);
		String status = prefs.getString("get_started_status", "");
		if(status.equals("done")){
			if(AppPrefrence.getLoginStatus(Splash.this)&&isConnected) {
				swipeTimer = new Timer();
				Update = new Runnable() {
					@Override
					public void run() {
						if(isRequestCompleted){
							swipeTimer.cancel();
							startActivity(intent);
							finish();
						}
					}
				};
				swipeTimer.schedule(new TimerTask() {
					@Override
					public void run() {
						handler.post(Update);
					}
				}, 0, 10);
			}
			else{
				if(!AppPrefrence.getLoginStatus(Splash.this)){
					startActivity(new Intent(getApplicationContext(), MobileActivity.class)
							.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				}
				else {
					startActivity(new Intent(getApplicationContext(), MainActivity.class)
							.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
				}
				finish();
			}
		}
		else{
			Intent intent = new Intent(Splash.this, getting_started.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
			startActivity(intent);
			finish();
		}
	}

	private void checkForFeedback(){
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(Splash.this)));
			getFeedbackData(params);
	}

	private void getFeedbackData(List<NameValuePair> params) {
		UploadTask task = new UploadTask(new UploadTask.TaskListner() {
			@Override
			public void taskResult(String result) {
				try {
					JSONObject json = new JSONObject(result);
					if(json.getString("success").equals("true")){
						JSONObject data = json.getJSONObject("order_data");
						String so = data.getString("so");
						int amount = data.getInt("amount");
						String pickup_date = data.getString("pickup_date");
						String pickup_time = data.getString("pickup_time");
						String delivery_date = data.getString("delivery_date");
						String delivery_time = data.getString("delivery_time");
						intent = new Intent(getApplicationContext(), Feedback.class);
						intent.putExtra("so", so);
						intent.putExtra("amount", amount+"");
						intent.putExtra("pickup_date", pickup_date);
						intent.putExtra("pickup_time", pickup_time);
						intent.putExtra("delivery_date", delivery_date);
						intent.putExtra("delivery_time", delivery_time);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

						// Capture event data
						Map<String, String> articleParams = new HashMap<String, String>();
						articleParams.put("mobile", AppPrefrence.getMobile(Splash.this));
						articleParams.put("so", so);
						articleParams.put("screen", "organic");
						//up to 10 params can be logged with each event
						FlurryAgent.logEvent("Feedback", articleParams);

						isRequestCompleted = true;
					}
					else {
						intent = new Intent(getApplicationContext(), MainActivity.class);
						intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						log_flurry();
						isRequestCompleted = true;
					}

				} catch (JSONException e){
					e.printStackTrace();
					intent = new Intent(getApplicationContext(), MainActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
					log_flurry();
					isRequestCompleted = true;
				}

			}

			@Override
			public void taskException() {
				isRequestCompleted = true;
				log_flurry();
				intent = new Intent(getApplicationContext(), MainActivity.class)
						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

			}
		},Splash.this, Constant.FEEDBACK_URL,params, false);

		task.execute();
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
		FlurryAgent.logEvent("Splash Screen");
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}
	private void log_flurry(){
		Map<String, String> articleParams = new HashMap<String, String>();
		articleParams.put("main_screen", "splash");
		FlurryAgent.logEvent("MainActivity", articleParams);
	}
	private void get_current_location(){
		CurrentLocation currentLocation = new CurrentLocation(Splash.this);
		currentLocation.getCurrentLocation(new CurrentLocation.LocationInterface() {
			@Override
			public void onResult(String address) {
				if(Constant.getLocation(Splash.this).equals(Constant.DEFAULTLOCATION)) {
					Constant.setLocation(address, Splash.this);
				}
			}

			@Override
			public void onError() {

			}
		}, false, false, false);
	}
}