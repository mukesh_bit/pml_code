package com.pick.navAdapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.pick.mylaundry.R;
import com.pick.navmodel.TransactionModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mukesh on 12/7/16.
 */

public class TransactionAdapter extends BaseAdapter {

    private ArrayList<TransactionModel> transactionModelArrayList;
    private LayoutInflater inflater;
    private Context context;
    private ViewHolder viewHolder;

    public TransactionAdapter(ArrayList<TransactionModel> transactionModelArrayList, Context context){
        this.transactionModelArrayList = transactionModelArrayList;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return transactionModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            convertView = inflater.inflate(R.layout.transaction_item, null);
            viewHolder = new ViewHolder();
            viewHolder.amount = (TextView) convertView.findViewById(R.id.amount);
            viewHolder.title = (TextView) convertView.findViewById(R.id.title);
            viewHolder.date = (TextView) convertView.findViewById(R.id.date);
            viewHolder.type_image = (ImageView) convertView.findViewById(R.id.type_image);

            convertView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if(transactionModelArrayList.get(position).getType().equals("in")){
            viewHolder.type_image.setImageResource(R.drawable.success);
        }
        else{
            viewHolder.type_image.setImageResource(R.drawable.out);
        }
        viewHolder.amount.setText(context.getResources().getString(R.string.Rs)+ " "+transactionModelArrayList.get(position).getAmount());
        viewHolder.date.setText(transactionModelArrayList.get(position).getDate());
        viewHolder.title.setText(transactionModelArrayList.get(position).getTitle());
        return convertView;
    }
    static class ViewHolder{
        private TextView title, date, amount;
        private ImageView type_image;
    }
}
