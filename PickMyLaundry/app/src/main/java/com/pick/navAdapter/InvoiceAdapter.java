package com.pick.navAdapter;

import java.util.List;

import com.pick.mylaundry.R;
import com.pick.navmodel.InvoiceListModel;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
 
  

public class InvoiceAdapter extends BaseAdapter {

	private Context context;
	private List<InvoiceListModel> list;
	 
 public InvoiceAdapter(Context context, List<InvoiceListModel> invoicelist) {
		super();
		this.context = context;
		this.list = invoicelist;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {

		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.invoice_list, null);
		}
		 	String fontPath = "font/helvetica-normal.ttf";
			Typeface tf = Typeface.createFromAsset(context.getAssets(),fontPath);
     		TextView pname = (TextView) convertView.findViewById(R.id.invoice_list_pname);
     		pname.setTypeface(tf);
			TextView invoiceamt = (TextView) convertView.findViewById(R.id.invoice_amount);
			invoiceamt.setTypeface(tf);
			TextView itemprice=(TextView) convertView.findViewById(R.id.invoice_itemprice);
			itemprice.setTypeface(tf);
			TextView item_total=(TextView)convertView.findViewById(R.id.invoice_items_total_price);
			item_total.setTypeface(tf);
			pname.setText(list.get(position).getProductname());
			Double wt=list.get(position).getQuantity();
			Double pr=list.get(position).getPrice();
			int price=pr.intValue();
			itemprice.setText(price+"");
			Double wtpr=wt*pr;
			int weight_price=wtpr.intValue();
			item_total.setText(""+weight_price);
			String un=list.get(position).getUnit();
			if(un.equals("pcs")){
				invoiceamt.setText(wt+" "+list.get(position).getUnit());
			}else{
				invoiceamt.setText(wt+" "+list.get(position).getUnit());
			}

  return convertView;
	}

}