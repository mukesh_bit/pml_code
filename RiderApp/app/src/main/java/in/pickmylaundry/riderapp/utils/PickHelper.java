package in.pickmylaundry.riderapp.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by 121 on 5/12/2016.
 */
public class PickHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION =3;
    // Database Name
    private static final String DATABASE_NAME = "order_list";

    public static final String TABLE_ADDRESS= "assigned_list";
    public static final String KEY_PRIMARY= "id";
    public static final String KEY_SO= "so";
    public static final String KEY_NAME="name";
    public static final String KEY_TIME="time";
    public static final String KEY_ADDRESS="address";
    public static final String KEY_MOBILE="mobile";
    public static final String KEY_SALESORDERID="salesorderid";
    public static final String KEY_TYPE = "type";
    public static final String KEY_STATUS = "status";
    public static final String KEY_SOSTATUS = "sostatus";
    public static final String KEY_PREFERENCE_COLOR = "color";
    public static final String KEY_NEXT_CUSTOMER = "isNext";
    public static final String KEY_DELIVERY_STATUS = "status_delivery";
    public static final String KEY_HUB = "hub";
    public static final String KEY_PROC_STATUS = "processing_status";
    public static final String KEY_LAUNDRY = "laundry";
    public static final String CREATE_TABLE_ASSIGNED = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ADDRESS + "(" +KEY_PRIMARY +" integer primary key autoincrement," + KEY_SO
            + " TEXT," +KEY_NAME+" TEXT,"+KEY_TIME+
            " TEXT,"+ KEY_ADDRESS+" TEXT,"+KEY_MOBILE+" TEXT,"+KEY_SALESORDERID+" TEXT,"+KEY_TYPE+" TEXT,"+KEY_STATUS+" TEXT,"+KEY_SOSTATUS+" TEXT,"+KEY_PREFERENCE_COLOR+" TEXT,"+KEY_NEXT_CUSTOMER+" TEXT, "+KEY_DELIVERY_STATUS+" TEXT, "+KEY_HUB+" TEXT, "+KEY_PROC_STATUS+" TEXT, "+KEY_LAUNDRY+" TEXT)";


    public static final String TABLE_ADDRESS_DETAIL= "assigned_list_detail";
    public static final String KEY_PRIMARY_DETAIL = "id";
    public static final String KEY_WASH_TYPE= "wash_type";
    public static final String KEY_INSTRUCTION="instruction";
    public static final String KEY_ORDER_TYPE="order_type";
    public static final String KEY_BILL="bill";
    public static final String KEY_PAYMENT_STATUS="payment_status";
    public static final String KEY_SALESORDERID_DETAIL="salesorderid";
    public static final String KEY_TYPE_DETAIL = "type";
    public static final String KEY_RATING = "rating";

    public static final String CREATE_TABLE_DETAIL = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ADDRESS_DETAIL + "(" +KEY_PRIMARY_DETAIL +" integer primary key autoincrement," + KEY_WASH_TYPE
            + " TEXT," +KEY_INSTRUCTION+" TEXT,"+KEY_ORDER_TYPE+
            " TEXT,"+ KEY_BILL+" TEXT,"+KEY_PAYMENT_STATUS+" TEXT,"+KEY_SALESORDERID_DETAIL+" TEXT,"+KEY_TYPE_DETAIL+" TEXT,"+KEY_STATUS+" TEXT, "+KEY_NEXT_CUSTOMER+" TEXT, "+KEY_RATING+" INTEGER)";


    public PickHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ASSIGNED);
        db.execSQL(CREATE_TABLE_DETAIL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS);
        onCreate(db);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS_DETAIL);
        onCreate(db);
    }
}
