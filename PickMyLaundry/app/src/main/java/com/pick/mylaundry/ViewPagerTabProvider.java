package com.pick.mylaundry;

/**
 * Created by qwew on 06-08-2015.
 */
public interface ViewPagerTabProvider {
    public String getTitle(int position);
}
