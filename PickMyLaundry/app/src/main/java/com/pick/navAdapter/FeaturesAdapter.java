package com.pick.navAdapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pick.mylaundry.R;
import com.pick.navmodel.FeatureItemModel;
import com.pick.volley.ImageLoader;

import java.util.ArrayList;

/**
 * Created by Bharti on 10/26/2016.
 */

public class FeaturesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private int color;
    private Context context;
    private ArrayList<FeatureItemModel> iDataSet; // for features
    private ArrayList<String> pDataSet;   // for points
    private int mViewType;
    private ImageLoader imageLoader;

    //viewType==0 for features and 1 for points
    public FeaturesAdapter(Context context, ArrayList<FeatureItemModel> iDataSet, ArrayList<String> pDataset, int viewType, int color){
        this.context = context;
        this.mViewType = viewType;
        this.iDataSet = iDataSet;
        this.pDataSet = pDataset;
        this.color = color;
        imageLoader = new ImageLoader(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(mViewType==0) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.feature_item, parent, false);
            return new MyViewHolder(view);
        }else{
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.features_advantage_item, parent, false);
            return new MyViewHolderPoints(view);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(mViewType==0){
            ((MyViewHolder) holder).itemImage.setColorFilter(color);
            imageLoader.DisplayImage(iDataSet.get(position).getIcon(), ((MyViewHolder) holder).itemImage, null, null);
            ((MyViewHolder) holder).itemHeading.setText(iDataSet.get(position).getHeading());
            ((MyViewHolder) holder).itemDesc.setText(iDataSet.get(position).getDesc());
        }else{
            ((MyViewHolderPoints) holder).itemPointsIcon.setTextColor(color);
            ((MyViewHolderPoints) holder).itemPointsdesc.setText(pDataSet.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if(mViewType==0){
            return iDataSet.size();
        }else {
            return pDataSet.size();
        }
    }

    //viewHolder class
    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView itemHeading;
        TextView itemDesc;
        ImageView itemImage;
        public MyViewHolder(View itemView) {
            super(itemView);
            itemHeading = (TextView) itemView.findViewById(R.id.tv_item_heading);
            itemDesc = (TextView) itemView.findViewById(R.id.tv_item_desc);
            itemImage = (ImageView) itemView.findViewById(R.id.iv_item_icon);
        }
    }

    class MyViewHolderPoints extends RecyclerView.ViewHolder{
        TextView itemPointsIcon;
        TextView itemPointsdesc;
        public MyViewHolderPoints(View itemView){
            super(itemView);
            itemPointsIcon  = (TextView) itemView.findViewById(R.id.tv_point_item_icon);
            itemPointsdesc = (TextView) itemView.findViewById(R.id.tv_point_item);
        }
    }


}
