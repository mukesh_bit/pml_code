package com.pick.mylaundry;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class AppLocationService extends Service implements LocationListener {

    protected LocationManager locationManager;
    Location location;
    public boolean isRequstingUpdates = false;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0; // 10 meters

    private static final long MIN_TIME_BW_UPDATES = 1; // 1 minute


    public AppLocationService(Context context) {
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
    }

    public Location getLocation(String provider) {
        if (locationManager.isProviderEnabled(provider)) {
            isRequstingUpdates = true;
            locationManager.requestLocationUpdates(provider, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            if (locationManager != null) {
                location = locationManager.getLastKnownLocation(provider);
                return location;
            }
        }
        else{
            Log.i("provider", provider+" not enabled");
        }
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.i("lat", location.getLatitude()+"");
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    public void stopGPS(){
        if(isRequstingUpdates)
            locationManager.removeUpdates(this);
    }

}