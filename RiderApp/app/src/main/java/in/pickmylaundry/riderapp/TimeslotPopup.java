package in.pickmylaundry.riderapp;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import in.pickmylaundry.riderapp.utils.AppPrefrence;
import in.pickmylaundry.riderapp.utils.Constant;
import in.pickmylaundry.riderapp.utils.UploadTask;

/**
 * Created by 121 on 12/13/2015.
 */
public class TimeslotPopup extends AppCompatActivity {
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private String[] tabTitles = new String[7];
    private String[] tabDates = new String[7];
    private HashMap<Integer, ArrayList<String>> inactive_slots_date = new HashMap<Integer, ArrayList<String>>();
    private TimeslotFragment timeslotFragment;
    private Bundle bundle;
    private ImageView back;
    private JSONObject timeslot_data;
    private JSONObject inactive_slots;
    private JSONObject slots;
    private String keys, type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.schedule_popup);

        Bundle bundle = getIntent().getExtras();
        type = bundle.getString("type");

        back = (ImageView) findViewById(R.id.closeImage);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimeslotPopup.this.onBackPressed();
            }
        });
        get_slot_data();

    }
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (int i=0; i<2;i++){
            Bundle tab_bundle = new Bundle();
            tab_bundle.putString("date", tabDates[i]);
            tab_bundle.putString("slots", slots.toString());
            tab_bundle.putStringArrayList("inactive_slots", inactive_slots_date.get(i));
            tab_bundle.putString("keys", keys);

            timeslotFragment = new TimeslotFragment();
            timeslotFragment.setArguments(tab_bundle);
            adapter.addFragment(timeslotFragment, tabTitles[i]);
        }

        //add later tab
        Bundle tab_bundle = new Bundle();
        tab_bundle.putString("type", type);

        TimeslotLaterFragment timeslotFragment = new TimeslotLaterFragment();
        timeslotFragment.setArguments(tab_bundle);
        adapter.addFragment(timeslotFragment, tabTitles[2]);

        viewPager.setAdapter(adapter);
    }
    private void setupTabTitles(){
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Calcutta"));
        tabTitles[0] = "Today";
        tabTitles[1] = "Tomorrow";
        tabTitles[2] = "Later";
        tabDates[0] = dateFormat.format(calendar.getTime());
        calendar.add(Calendar.DATE, 1);
        tabDates[1] = dateFormat.format(calendar.getTime());
        calendar.add(Calendar.DATE, 1);
        tabDates[2] = "Later";
        addInactiveSlots();
    }
    private void addInactiveSlots() {
        for(int j=0;j<3;j++) {
            String date = tabDates[j];
            ArrayList<String> slots_data = new ArrayList<String>();
            if (inactive_slots.has(date)) {
                JSONArray data = null;
                try {
                    data = inactive_slots.getJSONArray(date);
                    for (int i = 0; i < data.length(); i++) {
                        slots_data.add(data.getString(i));
                    }
                    inactive_slots_date.put(j, slots_data);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else {
                inactive_slots_date.put(j, slots_data);
            }
        }
    }
    private void get_slot_data(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rider_id", AppPrefrence.getMobile(TimeslotPopup.this));
        request_slot_data(params);
    }
    private void request_slot_data(Map<String, Object> params){
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    timeslot_data = new JSONObject(result);
                    inactive_slots = timeslot_data.getJSONObject("inactive_slots");
                    slots = timeslot_data.getJSONObject("slots");
                    keys = timeslot_data.getJSONArray("keys").toString();

                    //setup viewpager
                    viewPager = (ViewPager) findViewById(R.id.viewpager);
                    setupTabTitles();
                    setupViewPager(viewPager);
                    //setup tab
                    tabLayout = (TabLayout) findViewById(R.id.tabs);
                    tabLayout.setupWithViewPager(viewPager);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void taskException() {
                Constant.no_internet_connection(TimeslotPopup.this);
            }
        }, TimeslotPopup.this, Constant.slot_data, params);
        task.execute();
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}
