package com.pick.mylaundry;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by qwew on 09-09-2015.
 */
public class ReferralFragment extends Fragment {
    private TextView title, promocodetext, promomessagetext;
    private Button invite;
    private String promocode, promomessage, invitationmessage;
    private View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = inflater.inflate(R.layout.referral_layout, container, false);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "font/helvetica-normal.ttf");
        Typeface tf_regular = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaUltraComp.otf");
        title=(TextView)getActivity().findViewById(R.id.toolbar_title);
        title.setText("INVITE AND EARN");

        promocodetext=(TextView)rootView.findViewById(R.id.promocode);
        promomessagetext=(TextView)rootView.findViewById(R.id.promomessage);
        invite=(Button)rootView.findViewById(R.id.btninvitefriends);
        promocode = AppPrefrence.getPromoCode(getActivity()).toUpperCase();
        promocodetext.setText(promocode);
        promocodetext.setTypeface(tf_regular);
        invite.setTypeface(tf);
        promomessagetext.setTypeface(tf);
        makePromoMessageRequest();

        invite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeInvitationMessageRequest();

            }
        });

        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("mobile", AppPrefrence.getMobile(getActivity()));
        //up to 10 params can be logged with each event
        FlurryAgent.logEvent("referral", articleParams);

        return rootView;
    }

    private void makePromoMessageRequest(){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(getActivity())));
        sendPromoMessage(params);
        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("mobile", AppPrefrence.getMobile(getActivity()));
        //up to 10 params can be logged with each event
        FlurryAgent.logEvent("send_invite", articleParams);
    }

    private void sendPromoMessage(List<NameValuePair> params) {
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        promomessage=json.getString("promoMessage");
                        promomessagetext.setText(promomessage);

                    } else if(json.getString("success").equals("false")){
                        Snackbar.make(rootView, json.getString("msg"), Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                        promomessage="";
                        promomessagetext.setText(promomessage);
                    }

                } catch (JSONException e){
                    Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void taskException() {
                Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                promomessage="Invite Your Friends and Get Credits";
                promomessagetext.setText(promomessage);
            }
        },ReferralFragment.this.getActivity(), Constant.GETPROMOMESSAGE,params, true);
        task.execute();
    }

    private void makeInvitationMessageRequest(){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("promoCode", promocode));
        sendInvitationMessage(params);
    }

    private void sendInvitationMessage(List<NameValuePair> params) {
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        invitationmessage=json.getString("invitationMessage");
                        Intent i = new Intent(android.content.Intent.ACTION_SEND);
                        i.setType("text/plain");
                        //i.putExtra(android.content.Intent.EXTRA_SUBJECT,"Subject test");
                        i.putExtra(android.content.Intent.EXTRA_TEXT, invitationmessage);
                        startActivity(Intent.createChooser(i, "Share your code"));

                    } else if(json.getString("success").equals("false")){
                        invitationmessage="Hi, Download PickMyLaundry App from https://play.google.com/store/apps/details?id=com.pick.mylaundry " +
                                "and apply "+promocode+" as coupon code to earn referral credits";
                        Intent i = new Intent(android.content.Intent.ACTION_SEND);
                        i.setType("text/plain");
                        //i.putExtra(android.content.Intent.EXTRA_SUBJECT,"Subject test");
                        i.putExtra(android.content.Intent.EXTRA_TEXT, invitationmessage);
                        startActivity(Intent.createChooser(i, "Share your code"));
                    }

                } catch (JSONException e){
                    Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void taskException() {
                Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).show();
                invitationmessage="";

            }
        },ReferralFragment.this.getActivity(), Constant.GETINVITATIONMESSAGE,params, true);

        task.execute();
    }

}
