package in.pickmylaundry.vendorapp.model;

/**
 * Created by 121 on 2/12/2016.
 */
public class ReadyModel {
    private String so, name, clothes_description;
    private boolean CBStatus;
    private int salesorderId;

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getClothes_description() {
        return clothes_description;
    }

    public void setClothes_description(String clothes_description) {
        this.clothes_description = clothes_description;
    }

    public boolean getCBStatus() {
        return CBStatus;
    }
    public void setCBStatus(boolean CBStatus) {
        this.CBStatus = CBStatus;
    }

    public void setSalesorderId(int salesorderId) {
        this.salesorderId = salesorderId;
    }
    public int getSalesorderId() {
        return salesorderId;
    }

}
