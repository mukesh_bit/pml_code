package com.pick.navAdapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.pick.mylaundry.Invoice;
import com.pick.mylaundry.MainActivity;
import com.pick.mylaundry.R;
import com.pick.mylaundry.ScheduleActivity;
import com.pick.mylaundry.paytmTransaction;
import com.pick.navmodel.OrderListModel;
import com.pick.utill.AppPrefrence;
import com.pick.utill.CancelOrder;
import com.pick.volley.EmptyUtility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListAdapter extends BaseAdapter implements OnClickListener  {  
    private Context context;
	private List<OrderListModel> orderList;
	private ImageView[] status_icons = new ImageView[5];
	private View[] status_line = new View[5];
	private RelativeLayout action_btn_1_rl,action_btn_2_rl;
	private ImageView action_btn_1_icon,action_btn_2_icon;
	private TextView action_btn_1_tv,action_btn_2_tv, total_payable_tv;
	private Dialog dialog;
	private ListView listView;
	private ListAdapter adapter;
    
    public ListAdapter(Context context,List<OrderListModel> orderList, ListView listView) {
		this.context = context;
		this.orderList = orderList;
		this.listView = listView;
		adapter = this;

	}
    @Override
	public int getCount() {
		return orderList.size();
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
			if (convertView == null) {
				LayoutInflater mInflater = (LayoutInflater) context
						.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
				convertView = mInflater.inflate(R.layout.mybooking_row, null);
			}
	        String fontPath = "font/helvetica-normal.ttf";
			Typeface tf = Typeface.createFromAsset(context.getAssets(),fontPath);
			TextView pickuptime = (TextView) convertView.findViewById(R.id.pickup_date_time);
			pickuptime.setTypeface(tf);
			TextView pickuptime_title = (TextView) convertView.findViewById(R.id.pickup_time_title);
			pickuptime.setTypeface(tf);
			TextView deliverytime = (TextView) convertView.findViewById(R.id.delivery_date_time);
			deliverytime.setTypeface(tf);
			TextView deliverytime_title = (TextView) convertView.findViewById(R.id.delivery_time_title);
			pickuptime.setTypeface(tf);
			TextView so=(TextView) convertView.findViewById(R.id.so);
			so.setTypeface(tf);
			TextView status_tv = (TextView) convertView.findViewById(R.id.status);
			status_tv.setTypeface(tf);
			TextView bill = (TextView) convertView.findViewById(R.id.total_bill);
			bill.setTypeface(tf);
			TextView total_payable = (TextView) convertView.findViewById(R.id.total_payable);
			total_payable.setTypeface(tf);
			TextView order_date_time = (TextView) convertView.findViewById(R.id.order_date_time);
			order_date_time.setTypeface(tf);
			action_btn_1_rl = (RelativeLayout) convertView.findViewById(R.id.action_btn_1);
			action_btn_2_rl = (RelativeLayout) convertView.findViewById(R.id.action_btn_2);
			action_btn_1_icon = (ImageView) convertView.findViewById(R.id.action_btn_1_icon);
			action_btn_2_icon = (ImageView) convertView.findViewById(R.id.action_btn_2_icon);
			action_btn_1_tv = (TextView) convertView.findViewById(R.id.action_btn_1_title);
			action_btn_2_tv = (TextView) convertView.findViewById(R.id.action_btn_2_title);

			String tbill="";
			String pickup_date_time = get_formatted_date(orderList.get(position).getPickupdate(), "dd MMM''yy");
			pickup_date_time += " - " + orderList.get(position).getPickuptime();
			pickuptime.setText(pickup_date_time);
			String delivery_date_time = get_formatted_date(orderList.get(position).getDeliveryDate(), "dd MMM''yy");
			delivery_date_time += " - " + orderList.get(position).getPickuptime();
			deliverytime.setText(delivery_date_time);
			order_date_time.setText(orderList.get(position).getOrderdate());
			so.setText(orderList.get(position).getSalesorder_id().toString());
			String tempbill=orderList.get(position).getTotalbill().toString();
			if(!EmptyUtility.isEmpty(tempbill))
				 tbill=Integer.valueOf(tempbill)+"";
			tbill = "Rs "+tbill+"/-";


			String payable_bill = "Rs "+orderList.get(position).getTotal_payable()+"/-";

			status_line[0] = (View) convertView.findViewById(R.id.mybooking_line1);
			status_line[1] = (View) convertView.findViewById(R.id.mybooking_line2);
			status_line[2] = (View) convertView.findViewById(R.id.mybooking_line3);
			status_line[3] = (View) convertView.findViewById(R.id.mybooking_line4);
			status_icons[0] =(ImageView) convertView.findViewById(R.id.mybooking_status1);
			status_icons[1] =(ImageView) convertView.findViewById(R.id.mybooking_status2);
			status_icons[2] =(ImageView) convertView.findViewById(R.id.mybooking_status3);
			status_icons[3] =(ImageView) convertView.findViewById(R.id.mybooking_status4);
			status_icons[4] =(ImageView) convertView.findViewById(R.id.mybooking_status5);
			String status = orderList.get(position).getOrdderStatus().toString();

			status_line[0].setBackgroundResource(R.color.greyline);
			status_line[1].setBackgroundResource(R.color.greyline);
			status_line[2].setBackgroundResource(R.color.greyline);
			status_line[3].setBackgroundResource(R.color.greyline);

			status_icons[0].setImageResource(R.drawable.status_1);
			status_icons[1].setImageResource(R.drawable.status_2);
			status_icons[2].setImageResource(R.drawable.status_3);
			status_icons[3].setImageResource(R.drawable.status_4);
			status_icons[4].setImageResource(R.drawable.status_5);
			status_tv.setTextColor(Color.parseColor("#00ce00"));

			action_btn_1_rl.setVisibility(View.VISIBLE);
			action_btn_2_rl.setVisibility(View.VISIBLE);
			action_btn_1_icon.setImageResource(R.drawable.calendar_edit);
			action_btn_1_tv.setText("Reschedule");
			action_btn_1_rl.setTag("Reschedule");
			bill.setText("Invoice Not Generated");
			total_payable.setText("Invoice Not Generated");
			pickuptime_title.setText("Pickup By");
			deliverytime_title.setText("Delivery By");

			String payment_status = orderList.get(position).getPayment_status();

			if(status.equals("Vendor Assigned")){
				set_active_icons(1);
				status_tv.setText("Order Confirmed");
				action_btn_2_icon.setImageResource(R.drawable.cancel);
				action_btn_2_tv.setText("Cancel");
				action_btn_2_rl.setTag("Cancel");
           	}
			else if(status.equals("Created")) {
				status_tv.setText("Order Placed");
				set_active_icons(1);
				action_btn_2_icon.setImageResource(R.drawable.cancel);
				action_btn_2_tv.setText("Cancel");
				action_btn_2_rl.setTag("Cancel");
          	}
			else if( status.equals("Pickup Rider Assigned")){
				set_active_icons(2);
			   	status_tv.setText(status);
				action_btn_2_icon.setImageResource(R.drawable.call_icon);
				action_btn_2_tv.setText("Call Rider");
				action_btn_2_rl.setTag("Call");
			}
			else if(status.equals("Pickup Done")){
				status_tv.setText("In Processing");
				set_active_icons(3);
				pickuptime_title.setText("Picked Up By");
				action_btn_2_rl.setVisibility(View.GONE);
			}
			else if(status.equals("Invoice Created")){
				set_active_icons(3);
				if(orderList.get(position).getProcessing_status().toString().equals("Ready for Delivery")){
				   	status_tv.setText("Ready for Delivery");
				}
				else {
				   	status_tv.setText("In Processing");
				}
				action_btn_2_icon.setImageResource(R.drawable.credit_card);
				action_btn_2_tv.setText(payment_status);
				action_btn_2_rl.setTag(payment_status);
				pickuptime_title.setText("Picked Up By");
				bill.setText(tbill);
				total_payable.setText(payable_bill);
			}
			else if(status.equals("Out for Delivery")){
				set_active_icons(4);
				status_tv.setText(status);
				pickuptime_title.setText("Picked Up By");
				bill.setText(tbill);
				total_payable.setText(payable_bill);
				action_btn_2_icon.setImageResource(R.drawable.credit_card);
				action_btn_2_tv.setText(payment_status);
				action_btn_2_rl.setTag(payment_status);
				action_btn_1_icon.setImageResource(R.drawable.call_icon);
				action_btn_1_tv.setText("Call Rider");
				action_btn_1_rl.setTag("Call");
			}
			else if(status.equals("Delivered")){
				set_active_icons(5);
				status_tv.setText(status);
				pickuptime_title.setText("Picked Up By");
				deliverytime_title.setText("Delivered By");
				bill.setText(tbill);
				total_payable.setText(payable_bill);
				action_btn_2_icon.setImageResource(R.drawable.credit_card);
				action_btn_2_tv.setText(payment_status);
				action_btn_2_rl.setTag(payment_status);
				action_btn_1_icon.setImageResource(R.drawable.warning);
				action_btn_1_tv.setText("Report Issue");
				action_btn_1_rl.setTag("Issue");
			}
			else if(status.equals("Cancelled")){
			   	status_tv.setText(status);
				status_tv.setTextColor(Color.parseColor("#ff0000"));
				action_btn_1_rl.setVisibility(View.GONE);
				action_btn_2_rl.setVisibility(View.GONE);
         	}

			action_btn_1_rl.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					String tag = v.getTag().toString();
					if(tag.equals("Reschedule")){
						reschedule_order(position);
					}
					else if(tag.equals("Call")){
						call_rider(position);
					}
					else if(tag.equals("Issue")){
						sendmail(position);
					}

				}
			});
			action_btn_2_rl.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					String tag = v.getTag().toString();
					if(tag.equals("Pay Now")){
						pay_now(position);
					}
					else if(tag.equals("Cancel")){
						cancelOrder(position);
					}
					else if(tag.equals("Call")){
						call_rider(position);
					}
				}
			});
			return convertView;
	}


	@Override
	public Object getItem(int arg0) {
		return null;
	}
	@Override
	public void onClick(View v) {
		notifyDataSetChanged();
	}

	public String get_formatted_date(String date_value, String format){
		if(EmptyUtility.isEmpty(date_value) || date_value.equals("1970-01-01") || date_value.equals("0000-00-00"))
			return "";
		String formatted_date = date_value;
		SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = dt.parse(date_value);
			SimpleDateFormat monthFormat = new SimpleDateFormat(format);
			formatted_date = monthFormat.format(date);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formatted_date;
	}
	public void set_active_icons(int active_num){
		String package_ = context.getPackageName();
		for (int i=0; i<active_num; i++){
			int icon_id = context.getResources().getIdentifier("status_"+(i+1)+"_active", "drawable", package_);
			Log.i("resource", "status_" + (i + 1) + "_active" + "--" +icon_id + R.drawable.status_1_active);
			status_icons[i].setImageResource(icon_id);
			if(i>0){
				status_line[i-1].setBackgroundResource(R.color.greenline);
			}
		}
	}
	private void reschedule_order(int position){
		Intent intent = new Intent(context, ScheduleActivity.class);
		ArrayList<String> washType = new ArrayList<String>();
		String instruction = orderList.get(position).getInstruction();
		String category = orderList.get(position).getCategory();
		if(EmptyUtility.isNotEmpty(instruction)){
			String[] instruction_arr = instruction.split(",");
			for(int i=0;i<instruction_arr.length;i++){
				washType.add(instruction_arr[i].trim());
			}
		}
		else if(EmptyUtility.isNotEmpty(category)){
			if(category.equals("Laundry"))
				washType.add("Wash & Fold");
			else{
				washType.add("Dry Clean");
			}
		}
		if(washType.size()==0)
			washType.add("Wash & Fold");

		Bundle data_bundle = new Bundle();
		data_bundle.putStringArrayList("washType", washType);
		Log.i("po", orderList.get(position).getTurnaround());
		data_bundle.putInt("turnaround", Integer.parseInt(orderList.get(position).getTurnaround()));

		intent.putExtra("data_bundle", data_bundle);
		intent.putExtra("pickup_date", get_formatted_date(orderList.get(position).getPickupdate(), "dd-MM-yyyy"));
		intent.putExtra("pickup_time", orderList.get(position).getPickuptime());
		intent.putExtra("delivery_date", get_formatted_date(orderList.get(position).getDeliveryDate(), "dd-MM-yyyy"));
		intent.putExtra("delivery_time", orderList.get(position).getDeliveryTime());
		intent.putExtra("status", orderList.get(position).getOrdderStatus());
		intent.putExtra("salesorderid", orderList.get(position).getOrderid());
		intent.putExtra("isPreponable", orderList.get(position).isPreponable());
		context.startActivity(intent);

		Map<String, String> articleParams = new HashMap<String, String>();
		articleParams.put("mobile", AppPrefrence.getMobile(context));
		FlurryAgent.logEvent("Reschedule Clicked on my orders", articleParams);
	}

	private void call_rider(int position){
		String rider = orderList.get(position).getRider();
		String[] rider_arr = rider.split("-");
		Intent intent = new Intent("android.intent.action.CALL");
		if(rider_arr.length > 1 && rider_arr[1] != null && rider_arr[1].length() >8) {
			intent.setData(Uri.parse("tel:" + rider_arr[1]));
			context.startActivity(intent);
		}
		else{
			Toast.makeText(context, "Rider number not available.", Toast.LENGTH_LONG).show();
		}
	}
	private void pay_now(int position){
		OrderListModel orderModel = orderList.get(position);
		paytmTransaction paytmInstance = new paytmTransaction("", orderModel.getTotal_payable(), orderModel.getContactid(), orderModel.getOrdderStatus(), orderModel.getOrderid(), orderModel.getSalesorder_id(), context);
		paytmInstance.get_paytm_id_n_start_payment();
	}
	private void cancelOrder(final int position){
		CancelOrder cancel_order_ob = new CancelOrder(new CancelOrder.CancelOrderInterface() {
			@Override
			public void onResult() {
				Intent intent = new Intent(context, MainActivity.class);
				intent.putExtra("SESSION_ID", "1");
				intent.putExtra("id", 1);
				intent.putExtra("Context", "Review");
				context.startActivity(intent);
			}

			@Override
			public void onError() {
				Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_LONG).show();
			}
		},context, orderList.get(position).getOrderid());
		cancel_order_ob.cancel_order();
	}
	private void sendmail(int position) {
		String so = orderList.get(position).getSalesorder_id();
		Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto","orders@pickmylaundry.in", null));
		emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "User query - Order#"+so);
		emailIntent.putExtra(android.content.Intent.EXTRA_TEXT," ");
		try {
			context.startActivity(Intent.createChooser(emailIntent, "Send mail..."));
		}
		catch (ActivityNotFoundException ex) {
			Toast.makeText(context, "There is no email client installed.", Toast.LENGTH_SHORT).show();
		}
	}


}
