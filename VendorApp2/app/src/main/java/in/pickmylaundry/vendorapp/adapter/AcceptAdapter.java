package in.pickmylaundry.vendorapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import in.pickmylaundry.vendorapp.R;
import in.pickmylaundry.vendorapp.model.AcceptModel;

/**
 * Created by 121 on 2/12/2016.
 */
public class AcceptAdapter extends BaseAdapter {

    private ArrayList<AcceptModel> list;
    private Context mContext;

    public AcceptAdapter(Context context, ArrayList<AcceptModel> list) {
        this.list = list;
        this.mContext = context;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.accept_item, parent, false);
        }
        TextView so = (TextView) convertView.findViewById(R.id.so);
        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView desc = (TextView) convertView.findViewById(R.id.desc);
        CheckBox cb = (CheckBox) convertView.findViewById(R.id.cb);

        so.setText(list.get(position).getSo());
        name.setText(list.get(position).getName());
        desc.setText(list.get(position).getClothes_description());
        if(list.get(position).getCBStatus()) {
            cb.setChecked(true);
        }
        else
            cb.setChecked(false);
        cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                list.get(position).setCBStatus(isChecked);
                Log.i("list", list.get(position).getCBStatus()+"");
            }
        });
        return convertView;
    }
}
