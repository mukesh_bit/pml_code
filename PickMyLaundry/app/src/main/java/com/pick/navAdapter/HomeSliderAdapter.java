package com.pick.navAdapter;


import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.pick.mylaundry.R;
import com.pick.volley.ImageLoader;

import java.util.ArrayList;

/**
 * Created by 121 on 12/19/2015.
 */
public class HomeSliderAdapter extends PagerAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<String> sliderUrlArray;
    private ImageLoader imageLoader;

    public HomeSliderAdapter(Context context, ArrayList<String> sliderUrlArray) {
        this.mContext = context;
        this.sliderUrlArray = sliderUrlArray;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        imageLoader = new ImageLoader(context, 200, 500);
    }

    @Override
    public int getCount() {
        return sliderUrlArray.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.home_slider_item, container, false);
        ImageView sliderImage = (ImageView) itemView.findViewById(R.id.slider_image);

        if(sliderUrlArray.get(position).equals("default_home_image"))
            sliderImage.setImageResource(R.drawable.dc_fe);
        else {
            imageLoader.DisplayImage(sliderUrlArray.get(position), sliderImage, "home_slider", itemView);
        }
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public float getPageWidth(int position) {
        return 0.97f;
    }
}
