package in.pickmylaundry.riderapp.ui;

/**
 * Created by 121 on 8/5/2016.
 */
public abstract class SwipeLayoutCustomItems {
    public int width = 150;
    public int height = 150;
    private double[] actionConfirmDistanceFraction = {0.85, 0.2};
    public int gradientColor1;
    public String buttonText;

    public String getActionCancelText() {
        return ActionCancelText;
    }

    public void setActionCancelText(String actionCancelText) {
        ActionCancelText = actionCancelText;
    }

    public String ActionCancelText;

    public String getButtonText() {
        return buttonText;
    }

    public void setButtonText(String buttonText) {
        this.buttonText = buttonText;
    }

    public int getGradientColor2() {
        return gradientColor2;
    }

    public void setGradientColor2(int gradientColor2) {
        this.gradientColor2 = gradientColor2;
    }

    public int gradientColor2;
    public int postConfirmationColor;
    public String actionConfirmText;

    public int getPostConfirmationColor() {
        return postConfirmationColor;
    }

    public void setPostConfirmationColor(int postConfirmationColor) {
        this.postConfirmationColor = postConfirmationColor;
    }

    public String getActionConfirmText() {
        return actionConfirmText;
    }

    public void setActionConfirmText(String actionConfirmText) {
        this.actionConfirmText = actionConfirmText;
    }

    public String getButtonPressText(int type) {
        return ButtonPressText[type];
    }

    public void setButtonPressText(String[] buttonPressText) {
        ButtonPressText = buttonPressText;
    }

    private String[] ButtonPressText;

    public int getGradientColor3() {
        return gradientColor3;
    }

    public void setGradientColor3(int gradientColor3) {
        this.gradientColor3 = gradientColor3;
    }

    public int getGradientColor1() {
        return gradientColor1;
    }

    public void setGradientColor1(int gradientColor1) {
        this.gradientColor1 = gradientColor1;
    }

    private int gradientColor3;

    public double getActionConfirmDistanceFraction(int type) {
        return actionConfirmDistanceFraction[type];
    }

    public void setActionConfirmDistanceFraction(double[] actionConfirmDistanceFraction) {
        this.actionConfirmDistanceFraction = actionConfirmDistanceFraction;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public abstract void onSwipeConfirm(int type);
}
