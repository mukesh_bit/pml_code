package com.mylaundry.vendorapp;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class Splashscreen extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1000;
    public static final String Splashscreen = "Splashscreen";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splashscreen);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                SharedPreferences sharedPref = Splashscreen.this.getSharedPreferences(getString(R.string.PREFS_NAME), Context.MODE_PRIVATE);
                if((sharedPref.getString(getString(R.string.VendorName), null) == null)
                        && (sharedPref.getString(getString(R.string.Password), null) == null) && (sharedPref.getInt(getString(R.string.vendor_id),0) == 0)){
                    Intent i = new Intent(Splashscreen.this, LoginActivity.class);
                    startActivity(i);
                }
                else{
                    Intent i = new Intent(Splashscreen.this, MainActivity.class);
                    startActivity(i);
                }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}