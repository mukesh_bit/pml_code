package com.pick.navmodel;

public class OrderListModel {
	private  String Orderid="";
    private  String Orderdate="";
    private  String ordderStatus="";
    private String line1="";
    private String instruction="";
	private String total_payable;

	public String getContactid() {
		return contactid;
	}

	public void setContactid(String contactid) {
		this.contactid = contactid;
	}

	private String contactid;

	private String payment_status;

	public String getPayment_status() {
		return payment_status;
	}

	public void setPayment_status(String payment_status) {
		this.payment_status = payment_status;
	}


	public String getRider() {
		return rider;
	}

	public void setRider(String rider) {
		this.rider = rider;
	}

	private String rider;

	public boolean isPreponable() {
		return isPreponable;
	}

	public void setIsPreponable(boolean isPreponable) {
		this.isPreponable = isPreponable;
	}

	private boolean isPreponable;

	public String getTurnaround() {
		return turnaround;
	}

	public void setTurnaround(String turnaround) {
		this.turnaround = turnaround;
	}

	private String turnaround;
    private String line3="";

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getInstruction() {
		return instruction;
	}

	public void setInstruction(String instruction) {
		this.instruction = instruction;
	}

	private String category="";
    private String pickupdate,pickuptime,deliveryDate,deliveryTime,totalbill,salesorder_id;
	private String processing_status;

	public String getPickupdate() {
		return pickupdate;
	}
	public void setPickupdate(String pickupdate) {
		this.pickupdate = pickupdate;
	}
	public String getPickuptime() {
		return pickuptime;
	}
	public void setPickuptime(String pickuptime) {
		this.pickuptime = pickuptime;
	}
	public String getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getDeliveryTime() {
		return deliveryTime;
	}
	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}
	public String getTotalbill() {
		return totalbill;
	}
	public void setTotalbill(String totalbill) {
		this.totalbill = totalbill;
	}
	public String getOrderid() {
		return Orderid;
	}
	public void setOrderid(String orderid) {
		Orderid = orderid;
	}

	public String getSalesorder_id() {
		return salesorder_id;
	}

	public void setSalesorder_id(String salesorder_id) {
		this.salesorder_id = salesorder_id;
	}

	public String getOrderdate() {
		return Orderdate;
	}
	public void setOrderdate(String orderdate) {
		Orderdate = orderdate;
	}
	public String getOrdderStatus() {
		return ordderStatus;
	}
	public void setOrdderStatus(String ordderStatus) {
		this.ordderStatus = ordderStatus;
	}
	public String getLine1() {
		return line1;
	}
	public void setLine1(String line1) {
		this.line1 = line1;
	}
	public String getLine3() {
		return line3;
	}
	public void setLine3(String line3) {
		this.line3 = line3;
	}

	public String getProcessing_status() {
		return processing_status;
	}

	public void setProcessing_status(String processing_status) {
		this.processing_status = processing_status;
	}

	public String getTotal_payable() {
		return total_payable;
	}

	public void setTotal_payable(String total_payable) {
		this.total_payable = total_payable;
	}
}
