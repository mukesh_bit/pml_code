package com.pick.mylaundry;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.rey.material.widget.RadioButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by 121 on 12/13/2015.
 */
public class TimeslotFragment extends Fragment {
    private View rootView;
    private Bundle bundle;
    private String type;
    private RadioGroup slotGroup;
    private RadioButton[] rb = new RadioButton[8];
    private TextView scheduleTitle;
    private int min_index;
    private LinearLayout timeslot_ll;
    private ArrayList<String> inactive_slots;
    private JSONObject slots_ob;
    private LayoutInflater inflater;
    private String keys;
    private Boolean isPreponable = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = (View) inflater.inflate(R.layout.timeslots, container, false);
        this.inflater = inflater;

        bundle = (Bundle) getArguments();
        type = bundle.getString("type");
        min_index = bundle.getInt("min_index");
        keys = bundle.getString("keys");
        if(bundle.containsKey("isPreponable"))
            isPreponable = bundle.getBoolean("isPreponable");


        timeslot_ll = (LinearLayout) rootView.findViewById(R.id.timeslots);

        //set slot view
        inactive_slots = bundle.getStringArrayList("inactive_slots");
        addSlots();

        return rootView;
    }
    CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            RadioButton rb_checked = (RadioButton) rootView.findViewWithTag(buttonView.getTag());
            String selected_slot = rb_checked.getText().toString();
            try {
                TimeUnit.MILLISECONDS.sleep(300);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Intent intent = new Intent();
            intent.putExtra("date", bundle.getString("date"));
            intent.putExtra("slot", selected_slot);
            if (type.equals("Pickup"))
                getActivity().setResult(1, intent);
            else
                getActivity().setResult(2, intent);
            getActivity().finish();
        }
    };
    private void addSlots(){
        try {
            slots_ob = new JSONObject(bundle.getString("slots"));
            Log.i("slots_timeslot", slots_ob.toString());
            JSONArray keys_arr = new JSONArray(keys);
            Log.i("key_timeslot", keys_arr.toString());
            int iteration_count=0;
            for(int j=0; j<keys_arr.length();j++){
                String key = keys_arr.getString(j);
                Log.i("key_val", key);
                JSONArray slots_part = slots_ob.getJSONArray(key);
                View slot_heading_view = inflater.inflate(R.layout.slot_heading, null);
                timeslot_ll.addView(slot_heading_view);
                TextView heading = (TextView) slot_heading_view.findViewById(R.id.heading);
                heading.setText(key);
                for (int i=0;i<slots_part.length();i++){
                    View slot_view = inflater.inflate(R.layout.slot, null);
                    RadioButton rb = (RadioButton) slot_view.findViewById(R.id.slot);
                    rb.setTag("slot"+iteration_count);
                    if(isInactive(slots_part.get(i).toString())){
                        rb.setEnabled(false);
                        rb.setTextColor(Color.parseColor("#CECECE"));
                    }
                    else {
                        rb.setOnCheckedChangeListener(listener);
                    }
                    rb.setText(slots_part.get(i).toString());
                    timeslot_ll.addView(slot_view);
                    iteration_count++;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private boolean isInactive(String slot){
        if(inactive_slots.contains(slot))
            return true;
        else if(type.equals("Delivery")&&bundle.containsKey("pickup_slot")&&!isPreponable){
            String pickup_slot = bundle.getString("pickup_slot");
            String[] pickup_slot_arr = pickup_slot.split(" ");
            int pickup_slot_time = Integer.parseInt(pickup_slot_arr[0]);
            if(pickup_slot_arr[1].equals("PM"))
                pickup_slot_time += 12;
            String[] curr_slot_arr = slot.split(" ");
            int curr_slot_time = Integer.parseInt(curr_slot_arr[0]);
            if(curr_slot_arr[1].equals("PM"))
                curr_slot_time += 12;
            Log.i("inactive", pickup_slot+""+pickup_slot_time+""+slot+""+curr_slot_time);
            if(pickup_slot_time>curr_slot_time)
                return true;
            else
                return false;
        }
        return false;

    }
}
