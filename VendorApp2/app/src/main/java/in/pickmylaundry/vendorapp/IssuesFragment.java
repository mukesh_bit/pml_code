package in.pickmylaundry.vendorapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.vendorapp.Utils.Constant;
import in.pickmylaundry.vendorapp.Utils.UploadTask;

/**
 * Created by 121 on 1/30/2016.
 */
public class IssuesFragment extends android.app.Fragment {

    private EditText so;
    private Button get_so_detail;
    private String vendor;
    private UploadTask task;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        SharedPreferences prefs = getActivity().getSharedPreferences("login_status", getActivity().MODE_PRIVATE);
        vendor = prefs.getString("vendor_name", "");
        so = (EditText) rootView.findViewById(R.id.so);
        get_so_detail = (Button) rootView.findViewById(R.id.button);
        get_so_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               request_so_detail();
            }
        });
        return rootView;
    }
    private void request_so_detail(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("so", so.getText().toString());
        params.put("vendor", vendor);
        get_so_detail(params);
    }
    private void get_so_detail(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){
                        String name = response.getString("name");
                        JSONArray products = response.getJSONArray("products");
                        String[] list = new String[products.length()];
                        for (int i=0;i<products.length();i++){
                            list[i] = products.getString(i);
                        }
                        Intent intent = new Intent(getActivity(), AddImageActivity.class);
                        intent.putExtra("so", so.getText().toString());
                        intent.putExtra("name", name);
                        intent.putExtra("products", list);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(getActivity().getApplicationContext(), response.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, getActivity(), Constant.get_so_detail, params);
        task.execute();
    }
}
