package in.pickmylaundry.vendorapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.vendorapp.Utils.Constant;
import in.pickmylaundry.vendorapp.Utils.UploadTask;
import in.pickmylaundry.vendorapp.adapter.NavDrawerListAdapter;
import in.pickmylaundry.vendorapp.model.NavDrawerItems;

public class MainActivity extends AppCompatActivity {

    private static final int DEFAULT_ID = -1;
    private String[] navMenuItems;
    private TypedArray navMenuIcons;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private GoogleCloudMessaging gcm;

    private ArrayList<NavDrawerItems> navDrawerItems;
    private NavDrawerListAdapter adapter;    // nav drawer title
    private CharSequence mDrawerTitle;

    // used to store app title
    private CharSequence mTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    String SENDER_ID = "600458637781";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private String regid;
    private boolean isUnRegistered = false, isRegIDSent = true;
    private Context context;
    private Handler mHandler = new Handler();
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTitle = mDrawerTitle = getTitle();

        navMenuItems = getResources().getStringArray(R.array.nav_drawer_items);
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        navDrawerItems = new ArrayList<NavDrawerItems>();
        for(int i=0;i<navMenuItems.length;i++){
            NavDrawerItems item = new NavDrawerItems(navMenuItems[i], navMenuIcons.getResourceId(i, DEFAULT_ID));
            navDrawerItems.add(item);
        }

        //recycling typed array, do not touch array after it
        navMenuIcons.recycle();

        adapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

        //enable toggle button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_drawer);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ){
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        bundle = getIntent().getExtras();
        if(bundle != null){
            int id = Integer.parseInt(bundle.getString("id"));
            displayView(id);
        }
        else if (savedInstanceState == null) {
            // on first time display view for first nav item
            displayView(0);
        }

        //gcm registration
        context = getApplicationContext();
        regid = getRegistrationId(context);
        mHandler.postDelayed(mRunnable, 1000);


    }
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }
    private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new IssuesFragment();
                setTitle("Raise Issue");
                break;
            case 1:
                fragment = new HistoryActivity();
                setTitle("History");
                break;
            case 2:
                fragment = new AcceptFragment();
                setTitle("Accept Lots");
                break;
            case 3:
                fragment = new ReadyFragment();
                setTitle("Ready Lots");
                break;
            case 4:
                fragment = new TicketsFragment();
                break;
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            if(bundle != null)
                fragment.setArguments(bundle);
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment).commit();
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            mDrawerLayout.closeDrawer(mDrawerList);

        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_settings).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private final Runnable mRunnable = new Runnable() {

        @Override
        public void run() {
            // Check device for Play Services APK. If check succeeds, proceed with
            //  GCM registration.
            SharedPreferences prefs = getGCMPreferences(getApplicationContext());
            Boolean loggedOut = prefs.getBoolean("loggedOut", false);
            //Log.i("staus",""+loggedOut);
            if (checkPlayServices()&&!loggedOut) {
                gcm = GoogleCloudMessaging.getInstance(MainActivity.this);
                regid = getRegistrationId(context);
                //Log.i("register", "inside regid"+regid);
                if (regid.isEmpty() && !isUnRegistered) {
                    registerInBackground();
                    isRegIDSent = false;
                }
                else if(!isRegIDSent){
                    isRegIDSent = true;
                    postRegidRequest(regid);
                }

            } else {
                if(!isUnRegistered){
                    isUnRegistered = true;
                    try {
                        gcm.unregister();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            //findLocation();
            mHandler.postDelayed(mRunnable, 1000);
        }
    };
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.equals("")) {
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing registration ID is not guaranteed to work with
        // the new app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            return "";
        }
        return registrationId;
    }

    private boolean checkPlayServices() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
                //showErrorDialog(status);
            } else {
                Toast.makeText(this, "This device is not supported for notification service", Toast.LENGTH_LONG).show();
                finish();
            }
            return false;
        }
        return true;
    }


    private void registerInBackground() {
        final AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                String msg = "";
                SharedPreferences prefs = getGCMPreferences(getApplicationContext());
                Boolean loggedOut = prefs.getBoolean("loggedOut", false);
                if (loggedOut){
                    //task.cancel(true);
                    return null;
                }
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    if (!loggedOut)
                        regid = gcm.register(SENDER_ID);
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                }
                return null;
            }

        }.execute();

    }

    private void unregisterInBackground() {
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    gcm.unregister();
                    regid = "";
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                }
                return null;
            }

        }.execute();

    }

    private void storeRegistrationId(Context context, String regId) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences("gcmregistartion",
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    private void postRegidRequest(String regid){
        Map<String, Object> params = new HashMap<String, Object>();
        SharedPreferences prefs = getSharedPreferences("login_status", MODE_PRIVATE);
        String vendor = prefs.getString("vendor_name", "");
        params.put("vendor", vendor);
        params.put("reg_id", regid);
        Log.i("reg_id", regid);
        sendRegid(params);
    }

    private void sendRegid(Map<String, Object> params) {
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                    }

                } catch (JSONException e){
                    //Toast.makeText(getActivity().getApplicationContext(), "Oops Something Went Wrong", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            @Override
            public void taskException() {
                //Toast.makeText(getActivity().getApplicationContext(),"Please check Your Internet Connection",Toast.LENGTH_LONG).show();
            }
        },MainActivity.this, Constant.GCM_URL, params);

        task.execute();
    }

}
