package com.pick.navAdapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pick.mylaundry.R;
import com.pick.navmodel.TestimonialModel;
import com.pick.utill.Constant;
import com.pick.volley.ImageLoader;

import java.util.ArrayList;

/**
 * Created by 121 on 10/24/2016.
 */
public class TestimonialAdapter extends PagerAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private ArrayList<TestimonialModel> modelList;
    private ImageLoader imageLoader;
    private LinearLayout indicator_ll;

    public TestimonialAdapter(Context context, ArrayList<TestimonialModel> modelList, LinearLayout indicator_ll) {
        this.mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.modelList = modelList;
        imageLoader=new ImageLoader(context);
        this.indicator_ll = indicator_ll;
    }

    @Override
    public int getCount() {
        return modelList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.testimonial_item, container, false);

        ImageView testimonial_profile = (ImageView) itemView.findViewById(R.id.nav_profile_image);
        TextView name = (TextView) itemView.findViewById(R.id.name);
        TextView proefession = (TextView) itemView.findViewById(R.id.proefession);
        TextView testimonial = (TextView) itemView.findViewById(R.id.testimonial);

        name.setText(modelList.get(position).getName());
        proefession.setText(modelList.get(position).getProfession());
        testimonial.setText(modelList.get(position).getTestimonial());
        imageLoader.DisplayImage(modelList.get(position).getImage_url().toString(), testimonial_profile, "testimonial", itemView);

        container.addView(itemView);
        return itemView;
    }
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        add_indicator(0);
    }
    public void add_indicator(int active_position){
        indicator_ll.removeAllViews();
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams((int)Constant.convertDpToPixel(8, mContext),(int) Constant.convertDpToPixel(8, mContext));
        params.setMargins((int) Constant.convertDpToPixel(3, mContext), 0, 0, 0);
        for(int i=0; i<modelList.size(); i++) {
            TextView indicator_tv = new TextView(mContext);
            if(i==active_position)
                indicator_tv.setBackgroundResource(R.drawable.circle_dot_solid);
            else
                indicator_tv.setBackgroundResource(R.drawable.circle_dot);
            indicator_tv.setTag("indicator-"+i);
            indicator_tv.setLayoutParams(params);
            indicator_ll.addView(indicator_tv);
        }
    }
    public void set_data(ArrayList<TestimonialModel> modelList){
        this.modelList = modelList;
        notifyDataSetChanged();
    }
}
