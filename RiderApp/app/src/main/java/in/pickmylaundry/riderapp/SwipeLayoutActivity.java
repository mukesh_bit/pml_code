package in.pickmylaundry.riderapp;

import android.app.Activity;
import android.os.Bundle;

import in.pickmylaundry.riderapp.ui.SwipeLayout2;
import in.pickmylaundry.riderapp.ui.SwipeLayoutCustomItems;

/**
 * Created by 121 on 8/5/2016.
 */
public class SwipeLayoutActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.swipe_layout);

        SwipeLayout2 swipeLayout = (SwipeLayout2) findViewById(R.id.swipe_layout);
        SwipeLayoutCustomItems swipeLayoutCustomItems = new SwipeLayoutCustomItems(){
            @Override
            public void onSwipeConfirm(int type) {

            }
        };
        String[] pressedText = {">> Swipe Right to Confirm >>>>", "<<<< Swipe Right to Confirm <<"};
        swipeLayoutCustomItems.setButtonPressText(pressedText);
        swipeLayoutCustomItems.setGradientColor1(0xFF72c3ea);
        swipeLayoutCustomItems.setGradientColor3(0xFF3399CC);
        swipeLayoutCustomItems.setGradientColor2(0xFF98d8f7);
        swipeLayoutCustomItems.setPostConfirmationColor(0xFF3399CC);
        swipeLayoutCustomItems.setActionConfirmText("Confirmed");

        swipeLayout.setSwipeLayoutCustomItems(swipeLayoutCustomItems);
    }

}
