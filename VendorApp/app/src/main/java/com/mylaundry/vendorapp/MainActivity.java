package com.mylaundry.vendorapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private String urlget = "http://crm.pickmylaundry.in/web_services/vendor_app/get_so_info.php?";
    private TextView vendorname;
    private EditText editsalesorderid;
    SharedPreferences sharedPref;
    private Button btnsubmitso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedPref = MainActivity.this.getSharedPreferences(getString(R.string.PREFS_NAME), Context.MODE_PRIVATE);
        vendorname = (TextView) findViewById(R.id.vendorname);

        editsalesorderid = (EditText) findViewById(R.id.editsalesorderid);
        btnsubmitso=(Button)findViewById(R.id.btnsubmitso);
        vendorname.setText(sharedPref.getString(getString(R.string.VendorName), ""));

        btnsubmitso.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.title_bar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.button_logout) {
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.VendorName), null);
            editor.putString(getString(R.string.Password), null);
            editor.putInt(getString(R.string.vendor_id), 0);
            editor.commit();
            Intent i = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnsubmitso:
                getSoDetails(editsalesorderid.getText().toString());
                break;
        }
    }

    public void getSoDetails(final String salesorderid) {
        String tag_json_obj = "post";

        StringRequest post = new StringRequest(Request.Method.POST, urlget,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        try {
                            if (response != null) {
                                JSONObject obj = new JSONObject(response);
                                String success = obj.getString("success");
                                if (success.contains("true")) {
                                    Intent details = new Intent(MainActivity.this, ImageActivity.class);
                                    details.putExtra("salesorderid","SO"+salesorderid);
                                    details.putExtra("nameofcustomer", obj.getString("name"));
                                    details.putExtra("pickup_boy", obj.getString("pickup_boy"));
                                    details.putExtra("pickup_date",obj.getString("pickup_date") );
                                    startActivity(details);
                                    finish();
                                } else if (success.contains("false")) {
                                    Toast.makeText(MainActivity.this, obj.getString("msg"),
                                            Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Log.e("ServiceHandler", "Couldn't get any data from the url");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("MainActivity", "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(),
                         error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> postParam = new HashMap<String, String>();
                postParam.put("so", "SO"+salesorderid);
                return postParam;
            }
        };
        AppController.getInstance().addToRequestQueue(post, tag_json_obj);
    }

}