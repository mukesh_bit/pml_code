package com.pick.mylaundry;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.pick.navAdapter.AddressAdapter;
import com.pick.navmodel.AddressListModel;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.utill.Pickhelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by 121 on 12/14/2015.
 */
public class SelectAddress extends Activity {
    private ArrayList<AddressListModel> adressList;
    private Pickhelper dbhelper;
    private SQLiteDatabase db;
    private ListView addlist;
    private LinearLayout addAddress, noAddress;
    private TextView toolbar_title;
    private ImageView back, next;
    private Button addButton;
    private Bundle bundle;
    public static int ACTIVE_ADDRESS_POSITION;
    public static int ACTIVE_PRIMARY_POSITION;
    private View rootView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_address);
        rootView = (RelativeLayout) findViewById(R.id.rootView);
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/HelveticaNeue-Regular.ttf");
        toolbar_title = (TextView) findViewById(R.id.actionbar_custom_title);
        toolbar_title.setText("SELECT ADDRESS");
        toolbar_title.setTypeface(tf);
        bundle = getIntent().getExtras();
        addlist = (ListView) findViewById(R.id.addressList);
        noAddress = (LinearLayout) findViewById(R.id.no_address);
        addAddress = (LinearLayout) findViewById(R.id.add_address);
        addButton = (Button) findViewById(R.id.addButton);
        addAddress.setOnClickListener(addAddressListener);
        addButton.setOnClickListener(addAddressListener);
        setAddressList();
        RelativeLayout reviewOrder = (RelativeLayout) findViewById(R.id.reviewOrder);
        reviewOrder.setOnClickListener(proceedForwardListener);
        reviewOrder.setOnTouchListener(touchListener);
        back = (ImageView) findViewById(R.id.back_icon);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SelectAddress.this.onBackPressed();
            }
        });
        next = (ImageView) findViewById(R.id.nextImage);
        next.setOnClickListener(proceedForwardListener);
    }
    private View.OnClickListener addAddressListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                Intent intent = new Intent(SelectAddress.this, AddAddress.class);
                intent.putExtra("mode", "Add");
                startActivity(intent);

                Map<String, String> articleParams = new HashMap<String, String>();
                articleParams.put("mobile", AppPrefrence.getMobile(SelectAddress.this));
                articleParams.put("activity", "open");
                FlurryAgent.logEvent("Add Address", articleParams);
        }
    };
    private View.OnClickListener proceedForwardListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!adressList.isEmpty()) {
                Intent intent;
                intent = new Intent(SelectAddress.this, ScheduleActivity.class);
                bundle.putInt("primary_key", ACTIVE_PRIMARY_POSITION);
                intent.putExtra("data_bundle", bundle);
                startActivity(intent);
            }
            else{
                Snackbar.make(rootView, "Please select a address", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
            }
        }
    };
    private View.OnTouchListener touchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            next.setPressed(false);
            next.invalidate();
            next.setPressed(true);
            next.invalidate();
            return false;
        }
    };
    private void setAddressList() {
        delete_previous_orders();
        adressList= new ArrayList<AddressListModel>();
        dbhelper=new Pickhelper(SelectAddress.this);
        db=dbhelper.getWritableDatabase();
        db.execSQL(Pickhelper.CREATE_TABLE_ADDRESS);
        String selectQuery = "SELECT * FROM "+Pickhelper.TABLE_ADDRESS;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                final AddressListModel bean=new AddressListModel();
                bean.setPrimarykey(cursor.getInt(0));
                bean.setFlatno((cursor.getString(1)));
                bean.setLandmark(cursor.getString(2));
                bean.setArea(cursor.getString(3));
                bean.setType(cursor.getString(4));
                bean.setYes(cursor.getString(5));
                adressList.add(bean);
            } while (cursor.moveToNext());
        }
        ((ListView)addlist).setAdapter(new AddressAdapter(SelectAddress.this, adressList));
        if(adressList.isEmpty()){
            noAddress.setVisibility(View.VISIBLE);
            addAddress.setVisibility(View.GONE);
        }
        else{
            noAddress.setVisibility(View.GONE);
            addAddress.setVisibility(View.VISIBLE);
        }
        db.close();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==1&&resultCode==Activity.RESULT_OK){
            ((ListView) addlist).setAdapter(null);
            setAddressList();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ((ListView) addlist).setAdapter(null);
        setAddressList();
    }

    @Override
    protected void onStart() {
        super.onStart();

        FlurryAgent.setLogEnabled(false);
        FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
        FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("mobile", AppPrefrence.getMobile(SelectAddress.this));
        articleParams.put("activity", "started");
        FlurryAgent.logEvent("Select Address", articleParams);
    }
    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }
    private void delete_previous_orders(){
        SharedPreferences preferences = getSharedPreferences("address_delete", Context.MODE_PRIVATE);
        boolean isDeleted = preferences.getBoolean("isDeleted", false);
        if(!isDeleted){
            dbhelper = new Pickhelper(SelectAddress.this);
            db = dbhelper.getWritableDatabase();
            db.delete(Pickhelper.TABLE_ADDRESS, null, null);
            db.close();
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("isDeleted", true);
            editor.apply();
        }
    }
}
