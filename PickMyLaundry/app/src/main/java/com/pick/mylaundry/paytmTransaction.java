package com.pick.mylaundry;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 121 on 11/23/2015.
 */
public class paytmTransaction {
    private String paytm_id, total, contactid, orderstatus, order_id, salesorder_no;
    private Context context;
    private PaytmPGService Service = null;
    public paytmTransaction(String paytmid, String total, String contactId, String orderStatus, String order_id, String salesorder_no, Context context) {
        this.paytm_id = paytmid;
        this.total = total;
        this.contactid = contactId;
        this.orderstatus = orderStatus;
        this.order_id = order_id;
        this.salesorder_no = salesorder_no;
        this.context = context;
        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("mobile", AppPrefrence.getMobile(context));
        FlurryAgent.onStartSession(context, Constant.MY_FLURRY_APIKEY);
        FlurryAgent.logEvent("pay_now_clicked", articleParams);
    }
    public void startPayment(){
        //for testing environment
        //Service = PaytmPGService.getStagingService();
        //for production environment
    	Service = PaytmPGService.getProductionService();

        PaytmMerchant Merchant = new PaytmMerchant("http://crm.pickmylaundry.in/paytm/generateChecksum.php","http://crm.pickmylaundry.in/paytm/verifyChecksum.php");
        Map<String, String> paramMap = new HashMap<String, String>();

        //these are mandatory parameters
        paramMap.put("REQUEST_TYPE", "DEFAULT");
        paramMap.put("ORDER_ID", paytm_id);
        paramMap.put("MID", "Pickmy95386081654460");
        paramMap.put("CUST_ID", contactid);
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("INDUSTRY_TYPE_ID", "Retail");
        paramMap.put("WEBSITE", "PMLSPLWAP");
        paramMap.put("TXN_AMOUNT", total);
        paramMap.put("THEME", "merchant");
        paramMap.put("EMAIL", AppPrefrence.getEmail(context));
        paramMap.put("MOBILE_NO", AppPrefrence.getMobile(context));
        PaytmOrder Order = new PaytmOrder(paramMap);


        Service.initialize(Order, Merchant, null);
        Service.startPaymentTransaction(context, false, false, new PaytmPaymentTransactionCallback() {
            @Override
            public void onTransactionSuccess(Bundle bundle) {
                String respmsg = bundle.getString("RESPMSG");
                Intent intent = new Intent(context, PaymentStatus.class);
                intent.putExtra("order_id", order_id);
                intent.putExtra("amount", bundle.getString("TXNAMOUNT"));
                intent.putExtra("status", bundle.getString("STATUS"));
                intent.putExtra("sostatus", orderstatus);
                intent.putExtra("total", bundle.getString("TXNAMOUNT"));
                intent.putExtra("salesorder", salesorder_no);
                context.startActivity(intent);
                flurry_data("success");
            }

            @Override
            public void onTransactionFailure(String s, Bundle bundle) {
                Log.i("fail", "Cancelled");
                Intent intent = new Intent(context, PaymentStatus.class);
                intent.putExtra("paytm_id", paytm_id);
                intent.putExtra("contactid", contactid);
                intent.putExtra("order_id", order_id);
                intent.putExtra("status", "TXN_FAILURE");
                intent.putExtra("sostatus", orderstatus);
                intent.putExtra("total", total);
                intent.putExtra("salesorder", salesorder_no);
                context.startActivity(intent);
                flurry_data(s);
            }

            @Override
            public void networkNotAvailable() {
                Log.i("fail", "Network Not Available.");
            }

            @Override
            public void clientAuthenticationFailed(String s) {
                Log.i("fail", "Client Authentication failed.");
                //app.getLogger().error("clientAuthenticationFailed :" + s);
                flurry_data(s);
            }

            @Override
            public void someUIErrorOccurred(String s) {
                Log.i("fail", "UiError");
                //app.getLogger().error("someUIErrorOccurred :" + s);
                flurry_data(s);
            }

            @Override
            public void onErrorLoadingWebPage(int i, String s, String s2) {
                Log.i("fail", "ErrorLoadingWebpage");
                //app.getLogger().error("errorLoadingWebPage :" + i + "\n" + s + "\n" + s2);
                flurry_data(s + "\n" + s2);
            }

            public void flurry_data(String text) {
                Map<String, String> articleParams = new HashMap<String, String>();
                articleParams.put("mobile", AppPrefrence.getMobile(context));
                articleParams.put("status", text);
                //up to 10 params can be logged with each event
                FlurryAgent.logEvent("pay_now_result", articleParams);
            }
        });

    }

    public void get_paytm_id_n_start_payment(){
        //set params
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("orderid", order_id));

        new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        paytm_id = json.getString("paytm_id");
                        startPayment();
                    }
                    else{
                        Toast.makeText(context, "Some error occured. Please write to us, if this continue", Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e){
                    Toast.makeText(context, "Some error occured. Please write to us, if this continue", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(context, "Some error occured. Please write to us, if this continue", Toast.LENGTH_LONG).show();
            }
        },context, Constant.PAYTMID,params, true).execute();

    }

    }
