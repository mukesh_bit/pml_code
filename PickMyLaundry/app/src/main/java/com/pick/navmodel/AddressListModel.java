package com.pick.navmodel;

public class AddressListModel {
	private String  flatno,society,landmark,area,city,type,yes;
	private int primarykey,id;
	public int getPrimarykey() {
		return primarykey;
	}

	public void setPrimarykey(int primarykey) {
		this.primarykey = primarykey;
	}
	public String getFlatno() {
		return flatno;
	}

	public void setId(int id) {
		this.id = id;
	}
	public int getId() {
		return id;
	}

	public void setFlatno(String flatno) {
		this.flatno = flatno;
	}

	public String getSociety() {
		return society;
	}

	public void setSociety(String society) {
		this.society =society;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getYes() {
		return yes;
	}

	public void setYes(String yes) {
		this.yes = yes;
	}
	

}
