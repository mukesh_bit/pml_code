package com.pick.utill;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.paytm.pgsdk.PaytmMerchant;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.pick.mylaundry.MainActivity;
import com.pick.mylaundry.PaymentStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mukesh on 12/6/16.
 */

public class PaytmRecharge {
    private String paytm_id, total, contactid;
    private Context context;
    private PaytmPGService Service = null;

    public PaytmRecharge(String paytmid, String total, String contactId, Context context) {
        this.paytm_id = paytmid;
        this.total = total;
        this.contactid = contactId;
        this.context = context;
    }
    public void startPayment(){
        //for testing environment
        //Service = PaytmPGService.getStagingService();
        //for production environment
        Service = PaytmPGService.getProductionService();

        PaytmMerchant Merchant = new PaytmMerchant("http://crm.pickmylaundry.in/paytm/generateChecksum.php","http://crm.pickmylaundry.in/paytm/verifyChecksum.php");
        Map<String, String> paramMap = new HashMap<String, String>();

        //these are mandatory parameters
        paramMap.put("REQUEST_TYPE", "DEFAULT");
        paramMap.put("ORDER_ID", paytm_id);
        paramMap.put("MID", "Pickmy95386081654460");
        paramMap.put("CUST_ID", contactid);
        paramMap.put("CHANNEL_ID", "WAP");
        paramMap.put("INDUSTRY_TYPE_ID", "Retail");
        paramMap.put("WEBSITE", "PMLSPLWAP");
        paramMap.put("TXN_AMOUNT", total);
        paramMap.put("THEME", "merchant");
        paramMap.put("EMAIL", AppPrefrence.getEmail(context));
        paramMap.put("MOBILE_NO", AppPrefrence.getMobile(context));
        PaytmOrder Order = new PaytmOrder(paramMap);


        Service.initialize(Order, Merchant, null);
        Service.startPaymentTransaction(context, false, false, new PaytmPaymentTransactionCallback() {
            @Override
            public void onTransactionSuccess(Bundle bundle) {
                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra("SESSION_ID", "4");
                intent.putExtra("payment_status", "Success");
                intent.putExtra("payment_amount", total);
                context.startActivity(intent);
            }

            @Override
            public void onTransactionFailure(String s, Bundle bundle) {
                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra("SESSION_ID", "4");
                intent.putExtra("payment_status", "Failure");
                intent.putExtra("payment_amount", total);
                context.startActivity(intent);
            }

            @Override
            public void networkNotAvailable() {
                Log.i("fail", "Network Not Available.");
                Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_LONG).show();
            }

            @Override
            public void clientAuthenticationFailed(String s) {
                Log.i("fail", "Client Authentication failed.");
                Toast.makeText(context, "Authentication failure", Toast.LENGTH_LONG).show();
                flurry_data(s);
            }

            @Override
            public void someUIErrorOccurred(String s) {
                Log.i("fail", "UiError");
                Toast.makeText(context, "Some error occured", Toast.LENGTH_LONG).show();
                flurry_data(s);
            }

            @Override
            public void onErrorLoadingWebPage(int i, String s, String s2) {
                Log.i("fail", "ErrorLoadingWebpage");
                Toast.makeText(context, "Some error occured", Toast.LENGTH_LONG).show();
                flurry_data(s + "\n" + s2);
            }

            public void flurry_data(String text) {
                Map<String, String> articleParams = new HashMap<String, String>();
                articleParams.put("mobile", AppPrefrence.getMobile(context));
                articleParams.put("status", text);
                //up to 10 params can be logged with each event
                FlurryAgent.logEvent("pay_now_result", articleParams);
            }
        });

    }
}
