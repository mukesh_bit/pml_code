package in.pickmylaundry.vendorapp;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.vendorapp.Utils.Constant;
import in.pickmylaundry.vendorapp.Utils.UploadTask;
import in.pickmylaundry.vendorapp.adapter.ReadyAdapter;
import in.pickmylaundry.vendorapp.model.ReadyModel;

/**
 * Created by 121 on 2/12/2016.
 */
public class ReadyFragment extends android.app.Fragment {
    private View rootView;
    private ListView listView;
    private String vendor;
    private UploadTask task;
    private ArrayList<ReadyModel> list;
    private CheckBox cb;
    private ReadyAdapter adapter;
    private Button ready;
    private String so_send;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        rootView = (View) inflater.inflate(R.layout.ready_fragment, container, false);

        //get vendor name from prefs
        SharedPreferences prefs = getActivity().getSharedPreferences("login_status", getActivity().MODE_PRIVATE);
        vendor = prefs.getString("vendor_name", "");

        //list view
        listView = (ListView) rootView.findViewById(R.id.list);
        getListData();

        //select all cb listener
        cb = (CheckBox) rootView.findViewById(R.id.cb_all);
        cb.setOnCheckedChangeListener(listener);

        //ready button
        ready = (Button) rootView.findViewById(R.id.accept);
        ready.setOnClickListener(listener_accept);

        return rootView;
    }
    private CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            for (int i=0;i<list.size();i++)
                list.get(i).setCBStatus(isChecked);
            adapter.notifyDataSetChanged();
        }
    };
    private View.OnClickListener listener_accept = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            so_send = "";
            for (int i=0;i<list.size();i++){
                if(list.get(i).getCBStatus()){
                    so_send += "~"+list.get(i).getSalesorderId();
                }
            }
            if(!so_send.equals(""))
                accept_order();
            else
                Toast.makeText(getActivity(), "Please select at least one order", Toast.LENGTH_LONG).show();

        }
    };
    private void getListData(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("vendor", vendor);
        request_list_data(params);
    }
    private void request_list_data(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONArray response = new JSONArray(result);
                    list = new ArrayList<ReadyModel>();
                    for(int i=0; i<response.length();i++){
                        JSONObject obj = response.getJSONObject(i);
                        ReadyModel model = new ReadyModel();
                        model.setSo(obj.getString("so"));
                        model.setName(obj.getString("name"));
                        model.setSalesorderId(obj.getInt("salesorderid"));
                        model.setClothes_description(obj.getString("cloth_desc"));
                        list.add(model);
                    }
                    adapter = new ReadyAdapter(getActivity(), list);
                    listView.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void taskException() {
                Toast.makeText(getActivity(), "Error Occured.", Toast.LENGTH_LONG).show();
            }
        },getActivity(), Constant.READY_URL, params);
        task.execute();
    }
    public void accept_order(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("vendor", vendor);
        params.put("so", so_send);
        request_accept_order(params);
    }
    public void request_accept_order(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                Toast.makeText(getActivity(), "Accepted Succesfuly.", Toast.LENGTH_LONG).show();
                getListData();
            }

            @Override
            public void taskException() {
                Toast.makeText(getActivity(), "Error Occured.", Toast.LENGTH_LONG).show();
            }
        }, getActivity(), Constant.READY_ORDER, params);
        task.execute();
    }
}
