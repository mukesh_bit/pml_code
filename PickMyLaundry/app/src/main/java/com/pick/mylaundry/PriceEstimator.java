package com.pick.mylaundry;

/**
 * Created by Lalit on 8/18/2015.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PriceEstimator extends AppCompatActivity {

    private Toolbar toolbar;
    private static TabLayout tabLayout;
    private ViewPager viewPager;
    private PriceFragment priceFragment;
    private JSONObject priceList;
    private static RelativeLayout cartView;
    private TextView cartCount, cartTotal, title;
    private View rootView;
    private ImageView back_icon;
    private int[] tabIcons = {
            R.drawable.wf,
            R.drawable.men,
            R.drawable.women,
            R.drawable.household,
            R.drawable.woolen,
            R.drawable.accessories,
            R.drawable.kid
    };
    private String[] tabTitles = {
            "Laundry",
            "Men",
            "Women",
            "Household",
            "Woolen",
            "Accessories",
            "Kids"
    };
    private static Map<String, Map<String, Integer >> cart = new HashMap<String, Map<String, Integer >>();
    private static Map<String, Integer> item = new HashMap<String, Integer>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_tab);
        rootView = findViewById(R.id.rootView);

        title = (TextView) findViewById(R.id.actionbar_custom_title);
        title.setText("PRICE ESTIMATOR");

        //check for hint
        checkForHint();

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        cart.clear();
        cartView = (RelativeLayout) findViewById(R.id.cartView);
        cartCount = (TextView) findViewById(R.id.cart_items);
        cartTotal = (TextView) findViewById(R.id.cartTotal);
        cartCount.setText("0");
        cartTotal.setText("0");
        //get prices
        getPriceList();
        //on back button pressed
        back_icon = (ImageView) findViewById(R.id.back_icon);
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
                intent.putExtra("SESSION_ID", "0");
                intent.putExtra("id",1);
                intent.putExtra("Context","Review");
                startActivity(intent);
                finish();
            }
        });
    }
    private void checkForHint(){
        final SharedPreferences prefs = getSharedPreferences("price_hint", Context.MODE_PRIVATE);
        boolean isHintShown = prefs.getBoolean("isHintShown", false);
        if(!isHintShown) {
            final AppBarLayout appBar = (AppBarLayout) rootView.findViewById(R.id.appBar);
            final ImageView hint_image = (ImageView) rootView.findViewById(R.id.hint_image);
            appBar.setVisibility(View.GONE);
            hint_image.setVisibility(View.VISIBLE);
            hint_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putBoolean("isHintShown", true);
                    editor.commit();
                    appBar.setVisibility(View.VISIBLE);
                    hint_image.setVisibility(View.INVISIBLE);
                }
            });
        }
    }

    private void setupTabIcons() {
        for (int i=0; i<7;i++){
            TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
            tabOne.setText(tabTitles[i]);
            tabOne.setCompoundDrawablesWithIntrinsicBounds(0, tabIcons[i], 0, 0);
            tabLayout.getTabAt(i).setCustomView(tabOne);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        for (int i=0;i<7;i++) {
            priceFragment = new PriceFragment();
            Bundle args = new Bundle();
            try {
                args.putString("pricing_data", priceList.getJSONObject(tabTitles[i]).toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            args.putString("category", tabTitles[i]);
            priceFragment.setArguments(args);
            adapter.addFrag(priceFragment, tabTitles[i]);
        }
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
    private void getPriceList() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        makePriceRequest(params);
    }

    private void makePriceRequest(List<NameValuePair> params) {

        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if (json.getString("success").equals("true")) {
                        priceList = json.getJSONObject("data");
                        setupViewPager(viewPager);
                        tabLayout.setupWithViewPager(viewPager);
                        setupTabIcons();
                    } else if (json.getString("success").equals("false")) {
                        Snackbar.make(rootView, json.getString("msg"), Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    }
                } catch (JSONException e) {
                    Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void taskException() {
                Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
            }
        }, PriceEstimator.this, Constant.PRICELIST, params, true);

        task.execute();
    }
    public static Map<String, Map<String, Integer >> getCart(){
        return cart;
    }
    public static View getCartView() { return cartView.getRootView(); }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //initialize flurry
        FlurryAgent.setLogEnabled(false);
        // init Flurry
        FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
        FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("mobile", AppPrefrence.getMobile(PriceEstimator.this));
        //up to 10 params can be logged with each event
        FlurryAgent.logEvent("price_estimator", articleParams);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        intent.putExtra("SESSION_ID", "1");
        intent.putExtra("id",1);
        intent.putExtra("Context","Review");
        startActivity(intent);
        finish();
    }
}



