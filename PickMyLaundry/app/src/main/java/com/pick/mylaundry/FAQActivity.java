package com.pick.mylaundry;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * Created by 121 on 10/24/2016.
 */
public class FAQActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq_activity);

        TextView title = (TextView) findViewById(R.id.actionbar_custom_title);
        title.setText("Frequently Asked Questions");
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.pB);
        final FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_layout);

        ImageView back_iv = (ImageView) findViewById(R.id.back_icon);
        back_iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        WebView webView = (WebView) findViewById(R.id.web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://pickmylaundry.in/faqs_app.html");

        webView.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if(progress < 100 && progressBar.getVisibility() == ProgressBar.GONE){
                    progressBar.setVisibility(ProgressBar.VISIBLE);
                }

                progressBar.setProgress(progress);
                if(progress == 100) {
                    progressBar.setVisibility(ProgressBar.GONE);
                    frameLayout.setVisibility(View.GONE);
                }
            }
        });

    }
}
