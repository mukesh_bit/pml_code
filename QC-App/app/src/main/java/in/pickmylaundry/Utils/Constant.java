package in.pickmylaundry.Utils;

/**
 * Created by 121 on 3/9/2016.
 */
public class Constant {
    public final static String GET_REQUEST = "GET";
    public final static String POST_REQUEST = "POST";
    public final static String get_so_detail = "http://crm.pickmylaundry.in/web_services/qc_app/get_vendor_info.php";
    public final static String submit_untagged_data = "http://crm.pickmylaundry.in/web_services/qc_app/untagged_data.php";
    public final static String get_products = "http://crm.pickmylaundry.in/web_services/qc_app/get_products.php";
    public static final String LOGIN = "http://crm.pickmylaundry.in/web_services/qc_app/login.php";

    public static String get_vendor_detail = "http://crm.pickmylaundry.in/web_services/qc_app/get_vendor_detail.php";
    public static String submit_data = "http://crm.pickmylaundry.in/web_services/qc_app/submit_scaning.php";
}
