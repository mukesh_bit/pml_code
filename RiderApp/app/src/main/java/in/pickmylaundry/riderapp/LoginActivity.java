package in.pickmylaundry.riderapp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.riderapp.utils.AppPrefrence;
import in.pickmylaundry.riderapp.utils.Constant;
import in.pickmylaundry.riderapp.utils.UploadTask;

/**
 * Created by 121 on 5/15/2016.
 */
public class LoginActivity extends Activity {
    private EditText username, password;
    private Button login;
    private UploadTask task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        login = (Button) findViewById(R.id.login);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(username.getText().toString().trim().equals("") || password.getText().toString().trim().equals("")){
                    Toast.makeText(LoginActivity.this, "Please enter username and password", Toast.LENGTH_LONG).show();
                }
                else{
                    Map<String, Object> param = new HashMap<String, Object>();
                    param.put("username", username.getText().toString());
                    param.put("password", password.getText().toString());
                    request_login(param);
                }
            }
        });
    }
    private void request_login(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){
                        Toast.makeText(LoginActivity.this, "Logged In Successfully", Toast.LENGTH_LONG).show();
                        AppPrefrence.setNumber(LoginActivity.this, response.getString("id"));
                        AppPrefrence.setName(LoginActivity.this, response.getString("name"));
                        AppPrefrence.setPhone(LoginActivity.this, response.getString("mobile"));
                        SharedPreferences prefs = getSharedPreferences("gcmregistartion", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("registration_id", "");
                        editor.putInt("appVersion", 10);
                        editor.putBoolean("loggedOut", false);
                        editor.commit();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else{
                        Toast.makeText(LoginActivity.this, response.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(LoginActivity.this, "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(LoginActivity.this, "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, LoginActivity.this, Constant.LOGIN, params);
        task.execute();
    }
}
