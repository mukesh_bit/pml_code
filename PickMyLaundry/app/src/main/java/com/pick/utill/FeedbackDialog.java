package com.pick.utill;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.pick.mylaundry.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by 121 on 10/25/2016.
 */
public class FeedbackDialog {
    
    private Context mContext;
    
    public FeedbackDialog(Context context){
        this.mContext = context;
    }
    public void show_feedback_dialog(){
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog);
        dialog.show();
        TextView no = (TextView) dialog.findViewById(R.id.no);
        TextView rate_us = (TextView) dialog.findViewById(R.id.rate);

        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("mobile", AppPrefrence.getMobile(mContext));
        articleParams.put("activity", "shown");
        FlurryAgent.logEvent("Rate app", articleParams);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = mContext.getSharedPreferences("rate_app", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("isRated", true);
                editor.commit();

                dialog.dismiss();

                Map<String, String> articleParams = new HashMap<String, String>();
                articleParams.put("mobile", AppPrefrence.getMobile(mContext));
                articleParams.put("activity", "not_now");
                FlurryAgent.logEvent("Rate app", articleParams);
            }
        });
        rate_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                SharedPreferences pref = mContext.getSharedPreferences("rate_app", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean("isRated", true);
                editor.commit();

                Map<String, String> articleParams = new HashMap<String, String>();
                articleParams.put("mobile", AppPrefrence.getMobile(mContext));
                articleParams.put("activity", "rate_us_clicked");
                FlurryAgent.logEvent("Rate app", articleParams);

                mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.pick.mylaundry&hl=en")));
            }
        });
    }
}
