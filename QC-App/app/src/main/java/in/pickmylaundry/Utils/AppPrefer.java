package in.pickmylaundry.Utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by 121 on 3/13/2016.
 */
public class AppPrefer {
    public static String get_user_name(Context context){
        SharedPreferences prefs = context.getSharedPreferences("login_status", Context.MODE_PRIVATE);
        String user_name = prefs.getString("user_name", "Unknown");
        return user_name;
    }
}
