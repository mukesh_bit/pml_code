package com.pick.mylaundry;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Scroller;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.pick.datamodel.TestimonialData;
import com.pick.navAdapter.HomeSliderAdapter;
import com.pick.navAdapter.TestimonialAdapter;
import com.pick.navmodel.TestimonialModel;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.utill.FeedbackDialog;
import com.pick.utill.Pickhelper;
import com.pick.utill.ResizeAnimation;
import com.pick.utill.ShowInfoDialog;
import com.pick.utill.UriOpener;
import com.pick.volley.EmptyUtility;
import com.pick.volley.ImageLoader;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by 121 on 12/18/2015.
 */
public class HomeFragment2 extends Fragment {
    private View rootView;
    private ViewPager mViewpager, viewPager_testimonial;
    private LinearLayout washnfold, washniron, dc, premium;
    private ArrayList<String> selectedService = new ArrayList<String>();
    private ArrayList<String> sliderUrl = new ArrayList<String>();
    private JSONObject turnaroundTimeObj;
    private TextView proceed, card_title;
    private Handler handler = null;
    private Runnable Update;
    private RelativeLayout faq_rl;
    private int currentPage = 0;
    private ImageLoader mImageLoader;
    private ImageView mImageView, nextImage, card_image;
    private HomeSliderAdapter mHomeSliderAdapter;
    private TestimonialAdapter mTestimonialAdapter;
    private boolean isConnected;
    private ArrayList<String> imageUrl;
    private NetworkInfo activeNetwork;
    private boolean reverse = false;
    private ArrayList<Integer> interval = new ArrayList<Integer>();
    private boolean parts_loaded = false;
    private ConnectivityManager cm;
    private boolean service_hint_shown = false;
    private View home_root;
    private LinearLayout indicator_ll, card_ll;
    private ArrayList<TestimonialModel> testimonialModelArrayList = new ArrayList<TestimonialModel>();
    private Button service_features_btn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.home_fragment, container, false);
        home_root = getActivity().getWindow().getDecorView().getRootView().findViewById(R.id.home_root);
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "font/helvetica-normal.ttf");

        //check internet connection
        cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        activeNetwork = cm.getActiveNetworkInfo();
        isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();

        //image slider
        mViewpager = (ViewPager) rootView.findViewById(R.id.imagePager);
        viewPager_testimonial = (ViewPager) rootView.findViewById(R.id.viewPager_testimonials);
        currentPage = 0;


        //get home image
        setupSlider(false);
        set_listener();
        get_top_banner_data();
        get_card_data();
        setup_testimonial_slider();
        getDialogorOffers();

        //check for feedback dialog
        Bundle bundle = getArguments();
        if(bundle!=null){
            if(bundle.containsKey("isFeedbackDialogVisible"))
                showFeedbackDialog();
        }


        //flurry Capture event data
        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("mobile", AppPrefrence.getMobile(getActivity()));
        articleParams.put("activity", "opened");
        FlurryAgent.logEvent("Home Screen", articleParams);


        return rootView;
    }
    private void set_listener(){
        washnfold = (LinearLayout) rootView.findViewById(R.id.washnfold);
        washniron = (LinearLayout) rootView.findViewById(R.id.washniron);
        premium = (LinearLayout) rootView.findViewById(R.id.pl);
        dc = (LinearLayout) rootView.findViewById(R.id.dc);
        proceed = (TextView) rootView.findViewById(R.id.proceed);
        card_title = (TextView) rootView.findViewById(R.id.card_title);
        card_ll = (LinearLayout) rootView.findViewById(R.id.card_ll);
        card_image = (ImageView) rootView.findViewById(R.id.card_image);
        faq_rl = (RelativeLayout) rootView.findViewById(R.id.faq);
        indicator_ll = (LinearLayout) rootView.findViewById(R.id.indicator_container);
        service_features_btn = (Button) rootView.findViewById(R.id.service_features);


        //set listener
        washnfold.setOnClickListener(serviceClickListener);
        washniron.setOnClickListener(serviceClickListener);
        premium.setOnClickListener(serviceClickListener);
        dc.setOnClickListener(serviceClickListener);
        proceed.setOnClickListener(selectTimeslotListener);
        faq_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FAQActivity.class);
                startActivity(intent);
            }
        });
        service_features_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), ViewFeatures.class);
                startActivity(intent);
            }
        });
        add_circle_hint(premium);
    }
    private View.OnClickListener serviceClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            LinearLayout clickedLayout = (LinearLayout) rootView.findViewById(v.getId());
            TextView clickedService = (TextView) clickedLayout.getChildAt(1);
            String serviceClicked = (String) clickedLayout.getTag();
            if(selectedService.contains(serviceClicked)){
                selectedService.remove(serviceClicked);
                clickedLayout.setBackgroundResource(R.color.white);
                clickedService.setTextColor(Color.parseColor("#333333"));
            }
            else{
                selectedService.add(serviceClicked);
                clickedLayout.setBackgroundResource(R.color.colorPrimary);
                clickedService.setTextColor(getResources().getColor(R.color.white));
            }
            if(selectedService.size() > 0){
                proceed.setBackgroundResource(R.drawable.button_bg_blue);
                proceed.setTextColor(getResources().getColor(R.color.white));
            }
            else {
                proceed.setBackgroundResource(R.drawable.secondary_btn);
                proceed.setTextColor(getResources().getColor(R.color.dark_grey));
            }
            //show hint
            if(!service_hint_shown)
                set_hint_circle(serviceClicked);
            else
                reset_hint();
        }
    };
    private View.OnClickListener selectTimeslotListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(Constant.getLocation(getActivity()).equals("Select delivery Location...")){
                Toast.makeText(getActivity(), "Please Select your location from top", Toast.LENGTH_LONG).show();
            }
            else if(!parts_loaded){
                activeNetwork = cm.getActiveNetworkInfo();
                isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
                if(isConnected){
                    setupSlider(true);
                }
                else{
                    Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_LONG).show();                }
            }
            else if(selectedService.size()>0){
                Intent intent = new Intent(getActivity(), SelectAddress.class);
                intent.putStringArrayListExtra("washType", selectedService);
                intent.putExtra("turnaround", get_turnaround_time());
                startActivity(intent);

                Map<String, String> articleParams = new HashMap<String, String>();
                articleParams.put("mobile", AppPrefrence.getMobile(getActivity()));
                articleParams.put("from", "main screen");
                FlurryAgent.logEvent("Timeslot", articleParams);
            }
            else{
                //flurry Capture event data
                Map<String, String> articleParams = new HashMap<String, String>();
                articleParams.put("mobile", AppPrefrence.getMobile(getActivity()));
                articleParams.put("activity", "service type not selected");
                FlurryAgent.logEvent("Home Screen", articleParams);
                Toast.makeText(getActivity(), "Please select service type", Toast.LENGTH_LONG).show();
            }
        }
    };
    private void setupSlider(boolean isDialogVisible){
        boolean isImagesLoadable = initializeAdapter();
        getHomepageImage(isDialogVisible, isImagesLoadable);
    }
    private boolean initializeAdapter(){
        mViewpager.setVisibility(View.VISIBLE);
        ImageView default_image = (ImageView) rootView.findViewById(R.id.default_image);
        default_image.setVisibility(View.GONE);

        sliderUrl.clear();
        String images_data = AppPrefrence.getHomeImages(getActivity());
        if(EmptyUtility.isEmpty(images_data)){
            for(int i=0;i<3;i++) {
                sliderUrl.add("url");
                interval.add(4000);
            }
            setupAdapter();
            return true;
        }
        else{
            set_images_data(images_data);
            return false;
        }
    }
    private void setupAdapter(){
        mViewpager.setClipToPadding(false);
        mViewpager.setPageMargin(20);
        mViewpager.setOffscreenPageLimit(3);
        mHomeSliderAdapter = new HomeSliderAdapter(getActivity(), sliderUrl);
        mViewpager.setAdapter(mHomeSliderAdapter);
        mHomeSliderAdapter.notifyDataSetChanged();
    }

    private void getHomepageImage(boolean isDialogVisible, boolean isImageLoadable){
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        SharedPreferences prefs = getActivity().getSharedPreferences("homeScreenImage", Context.MODE_PRIVATE);
        String imageSrc = prefs.getString("image_src", "");
        params.add(new BasicNameValuePair("previous_image", imageSrc));
        requestHomescreenImage(params, isDialogVisible, isImageLoadable);
    }
    private void requestHomescreenImage(List<NameValuePair> params, final boolean isDialogVisible, final boolean isImageLoadable){
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        if (isImageLoadable)
                            set_images_data(result);
                        AppPrefrence.setHomeImages(getActivity(), result);
                    }
                    else{
                        Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e){
                    Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }
            @Override
            public void taskException() {
               Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_LONG).show();
            }
        },HomeFragment2.this.getActivity(), Constant.HOMESCREENIMAGES,params, isDialogVisible);
        task.execute();
    }
    private void startAutoSlide(){
        //auto slide
        if(handler != null) {
            handler.removeCallbacks(Update);
        }
        handler = new Handler();
        Update = new Runnable() {
            public void run() {
                currentPage = mViewpager.getCurrentItem();
                if (currentPage == sliderUrl.size()-1) {
                    reverse = true;
                }
                if(currentPage==0)
                    reverse = false;
                if(reverse)
                    mViewpager.setCurrentItem(--currentPage, true);
                else
                    mViewpager.setCurrentItem(++currentPage, true);
                handler.postDelayed(Update, interval.get(currentPage));
            }
        };

        if(sliderUrl.size()>1) {
            currentPage = 0;
            handler.postDelayed(Update, interval.get(0));
            try {
                Field mScroller;
                mScroller = ViewPager.class.getDeclaredField("mScroller");
                mScroller.setAccessible(true);
                FixedSpeedScroller scroller = new FixedSpeedScroller(mViewpager.getContext());
                mScroller.set(mViewpager, scroller);
            } catch (NoSuchFieldException e) {
            } catch (IllegalArgumentException e) {
            } catch (IllegalAccessException e) {
            }
        }
    }
    private void getDialogorOffers(){
        ShowInfoDialog infoDialog = new ShowInfoDialog(getActivity());
        infoDialog.check_for_info_dialog();
    }
    public class FixedSpeedScroller extends Scroller {

        private int mDuration = 1500;

        public FixedSpeedScroller(Context context) {
            super(context);
        }


        @Override
        public void startScroll(int startX, int startY, int dx, int dy, int duration) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy) {
            // Ignore received duration, use fixed one instead
            super.startScroll(startX, startY, dx, dy, mDuration);
        }
    }
    private void showFeedbackDialog(){
        FeedbackDialog feedbackDialog = new FeedbackDialog(getActivity());
        feedbackDialog.show_feedback_dialog();
    }
    @Override
    public void onPause() {
        super.onPause();
        if(handler!=null) {
            handler.removeCallbacks(Update);
            mViewpager.setCurrentItem(0);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if(handler!=null)
            startAutoSlide();
    }

    private int get_turnaround_time(){
        int max = 0;
        for (String service: selectedService) {
            try {
                if(turnaroundTimeObj.getInt(service+"_days") > max){
                    max = turnaroundTimeObj.getInt(service+"_days");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return max;
    }
    private void set_hint_circle(String service){
        if(selectedService.size() >= 1)
            service_hint_shown = true;
        reset_hint();
        if(service.equals("Premium Laundry")){
            add_circle_hint(dc);
        }
        else if(service.equals("Dry Clean")){
            add_circle_hint(premium);
        }
        else if(service.equals("Wash & Iron")){
            add_circle_hint(dc);
        }
        else if(service.equals("Wash & Fold")){
            add_circle_hint(washniron);
        }
    }
    private void add_circle_hint(LinearLayout hint_target_view){
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View circle_hint_view = inflater.inflate(R.layout.circle_hint, null);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) Constant.convertDpToPixel(15, getActivity()), (int) Constant.convertDpToPixel(15, getActivity()));
        layoutParams.setMargins(0, -1* (int) Constant.convertDpToPixel(83, getActivity()),(int) Constant.convertDpToPixel(2, getActivity()), 0);
        layoutParams.gravity = Gravity.RIGHT;
        circle_hint_view.setLayoutParams(layoutParams);
        hint_target_view.addView(circle_hint_view);

        //start animation
        Animation a = AnimationUtils.loadAnimation(getActivity(), R.anim.circle);
        a.setRepeatMode(Animation.RESTART);
        a.setRepeatCount(Animation.INFINITE);
        TextView tv = (TextView) hint_target_view.findViewById(R.id.circle);
        tv.startAnimation(a);
    }
    private void reset_hint(){
        RelativeLayout view_rl = (RelativeLayout) washniron.findViewById(R.id.root_view);
        if(view_rl != null)
           washniron.removeView(view_rl);
        view_rl = (RelativeLayout) washnfold.findViewById(R.id.root_view);
        if(view_rl != null)
            washnfold.removeView(view_rl);
        view_rl = (RelativeLayout) premium.findViewById(R.id.root_view);
        if(view_rl != null)
            premium.removeView(view_rl);
        view_rl = (RelativeLayout) dc.findViewById(R.id.root_view);
        if(view_rl != null)
            dc.removeView(view_rl);
    }
    private void setup_testimonial_slider(){
        mTestimonialAdapter = new TestimonialAdapter(getActivity(), testimonialModelArrayList, indicator_ll);
        viewPager_testimonial.setAdapter(mTestimonialAdapter);
        TestimonialData testimonialData = new TestimonialData(getActivity(), mTestimonialAdapter);
        testimonialData.getTestimonialData();
        mTestimonialAdapter.notifyDataSetChanged();
        viewPager_testimonial.setCurrentItem(0);
        viewPager_testimonial.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                viewPager_testimonial.setCurrentItem(position);
                mTestimonialAdapter.add_indicator(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
    private void get_card_data(){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(getActivity())));

        UploadTask uploadTask = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject object = new JSONObject(result);
                    if(object.getString("success").equals("true")){
                        set_card_data(object.getString("title"), object.getString("image_url"),
                                object.getString("uri_data"), object.getString("link"));
                    }
                    else {
                        set_card_data("Refer and Earn", Constant.DEFAULTCARDIMAGE, "", "app:refer");
                    }
                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
            @Override
            public void taskException() {

            }
        },HomeFragment2.this.getActivity(), Constant.HOMEPAGECARDDATA,params, false);
        StartAsyncTaskInParallel(uploadTask);
    }
    private void set_card_data(String title, String image_url, final String uri_data, final String link){
        card_title.setText(title);
        card_title.setBackgroundColor(getResources().getColor(R.color.white));
        mImageLoader = new ImageLoader(getActivity());
        mImageLoader.DisplayImage(image_url, card_image, "card", rootView);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT, (int)Constant.convertDpToPixel(150, getActivity()));
        params.setMargins(0,0,0,0);
        card_image.setLayoutParams(params);
        card_image.setBackgroundColor(getResources().getColor(R.color.white));
        card_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UriOpener uriOpener = new UriOpener(getActivity());
                uriOpener.open_link(link, uri_data);
            }
        });
    }
    private void get_top_banner_data(){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(getActivity())));

        UploadTask uploadTask = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject object = new JSONObject(result);
                    if(object.getString("success").equals("true")){
                        if(object.has("order_data")){
                            set_order_data(object.getJSONObject("order_data"));
                        }
                        else if(object.has("quick_order")){
                            set_quick_order(object.getJSONObject("quick_order"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void taskException() {
            }
        },HomeFragment2.this.getActivity(), Constant.HOMEPAGETOPBANNER,params, false);
        StartAsyncTaskInParallel(uploadTask);
    }
    private void set_order_data(final JSONObject order_data){
        RelativeLayout order_container = (RelativeLayout) rootView.findViewById(R.id.order_container);
        TextView box_title = (TextView) rootView.findViewById(R.id.box_title);
        TextView box_content_title = (TextView) rootView.findViewById(R.id.box_content_title);
        TextView box_content = (TextView) rootView.findViewById(R.id.box_content);
        TextView amount = (TextView) rootView.findViewById(R.id.amount);
        Button pay_now = (Button) rootView.findViewById(R.id.pay_now);

        try {
            box_title.setText(order_data.getString("title"));
            box_content_title.setText(order_data.getString("content_title"));
            box_content.setText(order_data.getString("content"));
            if(order_data.has("amount"))
                amount.setText(getResources().getString(R.string.Rs)+" "+order_data.getString("amount"));
            else
                amount.setVisibility(View.GONE);
            if(!order_data.getBoolean("payable")){
                pay_now.setVisibility(View.GONE);
                if(order_data.has("amount")){
                    RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, (int) Constant.convertDpToPixel(20, getActivity()));
                    layoutParams.addRule(RelativeLayout.CENTER_VERTICAL, RelativeLayout.TRUE);
                    layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
                    layoutParams.setMargins(0, 0, (int) Constant.convertDpToPixel(30, getActivity()), 0);
                    amount.setLayoutParams(layoutParams);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        order_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Invoice.class);
                try {
                    intent.putExtra(Constant.ORDERID, order_data.getString("salesorderid"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                startActivity(intent);
            }
        });
        pay_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paytmTransaction paytmInstance = null;
                try {
                    paytmInstance = new paytmTransaction("", order_data.getString("amount"), order_data.getString("contactid"), order_data.getString("status"), order_data.getString("salesorderid"), order_data.getString("salesorder_no"), getActivity());
                    paytmInstance.get_paytm_id_n_start_payment();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
        order_container.setVisibility(View.VISIBLE);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) order_container.getLayoutParams();
        ResizeAnimation a = new ResizeAnimation(order_container);
        a.setDuration(500);
        a.setParams(lp.height, lp.height+((int)Constant.convertDpToPixel(70, getActivity())));
        order_container.startAnimation(a);
    }
    private void set_quick_order(final JSONObject data){
        RelativeLayout quick_order_rl = (RelativeLayout) rootView.findViewById(R.id.quick_order_rl);
        TextView quick_order_title = (TextView) rootView.findViewById(R.id.quick_order_title);
        TextView quick_order_detail = (TextView) rootView.findViewById(R.id.quick_order_detail);
        Button place_order_btn = (Button) rootView.findViewById(R.id.place_order_btn);

        try {
            quick_order_title.setText(data.getString("title"));
            quick_order_detail.setText(data.getString("detail"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        quick_order_rl.setVisibility(View.VISIBLE);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) quick_order_rl.getLayoutParams();
        ResizeAnimation a = new ResizeAnimation(quick_order_rl);
        a.setDuration(500);
        a.setParams(lp.height, lp.height+((int)Constant.convertDpToPixel(60, getActivity())));
        quick_order_rl.startAnimation(a);
        place_order_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent intent = new Intent(getActivity(), ReviewActivity.class);
                try {
                    intent.putExtra("pickup_date", data.getString("date"));
                    intent.putExtra("pickup_time", data.getString("time"));
                    intent.putExtra("delivery_date", "");
                    intent.putExtra("delivery_time", "");
                    ArrayList<String> washType = new ArrayList<String>();
                    intent.putStringArrayListExtra("washType", washType);
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void StartAsyncTaskInParallel(UploadTask task) {
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
            task.execute();
    }
    private void set_images_data(String images_data){
        try {
            JSONObject json = new JSONObject(images_data);
            JSONArray sliderTextObj = json.getJSONArray("slider_url");
            JSONArray intervalObj = json.getJSONArray("interval");
            turnaroundTimeObj = json.getJSONObject("time");

            parts_loaded = true;

            sliderUrl.clear();
            interval.clear();
            for(int i=0;i<sliderTextObj.length();i++) {
                sliderUrl.add(sliderTextObj.getString(i));
                interval.add(intervalObj.getInt(i));
            }
            setupAdapter();
            startAutoSlide();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
