package com.pick.utill;

 

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;

import java.util.Calendar;

public class AppPrefrence {
	private static SQLiteDatabase db;
	 private static Pickhelper dbhelper;
	private static String MobileNUmber = "mobile";
	private static String CITY = "city";
	private static String PromoCode = "promocode";
	 private static String PROFILEURL="profileurl";
	 private static String LOGIN = "login";
	private static String OTPVALUE="otpvalue";
	 private static String Name = "name";
    private static String LName = "lname";
	private static String PATH = "path";
	private static String USERID="userid";
    private static String EMAIL = "email";
	 private static String PICKDATE="pickdate";
	private static String PICKTIME="picktime";
	private static String DELIDATE="delidate";
	private static String DELITIME="delitime";
	private static String WASHORDER="washorder";
	private static String LOGINVIA="loginvia";

	public static void setWashOrder(Context context, String pickupdate,String pickuptime,String deliverydate,String deliverytime,String list) {
		SharedPreferences _sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = _sharedPreferences.edit();
		editor.putString(PICKDATE,pickupdate);
		editor.putString(PICKTIME,pickuptime);
		editor.putString(DELITIME,deliverytime);
		editor.putString(DELIDATE,deliverydate);
		editor.putString(WASHORDER,list);
		editor.commit();
	}
	public static String getPdate(Context context) {
		String pdatec= PreferenceManager.getDefaultSharedPreferences(context)
				.getString(PICKDATE, "");
		return pdatec;
     } 
	public static String getPtime(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(PICKTIME, "");
     }
	public static String getDdate(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(DELIDATE, "");
     }
	public static String getDtime(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(DELITIME, "");
     }
	public static String getWashSchedule(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(WASHORDER, "");
     }
	 
	public static void setNumber(Context context, String mobile) {
		SharedPreferences _sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = _sharedPreferences.edit();
		editor.putString(MobileNUmber, mobile);

		editor.commit();
	}
	public static void setProfile(Context context, String profile) {
		SharedPreferences _sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = _sharedPreferences.edit();
		editor.putString(PROFILEURL,profile);
        editor.commit();
	}
	public static void setCity(Context context, String city) {
		SharedPreferences _sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = _sharedPreferences.edit();
		editor.putString(CITY, city);

		editor.commit();
	}

	public static String getProfile(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(PROFILEURL, "");
     }
	public static String getCity(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(CITY, "");
	}

	public static void setLogutStatus(Context context) {
		SharedPreferences _sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = _sharedPreferences.edit();
		editor.putBoolean(LOGIN,false);

		editor.commit();
	}
	 
	

	public static Boolean getOTPverify(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getBoolean(OTPVALUE,false);
  }
 
	/**
	 * get mobile number
	 * 
	 * @param context
	 * @return
	 */
	public static Boolean getLoginStatus(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getBoolean(LOGIN,false);
  }
	public static void setLoginStatus(Context context) {
		SharedPreferences _sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = _sharedPreferences.edit();
		editor.putBoolean(LOGIN,true);
        editor.commit();
	}
	public static String getLoginvia(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(LOGINVIA, "");

	}


	public static void saveUserInfo(Context context, String name,String email) {
		SharedPreferences _sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = _sharedPreferences.edit();
		editor.putString(Name, name);
		editor.putString(EMAIL, email);
		editor.commit();
	}
	public static void saveLoginVia(Context context, String via) {
		SharedPreferences _sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = _sharedPreferences.edit();
		editor.putString(LOGINVIA,via);
		 editor.commit();
	}
	 
	public static void saveUserInfo1(Context context, String mobile) {
		SharedPreferences _sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = _sharedPreferences.edit();
		 
		editor.putString(MobileNUmber,mobile);
	 
		editor.commit();
	}
	public static void saveUserid(Context context, String id) {
		SharedPreferences _sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = _sharedPreferences.edit();
		 
		editor.putString(USERID,id);
	 
		editor.commit();
	}

	public static void savePromoCode(Context context, String promocode){
		SharedPreferences _sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = _sharedPreferences.edit();

		editor.putString(PromoCode,promocode);

		editor.commit();
	}

	public static String getName(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(Name, "");

	}
	 
	public static String getLName(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(LName, "");

	}
	public static String getMobile(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(MobileNUmber, "");

	}

	public static String getPromoCode(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(PromoCode, "");

	}

	/**
	 * get Address of user
	 * 
	 * @param context
	 * @return
	 */
	public static String getAddress(Context context) {
		dbhelper=new Pickhelper(context);
    	db=dbhelper.getWritableDatabase();
    	String[] columns ={Pickhelper.KEY_PRIMARY,Pickhelper.KEY_FLATNO,Pickhelper.KEY_LANDMARK
    			,Pickhelper.KEY_AREA,Pickhelper.KEY_TYPE,Pickhelper.KEY_ACTIVE};
    	String condition="yes";
    	Cursor cursor = db.query(Pickhelper.TABLE_ADDRESS, columns, "active=?", new String[] { condition }, null, null, null); 
    	cursor.moveToFirst();
		//int count= cursor.getInt(0);
		String Add="";
		 
    	 if (cursor.moveToFirst()) {
            do {
              Add=cursor.getString(1);
              Add=Add+","+cursor.getString(2) ;
              Add=Add+","+cursor.getString(3) ;
              Add=Add+","+cursor.getString(4) ;
              Add=Add+","+cursor.getString(5) ;
               
               
            } while (cursor.moveToNext());
        }
		 
		return Add;

	}
	public static String getRAddress(Context context) {
		dbhelper=new Pickhelper(context);
    	db=dbhelper.getWritableDatabase();
    	String[] columns ={Pickhelper.KEY_PRIMARY,Pickhelper.KEY_FLATNO,Pickhelper.KEY_LANDMARK
    			,Pickhelper.KEY_AREA,Pickhelper.KEY_TYPE,Pickhelper.KEY_ACTIVE};
    	String condition="yes";
    	Cursor cursor = db.query(Pickhelper.TABLE_ADDRESS, columns, "active=?", new String[] { condition }, null, null, null); 
    	String Add="";
    	if (cursor.moveToFirst()) {
            do {
              Add=cursor.getString(1) ;
              Add=Add+" "+cursor.getString(2) ;
              Add=Add+"\n"+cursor.getString(3) ;
              Add=Add+"\n"+cursor.getString(4) ;
              Add=Add+"\n"+cursor.getString(5) ;
              Add=Add+","+cursor.getString(6) ; 
            } while (cursor.moveToNext());
        }
		return Add;

	}
	/**
	 * this method save user profile image path
	 * 
	 * @param context
	 * @param path
	 */
	public static void saveUserImagePath(Context context, String path) {
		SharedPreferences _sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = _sharedPreferences.edit();
		editor.putString(PATH, path);
		editor.commit();
	}

	public static String getImagePath(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(PATH, "");

	}

	
	 
	 public static String getEmail(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(EMAIL, "");

	}
 
 
	public static String getUserid(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context)
				.getString(USERID, "");
	}
	public static void setHomeImages(Context context, String data){
		SharedPreferences preferences = context.getSharedPreferences("Home_Images", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.putString("Images_data", data);
		Calendar calendar = Calendar.getInstance();
		editor.putString("time", calendar.getTimeInMillis()+"");
		editor.commit();
	}
	public static String getHomeImages(Context context){
		SharedPreferences preferences = context.getSharedPreferences("Home_Images", Context.MODE_PRIVATE);
		long time_stored = Long.parseLong(preferences.getString("time", "0"));
		Calendar calendar = Calendar.getInstance();
		if(calendar.getTimeInMillis() - time_stored > (1000 * 60 * 60 * 24 * 7))
			return "";
		else
			return preferences.getString("Images_data", "");
	}

}
