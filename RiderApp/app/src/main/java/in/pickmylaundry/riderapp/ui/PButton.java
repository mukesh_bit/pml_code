package in.pickmylaundry.riderapp.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Created by 121 on 8/4/2016.
 */
public class PButton extends Button {
    public PButton(Context context) {
        super(context);
        setFont(context);
    }

    public PButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont(context);
    }

    public PButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFont(context);
    }
    private void setFont(Context context){
        Typeface face=Typeface.createFromAsset(context.getAssets(), "OpenSans-Regular.ttf");
        setTypeface(face);
    }
}
