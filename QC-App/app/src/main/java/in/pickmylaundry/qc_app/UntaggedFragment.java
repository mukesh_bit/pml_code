package in.pickmylaundry.qc_app;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.pickmylaundry.Utils.Constant;
import in.pickmylaundry.Utils.UploadTask;

/**
 * Created by 121 on 5/5/2016.
 */
public class UntaggedFragment extends android.app.Fragment {
    private View rootView;
    private LinearLayout so_list;
    private Button add_so, submit;
    private UploadTask task;
    private Spinner spinner, spinner_location, spinner_tag;
    private ArrayAdapter<String> dataAdapter;
    private ArrayList<EditText> editTexts = new ArrayList<EditText>();
    private EditText so,color,brand;
    private AutoCompleteTextView type;
    private AlertDialog dialog_exit;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.frgmnt_untagged, container, false);

        //get elements
        so = (EditText) rootView.findViewById(R.id.so);
        type = (AutoCompleteTextView) rootView.findViewById(R.id.type);
        color = (EditText) rootView.findViewById(R.id.color);
        brand = (EditText) rootView.findViewById(R.id.brand);

        //set spinner data
        spinner = (Spinner) rootView.findViewById(R.id.laundromat);
        spinner_location = (Spinner) rootView.findViewById(R.id.location);
        spinner_tag = (Spinner) rootView.findViewById(R.id.tag);
        setSpinnerData();

        //add so
        so_list = (LinearLayout) rootView.findViewById(R.id.so_list);
        add_so = (Button) rootView.findViewById(R.id.add_so);
        add_so.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addSoView();
            }
        });

        submit = (Button) rootView.findViewById(R.id.submit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submit_data();
            }
        });
        return rootView;
    }
    private void addSoView(){
        LinearLayout l = new LinearLayout(UntaggedFragment.this.getActivity());
        l.setOrientation(LinearLayout.VERTICAL);
        EditText new_so = new EditText(UntaggedFragment.this.getActivity());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
        new_so.setText("SO");
        l.addView(new_so, lp);
        so_list.addView(l);
        editTexts.add(new_so);
    }
    private void setSpinnerData(){
        request_vendor_detail();

        List<String> categories = new ArrayList<String>();
        categories.add("Select Location");
        categories.add("Gurgaon");
        categories.add("South Delhi");
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                getActivity(),android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.reason_item);

        // attaching data adapter to spinner
        spinner_location.setAdapter(dataAdapter);

        categories = new ArrayList<String>();
        categories.add("Select Tagging Status");
        categories.add("Untagged");
        categories.add("Tagged");
        // Creating adapter for spinner
        dataAdapter = new ArrayAdapter<String>(
                getActivity(),android.R.layout.simple_spinner_item, categories);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(R.layout.reason_item);

        // attaching data adapter to spinner
        spinner_tag.setAdapter(dataAdapter);

        //get products data
        Map<String, Object> params = new HashMap<String, Object>();
        get_products(params);
    }
    private void submit_data(){
        String so_potential = "";
        if(so.getText().length() > 2)
            so_potential += so.getText().toString();
        for (int i=0; i<editTexts.size();i++){
            EditText so_et = editTexts.get(i);
            if(so_et.getText().length() > 2)
                so_potential += ", "+so_et.getText().toString();
        }
        if(!(so_potential.length()>=2)){
            Toast.makeText(UntaggedFragment.this.getActivity(), "Please add at least one so", Toast.LENGTH_LONG).show();
        }
        else if(type.getText().toString().equals("")){
            Toast.makeText(UntaggedFragment.this.getActivity(), "Please add cloth type", Toast.LENGTH_LONG).show();
        }
        else if(color.getText().toString().equals("")){
            Toast.makeText(UntaggedFragment.this.getActivity(), "Please add a color", Toast.LENGTH_LONG).show();
        }
        else if(brand.getText().toString().equals("")){
            Toast.makeText(UntaggedFragment.this.getActivity(), "Please add a brand", Toast.LENGTH_LONG).show();
        }
        else if(spinner.getSelectedItem().equals("Select a Laundromat")){
            Toast.makeText(UntaggedFragment.this.getActivity(), "Please select a vendor", Toast.LENGTH_LONG).show();
        }
        else if(spinner_location.getSelectedItem().equals("Select Location")){
            Toast.makeText(UntaggedFragment.this.getActivity(), "Please select a location", Toast.LENGTH_LONG).show();
        }
        else if(spinner_tag.getSelectedItem().equals("Select Tagging Status")){
            Toast.makeText(UntaggedFragment.this.getActivity(), "Please select tagging status", Toast.LENGTH_LONG).show();
        }
        else{
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("so", so_potential);
            params.put("type", type.getText().toString());
            params.put("color", color.getText().toString());
            params.put("brand", brand.getText().toString());
            params.put("location", spinner_location.getSelectedItem().toString());
            params.put("laundromat", spinner_location.getSelectedItem().toString());
            params.put("status", spinner_tag.getSelectedItem().toString());
            submit_data_untagged(params);
            Log.i("param", params.toString());
        }

    }
    private void request_vendor_detail(){
        Map<String, Object> params = new HashMap<String, Object>();
        get_vendor_detail(params);
    }
    private void get_vendor_detail(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONArray response = null;
                try {
                    response = new JSONArray(result);
                    List<String> categories = new ArrayList<String>();
                    categories.add("Select a Laundromat");
                    for (int i=0; i<response.length();i++){
                        categories.add(response.getString(i));
                    }

                    // Creating adapter for spinner
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                            getActivity(),android.R.layout.simple_spinner_item, categories);

                    // Drop down layout style - list view with radio button

                    dataAdapter.setDropDownViewResource(R.layout.reason_item);

                    // attaching data adapter to spinner
                    spinner.setAdapter(dataAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, UntaggedFragment.this.getActivity(), Constant.get_so_detail, params);
        task.execute();
    }
    private void submit_data_untagged (Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    String msg = "";
                    if(response.getString("success").equals("true")){
                        msg = "Cloth details submitted succesfully.";
                    }
                    else{
                        msg = "There was some error, please try again.";
                    }
                    final AlertDialog.Builder builder = new AlertDialog.Builder(UntaggedFragment.this.getActivity());
                    builder.setMessage(msg)
                            .setNegativeButton("Okay", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog_exit.dismiss();
                                }
                            });
                    dialog_exit =  builder.create();
                    dialog_exit.show();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, UntaggedFragment.this.getActivity(), Constant.submit_untagged_data, params);
        task.execute();
    }
    private void get_products (Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    String msg = "";
                    if(response.getString("success").equals("true")){
                        JSONArray data = response.getJSONArray("data");
                        List<String> list = new ArrayList<String>();
                        for(int i = 0; i < data.length(); i++){
                            list.add(data.getString(i));
                        }
                        String[] stringArray = list.toArray(new String[list.size()]);
                        ArrayAdapter adapter = new ArrayAdapter(UntaggedFragment.this.getActivity(),android.R.layout.simple_list_item_1,stringArray);
                        type.setAdapter(adapter);
                    }
                    else{
                        msg = "There was some error, please try again.";
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, UntaggedFragment.this.getActivity(), Constant.get_products, params);
        task.execute();
    }

}
