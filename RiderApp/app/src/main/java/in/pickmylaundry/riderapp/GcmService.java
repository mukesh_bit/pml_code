package in.pickmylaundry.riderapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;


/**
 * Service used for receiving GCM messages. When a message is received this service will log it.
 */
public class GcmService extends GcmListenerService {

    public static final String SEND_DATA_INTENT = "OrderBroadCast";
    private String orderid="";
    private String type,message,title,text, ticker;


    public GcmService() {
        //logger = new LoggingService.Logger(this);
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        sendNotification("Received: " + data.toString());
        type = data.getString("type");
        title = data.getString("title");
        text = data.getString("text");
        ticker = data.getString("ticker");
        orderid = data.getString("orderid");

        SharedPreferences sharedPref = GcmService.this.getSharedPreferences(getString(R.string.PREFS_NAME), Context.MODE_PRIVATE);
        generateNotification(message);
    }

    @Override
    public void onDeletedMessages() {
        sendNotification("Deleted messages on server");
    }

    @Override
    public void onMessageSent(String msgId) {
        sendNotification("Upstream message sent. Id=" + msgId);
    }

    @Override
    public void onSendError(String msgId, String error) {
        sendNotification("Upstream message send error. Id=" + msgId + ", error" + error);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
    private void sendNotification(String msg) {
        //logger.log(Log.INFO, msg);
    }

    private  void generateNotification(String message) {
        int icon = R.drawable.logo;
        long when = System.currentTimeMillis();
        NotificationManager notificationManager = (NotificationManager)
                getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(icon, message, when);


        Intent notificationIntent = new Intent(GcmService.this, MainActivity.class);
        notificationIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        notificationIntent.setAction(Intent.ACTION_MAIN);
        notificationIntent.putExtra("type", type);
        notificationIntent.putExtra("salesorderid", orderid);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // set intent so it does not start a new activity
        // notificationIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent intent =
                PendingIntent.getActivity(GcmService.this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        // notification.setLatestEventInfo(GcmService.this, title, message, intent);
        notification.flags |= Notification.FLAG_AUTO_CANCEL;


        notification = new NotificationCompat.Builder(GcmService.this)
                .setContentTitle(title)
                .setContentText(text)
                .setTicker(ticker)
                .setWhen(System.currentTimeMillis())
                .setContentIntent(intent)
                .setAutoCancel(true)
                .setSmallIcon(R.drawable.ic_launcher)
                .build();

        notification.sound = Uri.parse("android.resource://"
                + getPackageName() + "/" + R.raw.siren);

        long[] vibrate = { 0, 100, 200, 300 };
        notification.vibrate = vibrate;

        notificationManager =
                (NotificationManager)getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);

    }
}
