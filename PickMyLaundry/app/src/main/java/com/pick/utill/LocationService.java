package com.pick.utill;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.pick.mylaundry.ChooseLocality;

import java.util.Calendar;

/**
 * Created by Mukesh on 11/4/16.
 */

public class LocationService extends Service implements LocationListener {

    protected LocationManager locationManager;
    private Location location;
    public boolean isRequstingUpdates = false;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 500; // 10 meters
    private static final long MIN_TIME_BW_UPDATES = 1000 * 5; // 1 minute
    private Context context;
    private ChooseLocality chooseLocality;
    private LocationListenerInterface locationListenerInterface;

    public interface LocationListenerInterface{
        public void onResult(Location location);
        public void onError();
    }


    public LocationService(LocationListenerInterface locationListenerInterface, Context context) {
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        this.context = context;
        this.locationListenerInterface = locationListenerInterface;
    }

    public boolean getLocation(final String provider, boolean isLastlocationGood ) {
        if (locationManager.isProviderEnabled(provider)) {
            isRequstingUpdates = true;
            locationManager.requestLocationUpdates(provider, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

            if(isLastlocationGood) {
                Calendar calendar = Calendar.getInstance();
                final Location location = locationManager.getLastKnownLocation(provider);
                if(location != null) {

                    int time_diff = (int) (calendar.getTimeInMillis() - location.getTime()) / (60 * 1000);
                    int time_delay = 0;
                    if (time_diff <= 5) {
                        locationListenerInterface.onResult(location);
                        return true;
                    } else if (time_diff < 30) {
                        time_delay = 1500;
                    } else if (time_diff < 120) {
                        time_delay = 2000;
                    } else if (time_diff < 60 * 24) {
                        time_delay = 3000;
                    } else {
                        time_delay = 4000;
                    }
                    if (time_delay > 0) {
                        Handler handler = new Handler();
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (isRequstingUpdates) {
                                    locationListenerInterface.onResult(location);
                                }
                            }
                        }, time_delay);
                    }
                }
            }
            return true;
        }
        return false;
    }

    public void stopGPS(){
        locationManager.removeUpdates(this);
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onLocationChanged(Location location) {
        if(isRequstingUpdates) {
            locationListenerInterface.onResult(location);
            stopGPS();
            isRequstingUpdates = false;
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
