package com.pick.mylaundry;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileFragment extends Fragment implements View.OnClickListener {
	private TextView profile_number,profile_name,profile_email,profile_loginVia;
	private  Button logoutfb;
	  @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		     super.onCreate(savedInstanceState);
		  	String fontPath = "font/helvetica-normal.ttf";
			Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), fontPath);
			View rootView = inflater.inflate(R.layout.profile, container, false);
       		TextView title=(TextView)getActivity().findViewById(R.id.toolbar_title);
		  	title.setText(AppPrefrence.getName(getActivity()));
		  	profile_number=(TextView) rootView.findViewById(R.id.profile_number);
		  	profile_number.setText(AppPrefrence.getMobile(getActivity()));
		 	profile_name=(TextView) rootView.findViewById(R.id.profile_name);
			profile_name.setText(AppPrefrence.getName(getActivity()));
			profile_loginVia=(TextView) rootView.findViewById(R.id.profile_loginVia);
		    if(!AppPrefrence.getLoginvia(getActivity()).equals("Default"))
				profile_loginVia.setText(AppPrefrence.getLoginvia(getActivity()));
			profile_email=(TextView)rootView.findViewById(R.id.profile_email);
			profile_email.setText(AppPrefrence.getEmail(getActivity()));
			logoutfb=(Button)rootView.findViewById(R.id.profile_flogout);
			profile_number.setTypeface(tf);
			profile_name.setTypeface(tf);
			profile_loginVia.setTypeface(tf);
			profile_email.setTypeface(tf);
			logoutfb.setTypeface(tf);
			TextView profile_details=(TextView) rootView.findViewById(R.id.profile_details);
			profile_details.setTypeface(tf);
			TextView profile_phone_heading=(TextView) rootView.findViewById(R.id.profile_phone_heading);
			profile_phone_heading.setTypeface(tf);
			TextView profile_mydetail_head=(TextView) rootView.findViewById(R.id.profile_mydetail_head);
			profile_mydetail_head.setTypeface(tf);
			TextView profile_name_head=(TextView) rootView.findViewById(R.id. profile_name_head);
		    profile_name_head.setTypeface(tf);
			 TextView profile_email_head=(TextView)rootView.findViewById(R.id.profile_email_head);
		      profile_email_head.setTypeface(tf);
			  logoutfb.setOnClickListener(new View.OnClickListener() {

				  @Override
				  public void onClick(View v) {
					  logout();
				  }
			  });
		  Map<String, String> articleParams = new HashMap<String, String>();
		  articleParams.put("mobile", AppPrefrence.getMobile(getActivity()));
		  //up to 10 params can be logged with each event
		  FlurryAgent.logEvent("profile", articleParams);
	         return rootView;
		
	 }
	 public void logout() {
		 if(Constant.isConnectingToInternet(getActivity())){
			 AppPrefrence.setLogutStatus(getActivity());
			 AppPrefrence.saveUserInfo(getActivity(), "", "");
			 AppPrefrence.setCity(getActivity(),"");
			 Intent intent = new Intent(getActivity(),MobileActivity.class);
			 intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			 intent.putExtra("LOGOUT", true);
             intent.putExtra("SESSION-ID",10);
             SharedPreferences prefs = getActivity().getSharedPreferences("gcmregistartion", Context.MODE_PRIVATE);
             SharedPreferences.Editor editor = prefs.edit();
             editor.putString("registration_id", "");
             editor.putInt("appVersion", 10);
             editor.putBoolean("loggedOut", true);
             editor.commit();
             postRegidRequest();
			 startActivity(intent);
			 Map<String, String> articleParams = new HashMap<String, String>();
			 articleParams.put("mobile", AppPrefrence.getMobile(getActivity()));
			 FlurryAgent.logEvent("log_out_clicked", articleParams);
			 //getActivity().finish();
		  }else{
			  Toast.makeText(getActivity(), "Please check your internet connection", Toast.LENGTH_SHORT).show();
		  }
	 }

        private void postRegidRequest(){
            List<NameValuePair> params=new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(getActivity())));
            params.add(new BasicNameValuePair("reg_id", ""));
            sendRegid(params);
        }

    private void sendRegid(List<NameValuePair> params) {
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                    }

                } catch (JSONException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void taskException() {
            }
        },getActivity(), Constant.GCM_URL,params, false);

        task.execute();
    }

    @Override
	public void onClick(View v) {


	}



	  	 
	 }


