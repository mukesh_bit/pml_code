package com.pick.mylaundry;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.pick.utill.Constant;
import com.pick.utill.CurrentLocation;
import com.pick.utill.LocationService;
import com.pick.volley.EmptyUtility;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mukesh on 11/3/16.
 */

public class ChooseLocality extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private Handler handler;
    private ListView location_lv, cities_lv;
    private UploadTask uploadTask;
    private LinearLayout info_ll;
    private TextView term;
    private ProgressBar loading_spinner;
    private ArrayAdapter<String> adapter_location;
    private ProgressDialog progressDialog;
    private LocationService locationService;
    private RelativeLayout currentLocation;
    private View rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.locality_activity);
        term = (TextView) findViewById(R.id.term);
        location_lv = (ListView) findViewById(R.id.location_lv);
        cities_lv = (ListView) findViewById(R.id.operational_city_lv);
        info_ll = (LinearLayout) findViewById(R.id.info_ll);
        loading_spinner = (ProgressBar) findViewById(R.id.loading_spinner);
        rootView = (LinearLayout) findViewById(R.id.root_view);
        currentLocation = (RelativeLayout) findViewById(R.id.currentLocation);
        ImageView back_btn = (ImageView) findViewById(R.id.back_icon);

        //get url data
        get_url_data();

        term.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(final CharSequence s, int start, int before, int count) {
                if(s.length() > 2){
                    Runnable runnable = new Runnable() {
                        @Override
                        public void run() {
                            get_data(s.toString());
                        }
                    };
                    if (handler != null) {
                        handler.removeCallbacksAndMessages(null);
                    } else {
                        handler = new Handler();
                    }
                    handler.postDelayed(runnable, 500);
                    location_lv.setVisibility(View.VISIBLE);
                    info_ll.setVisibility(View.GONE);
                    loading_spinner.setVisibility(View.GONE);
                }
                else if(s.length() == 0){
                    location_lv.setVisibility(View.GONE);
                    info_ll.setVisibility(View.VISIBLE);
                }
                else{
                    adapter_location = new ArrayAdapter<String>(ChooseLocality.this, R.layout.location_tv, new String[0]);
                    location_lv.setAdapter(adapter_location);
                    location_lv.deferNotifyDataSetChanged();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        currentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getCurrentLocation();
            }
        });
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ChooseLocality.super.onBackPressed();
            }
        });
    }

    public void get_data(String input){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        String url = getPlaceAutoCompleteUrl(input);
        if(uploadTask != null)
            uploadTask.cancel(true);
        uploadTask = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if(jsonObject.getString("status").equals("OK")){
                        JSONArray predictions = jsonObject.getJSONArray("predictions");
                        String[] suggestion_arr = new String[predictions.length()];
                        for (int i=0; i< predictions.length(); i++){
                            suggestion_arr[i] = predictions.getJSONObject(i).getString("description");
                        }
                        adapter_location = new ArrayAdapter<String>(ChooseLocality.this, R.layout.location_tv, suggestion_arr);
                        location_lv.setAdapter(adapter_location);
                        location_lv.deferNotifyDataSetChanged();
                        set_listener();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void taskException() {

            }
        }, ChooseLocality.this, url, params, false);
        uploadTask.execute();
    }

    private String getPlaceAutoCompleteUrl(String input) {
        StringBuilder urlString = new StringBuilder();
        urlString.append("https://maps.googleapis.com/maps/api/place/autocomplete/json");
        urlString.append("?input=");
        try {
            urlString.append(URLEncoder.encode(input, "utf8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        urlString.append("&key=" + Constant.GOOGLEAPIKEY);
        urlString.append(get_url());

        return urlString.toString();
    }

    private String get_url(){
        SharedPreferences preferences = getSharedPreferences("api_url", Context.MODE_PRIVATE);
        return preferences.getString("url", "&location=28.572123,77.202301&radius=160&key=AIzaSyDPWF7HrzJoN5pGi3cutBbPEWZuillgi-k");
    }
    private void get_url_data(){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        UploadTask uploadTask = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    //change url
                    SharedPreferences preferences = getSharedPreferences("api_url", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putString("url", jsonObject.getString("url"));
                    editor.apply();

                    //add cities
                    JSONArray cities = jsonObject.getJSONArray("cities");
                    String[] city_arr = new String[cities.length()];
                    for (int i=0; i<cities.length(); i++){
                        city_arr[i] = cities.getString(i);
                    }
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(ChooseLocality.this, R.layout.location_tv, city_arr);
                    cities_lv.setAdapter(adapter);
                    cities_lv.deferNotifyDataSetChanged();
                    cities_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Toast toast = Toast.makeText(ChooseLocality.this, "Please select your locality in the search bar", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                        }
                    });

                    if(term.getText().toString().length() == 0)
                        info_ll.setVisibility(View.VISIBLE);
                    loading_spinner.setVisibility(View.GONE);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(ChooseLocality.this, "Please check your internet connection", Toast.LENGTH_LONG).show();

                }
            }
            @Override
            public void taskException() {
                Toast.makeText(ChooseLocality.this, "Please check your internet connection", Toast.LENGTH_LONG).show();
            }
        }, ChooseLocality.this, Constant.LOCATIONURL, params, false);
        uploadTask.execute();
    }
    private void set_listener(){
        location_lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selected_location = ((TextView)view).getText().toString();
                Constant.setLocation(selected_location, ChooseLocality.this);
                setResult(1);
                finish();
            }
        });
    }
    private void getCurrentLocation(){
        CurrentLocation currentLocation = new CurrentLocation(this);
        currentLocation.getCurrentLocation(new CurrentLocation.LocationInterface() {
            @Override
            public void onResult(String address) {
                Constant.setLocation(address, ChooseLocality.this);
                setResult(1);
                finish();
            }

            @Override
            public void onError() {

            }
        }, true, true, true);
    }
    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    protected void onResume() {
        super.onResume();
        CurrentLocation currentLocation = new CurrentLocation(ChooseLocality.this);
        currentLocation.getCurrentLocation(new CurrentLocation.LocationInterface() {
            @Override
            public void onResult(String address) {
                if(Constant.getLocation(ChooseLocality.this).equals(Constant.DEFAULTLOCATION)) {
                    Constant.setLocation(address, ChooseLocality.this);
                }
            }

            @Override
            public void onError() {

            }
        }, false, false, false);
    }
}
