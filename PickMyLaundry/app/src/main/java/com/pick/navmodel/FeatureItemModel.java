package com.pick.navmodel;

/**
 * Created by Mukesh on 11/10/16.
 */

public class FeatureItemModel {
    private String icon;
    private String heading;
    private String desc;

    public FeatureItemModel() {
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }
}
