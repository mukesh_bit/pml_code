package in.pickmylaundry.riderapp;


    import android.app.Activity;
    import android.content.DialogInterface;
    import android.content.Intent;
    import android.graphics.Color;
    import android.os.Bundle;
    import android.support.annotation.Nullable;
    import android.support.v4.app.Fragment;
    import android.support.v7.app.AlertDialog;
    import android.util.Log;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;
    import android.widget.CompoundButton;
    import android.widget.LinearLayout;
    import android.widget.RadioGroup;
    import android.widget.TextView;
    import android.widget.Toast;

    import com.rey.material.widget.RadioButton;

    import org.json.JSONArray;
    import org.json.JSONException;
    import org.json.JSONObject;

    import java.util.ArrayList;
    import java.util.concurrent.TimeUnit;

/**
 * Created by 121 on 12/13/2015.
 */
public class TimeslotFragment extends Fragment {
    private View rootView;
    private Bundle bundle;
    private RadioButton[] radioButtons;
    private LinearLayout timeslot_ll;
    private ArrayList<String> inactive_slots;
    private JSONObject slots_ob;
    private LayoutInflater inflater;
    private String keys;
    private String selected_slot;
    private RadioButton rb_checked;
    private boolean dialog_visibility = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = (View) inflater.inflate(R.layout.timeslots, container, false);
        this.inflater = inflater;

        bundle = (Bundle) getArguments();
        keys = bundle.getString("keys");

        timeslot_ll = (LinearLayout) rootView.findViewById(R.id.timeslots);

        //set slot view
        inactive_slots = bundle.getStringArrayList("inactive_slots");
        addSlots();

        return rootView;
    }
    CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(dialog_visibility) {
                rb_checked = (RadioButton) rootView.findViewWithTag(buttonView.getTag());
                selected_slot = rb_checked.getText().toString();
                show_dialog();
            }
            dialog_visibility = true;
        }
    };
    private void addSlots(){
        try {
            slots_ob = new JSONObject(bundle.getString("slots"));
            JSONArray keys_arr = new JSONArray(keys);
            int iteration_count=0;
            for(int j=0; j<keys_arr.length();j++){
                String key = keys_arr.getString(j);
                JSONArray slots_part = slots_ob.getJSONArray(key);
                View slot_heading_view = inflater.inflate(R.layout.slot_heading, null);
                timeslot_ll.addView(slot_heading_view);
                TextView heading = (TextView) slot_heading_view.findViewById(R.id.heading);
                heading.setText(key);
                radioButtons = new RadioButton[slots_part.length()];
                for (int i=0;i<slots_part.length();i++){
                    View slot_view = inflater.inflate(R.layout.slot, null);
                    RadioButton rb = (RadioButton) slot_view.findViewById(R.id.slot);
                    rb.setTag("slot"+iteration_count);
                    if(isInactive(slots_part.get(i).toString())){
                        rb.setEnabled(false);
                        rb.setTextColor(Color.parseColor("#CECECE"));
                    }
                    else {
                        rb.setOnCheckedChangeListener(listener);
                    }
                    rb.setText(slots_part.get(i).toString());
                    timeslot_ll.addView(slot_view);
                    iteration_count++;
                    radioButtons[i] = rb;
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private boolean isInactive(String slot){
        if(inactive_slots.contains(slot))
            return true;
        return false;

    }
    private void show_dialog(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage("Are you sure,You want to schedule it to "+bundle.getString("date")+" between "+selected_slot);
        alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                Intent intent = new Intent();
                intent.putExtra("date", bundle.getString("date"));
                intent.putExtra("slot", selected_slot);
                getActivity().setResult(1, intent);
                getActivity().finish();
            }
        });

        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                dialog_visibility = false;
                rb_checked.setChecked(false);
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
