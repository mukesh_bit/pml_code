package com.pick.utill;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class Pickhelper extends SQLiteOpenHelper {
	 
    // Logcat tag
    private static final String LOG ="DatabaseHelper";
 
    // Database Version
    private static final int DATABASE_VERSION =3;
 
    // Database Name
    private static final String DATABASE_NAME = "pml_manager";
 
    // Table Names 
    public static final String TABLE_ADDRESS= "cust_Address";
    public static final String TABLE_TESTIMONIAL= "cust_testimonial";
    public static final String KEY_FLATNO= "flatno";
    public static final String KEY_LANDMARK="landmark";
    public static final String KEY_AREA="area";
    public static final String KEY_TYPE="type";
    public static final String KEY_NAME= "name";
    public static final String KEY_IMAGE_URL="image_url";
    public static final String KEY_PROFEFSSION="profession";
    public static final String KEY_TESTIMONIAL="testimonial";
    public static final String KEY_ACTIVE="active";
    public static final String KEY_PRIMARY="primarykey";
    public static final String CREATE_TABLE_ADDRESS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ADDRESS + "(" +KEY_PRIMARY +" integer primary key autoincrement," + KEY_FLATNO
            + " TEXT," +KEY_LANDMARK+" TEXT,"+KEY_AREA+
            " TEXT,"+ KEY_TYPE+" TEXT,"+KEY_ACTIVE+" TEXT)";
    public static final String CREATE_TABLE_TESTIMONIAL = "CREATE TABLE IF NOT EXISTS "
            + TABLE_TESTIMONIAL + "(" +KEY_PRIMARY +" integer primary key autoincrement," + KEY_NAME
            + " TEXT," +KEY_IMAGE_URL+" TEXT,"+KEY_PROFEFSSION+
            " TEXT,"+ KEY_TESTIMONIAL+" TEXT)";
    public Pickhelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
 
    @Override
    public void onCreate(SQLiteDatabase db) {
     // creating required tables
         db.execSQL(CREATE_TABLE_ADDRESS);
     }
 
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS);
        onCreate(db);
    }
}