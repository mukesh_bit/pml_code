package in.pickmylaundry.riderapp.utils;

import android.content.Context;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by 121 on 3/9/2016.
 */
public class Constant {
    public final static String GET_REQUEST = "GET";
    public final static int POSITIVE_SWIPE = 0;
    public final static int NEGATIVE_SWIPE = 1;
    public final static String POST_REQUEST = "POST";
    public final static String GCM_URL = "http://crm.pickmylaundry.in/web_services/rider_app/store_regid.php";
    public static final String LOGIN = "http://crm.pickmylaundry.in/web_services/rider_app/login.php";
    public static final String CALL = "http://crm.pickmylaundry.in/web_services/rider_app/call_customer.php";

    public static String get_assigned_list = "http://crm.pickmylaundry.in/web_services/rider_app/get_assigned_list.php";
    public static String get_assigned_list2 = "http://crm.pickmylaundry.in/web_services/rider_app/get_assigned_list2.php";
    public static String get_completed_list = "http://crm.pickmylaundry.in/web_services/rider_app/get_completed_list.php";
    public static String get_completed_list2 = "http://crm.pickmylaundry.in/web_services/rider_app/get_completed_list2.php";
    public static String send_sms = "http://crm.pickmylaundry.in/web_services/rider_app/send_sms.php";
    public static String not_done = "http://crm.pickmylaundry.in/web_services/rider_app/not_done.php";
    public static String new_so = "http://crm.pickmylaundry.in/web_services/rider_app/new_so.php";
    public static String done = "http://crm.pickmylaundry.in/web_services/rider_app/done.php";
    public static String not_done_reason = "http://crm.pickmylaundry.in/web_services/rider_app/not_done_reason.php";
    public static String slot_data = "http://crm.pickmylaundry.in/web_services/rider_app/get_slot_data.php";
    public static String incentive = "http://crm.pickmylaundry.in/web_services/rider_app/incentive_list.php";
    public static String next_customer = "http://crm.pickmylaundry.in/web_services/rider_app/next_customer.php";
    public static String mileage = "http://crm.pickmylaundry.in/web_services/rider_app/get_mileage_data.php";
    public static String check_otp = "http://crm.pickmylaundry.in/web_services/rider_app/check_otp.php";
    public static String image_post = "http://crm.pickmylaundry.in/web_services/rider_app/upload_image.php";
    public static String laundry_list = "http://crm.pickmylaundry.in/web_services/rider_app/get_laundry_list.php";
    public static String drop_location = "http://crm.pickmylaundry.in/web_services/rider_app/set_drop_location.php";


    public static String get_current_time(String format){
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        return sdf.format(cal.getTime());
    }

    //return time in sec
    public static long compare_dates(String date1, String date2, String format_date){
        SimpleDateFormat format = new SimpleDateFormat(format_date);
        try {
            Date d1 = format.parse(date1);
            Date d2 = format.parse(date2);

            long diff = d1.getTime() - d2.getTime();
            return diff/1000;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }
    public static void no_internet_connection(Context context){
        Toast.makeText(context, "Check your internet connection", Toast.LENGTH_LONG).show();
    }
}
