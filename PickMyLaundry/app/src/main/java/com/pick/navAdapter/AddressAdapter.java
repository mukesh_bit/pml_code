package com.pick.navAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flurry.android.FlurryAgent;
import com.pick.mylaundry.AddAddress;
import com.pick.mylaundry.R;
import com.pick.mylaundry.SelectAddress;
import com.pick.navmodel.AddressListModel;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.utill.Pickhelper;
import com.rey.material.widget.RadioButton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddressAdapter extends BaseAdapter implements OnClickListener {
	private Context context;
	private String contextstring;
	private List<AddressListModel> addressList;
	private SQLiteDatabase db;
	private Pickhelper dbhelper;
	private AddressAdapter adapter;
	private int checkadd;
	public AddressAdapter(Context context,List<AddressListModel> addresslist){
		this.context=context;
		this.addressList=addresslist;
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return addressList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater mInflater = (LayoutInflater) context
					.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.address_row, null);
		}
		String fontPath = "font/helvetica-normal.ttf";
		Typeface tf = Typeface.createFromAsset(context.getAssets(),fontPath);
		adapter = this;
		final RadioButton rb=(RadioButton) convertView.findViewById(R.id.radio_address);
		final LinearLayout address_detail_ll = (LinearLayout) convertView.findViewById(R.id.addressDetail);
		rb.setTypeface(tf);
		View.OnClickListener address_select_listener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				int primary_key=addressList.get(position).getPrimarykey();
				dbhelper=new Pickhelper(context);
				db=dbhelper.getWritableDatabase();
				ContentValues values1 = new ContentValues();
				values1.put(Pickhelper.KEY_ACTIVE,"no");
				db.update(Pickhelper.TABLE_ADDRESS, values1, null, null);
				ContentValues values = new ContentValues();
				values.put(Pickhelper.KEY_ACTIVE, "yes");
				db.update(Pickhelper.TABLE_ADDRESS, values, Pickhelper.KEY_PRIMARY + "=" + primary_key, null);
				addressList.get(SelectAddress.ACTIVE_ADDRESS_POSITION).setYes("No");
				addressList.get(position).setYes("yes");
				adapter.notifyDataSetChanged();
				db.close();
				Constant.setLocation(addressList.get(position).getArea(), context);

			}
		};
		address_detail_ll.setOnClickListener(address_select_listener);
		rb.setOnClickListener(address_select_listener);
		address_detail_ll.setOnClickListener(address_select_listener);
		ImageView remove=(ImageView) convertView.findViewById(R.id.delete);
		remove.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
				alertDialog.setMessage("Are you sure you want to delete this address?");
				alertDialog.setPositiveButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
						Map<String, String> articleParams = new HashMap<String, String>();
						articleParams.put("mobile", AppPrefrence.getMobile(context));
						articleParams.put("activity", "no_clicked");
						//up to 10 params can be logged with each event
						FlurryAgent.logEvent("Remove address", articleParams);
					}
				});
				alertDialog.setNegativeButton("Yes", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						int primary_key = addressList.get(position).getPrimarykey();
						String activeStatus = addressList.get(position).getYes();
						dbhelper = new Pickhelper(context);
						db = dbhelper.getWritableDatabase();
						db.delete(Pickhelper.TABLE_ADDRESS, Pickhelper.KEY_PRIMARY + "=" + primary_key, null);
						addressList.remove(position);
						if (activeStatus.equals("yes") && !addressList.isEmpty()) {
							int primary_key_active = addressList.get(0).getPrimarykey();
							addressList.get(0).setYes("yes");
							ContentValues values = new ContentValues();
							values.put(Pickhelper.KEY_ACTIVE, "yes");
							db.update(Pickhelper.TABLE_ADDRESS, values, Pickhelper.KEY_PRIMARY + "= " + primary_key_active, null);
							SelectAddress.ACTIVE_ADDRESS_POSITION = 0;
							SelectAddress.ACTIVE_PRIMARY_POSITION = primary_key_active;
							Constant.setLocation(addressList.get(0).getArea(), context);
						}
						adapter.notifyDataSetChanged();
						dialog.cancel();
						db.close();
						Map<String, String> articleParams = new HashMap<String, String>();
						articleParams.put("mobile", AppPrefrence.getMobile(context));
						articleParams.put("activity", "yes_clicked");
						//up to 10 params can be logged with each event
						FlurryAgent.logEvent("Remove address", articleParams);
					}
				});
				alertDialog.show();
				Map<String, String> articleParams = new HashMap<String, String>();
				articleParams.put("mobile", AppPrefrence.getMobile(context));
				articleParams.put("activity", "clicked");
				//up to 10 params can be logged with each event
				FlurryAgent.logEvent("Remove address", articleParams);
			}
		});
		ImageView edit=(ImageView) convertView.findViewById(R.id.edit);
		TextView full_address_tv = (TextView) convertView.findViewById(R.id.full_address);
		TextView label_tv = (TextView) convertView.findViewById(R.id.adress_label);
		String active;
		active = addressList.get(position).getYes().toString();
		String address=addressList.get(position).getFlatno().toString()+"\n"+
						addressList.get(position).getLandmark().toString()+"\n"+
						addressList.get(position).getArea().toString()+"\n";
		full_address_tv.setText(address);
		String type;
		if(addressList.get(position).getType() != null)
		 	type=addressList.get(position).getType().toString();
		else
			type = "Other";
		label_tv.setText(type);
		//String active=addressList.get(position).getYes().toString();
		if(active.equals("yes")){
			rb.setChecked(true);
			SelectAddress.ACTIVE_ADDRESS_POSITION = position;
			SelectAddress.ACTIVE_PRIMARY_POSITION = addressList.get(position).getPrimarykey();
			full_address_tv.setTextColor(Color.parseColor("#006699"));
			label_tv.setTextColor(Color.parseColor("#006699"));
		}else{
			rb.setChecked(false);
			full_address_tv.setTextColor(Color.parseColor("#777777"));
			label_tv.setTextColor(Color.parseColor("#777777"));
		}
		edit.setOnClickListener(new OnClickListener() {
			 @Override
			public void onClick(View v) {
				 int primary_key=addressList.get(position).getPrimarykey();
				 Intent intent = new Intent(context, AddAddress.class);
				 intent.putExtra("mode", "Edit");
				 intent.putExtra("primary_key",primary_key);
				 ((Activity)context).startActivityForResult(intent, 1);

				 Map<String, String> articleParams = new HashMap<String, String>();
				 articleParams.put("mobile", AppPrefrence.getMobile(context));
				 //up to 10 params can be logged with each event
				 FlurryAgent.logEvent("Edit address", articleParams);
			}
		});
		return convertView;
	}

	 
	@Override
	public void onClick(View v) { }

}
