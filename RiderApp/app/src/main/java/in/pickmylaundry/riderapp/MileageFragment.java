package in.pickmylaundry.riderapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.riderapp.utils.AppPrefrence;
import in.pickmylaundry.riderapp.utils.Constant;
import in.pickmylaundry.riderapp.utils.ImageLoader;
import in.pickmylaundry.riderapp.utils.PickHelper;
import in.pickmylaundry.riderapp.utils.UploadTask;

/**
 * Created by 121 on 9/20/2016.
 */
public class MileageFragment extends Fragment {

    private View rootView;
    private TextView shift1, shift2;
    private JSONObject data_json = new JSONObject();
    private JSONObject shift1_data,shift2_data;
    private LinearLayout login_otp_ll, logout_otp_ll, data_ll;
    private EditText login_reading_et, logout_reading_et, login_otp, logout_otp;
    private Button add_image_login, add_image_logout, done_login, done_logout;
    private ImageView login_reading_iv, logout_reading_iv;
    private RelativeLayout logout_detail, login_detail;
    private ImageLoader imageLoader;
    private Button login_btn, logout_btn;
    private String active_shift = "shift1";
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
    private Uri fileUri;
    private String filepath = "";
    private String image_type, image_shift, timestamp;
    private ContentBody foto_body;
    private TextView login_time, logout_time;
    private PickHelper dbHelper;
    private SQLiteDatabase db;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.mileage,null);

        getActivity().setTitle("Mileage");

        shift1 = (TextView) rootView.findViewById(R.id.shift1);
        shift2 = (TextView) rootView.findViewById(R.id.shift2);

        data_ll = (LinearLayout) rootView.findViewById(R.id.data_ll);

        login_otp_ll = (LinearLayout) rootView.findViewById(R.id.otp);
        logout_otp_ll = (LinearLayout) rootView.findViewById(R.id.otp_logout);
        login_reading_et = (EditText) rootView.findViewById(R.id.mileage_login);
        logout_reading_et = (EditText) rootView.findViewById(R.id.mileage_logout);
        add_image_login = (Button) rootView.findViewById(R.id.add_image_login);
        add_image_logout = (Button) rootView.findViewById(R.id.add_image_logout);
        done_login = (Button) rootView.findViewById(R.id.done_login);
        done_logout = (Button) rootView.findViewById(R.id.done_logout);
        login_reading_iv = (ImageView) rootView.findViewById(R.id.reading_login_iv);
        logout_reading_iv = (ImageView) rootView.findViewById(R.id.reading_logout_iv);
        logout_detail = (RelativeLayout) rootView.findViewById(R.id.logout_detail);
        login_detail = (RelativeLayout) rootView.findViewById(R.id.login_detail);
        login_time = (TextView) rootView.findViewById(R.id.login_time);
        logout_time = (TextView) rootView.findViewById(R.id.logout_time);

        logout_btn = (Button) rootView.findViewById(R.id.logout_btn);
        login_btn = (Button) rootView.findViewById(R.id.login_btn);
        login_otp = (EditText) rootView.findViewById(R.id.login_otp);
        logout_otp = (EditText) rootView.findViewById(R.id.logout_otp);

        shift1.setOnClickListener(listener);
        shift2.setOnClickListener(listener);

        get_data();

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = login_otp.getText().toString();
                if(otp.length() >2)
                    set_otp_param("login", otp, active_shift);
                else
                    Toast.makeText(getActivity(), "Please enter valid otp", Toast.LENGTH_LONG).show();
            }
        });
        logout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otp = logout_otp.getText().toString();
                int pending_orders = get_pending_orders();

                if(pending_orders > 0)
                    Toast.makeText(getActivity(), "Please complete your pending orders first", Toast.LENGTH_LONG).show();
                else if(otp.length() > 2)
                    set_otp_param("logout", otp, active_shift);
                else
                    Toast.makeText(getActivity(), "Please enter valid otp", Toast.LENGTH_LONG).show();
            }
        });
        add_image_login.setOnClickListener(cameraListener);
        add_image_logout.setOnClickListener(cameraListener);

        done_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login_reading = login_reading_et.getText().toString();
                if(filepath.length() > 2 && login_reading.length() > 2) {
                    new UploadImage(getActivity(), filepath, foto_body, image_type, image_shift, login_reading, timestamp, new UploadImage.AsyncResponse() {
                        @Override
                        public void imageResponse(String output) {
                            image_upload_response(output);
                        }
                    }).execute();
                }
                else if(login_reading.length() <=2 ){
                    Toast.makeText(getActivity(), "Please enter km reading", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getActivity(), "Please click image of reading", Toast.LENGTH_LONG).show();
                }
            }
        });
        done_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String logout_reading = logout_reading_et.getText().toString();

                if(filepath.length() > 2 && logout_reading.length() > 2) {
                    int diff = Integer.parseInt(logout_reading) - Integer.parseInt(login_reading_et.getText().toString());
                    if(diff <0 || diff > 200)
                        Toast.makeText(getActivity(), "Wrong km reading", Toast.LENGTH_LONG).show();
                    else {
                        new UploadImage(getActivity(), filepath, foto_body, image_type, image_shift, logout_reading, timestamp, new UploadImage.AsyncResponse() {
                            @Override
                            public void imageResponse(String output) {
                                image_upload_response(output);
                            }
                        }).execute();
                    }
                }
                else if(logout_reading.length() <=2 ){
                    Toast.makeText(getActivity(), "Please enter km reading", Toast.LENGTH_LONG).show();
                }
                else{
                    Toast.makeText(getActivity(), "Please click image of reading", Toast.LENGTH_LONG).show();
                }
            }
        });

        return rootView;
    }
    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String shift = v.getTag().toString();
            if(shift.equals("shift2")){
                String logout_time = "";
                try {
                    JSONObject logout_time_ob = shift1_data.getJSONObject("logout");
                    logout_time = logout_time_ob.getString("time");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(!logout_time.equals("")){
                    shift2.setBackgroundColor(Color.parseColor("#3399CC"));
                    shift1.setBackgroundColor(Color.parseColor("#e6e6e6"));
                    shift1.setTextColor(Color.parseColor("#000000"));
                    shift2.setTextColor(Color.parseColor("#ffffff"));
                    reset_data();
                    set_data("shift2");
                }
                else{
                    Toast.makeText(getActivity(), "Please complete first shift", Toast.LENGTH_LONG).show();
                }
                active_shift = "shift2";

            }
            else{
                shift1.setBackgroundColor(Color.parseColor("#3399CC"));
                shift2.setBackgroundColor(Color.parseColor("#e6e6e6"));
                shift2.setTextColor(Color.parseColor("#000000"));
                shift1.setTextColor(Color.parseColor("#ffffff"));
                reset_data();
                set_data("shift1");
                active_shift = "shift1";
            }

        }
    };
    private void reset_data(){
        login_reading_et.setText("");
        login_reading_iv.setImageBitmap(null);
        login_reading_iv.invalidate();
        logout_reading_et.setText("");
        logout_reading_iv.setImageBitmap(null);
        logout_reading_iv.invalidate();
        login_otp.setText("");
        logout_otp.setText("");
    }
    private void get_data(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rider_id", AppPrefrence.getMobile(getActivity()));
        get_mileage_data(params);
    }
    private void get_mileage_data(Map<String, Object> params){
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result)  {
                try {
                    JSONObject object = new JSONObject(result);
                    if(object.getBoolean("success")){
                        data_json = object.getJSONObject("data");
                        shift1_data = data_json.getJSONObject("shift1");
                        shift2_data = data_json.getJSONObject("shift2");
                        set_data(active_shift);
                        data_ll.setVisibility(View.VISIBLE);
                    }
                    else{
                        Toast.makeText(getActivity(), "Check your internet connection", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void taskException() {
                Constant.no_internet_connection(getActivity());
            }
        }, getActivity(), Constant.mileage, params);
        task.execute();
    }
    private void set_data(String shift_no){
        String login_reading = "", login_image_url = "", login_time_val="";
        try {
            JSONObject shift_data = data_json.getJSONObject(shift_no);
            login_reading = shift_data.getJSONObject("login").getString("reading");
            login_image_url = shift_data.getJSONObject("login").getString("image_url");
            login_time_val = shift_data.getJSONObject("login").getString("time");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(login_reading.equals("")){
            login_otp_ll.setVisibility(View.VISIBLE);
            login_detail.setVisibility(View.GONE);
        }
        else{
            login_otp_ll.setVisibility(View.GONE);
            login_detail.setVisibility(View.VISIBLE);
            imageLoader = new ImageLoader(login_image_url, login_reading_iv);
            imageLoader.execute();
            add_image_login.setVisibility(View.GONE);
            done_login.setVisibility(View.GONE);
            login_time.setText("Logged in at " + login_time_val);
            login_reading_et.setText(login_reading+"");
            login_reading_et.setEnabled(false);
        }

        String logout_reading = "", logout_image_url = "", logout_time_val = "";
        try {
            JSONObject shift_data = data_json.getJSONObject(shift_no);
            logout_reading = shift_data.getJSONObject("logout").getString("reading");
            logout_image_url = shift_data.getJSONObject("logout").getString("image_url");
            logout_time_val = shift_data.getJSONObject("logout").getString("time");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        if(login_reading.equals("")){
            logout_otp_ll.setVisibility(View.GONE);
            logout_detail.setVisibility(View.GONE);
        }
        else if(logout_reading.equals("")){
            logout_otp_ll.setVisibility(View.VISIBLE);
            logout_detail.setVisibility(View.GONE);
        }
        else{
            logout_otp_ll.setVisibility(View.GONE);
            logout_detail.setVisibility(View.VISIBLE);
            logout_reading_et.setEnabled(false);
            imageLoader = new ImageLoader(logout_image_url, logout_reading_iv);
            imageLoader.execute();
            add_image_logout.setVisibility(View.GONE);
            done_logout.setVisibility(View.GONE);
            logout_time.setText("Logged in at " + logout_time_val);
            logout_reading_et.setText(logout_reading);
        }
        filepath = "";
    }
    private void set_otp_param(String type, String otp, String shift){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rider_id", AppPrefrence.getMobile(getActivity()));
        params.put("type", type);
        params.put("otp", otp);
        params.put("shift", shift);
        set_otp_request(params);
    }
    private void set_otp_request(final Map<String, Object> params){
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result)  {
                try {
                    JSONObject object = new JSONObject(result);
                    if(object.getBoolean("success")){
                        String type = params.get("type").toString();
                        timestamp = object.getString("time");
                        if(type.equals("login"))
                            login_time.setText("Logged in at "+timestamp);
                        else
                            logout_time.setText("Logged out at " + timestamp);
                        show_imageview(type);
                    }
                    else{
                        Toast.makeText(getActivity(), "Invalid otp", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void taskException() {
                Constant.no_internet_connection(getActivity());
            }
        }, getActivity(), Constant.check_otp, params);
        task.execute();
    }
    private void show_imageview(String type){
        if(type.equals("login")){
            login_detail.setVisibility(View.VISIBLE);
            login_reading_et.setEnabled(true);
            add_image_login.setVisibility(View.VISIBLE);
            done_login.setVisibility(View.VISIBLE);
            login_otp_ll.setVisibility(View.GONE);
        }
        else{
            logout_detail.setVisibility(View.VISIBLE);
            logout_reading_et.setEnabled(true);
            add_image_logout.setVisibility(View.VISIBLE);
            done_logout.setVisibility(View.VISIBLE);
            logout_otp_ll.setVisibility(View.GONE);
        }
    }
    private View.OnClickListener cameraListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String type = v.getTag().toString();
            captureImage(type, active_shift);
        }
    };
    private void captureImage(String type, String shift) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);// create a file to save the image
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
        image_type = type;
        image_shift = shift;

        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }
    private Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }
    /**
     * Create a File for saving an image or video
     */
    private File getOutputMediaFile(int type) {
        File mediaStorageDir = null;
        String state = Environment.getExternalStorageState();
        mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "PML Rider");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("PML VendorApp", "failed to create directory");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = null;
        if (type == MEDIA_TYPE_IMAGE) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        } else {
            return null;
        }
        return mediaFile;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.i("image", resultCode+"-"+getActivity().RESULT_OK+"-"+requestCode+"-"+CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {
            if (resultCode == getActivity().RESULT_OK) {
                filepath= fileUri.getPath();
                BitmapFactory.Options options = new BitmapFactory.Options();

                options.inSampleSize = 8;

                final Bitmap bitmap = BitmapFactory.decodeFile(filepath, options);
                if(image_type.equals("login")){
                    login_reading_iv.setImageBitmap(bitmap);
                }
                else{
                    logout_reading_iv.setImageBitmap(bitmap);
                }
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 70, bos);
                InputStream in = new ByteArrayInputStream(bos.toByteArray());
                foto_body = new InputStreamBody(in, "image/jpeg");

            } else if (resultCode == getActivity().RESULT_CANCELED) {
                Toast.makeText(getActivity(), "Image not captured, try again", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getActivity(), "Image not captured, try again", Toast.LENGTH_LONG).show();
            }
        }

    }

    public void image_upload_response(String output){
        try {
            JSONObject object = new JSONObject(output);
            if(object.getString("success").equals("true")){
                get_data();
            }
            else{
                Toast.makeText(getActivity(), "There was some problem while uploading image, refresh it", Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
    private int get_pending_orders(){
        dbHelper = new PickHelper(getActivity());
        db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+PickHelper.TABLE_ADDRESS+" WHERE "+PickHelper.KEY_STATUS+" = 'pending'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }
}

