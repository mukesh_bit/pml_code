package in.pickmylaundry.riderapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import in.pickmylaundry.riderapp.DetailActivity;
import in.pickmylaundry.riderapp.MainActivity;
import in.pickmylaundry.riderapp.Model.AssignedListModel;
import in.pickmylaundry.riderapp.R;
import in.pickmylaundry.riderapp.utils.AppPrefrence;
import in.pickmylaundry.riderapp.utils.Constant;
import in.pickmylaundry.riderapp.utils.UploadTask;

/**
 * Created by 121 on 5/12/2016.
 */
public class AssignedListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<AssignedListModel> list = new ArrayList<AssignedListModel>();
    private LayoutInflater inflater;
    private String status;
    private UploadTask task;


    public AssignedListAdapter(Context context, ArrayList<AssignedListModel> list, String status){
        this.context = context;
        this.list = list;
        this.status = status;
        Log.i("list_values", list.toString());
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.order_list_item, parent, false);
        }

            RelativeLayout container_rl = (RelativeLayout) convertView.findViewById(R.id.container_rl);
            TextView so = (TextView) convertView.findViewById(R.id.so);
            TextView name = (TextView) convertView.findViewById(R.id.name);
            TextView address = (TextView) convertView.findViewById(R.id.address);
            TextView slot = (TextView) convertView.findViewById(R.id.slot);
            TextView urgent = (TextView) convertView.findViewById(R.id.urgent);
            ImageView status_iv = (ImageView) convertView.findViewById(R.id.status);
            LinearLayout active_customer = (LinearLayout) convertView.findViewById(R.id.active_order);

            so.setText(list.get(position).getSo());
            name.setText(list.get(position).getName());
            address.setText(list.get(position).getAddress());
            slot.setText(list.get(position).getTime());
            if(list.get(position).getColor().equals("")) {
                urgent.setVisibility(View.GONE);
            }
            else {
                urgent.setVisibility(View.VISIBLE);
                urgent.setBackgroundColor(Color.parseColor(list.get(position).getColor()));
            }

            String status_val = list.get(position).getStatus();
            Button detail = (Button) convertView.findViewById(R.id.detail_container);


            if(list.get(position).getIsNext().equals("true")) {
                detail.setVisibility(View.GONE);
                active_customer.setVisibility(View.VISIBLE);
                so.setTextColor(Color.parseColor("#ffffff"));
                if(status_val.equals("Confirmed"))
                    status_iv.setImageResource(R.drawable.done);
                else if(status_val.equals("Denied"))
                    status_iv.setImageResource(R.drawable.denied);
                else{
                    status_iv.setImageResource(0);
                }
            }
            else{
                active_customer.setVisibility(View.GONE);
                so.setTextColor(Color.parseColor("#f57a1d"));
                if(status_val.equals("Confirmed"))
                    status_iv.setImageResource(R.drawable.confirmed);
                else if(status_val.equals("Denied"))
                    status_iv.setImageResource(R.drawable.denied);
                else{
                    status_iv.setImageResource(0);
                }
            }

            //detail click listener

            Button call = (Button) convertView.findViewById(R.id.call_container);
            container_rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtra("salesorderid", list.get(position).getSalesorderid());
                    intent.putExtra("type", list.get(position).getType());
                    intent.putExtra("status", status);
                    intent.putExtra("sostatus", list.get(position).getSostatus());
                    context.startActivity(intent);
                }
            });
            call.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    makecall(list.get(position).getMobile());
                }
            });
            detail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestNextCustomer(list.get(position).getSalesorderid(), list.get(position).getType());
                }
            });
            ImageView ribbon = (ImageView) convertView.findViewById(R.id.ribbon);
            if(list.get(position).getSostatus().equals("Not Picked Up") || list.get(position).getSostatus().equals("Not Delivered")){
                ribbon.setVisibility(View.VISIBLE);
            }
            else{
                ribbon.setVisibility(View.GONE);
            }
        return convertView;
    }
    private void makecall(String mobile) {
        Intent intent = new Intent("android.intent.action.CALL");
        intent.setData(Uri.parse("tel:"+mobile));
        context.startActivity(intent);
        /*Map<String, Object> param = new HashMap<String, Object>();
        param.put("from", AppPrefrence.getPhone(context));
        param.put("to", mobile);
        request_call(param, mobile);*/
    }
    private void request_call(Map<String, Object> params, final String mobile){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){

                    }
                    else{
                        //Toast.makeText(context, response.getString("msg"), Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(context, "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, context, Constant.CALL, params);
        task.execute();
    }
    private void requestNextCustomer(String salesorderid, String type){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rider_id", AppPrefrence.getMobile(context));
        params.put("salesorderid", salesorderid);
        params.put("type", type);
        setNextCustomer(params);
    }
    private void setNextCustomer(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){
                        Intent intent = new Intent(context, MainActivity.class);
                        context.startActivity(intent);
                    }
                    else{
                        Toast.makeText(context, response.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(context, "Check your internet connection", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(context, "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, context, Constant.next_customer, params);
        task.execute();
    }
}
