package com.pick.navAdapter;


/**
 * Created by Ravi on 29/07/15.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pick.mylaundry.R;
import com.pick.navmodel.NavDrawerItem;

import java.util.Collections;
import java.util.List;


public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {
    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());
        holder.img.setImageResource(current.getIcon());
        if(position==5){
            holder.vw.setVisibility(View.VISIBLE);
            holder.new_tv.setVisibility(View.VISIBLE);
        }else if(position==6){
            holder.vw2.setVisibility(View.VISIBLE);
        }
        else if(position==4){
            holder.new_tv.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title, new_tv;
        ImageView img;
        View vw,vw2;
        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.nav_drawer_title);
             img=(ImageView) itemView.findViewById(R.id.nav_drawer_icon);
             vw=(View)itemView.findViewById(R.id.nav_drawer_menu_divider);
             vw2=(View)itemView.findViewById(R.id.nav_drawer_menu_divider2);
            new_tv=(TextView) itemView.findViewById(R.id.new_nav);

        }
    }
}
