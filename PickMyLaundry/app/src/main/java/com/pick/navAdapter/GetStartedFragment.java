package com.pick.navAdapter;

import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pick.mylaundry.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by 121 on 1/5/2016.
 */
public class GetStartedFragment extends android.support.v4.app.Fragment {
    private int[] background = {
            R.drawable.getting_started_1,
            R.drawable.getting_started_2,
            R.drawable.getting_started_3
    };
    private String[] screen_text = {
            "Schedule a Pickup",
            "Meet rider at doorstep",
            "Washed in Top Notch Facility"
    };
    private Timer swipeTimer;
    private Handler handler;
    private Runnable Update;
    private DisplayMetrics displayMetrics;
    private LinearLayout background_iv;
    private int count;
    private RelativeLayout layout;
    private View itemView;
    private ImageView bike_image, mobile_image, signal_image;
    private boolean reverse;
    private RelativeLayout.LayoutParams rl_lp;
    private Thread thread;
    private Timer t;

    public GetStartedFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        int position = getArguments().getInt("position");
        displayMetrics = getResources().getDisplayMetrics();
        if(position==3){
            itemView = inflater.inflate(R.layout.get_started_bike, container, false);
            bike_image = (ImageView) itemView.findViewById(R.id.bike);
            mobile_image = (ImageView) itemView.findViewById(R.id.mobile);
            TextView payment_tv = (TextView) itemView.findViewById(R.id.payment_tv);
            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaUltraComp.otf");
            payment_tv.setTypeface(tf);
            count = 0;
            layout = (RelativeLayout) itemView.findViewById(R.id.container);
            BitmapDrawable bg = (BitmapDrawable) itemView.getResources().getDrawable(R.drawable.background);
            bg.setTileModeX(Shader.TileMode.REPEAT);
            if(Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN)
                layout.setBackgroundDrawable(bg);
            else
                layout.setBackground(bg);
            swipeTimer = new Timer();
            handler = new Handler();
            Update = new Runnable() {
                public void run() {
                    int width = displayMetrics.widthPixels;
                    int height = displayMetrics.heightPixels;
                    if(width>800)
                        count+=4;
                    else
                        count+=2;
                    RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(1300+count, ViewGroup.LayoutParams.WRAP_CONTENT);
                    int rl_height = layout.getMeasuredHeight();
                    int position = 1225 * height / 2667;
                    lp.setMargins(0 - count, position - rl_height, 0, 0);
                    layout.setLayoutParams(lp);
                    layout.setVisibility(View.VISIBLE);
                    if(count<=4){
                        int bike_margin_top = 760 * height / 2667;
                        int bike_height = 422 * height / 2667;
                        LinearLayout.LayoutParams lp_ll = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, bike_height);
                        lp_ll.setMargins(0, bike_margin_top, 0, 0);
                        bike_image.setLayoutParams(lp_ll);
                        bike_image.setVisibility(View.VISIBLE);
                        int mobile_height = 750 * height / 2667;
                        lp_ll = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, mobile_height);
                        final float scale = getResources().getDisplayMetrics().density;
                        int dpHeightInPx  = (int) (20 * scale);
                        lp_ll.setMargins(0, dpHeightInPx, 0, 0);
                        mobile_image.setLayoutParams(lp_ll);
                        mobile_image.setVisibility(View.VISIBLE);
                    }
                }
            };
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 0, 10);
        }
        else{
            itemView = inflater.inflate(R.layout.default_get_started, container, false);
            ImageView get_started_image = (ImageView) itemView.findViewById(R.id.get_started_image);
            TextView screen_text_tv = (TextView) itemView.findViewById(R.id.screen_text);
            screen_text_tv.setText(screen_text[position]);
            Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "font/helveticaUltraComp.otf");
            screen_text_tv.setTypeface(tf);
            get_started_image.setImageResource(background[position]);
            if(position==0){
                MyRunnable seperate = new MyRunnable();
                thread = new Thread(seperate);
                thread.start();
            }
        }
        return itemView;
    }
    @Override
    public void onPause() {
        super.onPause();
        if(swipeTimer!=null)
            swipeTimer.cancel();
        if(t!=null)
            t.cancel();
    }

    public class MyRunnable implements Runnable {
        public MyRunnable() {
        }
        public void run() {
            count = 0;
            signal_image = (ImageView) itemView.findViewById(R.id.signal);
            t = new Timer();
            t.scheduleAtFixedRate(new TimerTask() {
                @Override
                public void run() {
                    int width = displayMetrics.widthPixels;
                    int height = displayMetrics.heightPixels;
                    int signal_margin_left = 924 * width / 1667;
                    int signal_margin_top = 1000 * height / 2667;
                    if(getActivity() == null)
                        return;
                    if (count == 3) {
                        int dimen = width / 13;
                        rl_lp = new RelativeLayout.LayoutParams(dimen, dimen);
                        rl_lp.setMargins(signal_margin_left, signal_margin_top - dimen, 0, 0);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                signal_image.setImageResource(R.drawable.wifisignal3);
                                signal_image.setLayoutParams(rl_lp);
                                signal_image.setVisibility(View.VISIBLE);
                            }
                        });
                        count = -1;
                    } else if (count == 2) {
                        int dimen = width/20;
                        rl_lp = new RelativeLayout.LayoutParams(dimen, dimen);
                        rl_lp.setMargins(signal_margin_left, signal_margin_top - dimen, 0, 0);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                signal_image.setImageResource(R.drawable.wifisignal2);
                                signal_image.setLayoutParams(rl_lp);
                                signal_image.setVisibility(View.VISIBLE);
                            }
                        });
                    } else if(count==1) {
                        int dimen = width/45;
                        rl_lp = new RelativeLayout.LayoutParams(dimen, dimen);
                        rl_lp.setMargins(signal_margin_left, signal_margin_top - dimen, 0, 0);
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                signal_image.setImageResource(R.drawable.wifisignal1);
                                signal_image.setLayoutParams(rl_lp);
                                signal_image.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                    else{
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                signal_image.setVisibility(View.GONE);
                            }
                        });
                    }
                    count++;
                }

            }, 0, 1000);

        }
    }
}
