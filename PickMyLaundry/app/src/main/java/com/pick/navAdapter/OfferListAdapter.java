package com.pick.navAdapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.pick.mylaundry.MainActivity;
import com.pick.mylaundry.R;
import com.pick.navmodel.OfferListModel;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.volley.EmptyUtility;
import com.pick.volley.ImageLoader;
import com.pick.volley.UploadTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mukesh on 10/21/2015.
 */
public class OfferListAdapter extends BaseAdapter {

    private Context context;
    private List<OfferListModel> offerList;
    private ImageLoader imageLoader;
    private AlertDialog.Builder alertDialog;
    private String alertMessage;

    public OfferListAdapter(Context context, List<OfferListModel> offerList) {
        this.context = context;
        this.offerList = offerList;
        imageLoader=new ImageLoader(context);
    }

    @Override
    public int getCount() {
        return offerList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.offer_row, null);
        }
        String fontPath_bold = "font/Helvetica-Bold.otf";
        Typeface tf_bold = Typeface.createFromAsset(context.getAssets(), fontPath_bold);
        String fontPath = "font/helvetica-normal.ttf";
        Typeface tf = Typeface.createFromAsset(context.getAssets(), fontPath);
        TextView shortDesc = (TextView) convertView.findViewById(R.id.shortDesc);
        shortDesc.setTypeface(tf);
        TextView detailedDesc = (TextView) convertView.findViewById(R.id.detailedDesc);
        detailedDesc.setTypeface(tf);
        final TextView expiry_date = (TextView) convertView.findViewById(R.id.expiry_date);
        expiry_date.setTypeface(tf);
        ImageView thumbNail = (ImageView) convertView.findViewById(R.id.thumbnail);

        imageLoader.DisplayImage(offerList.get(position).getImageUrl().toString(), thumbNail, "offer", convertView);
        String detail_desc = offerList.get(position).getCouponDescription();
        if(EmptyUtility.isNotEmpty(detail_desc)){
            detailedDesc.setText(detail_desc);
        }

        shortDesc.setText(offerList.get(position).getTitle().toString());
        expiry_date.setText(offerList.get(position).getExpiryDate().toString());
        Button redeem = (Button) convertView.findViewById(R.id.redeem);
        final String validity_desc = offerList.get(position).getExpiryDate();
        if(validity_desc.contains("Expired"))
            expiry_date.setTextColor(Color.parseColor("#ff0000"));
        else
            expiry_date.setTextColor(Color.parseColor("#878787"));
        redeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String couponCode = offerList.get(position).getCouponCode();
                if (validity_desc.contains("Expired")) {
                    alertMessage = "Sorry, Coupon has expired";
                    showAlertDialog();
                } else {
                    alertMessage = offerList.get(position).getSuccessMessage();
                    makeCouponRequest(couponCode);
                }
                Map<String, String> articleParams = new HashMap<String, String>();
                articleParams.put("mobile", AppPrefrence.getMobile(context));
                //up to 10 params can be logged with each event
                FlurryAgent.logEvent("offer_redeem", articleParams);
            }
        });
        return convertView;
    }

    public void showAlertDialog(){
        alertDialog = new AlertDialog.Builder(context);
        alertDialog.setMessage(alertMessage);
        if(alertMessage.contains("Successfully")){
            alertDialog.setPositiveButton("Schedule Now", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(context,MainActivity.class);
                    intent.putExtra("SESSION_ID", "0");
                    intent.putExtra("id",1);
                    intent.putExtra("Context", "Review");
                    context.startActivity(intent);
                    dialog.cancel();
                }
            });
            alertDialog.setNegativeButton("Ok", new DialogInterface.OnClickListener(){

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        else{
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        }
        alertDialog.show();
    }

    private void makeCouponRequest(String code){
        List<NameValuePair> params=new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(context)));
        params.add(new BasicNameValuePair("code", code));
        sendCouponCode(params, code);
    }

    private void sendCouponCode(List<NameValuePair> params, final String code) {
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        final SharedPreferences pref = context.getSharedPreferences("redeem", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = pref.edit();
                        editor.putString("appliedCoupon", code);
                        editor.commit();
                    } else if(json.getString("success").equals("false")){
                        alertMessage = json.getString("msg");
                    }
                    showAlertDialog();

                } catch (JSONException e){
                    Toast.makeText(context, "Please check Your Internet Connection", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(context,"Please check Your Internet Connection",Toast.LENGTH_LONG).show();

            }
        },context, Constant.CouponCode,params, true);

        task.execute();
    }

}
