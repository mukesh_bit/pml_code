package com.pick.mylaundry;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pick.navAdapter.PriceListAdapter;
import com.pick.navmodel.PriceModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by 121 on 12/9/2015.
 */
public class PriceFragment extends Fragment {
    private View rootview;
    private String price_data;
    private String category;
    private ListView priceListView;
    private ArrayList<PriceModel> priceList;
    private Map<String, Map<String, Integer >> cartItems;
    private Map<String, Integer> item;
    private TextView cartCount, cartTotal;
    private RelativeLayout info_rl;

    public PriceFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_one, container, false);
        Bundle bundle = getArguments();
        price_data = bundle.getString("pricing_data");
        category = bundle.getString("category");
        priceListView = (ListView) rootview.findViewById(R.id.priceList);
        cartItems = PriceEstimator.getCart();
        setPriceData();

        //close info bar on click
        info_rl = (RelativeLayout) rootview.findViewById(R.id.info_rl);
        info_rl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("root", "clicked");
                info_rl.setVisibility(View.GONE);
            }
        });
        return rootview;
    }

    private void setPriceData(){
        try {
            priceList = new ArrayList<PriceModel>();
            JSONObject priceListData = new JSONObject(price_data);
            for(int i=0;i<priceListData.length();i++){
                JSONObject data = priceListData.getJSONObject(i+"");
                final PriceModel bean = new PriceModel();
                bean.setImageUrl(data.getString("imageUrl"));
                bean.setLabel(data.getString("label"));
                bean.setPrice(data.getString("price"));
                bean.setCategory(category);
                if(Integer.parseInt(data.getString("price")) < 0)
                    bean.setItemType("header");
                else
                    bean.setItemType("item");
                if(cartItems.containsKey(category)){
                    item = cartItems.get(category);
                    if(item.containsKey(data.getString("label"))){
                        bean.setCount(item.get(data.getString("label")));
                    }
                    else{
                        bean.setCount(0);
                    }
                }
                else {
                    bean.setCount(0);
                }
                priceList.add(bean);
            }
            ((ListView) priceListView).setAdapter(new PriceListAdapter(getActivity(), priceList, priceListView, rootview));
            ((ListView) priceListView).deferNotifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}


