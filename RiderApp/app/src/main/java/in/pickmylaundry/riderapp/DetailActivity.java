package in.pickmylaundry.riderapp;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.rey.material.widget.RadioButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.pickmylaundry.riderapp.ui.SwipeLayout;
import in.pickmylaundry.riderapp.ui.SwipeLayoutCustomItems;
import in.pickmylaundry.riderapp.utils.AppPrefrence;
import in.pickmylaundry.riderapp.utils.Constant;
import in.pickmylaundry.riderapp.utils.PickHelper;
import in.pickmylaundry.riderapp.utils.UploadTask;
import okhttp3.internal.Util;

/**
 * Created by 121 on 5/13/2016.
 */
public class DetailActivity extends Activity{
    private PickHelper dbHelper;
    private SQLiteDatabase db;
    private String salesorderid, type,mobile, status;
    private TextView so,name,address,slot,wash_type,order_type,instruction,type_tv,bill,payment_status;
    private ImageView back;
    private LinearLayout  response_container;
    private Bundle bundle;
    private UploadTask task;
    private Dialog dialog, dialog_cloth;
    private RadioButton[] btns;
    private ViewGroup radioGroup;
    private LayoutInflater layoutInflater;
    private String reason = "", next_customer = "", hub = "", processing_status = "", laundry_name="";
    private String sostatus;
    private Button call, sms_button, tag_button, request_so, drop_btn;
    private android.widget.RadioButton payment_link;
    private RadioGroup sms_group;
    private RelativeLayout wash_container,not_done,done,payment_container, instruction_container;
    private SwipeLayout swipeLayout;
    private TextView pl,dc,laundry;
    private Map<String, Map<String, String>> order_list;
    private EditText wf_kg,wi_kg,pl_kg,dc_kg;
    private TextView blanket_increment,blanket_decrement,shoes_increment,shoes_decrement,curtain_increment,curtain_decrement;
    private TextView laundry_adj, laundry_dd, dc_adj, dc_dd, others_adj, others_dd, blanket_count, shoes_count, curtain_count;
    private TextView same_day_dd, same_day_adj,drop_info;
    private LinearLayout laundry_ll, dc_ll, pl_ll, diff_day_ll, others_ll, drop_ll;
    private Spinner laundry_spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_detail);

        //get intent data
        bundle = getIntent().getExtras();
        salesorderid = bundle.getString("salesorderid");
        type = bundle.getString("type");
        status = bundle.getString("status");
        sostatus = bundle.getString("sostatus");

        so = (TextView) findViewById(R.id.so);
        name = (TextView) findViewById(R.id.name);
        address = (TextView) findViewById(R.id.address);
        slot = (TextView) findViewById(R.id.slot);
        wash_type = (TextView) findViewById(R.id.wash_type);
        order_type = (TextView) findViewById(R.id.order_type);
        instruction = (TextView) findViewById(R.id.instruction);
        type_tv = (TextView) findViewById(R.id.type);
        payment_status = (TextView) findViewById(R.id.payment_status);
        bill = (TextView) findViewById(R.id.bill);
        call = (Button) findViewById(R.id.call);
        back = (ImageView) findViewById(R.id.back);
        not_done = (RelativeLayout) findViewById(R.id.not_done_container);
        done = (RelativeLayout) findViewById(R.id.done_container);
        payment_link = (android.widget.RadioButton) findViewById(R.id.payment_link);
        sms_group = (RadioGroup) findViewById(R.id.radio_group);
        sms_button = (Button) findViewById(R.id.send_sms);
        laundry = (TextView) findViewById(R.id.laundry);
        pl = (TextView) findViewById(R.id.pl);
        dc = (TextView) findViewById(R.id.dc);
        tag_button = (Button) findViewById(R.id.tag);
        layoutInflater = (LayoutInflater) getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        request_so = (Button) findViewById(R.id.request_so);

        //layout
        wash_container = (RelativeLayout) findViewById(R.id.wash_container);
        payment_container = (RelativeLayout) findViewById(R.id.payment_container);
        response_container = (LinearLayout) findViewById(R.id.response_container);
        instruction_container = (RelativeLayout) findViewById(R.id.instruction_container);

        //spinner
        drop_ll = (LinearLayout) findViewById(R.id.drop_ll);
        drop_btn = (Button) findViewById(R.id.drop);
        laundry_spinner = (Spinner) findViewById(R.id.laundry_spinner);
        drop_info = (TextView) findViewById(R.id.drop_info);

        //configure swipe button
        swipeLayout = (SwipeLayout) findViewById(R.id.swipe_layout);
        if(!status.equals("completed"))
            configure_swipe_button();

        //set data
        setData();
        setBasicData();
        set_spinner();


        if(type.equals("Pickup")){
            payment_container.setVisibility(View.GONE);
            request_so.setVisibility(View.GONE);
        }
        else{
            wash_container.setVisibility(View.GONE);
        }
        if(status.equals("completed")) {
            response_container.setVisibility(View.GONE);
            swipeLayout.setVisibility(View.GONE);
        }


        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makecall(mobile);
            }
        });
        if(type.equals("Pickup")){
            payment_link.setVisibility(View.GONE);
            payment_container.setVisibility(View.GONE);
        }
        else{
            wash_container.setVisibility(View.VISIBLE);
        }

        //response handler
        if(sostatus.equals("Not Picked Up") || sostatus.equals("Not Delivered"))
            not_done.setBackgroundColor(Color.parseColor("#cccccc"));
        else {
            not_done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    show_next_customer_dialog("", "not_done");
                }
            });
        }
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //done();
                if(type.equals("Pickup")){
                    show_cloth_detail_dialog();
                }
                else{
                    show_next_customer_dialog("", "done");
                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //send sms
        sms_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int buttonId = sms_group.getCheckedRadioButtonId();
                android.widget.RadioButton selectedButton = (android.widget.RadioButton) findViewById(buttonId);
                if (selectedButton != null)
                    send_sms(selectedButton.getTag().toString());
            }
        });
        request_so.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Map<String, Object> params = new HashMap<String, Object>();
                request_new_so(params);
            }
        });

        //drop at laundry click listener
        drop_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String laundry_selected = laundry_spinner.getSelectedItem().toString();
                if(laundry_selected == null)
                    Toast.makeText(DetailActivity.this, "Please select laundry", Toast.LENGTH_LONG).show();
                else if(laundry_selected.equals("Select Laundromat"))
                    Toast.makeText(DetailActivity.this, "Please select laundry", Toast.LENGTH_LONG).show();
                else{
                    requestDrop(laundry_selected);
                }
            }
        });


    }
    private void configure_swipe_button(){
        SwipeLayoutCustomItems swipeLayoutCustomItems = new SwipeLayoutCustomItems(){
            @Override
            public void onSwipeConfirm(int type) {
                if(type == Constant.POSITIVE_SWIPE) {
                    requestNextCustomer();
                }
                else
                    show_dialog_box("");

            }
        };
        String[] pressedText = {">> Swipe Right to Confirm >>>>", "<<<< Swipe Left for not done<<"};
        double[] distanceFraction = {0.8, 0.2};
        swipeLayoutCustomItems.setButtonPressText(pressedText);
        swipeLayoutCustomItems.setActionConfirmDistanceFraction(distanceFraction);
        swipeLayoutCustomItems.setButtonText("Going to this Customer -->");
        swipeLayoutCustomItems.setActionConfirmText("On the way to this customer");
        swipeLayoutCustomItems.setActionCancelText("Not going to this customer");

        swipeLayout.setSwipeLayoutCustomItems(swipeLayoutCustomItems);
    }
    private void setData(){
        dbHelper = new PickHelper(getApplicationContext());
        db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+PickHelper.TABLE_ADDRESS_DETAIL+" WHERE "+PickHelper.KEY_SALESORDERID_DETAIL+" = "+salesorderid;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            do {
                String wash_type = cursor.getString(1);
                String[] wash_type_arr = wash_type.split(",");
                for (int i=0; i<wash_type_arr.length;i++)
                    wash_type_arr[i] = wash_type_arr[i].trim();
                
                String[] laundry_arr = new String[2];int i=0;
                String laundry_text = "";
                if(Arrays.asList(wash_type_arr).contains("Wash & Fold")) {
                    laundry_arr[i++] = "WF";
                    laundry_text = "WF";
                }
                if(Arrays.asList(wash_type_arr).contains("Wash & Iron")) {
                    if(i==1)
                        laundry_text += "\n";
                    laundry_arr[i++] = "WI";
                    laundry_text += "WI";
                }
                if(i==2){
                    View laundry_divider = (View) findViewById(R.id.laundry_divider);
                    laundry_divider.setVisibility(View.VISIBLE);
                }
                laundry.setText(laundry_text);
                if(i>0)
                    laundry.setVisibility(View.VISIBLE);
                if(Arrays.asList(wash_type_arr).contains("Premium Laundry")) {
                    pl.setVisibility(View.VISIBLE);
                    Log.i("pl", "visible");
                }
                if(Arrays.asList(wash_type_arr).contains("Dry Clean")) {
                    dc.setVisibility(View.VISIBLE);
                    Log.i("dc", "visible");
                }
                Log.i("wash_type", wash_type);

                if(cursor.getString(2).trim().length() > 2)
                    instruction.setText(cursor.getString(2));
                else
                    instruction_container.setVisibility(View.GONE);
                bill.setText(cursor.getString(4));
                payment_status.setText(cursor.getString(5));
                type_tv.setText(cursor.getString(7));
                if(cursor.getString(9).equals("false")) {
                    swipeLayout.setVisibility(View.VISIBLE);
                    response_container.setVisibility(View.GONE);
                }
                else{
                    swipeLayout.setVisibility(View.GONE);
                    response_container.setVisibility(View.VISIBLE);
                }
                if(cursor.getString(3).equals("New") || cursor.getString(3).equals("Rework")){
                    tag_button.setVisibility(View.VISIBLE);
                    tag_button.setText(cursor.getString(3));
                }
                else if(cursor.getInt(10) > 4){
                    tag_button.setVisibility(View.VISIBLE);
                    tag_button.setText("Elite");
                }

            }while (cursor.moveToNext());

        }
        db.close();
    }
    private void setBasicData(){
        db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+PickHelper.TABLE_ADDRESS+" WHERE "+PickHelper.KEY_SALESORDERID+" = "+salesorderid;
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.moveToFirst()){
            do {
                so.setText(cursor.getString(1));
                name.setText(cursor.getString(2));
                slot.setText(cursor.getString(3));
                address.setText(cursor.getString(4));
                mobile = cursor.getString(5);
                hub = cursor.getString(13);
                processing_status = cursor.getString(14);
                laundry_name = cursor.getString(15);

            }while (cursor.moveToNext());
        }
        db.close();
    }

    private void send_sms(String content){
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("rider_id", AppPrefrence.getMobile(DetailActivity.this));
            params.put("salesorderid", salesorderid);
            params.put("content", content);
            request_sms(params);
    }
    private void request_sms(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){
                        Toast.makeText(DetailActivity.this, "Sms Sent Succesfully", Toast.LENGTH_LONG).show();
                    }
                    else{
                        Toast.makeText(DetailActivity.this, "There was some error, please try again", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(DetailActivity.this, "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(DetailActivity.this, "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, DetailActivity.this, Constant.send_sms, params);
        task.execute();
    }
    private void show_dialog_box(final String next_customer){
        dialog = new Dialog(DetailActivity.this, R.style.FullHeightDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.not_done);
        radioGroup = (ViewGroup) dialog.findViewById(R.id.radio_grp);
        setReasonData();
        dialog.show();
        reason = "";

        TextView close = (TextView) dialog.findViewById(R.id.closeDialog);
        final TextView not_done = (TextView) dialog.findViewById(R.id.not_done);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        not_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < btns.length; i++) {
                    if (btns[i].isChecked())
                        reason = btns[i].getText().toString();
                }
                if (reason.equals(""))
                    Toast.makeText(getApplicationContext(), "Please select a reason for cancellation", Toast.LENGTH_LONG).show();
                else if (reason.equals("Customer Not Available")) {
                    Intent intent = new Intent(DetailActivity.this, TimeslotPopup.class);
                    intent.putExtra("type", type);
                    startActivityForResult(intent, 1);
                } else if (reason.equals("Other")) {
                    show_comment_box();
                } else {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put("next_customer", next_customer);
                    not_done(reason, params);
                }
            }
        });
    }
    private void not_done(String reason, Map<String, Object> params){
        params.put("rider_id", AppPrefrence.getMobile(DetailActivity.this));
        params.put("salesorderid", salesorderid);
        params.put("type", type);
        params.put("reason", reason);
        request_not_done(params, Constant.not_done);
        dialog.dismiss();
    }
    private void request_new_so(Map<String, Object> params){
        params.put("rider_id", AppPrefrence.getMobile(DetailActivity.this));
        params.put("salesorderid", salesorderid);
        params.put("type", type);
        request_not_done(params, Constant.new_so);
    }
    private void done(String next_customer, String cloth_detail){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rider_id", AppPrefrence.getMobile(DetailActivity.this));
        params.put("salesorderid", salesorderid);
        params.put("type", type);
        params.put("next_customer", next_customer);
        params.put("cloth_data", cloth_detail);
        request_not_done(params, Constant.done);
    }
    private void request_not_done(Map<String, Object> params, final String url){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true") && url.equals(Constant.new_so)){
                        Toast.makeText(DetailActivity.this, "Request Submitted Succesfully", Toast.LENGTH_LONG).show();
                    }
                    else if(response.getString("success").equals("true")){
                        Toast.makeText(DetailActivity.this, "Submitted Succesfully", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(DetailActivity.this, MainActivity.class);
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(DetailActivity.this, response.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(DetailActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(DetailActivity.this, "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, DetailActivity.this, url, params);
        task.execute();
    }
    private void setReasonData(){
        CompoundButton.OnCheckedChangeListener listener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    for (int i=0; i < btns.length;i++){
                        btns[i].setChecked(buttonView == btns[i]);
                    }
                }
            }
        };

        try {
            SharedPreferences prefs = getSharedPreferences("not_done_reason", Context.MODE_PRIVATE);
            JSONObject reason_ob_list = new JSONObject(prefs.getString("reason_list", ""));
            JSONArray reason_arr = reason_ob_list.getJSONArray(type);
            btns = new RadioButton[reason_arr.length()];

            for (int i=0; i< reason_arr.length(); i++){
                View radio_box = View.inflate(this, R.layout.radio_btn, null);
                radioGroup.addView(radio_box);
                btns[i] = (RadioButton) radio_box.findViewById(R.id.radio_btn);
                btns[i].setText(reason_arr.getString(i));
                btns[i].setOnCheckedChangeListener(listener);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode != RESULT_CANCELED) {
            String date  = data.getStringExtra("date");
            String slot = data.getStringExtra("slot");
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("date", date);
            params.put("slot", slot);
            params.put("next_customer", next_customer);
            not_done(reason, params);
        }
    }
    private void show_comment_box(){
        dialog = new Dialog(DetailActivity.this, R.style.CustomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.other_reason);
        final EditText reason_other_et = (EditText) dialog.findViewById(R.id.reason_other);
        Button reason_other_submit = (Button) dialog.findViewById(R.id.submit);
        Button reason_other_cancel = (Button) dialog.findViewById(R.id.cancel);
        reason_other_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (reason_other_et.getText().toString().equals("")) {
                    Toast.makeText(DetailActivity.this, "Please enter reason", Toast.LENGTH_LONG).show();
                } else {
                    Map<String, Object> params = new HashMap<String, Object>();
                    params.put("reason_other", reason_other_et.getText().toString());
                    params.put("next_customer", next_customer);
                    not_done(reason, params);
                }
            }
        });
        reason_other_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        dialog.show();
    }
    private void requestNextCustomer(){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rider_id", AppPrefrence.getMobile(DetailActivity.this));
        params.put("salesorderid", salesorderid);
        params.put("type", type);
        setNextCustomer(params);
    }
    private void setNextCustomer(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){
                        response_container.setVisibility(View.VISIBLE);
                        update_active_customer();
                    }
                    else{
                        Toast.makeText(DetailActivity.this, response.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(DetailActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(DetailActivity.this, "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, DetailActivity.this, Constant.next_customer, params);
        task.execute();
    }

    private void update_active_customer(){
        db = dbHelper.getWritableDatabase();
        db.execSQL("UPDATE "+ PickHelper.TABLE_ADDRESS + " SET "+PickHelper.KEY_NEXT_CUSTOMER+" = 'false'");
        db.execSQL("UPDATE "+ PickHelper.TABLE_ADDRESS_DETAIL + " SET "+PickHelper.KEY_NEXT_CUSTOMER+" = 'false'");
        db.execSQL("UPDATE "+ PickHelper.TABLE_ADDRESS + " SET "+PickHelper.KEY_NEXT_CUSTOMER+" = 'true' where "+ PickHelper.KEY_SALESORDERID + " = "+ salesorderid);
        db.execSQL("UPDATE " + PickHelper.TABLE_ADDRESS_DETAIL + " SET " + PickHelper.KEY_NEXT_CUSTOMER + " = 'true' where " + PickHelper.KEY_SALESORDERID_DETAIL + " = " + salesorderid);
    }
    private void show_next_customer_dialog(final String cloth_detail, final String completion_type) {
        dialog = new Dialog(DetailActivity.this, R.style.FullHeightDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.next_customer);
        final ViewGroup radioGroup = (ViewGroup) dialog.findViewById(R.id.radio_group);
        db = dbHelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + PickHelper.TABLE_ADDRESS + " WHERE " + PickHelper.KEY_STATUS + " = '" + status + "' order by "+PickHelper.KEY_TYPE;
        Cursor cursor = db.rawQuery(selectQuery, null);
        String type = "";
        FrameLayout.LayoutParams params_btn = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
        params_btn.topMargin = 15;
        if (cursor.moveToFirst()) {
            do {
                String item = cursor.getString(2) + " - " + cursor.getString(3);
                if(cursor.getString(6).equals(salesorderid))
                    continue;
                if(!cursor.getString(7).equals(type)){
                    TextView tv = new TextView(this);
                    tv.setText(cursor.getString(7));
                    RadioGroup.LayoutParams params = new RadioGroup.LayoutParams(RadioGroup.LayoutParams.MATCH_PARENT, RadioGroup.LayoutParams.WRAP_CONTENT);
                    params.setMargins(15,15,15,15);
                    tv.setLayoutParams(params);
                    type = cursor.getString(7);
                    radioGroup.addView(tv);
                }
                android.widget.RadioButton radioButton = new android.widget.RadioButton(this);
                radioButton.setText(item);
                radioButton.setTag(cursor.getString(6)+"-"+cursor.getString(7));
                radioButton.setLayoutParams(params_btn);
                radioGroup.addView(radioButton);
            } while (cursor.moveToNext());
        }
        android.widget.RadioButton radioButton = new android.widget.RadioButton(this);
        radioButton.setText("None");
        radioButton.setTag("None");
        radioButton.setLayoutParams(params_btn);
        radioGroup.addView(radioButton);
        dialog.show();

        //add done listener
        Button done_btn = (Button) dialog.findViewById(R.id.done_next);
        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RadioGroup radioGroup = (RadioGroup) dialog.findViewById(R.id.radio_group);
                int button_selected = radioGroup.getCheckedRadioButtonId();
                android.widget.RadioButton tag_btn = (android.widget.RadioButton) dialog.findViewById(button_selected);
                if(tag_btn != null)
                    next_customer = tag_btn.getTag().toString();
                if (tag_btn != null && completion_type.equals("done")) {
                    done(tag_btn.getTag().toString(), cloth_detail);
                } else if (tag_btn != null && completion_type.equals("not_done")) {
                    show_dialog_box(tag_btn.getTag().toString());
                } else {
                    Toast.makeText(DetailActivity.this, "Please select next customer or none", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    private void show_cloth_detail_dialog(){
        dialog_cloth = new Dialog(DetailActivity.this, R.style.FullHeightDialog);
        dialog_cloth.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog_cloth.setContentView(R.layout.cloth_details);
        dialog_cloth.show();

        wf_kg = (EditText) dialog_cloth.findViewById(R.id.wf_kg);
        wi_kg = (EditText) dialog_cloth.findViewById(R.id.wi_kg);
        pl_kg = (EditText) dialog_cloth.findViewById(R.id.pl_kg);
        dc_kg = (EditText) dialog_cloth.findViewById(R.id.dc_pcs);

        blanket_increment = (TextView) dialog_cloth.findViewById(R.id.blanket_increment);
        blanket_decrement = (TextView) dialog_cloth.findViewById(R.id.blanket_decrement);
        blanket_count = (TextView) dialog_cloth.findViewById(R.id.blanket_count);
        shoes_increment = (TextView) dialog_cloth.findViewById(R.id.shoes_increment);
        shoes_decrement = (TextView) dialog_cloth.findViewById(R.id.shoes_decrement);
        shoes_count = (TextView) dialog_cloth.findViewById(R.id.shoes_count);
        curtain_increment = (TextView) dialog_cloth.findViewById(R.id.curtain_increment);
        curtain_decrement = (TextView) dialog_cloth.findViewById(R.id.curtain_decrement);
        curtain_count = (TextView) dialog_cloth.findViewById(R.id.curtain_count);

        same_day_dd = (TextView) dialog_cloth.findViewById(R.id.same_day_dd);
        diff_day_ll = (LinearLayout) dialog_cloth.findViewById(R.id.diff_day_ll);
        same_day_adj = (TextView) dialog_cloth.findViewById(R.id.same_day_adj);
        laundry_ll = (LinearLayout) dialog_cloth.findViewById(R.id.laundry);
        laundry_dd = (TextView) dialog_cloth.findViewById(R.id.laundry_dd);
        laundry_adj = (TextView) dialog_cloth.findViewById(R.id.laundry_adj);
        dc_ll = (LinearLayout) dialog_cloth.findViewById(R.id.dc);
        dc_dd = (TextView) dialog_cloth.findViewById(R.id.dc_dd);
        dc_adj = (TextView) dialog_cloth.findViewById(R.id.dc_adj);
        others_ll = (LinearLayout) dialog_cloth.findViewById(R.id.others);
        others_dd = (TextView) dialog_cloth.findViewById(R.id.others_dd);
        others_adj = (TextView) dialog_cloth.findViewById(R.id.others_adj);
        Button done_btn = (Button) dialog_cloth.findViewById(R.id.done_btn);

        wf_kg.addTextChangedListener(textChangeListener);
        wi_kg.addTextChangedListener(textChangeListener);
        pl_kg.addTextChangedListener(textChangeListener);
        dc_kg.addTextChangedListener(textChangeListener);

        curtain_increment.setOnClickListener(clickListener);
        shoes_increment.setOnClickListener(clickListener);
        blanket_increment.setOnClickListener(clickListener);

        curtain_decrement.setOnClickListener(clickListener_decrement);
        shoes_decrement.setOnClickListener(clickListener_decrement);
        blanket_decrement.setOnClickListener(clickListener_decrement);
        done_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject cloth_data = new JSONObject();
                int complex_product = 0, dc_product =0;
                try {
                    if(wf_kg.getText().toString().length() > 0)
                        cloth_data.put("wf", wf_kg.getText().toString());
                    if(wi_kg.getText().toString().length() > 0)
                        cloth_data.put("wi", wi_kg.getText().toString());
                    if(pl_kg.getText().toString().length() > 0)
                        cloth_data.put("pl", pl_kg.getText().toString());
                    if(dc_kg.getText().toString().length() > 0) {
                        cloth_data.put("dc", dc_kg.getText().toString());
                        dc_product += Integer.parseInt(dc_kg.getText().toString());
                    }
                    else{
                        dc_product = 0;
                    }
                    if(!blanket_count.getText().toString().equals("0")) {
                        complex_product += Integer.parseInt(blanket_count.getText().toString());
                        cloth_data.put("blanket", blanket_count.getText().toString());
                    }
                    if(!shoes_count.getText().toString().equals("0")) {
                        cloth_data.put("shoes", shoes_count.getText().toString());
                        complex_product += Integer.parseInt(shoes_count.getText().toString());
                    }
                    if(!curtain_count.getText().toString().equals("0")) {
                        cloth_data.put("curtain", curtain_count.getText().toString());
                        complex_product += Integer.parseInt(curtain_count.getText().toString());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                if(cloth_data.length() == 0)
                    Toast.makeText(DetailActivity.this, "Add clothes info", Toast.LENGTH_LONG).show();
                else if(complex_product > dc_product){
                    Toast.makeText(DetailActivity.this, "Error in counting of dc clothes", Toast.LENGTH_LONG).show();
                }
                else{
                    show_next_customer_dialog(cloth_data.toString(), "done");
                    dialog_cloth.hide();
                }
            }
        });
    }
    private View.OnClickListener clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String tag = v.getTag().toString();
            int id = getResources().getIdentifier(tag + "_count", "id", getPackageName());
            TextView count = (TextView) dialog_cloth.findViewById(id);
            int count_val = Integer.parseInt(count.getText().toString());
            count_val++;
            count.setText(count_val+"");
            update_delivery_date();
        }
    };
    private View.OnClickListener clickListener_decrement = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String tag = v.getTag().toString();
            int id = getResources().getIdentifier(tag + "_count", "id", getPackageName());
            TextView count = (TextView) dialog_cloth.findViewById(id);
            int count_val = Integer.parseInt(count.getText().toString());
            if(count_val > 0)
                count_val--;
            count.setText(count_val+"");
            update_delivery_date();
        }
    };
    private TextWatcher textChangeListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            update_delivery_date();
        }
    };
    private void update_delivery_date(){
        String date_after_2_days = add_days_to_date(2);
        String date_after_3_days = add_days_to_date(3);
        String date_after_7_days = add_days_to_date(7);

        int complex_products = 0, normal_dc = 0, adj;

        int laundry_bill =0, pl_bill=0, dc_bill=0, normal_dc_bill = 0;

        if(wf_kg.getText().toString().trim().length() > 0){
            laundry_bill += Double.parseDouble(wf_kg.getText().toString())  * 67.85;
        }
        if(wi_kg.getText().toString().trim().length() > 0){
            laundry_bill += Double.parseDouble(wi_kg.getText().toString())  * 102.35;
        }
        if(pl_kg.getText().toString().trim().length() > 0){
            pl_bill += Double.parseDouble(pl_kg.getText().toString())  * 171.35;
        }

        if(blanket_count.getText().toString().trim().length() > 0)
            complex_products += Integer.parseInt(blanket_count.getText().toString());
        if(shoes_count.getText().toString().trim().length() > 0)
            complex_products += Integer.parseInt(shoes_count.getText().toString());
        if(curtain_count.getText().toString().trim().length() > 0)
            complex_products += Integer.parseInt(curtain_count.getText().toString());

        if(complex_products > 0){
            dc_bill = 250;
        }
        else if(dc_kg.getText().toString().compareTo("0") > 0 && dc_kg.getText().toString().trim().length() > 0){
            dc_bill = Integer.parseInt(dc_kg.getText().toString()) * 92;
        }

        if(dc_kg.getText().toString().trim().length() > 0) {
            normal_dc = Integer.parseInt(dc_kg.getText().toString()) - complex_products;
            normal_dc_bill = (normal_dc > 0) ? normal_dc * 92 : 0;
        }

        int total_category = 0;
        if(laundry_bill > 0){
            laundry_ll.setVisibility(View.VISIBLE);
            laundry_dd.setText(date_after_2_days);
            adj = (250-laundry_bill > 0)?(250-laundry_bill):0;
            laundry_adj.setText("Adjust. "+adj);
            same_day_dd.setText(date_after_2_days);
            total_category++;
        }
        else{
            laundry_ll.setVisibility(View.GONE);
        }

        if(pl_bill > 0 || normal_dc > 0){
            dc_ll.setVisibility(View.VISIBLE);
            dc_dd.setText(date_after_3_days);
            adj = (250-pl_bill-normal_dc_bill > 0)?(250-pl_bill-normal_dc_bill):0;
            dc_adj.setText("Adjust. "+adj);
            total_category++;
            same_day_dd.setText(date_after_3_days);
        }
        else{
            dc_ll.setVisibility(View.GONE);
        }

        if(complex_products > 0){
            others_ll.setVisibility(View.VISIBLE);
            others_dd.setText(date_after_7_days);
            total_category++;
            same_day_dd.setText(date_after_7_days);
        }
        else{
            others_ll.setVisibility(View.GONE);
        }
        if(total_category > 1)
            diff_day_ll.setVisibility(View.VISIBLE);
        else
            diff_day_ll.setVisibility(View.GONE);

        adj = 250 - laundry_bill - pl_bill - dc_bill;
        adj = (adj<0)?0:adj;
        same_day_adj.setText("Adjust. "+adj);
    }
    private String add_days_to_date(int no_of_days){
        Date dt = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, no_of_days);  // number of days to add
        return sdf.format(c.getTime());
    }
    private void makecall(String mobile) {
        Intent intent = new Intent("android.intent.action.CALL");
        intent.setData(Uri.parse("tel:"+mobile));
        startActivity(intent);

        /*Map<String, Object> param = new HashMap<String, Object>();
        param.put("from", AppPrefrence.getPhone(DetailActivity.this));
        param.put("to", mobile);
        request_call(param);*/
    }
    private void request_call(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){

                    }
                    else{
                        Toast.makeText(DetailActivity.this, response.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(DetailActivity.this, "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(DetailActivity.this, "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, DetailActivity.this, Constant.CALL, params);
        task.execute();
    }
    private void set_spinner(){

        if((processing_status.equals("Dropped at Laundromat") || processing_status.equals("Accepted by Laundromat")) && status.equals("completed") && hub.equals("Bengaluru")){
            drop_info.setVisibility(View.VISIBLE);
            drop_info.setText("Dropped at "+laundry_name);
            drop_ll.setVisibility(View.GONE);
            return;
        }
        else{
            drop_info.setVisibility(View.GONE);
        }

        if(hub.equals("Bengaluru") && status.equals("completed")){
            drop_ll.setVisibility(View.VISIBLE);
            drop_info.setVisibility(View.GONE);

            // Spinner Drop down elements
            List<String> categories = new ArrayList<String>();
            categories.add("Select Laundromat");
            SharedPreferences prefs = getSharedPreferences("laundry_list", Context.MODE_PRIVATE);
            try {
                JSONArray laundry_list_ob = new JSONArray(prefs.getString("laundry_list", ""));
                for (int i=0;i<laundry_list_ob.length();i++){
                    categories.add(laundry_list_ob.getString(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // Creating adapter for spinner
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);

            // Drop down layout style - list view with radio button
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            // attaching data adapter to spinner
            laundry_spinner.setAdapter(dataAdapter);
        }
        else{
            drop_ll.setVisibility(View.GONE);
        }

    }
    private void requestDrop(String laundry){
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rider_id", AppPrefrence.getMobile(DetailActivity.this));
        params.put("laundry", laundry);
        params.put("salesorderid", salesorderid);
        setDropLocation(params, laundry);
    }
    private void setDropLocation(Map<String, Object> params, final String laundry_selected){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONObject response = null;
                try {
                    response = new JSONObject(result);
                    if(response.getString("success").equals("true")){
                        drop_ll.setVisibility(View.GONE);
                        drop_info.setVisibility(View.VISIBLE);
                        drop_info.setText("Dropped at "+laundry_selected);
                        db = dbHelper.getWritableDatabase();
                        String sql = "UPDATE "+ PickHelper.TABLE_ADDRESS + " SET "+PickHelper.KEY_PROC_STATUS+" = 'Dropped at Laundromat', "
                                        +PickHelper.KEY_LAUNDRY+" = '"+laundry_selected+"' WHERE "+PickHelper.KEY_SALESORDERID+" = '"+salesorderid+"'";
                        db.execSQL(sql);
                    }
                    else{
                        Toast.makeText(DetailActivity.this, response.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(DetailActivity.this, "Check your internet connection", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(DetailActivity.this, "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, DetailActivity.this, Constant.drop_location, params);
        task.execute();
    }
}

