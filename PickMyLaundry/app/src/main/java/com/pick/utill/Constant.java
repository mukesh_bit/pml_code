package com.pick.utill;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.pick.mylaundry.R;

import static com.facebook.FacebookSdk.getApplicationContext;

public class Constant {
	public static final String SID="gaurav_pickmylaundry";
	public static final String TOKEN="95dd817a3e2ebf03d5385ff25f84e1ef";
	public static final String PICKDATE="date";
	public static final String TSLOT="tslot";
	public static final String CONTACT="http://crm.pickmylaundry.in/web_services/android_app/api/create_contact.php";
	public static final String REGISTER="http://crm.pickmylaundry.in/ums_integration/register.php";
	public static final String VERIFY="http://crm.pickmylaundry.in/ums_integration/otp_verification_link.php";
	public static final String RESENDOTP="http://crm.pickmylaundry.in/ums_integration/webservice_return_pervious_otp_code.php";
	public static final String LOGIN="http://crm.pickmylaundry.in/ums_integration/web_service_login.php";
	public static final String FORGOTPASS="http://crm.pickmylaundry.in/ums_integration/webservice_forgetpassword.php";
	public static final String PICKURL="http://crm.pickmylaundry.in/ums_integration/webservice_create_salesorderNEW.php";
	public static final String UPDATEPROFILE="http://crm.pickmylaundry.in/ums_integration/webservice_update_contact.php";
	public static final String INVOICELIST="http://crm.pickmylaundry.in/ums_integration/webservice_new_orderstatus.php";
	public static final String VIEWPROFILE="http://crm.pickmylaundry.in/ums_integration/webservice_viewcustomer.php";
	public static final String PRICE="http://alssolutions.in/pickmylaundryprice.html";
	public static final String TERM="http://alssolutions.in/term.html";
	public static final String OREDERLIST="http://crm.pickmylaundry.in/web_services/android_app/api/my_orders.php";
	public static final String OTP = "http://crm.pickmylaundry.in/web_services/android_app/api/send_otp.php";
	public static final String OFFERLIST = "http://crm.pickmylaundry.in/web_services/android_app/getOfferList.php";
	public static final String PRICELIST = "http://crm.pickmylaundry.in/web_services/android_app/getPriceList2.php";
	public static final String PRIVACY="http://alssolutions.in/privacy.html";
	public static final String CouponCode="http://crm.pickmylaundry.in/web_services/coupon_code/index.php";
	public static final String GETCREDITS="http://crm.pickmylaundry.in/web_services/coupon_code/getCredits.php";
	public static final String GETPROMOMESSAGE="http://crm.pickmylaundry.in/web_services/coupon_code/getPromoMessage.php";
	public static final String GETINVITATIONMESSAGE="http://crm.pickmylaundry.in/web_services/coupon_code/getInvitationMessage.php";
	public static final String GETPAYABLECREDITS="http://crm.pickmylaundry.in/web_services/coupon_code/getMycashBalance.php";
	public static final String GETPROMOCODE="http://crm.pickmylaundry.in/web_services/coupon_code/getPromoCode.php";
	public static final String GETDIALOG="http://crm.pickmylaundry.in/web_services/android_app/getDialogInfo.php";
	public static final String GETMINTEXT="http://crm.pickmylaundry.in/web_services/android_app/getMinText.php";
	public static final String PAYMENTURL = "http://crm.pickmylaundry.in/web_services/android_app/getPaymentUrl.php";
	public static final String GETPAYTMORDERID = "http://crm.pickmylaundry.in/paytm/getPaytmId.php";
	public static final String CANCELORDER = "http://crm.pickmylaundry.in/web_services/android_app/cancel_order.php";
	public static final String HOMESCREENIMAGES = "http://crm.pickmylaundry.in/web_services/android_app/getHomeImages.php";

	public static final String GCM_URL="http://crm.pickmylaundry.in/web_services/android_app/register.php";
	public static final String FEEDBACK_URL="http://crm.pickmylaundry.in/web_services/android_app/getFeedback.php";
	public static final String SAVEFEEDBACK_URL="http://crm.pickmylaundry.in/web_services/android_app/saveFeedback.php";
	public static final String ORDERID="orderid";
	public static final String ORDERSTATUS="orderstatus";
	public static final String GETTIMESLOT = "http://crm.pickmylaundry.in/web_services/android_app/api/get_timeslots.php";
	public static final String MY_FLURRY_APIKEY = "52CNFN8HFPRYPXTSS4GZ";
	public static final String RESCHEDULE = "http://crm.pickmylaundry.in/web_services/android_app/api/reschedule.php";
	public static final String PAYTMID = "http://crm.pickmylaundry.in/paytm/getPaytmId.php";
	public static final String HOMEPAGECARDDATA = "http://crm.pickmylaundry.in/web_services/android_app/api/get_home_page_card_data.php";
	public static final String DEFAULTCARDIMAGE = "http://crm.pickmylaundry.in/web_services/android_app/images/default_card_image.jpg";
	public static final String TESTIMONIAL = "http://crm.pickmylaundry.in/web_services/android_app/api/get_testimonials.php";
	public static final String HOMEPAGETOPBANNER = "http://crm.pickmylaundry.in/web_services/android_app/api/home_page_top_banner.php";
	public static final String GOOGLEAPIKEY = "AIzaSyDPWF7HrzJoN5pGi3cutBbPEWZuillgi-k";
	public static final String LOCATIONURL = "http://crm.pickmylaundry.in/web_services/android_app/api/get_location_url.php";
	public static final String FEATURES = "http://crm.pickmylaundry.in/web_services/android_app/api/features.php";
	public static final String TRANSACTIONHISTORY = "http://crm.pickmylaundry.in/web_services/android_app/api/transaction_history.php";
	public static final String DEFAULTLOCATION = "Select delivery Location...";
	public static final String VERSION = "2.1.4";
	private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
	public static final String GETRECHARGEID = "http://crm.pickmylaundry.in/paytm/get_recharge_id.php";




	 @SuppressWarnings("deprecation")
	public static void displayDialog(String msg, String tilte,Context context){

		AlertDialog alertDialog = new AlertDialog.Builder(context).create();

		// Setting Dialog Title
		alertDialog.setTitle(tilte);

		// Setting Dialog Message
		alertDialog.setMessage(msg);

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.plogo);

		// Setting OK Button
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();

			}
		});

		// Showing Alert Message
		alertDialog.show();

	}
	public static void printLog(String msg) {

		Log.d("printLog()", "" + msg);
	}

	public static boolean isConnectingToInternet(Context _context) {
		ConnectivityManager connectivity = (ConnectivityManager) _context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		if (connectivity != null) {
			NetworkInfo[] info = connectivity.getAllNetworkInfo();
			if (info != null)
				for (int i = 0; i < info.length; i++)
					if (info[i].getState() == NetworkInfo.State.CONNECTED) {
						return true;
					}

		}
		return false;
	}
	public final static boolean isValidEmail(CharSequence target) {
		if (target == null) {
			return false;
		} else {
			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}
	 public static float convertDpToPixel(float dp, Context context){
		 Resources resources = context.getResources();
		 DisplayMetrics metrics = resources.getDisplayMetrics();
		 float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
		 return px;
	 }
	 public static void setLocation(String location, Context context){
		 SharedPreferences preferences = context.getSharedPreferences("selected_location", Context.MODE_PRIVATE);
		 SharedPreferences.Editor editor = preferences.edit();
		 editor.putString("location", location);
		 editor.apply();
	 }
	 public static String getLocation(Context context){
		 SharedPreferences preferences = context.getSharedPreferences("selected_location", Context.MODE_PRIVATE);
		 return preferences.getString("location", "Select delivery Location...");
	 }
	 public static boolean checkPlayServices(Context context) {
		 int resultCode = GooglePlayServicesUtil
				 .isGooglePlayServicesAvailable(context);
		 if (resultCode != ConnectionResult.SUCCESS) {
			 if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				 GooglePlayServicesUtil.getErrorDialog(resultCode, (Activity) context, PLAY_SERVICES_RESOLUTION_REQUEST).show();
			 } else {
				 Toast.makeText(getApplicationContext(),
						 "This device is not supported.", Toast.LENGTH_LONG)
						 .show();
			 }
			 return false;
		 }
		 return true;
	 }

}
