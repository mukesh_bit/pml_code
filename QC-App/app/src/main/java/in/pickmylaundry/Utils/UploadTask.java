package in.pickmylaundry.Utils;

    import android.app.ProgressDialog;
    import android.content.Context;
    import android.os.AsyncTask;

    import java.util.Map;

/**
 * Created by 121 on 1/31/2016.
 */
public class UploadTask extends AsyncTask<String, String, String> {

    private ProgressDialog pDialog = null;
    private Context context;
    private String url;
    private Map<String, Object> params_nameValue;

    public interface TaskListner
    {
        public void taskResult(String result);
        public void taskException();
    }

    private TaskListner listner;

    public UploadTask(TaskListner listener, Context context, String url, Map<String, Object> params){
        this.context = context;
        this.listner = listener;
        this.params_nameValue = params;
        this.url = url;
    }

    @Override
    protected String doInBackground(String... params) {
        String json;
        ServiceHandler serviceHandler = new ServiceHandler(url, Constant.POST_REQUEST, params_nameValue);
        json = serviceHandler.makeCall();
        return json;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Loading...");
        pDialog.show();
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        if(pDialog != null)
            pDialog.dismiss();
        if(!result.isEmpty()){
            listner.taskResult(result);
        }
        else
            listner.taskException();
    }
}
