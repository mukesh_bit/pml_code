package com.pick.utill;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.design.widget.Snackbar;

import com.pick.mylaundry.ChooseLocality;
import com.pick.mylaundry.LocationAddress;

/**
 * Created by Mukesh on 11/9/16.
 */

public class CurrentLocation {

    private Context context;
    private LocationService locationService;
    private ProgressDialog progressDialog;
    private LocationInterface locationInterface;
    private boolean isLocationRequested_gps, isLocationRequested_network;

    public interface LocationInterface{
        public void onResult(String address);
        public void onError();
    }


    public CurrentLocation(Context context){
        this.context = context;
    }

    public void getCurrentLocation(LocationInterface locationInterface, boolean isDialogVisible, boolean isLocationSettingVisible, boolean isLastLocationGood){
        this.locationInterface = locationInterface;
        LocationService.LocationListenerInterface locationListenerInterface = new LocationService.LocationListenerInterface() {
            @Override
            public void onResult(Location location) {
                setLocation(location);
            }

            @Override
            public void onError() {

            }
        };
        locationService = new LocationService(locationListenerInterface, context);
        if(isDialogVisible) {
            progressDialog = new ProgressDialog(context);
            progressDialog.show();
            progressDialog.setMessage("Locating you...");
            progressDialog.setCanceledOnTouchOutside(true);
        }

        isLocationRequested_gps = locationService.getLocation(LocationManager.GPS_PROVIDER, isLastLocationGood);
        isLocationRequested_network = locationService.getLocation(LocationManager.NETWORK_PROVIDER, isLastLocationGood);

        if (!isLocationRequested_gps && !isLocationRequested_network) {
            if(progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            if(isLocationSettingVisible)
                showSettingsAlert();
        }
    }
    public void setLocation(Location location){
        double latitude = location.getLatitude();
        double longitude = location.getLongitude();
        LocationAddress locationAddress = new LocationAddress();
        locationAddress.getAddressFromLocation(latitude, longitude, context, new  CurrentLocation.GeocoderHandler());
    }

    private class GeocoderHandler extends Handler {

        @Override
        public void handleMessage(Message message) {
            String locationAddress;String locality = "";
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    locality = bundle.getString("locality");
                    break;
                default:
                    locationAddress = "No Data";
            }
            if(progressDialog != null && progressDialog.isShowing())
                progressDialog.dismiss();
            if(locationAddress !=null && !locationAddress.equals("No Data")){
                if(locality.length() > 3)
                    locationAddress = locality;
                locationAddress = locationAddress.replace("\n", ", ");
                locationInterface.onResult(locationAddress.trim());
            }
        }
    }
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setTitle("Improve location accuracy?");
        alertDialog.setMessage("Please turn on your location services");
        alertDialog.setPositiveButton("Enable",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(intent);
                    }
                });
        alertDialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        alertDialog.show();
    }
}
