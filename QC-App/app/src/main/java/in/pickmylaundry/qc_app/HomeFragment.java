package in.pickmylaundry.qc_app;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.pickmylaundry.Utils.Constant;
import in.pickmylaundry.Utils.UploadTask;

/**
 * Created by 121 on 3/9/2016.
 */
public class HomeFragment extends android.app.Fragment {
    private View rootView;
    private UploadTask task;
    private Spinner spinner;
    private ArrayAdapter<String> dataAdapter;
    private Button get_data;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.home_fragment, container, false);
        spinner = (Spinner) rootView.findViewById(R.id.spinner);
        request_vendor_detail();
        get_data = (Button) rootView.findViewById(R.id.button);
        get_data.setEnabled(false);
        get_data.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!spinner.getSelectedItem().toString().equals("")){
                    Intent intent = new Intent(getActivity(), VendorDetail.class);
                    intent.putExtra("vendor", spinner.getSelectedItem().toString());
                    startActivity(intent);
                }
            }
        });
        return rootView;
    }
    private void request_vendor_detail(){
        Map<String, Object> params = new HashMap<String, Object>();
        get_vendor_detail(params);
    }
    private void get_vendor_detail(Map<String, Object> params){
        task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                JSONArray response = null;
                try {
                    response = new JSONArray(result);
                    List<String> categories = new ArrayList<String>();
                    for (int i=0; i<response.length();i++){
                        categories.add(response.getString(i));
                    }
                    Log.i("data", categories.toString());

                    // Creating adapter for spinner
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(
                            getActivity(),android.R.layout.simple_spinner_item, categories);

                    // Drop down layout style - list view with radio button

                    dataAdapter.setDropDownViewResource(R.layout.reason_item);

                    // attaching data adapter to spinner
                    spinner.setAdapter(dataAdapter);
                    get_data.setEnabled(true);


                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void taskException() {
                Toast.makeText(getActivity().getApplicationContext(), "There was some error, please try again after some time", Toast.LENGTH_LONG).show();
            }
        }, HomeFragment.this.getActivity(), Constant.get_so_detail, params);
        task.execute();
    }

}
