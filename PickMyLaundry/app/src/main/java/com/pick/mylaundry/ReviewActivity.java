package com.pick.mylaundry;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.pick.utill.AppPrefrence;
import com.pick.utill.Constant;
import com.pick.utill.Pickhelper;
import com.pick.volley.EmptyUtility;
import com.pick.volley.UploadTask;
import com.rey.material.widget.CheckBox;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by 121 on 12/18/2015.
 */
public class ReviewActivity extends Activity {
    private Bundle bundle, data_bundle;
    private int primary_key;
    private View divider_service;
    private TextView address_label_tv, address_detail_tv;
    private Pickhelper dbhelper;
    private SQLiteDatabase db;
    private String selected_address = "";
    private EditText coupon_et;
    private TextView apply_coupon_tv, coupon_description_tv, title, info_tv;
    private String coupon_code, paidCredits, estimated_clothes = "";
    private boolean isCouponApplied = false, isCreditUsed = false;
    private ScrollView reviewScroll;
    private LinearLayout action_bar_custom_slide;
    private NetworkInfo activeNetwork;
    private ConnectivityManager cm;
    private RelativeLayout rootView;
    private RelativeLayout service_types;
    private RelativeLayout others_info_rl;
    private RelativeLayout hint_view;
    private LinearLayout carpet_ll, blanket_ll,curtain_ll, delivery_ll, btw20_container;
    private ArrayList<String> product_added = new ArrayList<String>();
    private TextView lt20, gt40, btw20, mycash_balance, mycash_description;
    private ArrayList<String> washType;
    private ImageView info_close;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review);
        rootView = (RelativeLayout) findViewById(R.id.rootView);
        Typeface tf = Typeface.createFromAsset(getAssets(), "font/HelveticaNeue-Regular.ttf");
        reviewScroll = (ScrollView) findViewById(R.id.reviewScroll);
        //get extra data
        bundle = getIntent().getExtras();
        if(bundle.containsKey("bundle_login"))
            bundle = bundle.getBundle("bundle_login");
        washType = bundle.getStringArrayList("washType");

        //check internet connection
        cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        //service type
        for(int i=1;i<=washType.size();i++){
            int resid = getResources().getIdentifier("service_tv"+i, "id", getPackageName());
            TextView service_tv = (TextView) findViewById(resid);
            service_tv.setText(washType.get(i-1));
            service_tv.setVisibility(View.VISIBLE);
            if(i==2 || i==3) {
                resid = getResources().getIdentifier("line" + i, "id", getPackageName());
                View divider = findViewById(resid);
                divider.setVisibility(View.VISIBLE);
            }
        }

        //pickup & delivery time
        TextView pickup_date_tv = (TextView) findViewById(R.id.pickup_date);
        TextView delivery_date_tv = (TextView) findViewById(R.id.delivery_date);
        TextView pickup_time_tv = (TextView) findViewById(R.id.pickup_time);
        TextView delivery_time_tv = (TextView) findViewById(R.id.delivery_time);
        delivery_ll = (LinearLayout) findViewById(R.id.delivery_ll);
        service_types = (RelativeLayout) findViewById(R.id.service_types);
        divider_service = (View) findViewById(R.id.divider_service);
        others_info_rl = (RelativeLayout) findViewById(R.id.other_info_rl);
        info_tv = (TextView) findViewById(R.id.info_val);
        info_close = (ImageView) findViewById(R.id.info_close);
        hint_view = (RelativeLayout) findViewById(R.id.hint_view);

        String convertDateFormat = getDateFormat(bundle.getString("pickup_date"));
        pickup_date_tv.setText(convertDateFormat);
        pickup_time_tv.setText(bundle.getString("pickup_time"));
        if(bundle.getString("delivery_date").equals("")){
            delivery_ll.setVisibility(View.GONE);
            service_types.setVisibility(View.GONE);
            divider_service.setVisibility(View.GONE);
            others_info_rl.setVisibility(View.VISIBLE);
            info_tv.setText("We will communicate delivery date to you during pickup");
        }
        else{
            convertDateFormat = getDateFormat(bundle.getString("delivery_date"));
            delivery_date_tv.setText(convertDateFormat);
            delivery_time_tv.setText(bundle.getString("delivery_time"));
        }

        //address detail
        address_label_tv = (TextView) findViewById(R.id.address_label);
        address_detail_tv = (TextView) findViewById(R.id.address);
        setAddress();

        //coupon and credits
        coupon_et = (EditText) findViewById(R.id.coupon_code);
        apply_coupon_tv = (TextView) findViewById(R.id.apply);
        coupon_description_tv = (TextView) findViewById(R.id.coupon_description);
        apply_coupon_tv.setOnClickListener(apply_coupon_listener);
        mycash_balance = (TextView) findViewById(R.id.mycash_balance);
        mycash_description = (TextView) findViewById(R.id.mycash_description);

        use_credit_listener();


        //confirm order
        RelativeLayout confirmOrder_rl = (RelativeLayout) findViewById(R.id.confirmOrder);
        TextView confirmOrder_tv = (TextView) findViewById(R.id.confirmOrder_tv);
        confirmOrder_rl.setOnClickListener(confirmOrderListener);

        //back button listener & title
        title=(TextView) findViewById(R.id.actionbar_custom_title);
        title.setText("REVIEW");
        title.setTypeface(tf);
        action_bar_custom_slide = (LinearLayout) findViewById(R.id.action_bar_custom_slide);
        action_bar_custom_slide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ReviewActivity.this.onBackPressed();
            }
        });

        //check for previous coupons applied
        SharedPreferences pref = getSharedPreferences("redeem", Context.MODE_PRIVATE);
        String code = pref.getString("appliedCoupon", "");
        if(EmptyUtility.isNotEmpty(code)){
            coupon_et.setText(code);
            apply_coupon_tv.performClick();
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("appliedCoupon", "");
            editor.commit();
        }

        //estimated clothes listener
        blanket_ll = (LinearLayout) findViewById(R.id.blanket);
        curtain_ll = (LinearLayout) findViewById(R.id.curtain);
        carpet_ll = (LinearLayout) findViewById(R.id.carpet);
        btw20_container = (LinearLayout) findViewById(R.id.on_hint);
        lt20 = (TextView) findViewById(R.id.lt20);
        gt40 = (TextView) findViewById(R.id.gt40);
        btw20 = (TextView) findViewById(R.id.btw20);

        blanket_ll.setOnClickListener(product_listener);
        curtain_ll.setOnClickListener(product_listener);
        carpet_ll.setOnClickListener(product_listener);
        lt20.setOnClickListener(estimated_cloth_listener);
        btw20.setOnClickListener(estimated_cloth_listener);
        gt40.setOnClickListener(estimated_cloth_listener);
        start_circle_hint();

        info_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                others_info_rl.setVisibility(View.GONE);
            }
        });

    }
    private String getDateFormat(String date){
        SimpleDateFormat parseFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat dateFormat = new SimpleDateFormat("d MMM");
        Date parsed_date = new Date();
        try {
            parsed_date = parseFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return dateFormat.format(parsed_date);
    }
    private void setAddress(){
        dbhelper = new Pickhelper(getApplicationContext());
        db = dbhelper.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+Pickhelper.TABLE_ADDRESS+" WHERE "+Pickhelper.KEY_ACTIVE+" = 'yes'";
        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor.getCount() == 0){
            Intent intent = new Intent(ReviewActivity.this, AddAddress.class);
            intent.putExtra("mode", "review");
            startActivityForResult(intent, 1);
        }
        if (cursor.moveToFirst()){
            selected_address = cursor.getString(1)+"\n"+cursor.getString(2)+"\n"+cursor.getString(3);
            address_detail_tv.setText(selected_address);
            address_label_tv.setText(cursor.getString(4).toUpperCase());
        }
        db.close();
    }
    private void start_circle_hint(){
        //start animation
        Animation a = AnimationUtils.loadAnimation(this, R.anim.circle);
        a.setRepeatMode(Animation.RESTART);
        a.setRepeatCount(Animation.INFINITE);
        TextView tv = (TextView) hint_view.findViewById(R.id.circle);
        tv.startAnimation(a);
    }
    private View.OnClickListener apply_coupon_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(apply_coupon_tv.getWindowToken(), 0);
            reviewScroll.fullScroll(View.FOCUS_DOWN);
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("code", coupon_et.getText().toString()));
            params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(ReviewActivity.this)));
            params.add(new BasicNameValuePair("pickup_date", bundle.getString("pickup_date")));
            params.add(new BasicNameValuePair("delivery_date", bundle.getString("delivery_date")));
            params.add(new BasicNameValuePair("pickuptime_slot",bundle.getString("pickup_time")));
            params.add(new BasicNameValuePair("deliverytime_slot", bundle.getString("delivery_time")));
            params.add(new BasicNameValuePair("city", Constant.getLocation(ReviewActivity.this)));
            activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            if(isConnected)
                makeCouponCodeRequest(params);
            else
                Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
            Map<String, String> articleParams = new HashMap<String, String>();
            articleParams.put("mobile", AppPrefrence.getMobile(ReviewActivity.this));
            articleParams.put("coupon", coupon_et.getText().toString());
            //up to 10 params can be logged with each event
            FlurryAgent.logEvent("coupon_code_apply", articleParams);
        }
    };
    private void makeCouponCodeRequest(List<NameValuePair> params){
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    coupon_description_tv.setText(json.getString("msg"));
                    if(json.getString("success").equals("true")){
                        coupon_description_tv.setTextColor(getResources().getColor(android.R.color.holo_green_dark));
                        isCouponApplied = true;
                        coupon_code = coupon_et.getText().toString();
                        isCreditUsed = false;
                    }
                    else {
                        coupon_description_tv.setTextColor(Color.parseColor("#ff0000"));
                        isCouponApplied = false;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            @Override
            public void taskException() {

            }
        }, ReviewActivity.this, Constant.CouponCode, params, true);
        task.execute();
    }
    private void use_credit_listener(){
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(getApplicationContext())));
            activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            if(isConnected)
                makeMyCashBalanceRequest(params);
            else if(!isConnected)
                Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();


            Map<String, String> articleParams = new HashMap<String, String>();
            articleParams.put("mobile", AppPrefrence.getMobile(ReviewActivity.this));
            //up to 10 params can be logged with each event
            FlurryAgent.logEvent("myCash_apply", articleParams);
    }
    private View.OnClickListener product_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String product = v.getTag().toString();
            if(product_added.contains(product)){
                v.setBackgroundResource(R.drawable.product_round);
                ((ImageView)((LinearLayout) v).getChildAt(1)).setImageResource(R.drawable.add);
                ((TextView)((LinearLayout) v).getChildAt(0)).setTextColor(getResources().getColor(R.color.textColorBlack));
                product_added.remove(product);
            }
            else{
                v.setBackgroundResource(R.drawable.product_round_active);
                ((ImageView)((LinearLayout) v).getChildAt(1)).setImageResource(R.drawable.tick);
                ((TextView)((LinearLayout) v).getChildAt(0)).setTextColor(getResources().getColor(R.color.white));
                product_added.add(product);
            }
            if(product_added.size() == 0){
                others_info_rl.setVisibility(View.GONE);
            }
            else{
                others_info_rl.setVisibility(View.VISIBLE);
                info_tv.setText("Garments like curtains, carpets, blankets etc. take more time than 72 hrs");
            }
        }
    };
    private View.OnClickListener estimated_cloth_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            lt20.setBackgroundResource(R.color.white);
            lt20.setTextColor(getResources().getColor(R.color.textColorBlack));
            btw20.setBackgroundResource(R.color.white);
            btw20.setTextColor(getResources().getColor(R.color.textColorBlack));
            gt40.setBackgroundResource(R.color.white);
            gt40.setTextColor(getResources().getColor(R.color.textColorBlack));
            btw20_container.setBackgroundResource(R.color.white);
            v.setBackgroundResource(R.color.colorPrimary);
            ((TextView)v).setTextColor(getResources().getColor(R.color.white));
            estimated_clothes = ((TextView)v).getTag().toString();
            hint_view.setVisibility(View.GONE);
        }
    };
    private void makeMyCashBalanceRequest(List<NameValuePair> params){
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);

                    if(json.getString("success").equals("true")){
                        mycash_description.setText(json.getString("msg"));
                        mycash_balance.setText("MyCash Balance: ₹"+json.getString("remainingCredits"));
                        mycash_description.setVisibility(View.VISIBLE);
                        mycash_balance.setVisibility(View.VISIBLE);
                    }
                    else{
                        mycash_description.setVisibility(View.GONE);
                        mycash_balance.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void taskException() {

            }
        }, ReviewActivity.this, Constant.GETPAYABLECREDITS,params, true);
        task.execute();
    }
    private View.OnClickListener confirmOrderListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            List<NameValuePair> params=new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("primary_number", AppPrefrence.getMobile(getApplicationContext())));
            params.add(new BasicNameValuePair("pickup_date", bundle.getString("pickup_date")));
            params.add(new BasicNameValuePair("delivery_date",bundle.getString("delivery_date")));
            params.add(new BasicNameValuePair("pickuptime_slot",bundle.getString("pickup_time")));
            params.add(new BasicNameValuePair("deliverytime_slot", bundle.getString("delivery_time")));
            params.add(new BasicNameValuePair("address", selected_address));
            params.add(new BasicNameValuePair("estimated_clothes", estimated_clothes));
            params.add(new BasicNameValuePair("products", product_added.toString()));
            params.add(new BasicNameValuePair("name", AppPrefrence.getName(getApplicationContext())));
            params.add(new BasicNameValuePair("email", AppPrefrence.getEmail(getApplicationContext())));
            //ArrayList<String> washType = data_bundle.getStringArrayList("washType");
            String instruction = "";
            for(int i=0;i<washType.size();i++){
                if(i>0)
                    instruction += ", ";
                instruction += washType.get(i);
            }
            params.add(new BasicNameValuePair("instruction", instruction));
            if(isCouponApplied){
                params.add(new BasicNameValuePair("code", coupon_code));
                params.add(new BasicNameValuePair("coupon_description", coupon_description_tv.getText().toString()));
            }
            activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            if(isConnected) {
                if(selected_address.equals("")){
                    Toast.makeText(ReviewActivity.this, "Please Add Address", Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(ReviewActivity.this, AddAddress.class);
                    intent.putExtra("mode", "review");
                    startActivityForResult(intent, 1);
                }
                else if(estimated_clothes.equals("")){
                    Snackbar.make(rootView, "Please give us your estimated number of clothes", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();
                    btw20_container.setBackgroundResource(R.drawable.warning_border);
                }
                else {
                    confirmOrderRequest(params);
                    Map<String, String> articleParams = new HashMap<String, String>();
                    articleParams.put("mobile", AppPrefrence.getMobile(ReviewActivity.this));
                    FlurryAgent.logEvent("confirm_order_btn_clicked", articleParams);
                }
            }
            else
                Snackbar.make(rootView, "Please check your internet connection", Snackbar.LENGTH_LONG).setActionTextColor(Color.WHITE).show();

        }
    };
    private void confirmOrderRequest(List<NameValuePair> params){
        UploadTask task = new UploadTask(new UploadTask.TaskListner() {
            @Override
            public void taskResult(String result) {
                try {
                    JSONObject json = new JSONObject(result);
                    if(json.getString("success").equals("true")){
                        Intent intent = new Intent(ReviewActivity.this, ConfirmationActivity.class);
                        intent.putExtra("so", json.getString("salesorder_id"));
                        intent.putExtra("order_id", json.getString("orderid"));
                        startActivity(intent);
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "There was some problem, please try again later", Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void taskException() {

            }
        }, ReviewActivity.this, Constant.PICKURL,params, true);
        task.execute();
    }

    @Override
    protected void onStart() {
        super.onStart();

        FlurryAgent.setLogEnabled(false);
        FlurryAgent.init(this, Constant.MY_FLURRY_APIKEY);
        FlurryAgent.onStartSession(this, Constant.MY_FLURRY_APIKEY);
        Map<String, String> articleParams = new HashMap<String, String>();
        articleParams.put("mobile", AppPrefrence.getMobile(ReviewActivity.this));
        FlurryAgent.logEvent("review", articleParams);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FlurryAgent.onEndSession(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            setAddress();
        }
    }
}
