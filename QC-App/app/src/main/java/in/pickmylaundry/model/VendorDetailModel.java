package in.pickmylaundry.model;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by 121 on 3/10/2016.
 */
public class VendorDetailModel {
    private String so,name,cloth_desc;
    private int salesorderid, total_cloth;
    private ArrayList<Map<String, String>> barcode_list;
    private String order_type;

    public String getProcessing_instruction() {
        return processing_instruction;
    }

    public void setProcessing_instruction(String processing_instruction) {
        this.processing_instruction = processing_instruction;
    }

    private String processing_instruction;

    public boolean getIs_qc() {
        return is_qc;
    }

    public void setIs_qc(boolean is_qc) {
        this.is_qc = is_qc;
    }

    private boolean is_qc;

    public ArrayList<Map<String, String>> getBarcode_list() {
        return barcode_list;
    }

    public void setBarcode_list(ArrayList<Map<String, String>> barcode_list) {
        this.barcode_list = barcode_list;
    }

    public int getSalesorderid() {
        return salesorderid;
    }

    public void setSalesorderid(int salesorderid) {
        this.salesorderid = salesorderid;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCloth_desc() {
        return cloth_desc;
    }

    public void setCloth_desc(String cloth_desc) {
        this.cloth_desc = cloth_desc;
    }

    public int getTotal_cloth() {
        return total_cloth;
    }

    public void setTotal_cloth(int total_cloth) {
        this.total_cloth = total_cloth;
    }

    public String getOrder_type() {
        return order_type;
    }

    public void setOrder_type(String order_type) {
        this.order_type = order_type;
    }
}
